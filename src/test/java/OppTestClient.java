import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.Map;

import com.ksign.kdn.client.NioClient;
import com.ksign.kdn.client.OppProtocol;
import com.ksign.kdn.client.OppReqHandler;
import com.ksign.kdn.client.OppResHandler;


public class OppTestClient {
	String url="27.1.217.20";
	int port = 7502;
	public OppTestClient() throws UnknownHostException, IOException{
		initClient();
	}//"27.1.217.20", 7502
	 
	public static void main(String[] args) throws UnknownHostException, IOException{
	
		OppTestClient oppt = new OppTestClient();
		
		Map  param = new HashMap();
		param.put("userid", "admin");		
		param.put("userpwd", "8888");
		 
		OppReqHandler oppReq = new OppReqHandler(OppProtocol.IDPW_LOGIN, param);
		
		OppResHandler res = oppt.send(oppReq,  new OppResHandler());
		res.waitForResponse();/**/
	}
	
	private  NioClient client;
	 private  Thread t ;
	 
	 
	 
	 private  void initClient() throws UnknownHostException, IOException{
		 client = new NioClient(InetAddress.getByName(url), port);
		 if(client == null){
		  client = new NioClient(InetAddress.getByName(url), port);
		 }
	 }
	 
	 private  void threadStart() throws UnknownHostException, IOException{
		 initClient();
		 if(t == null || !t.isAlive()){
		   t = new Thread(client);
		   t.setDaemon(true);
			 t.start();
		 }
		
	 }
	 
	 public  OppResHandler send(OppReqHandler oppReq,OppResHandler oppRes) throws IOException{
		
		/* System.out.println(1);
		Socket cl = new Socket(url,port);
		System.out.println(2);
		OutputStream ou =cl.getOutputStream() ;
		ou.write(oppReq.getReqMessage().length);
		ou.flush();
		ou.write(oppReq.getReqMessage());
		ou.flush();
		System.out.println(3);
		InputStream in = cl.getInputStream();
		byte[] buffer =new byte[1024];
		in.read(buffer);
		System.out.println(4);
		
		System.out.println(new String(buffer));
		in.close();
		cl.close();
		*/
		 threadStart();
		System.out.println ("%%%%%%%%%%%%%  Thread is Live "+t.isAlive());
		 
		 client.send(oppReq.getReqMessage(), oppRes);
		
		 return oppRes;
	 }
		
}
