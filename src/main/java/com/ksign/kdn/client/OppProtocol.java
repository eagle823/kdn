package com.ksign.kdn.client;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

public enum OppProtocol {

	 

	CERT_LOGIN					("인증서 로그인",			(byte)0x10 	,"dn$userip"	, "OPERATOR_ID$DN$OPERATOR_NAME$MP$CP$AP$TYPE" ,"map" ),
	IDPW_LOGIN					("ID/PW 로그인",			(byte)0x11	,"userid$userpwd"	, "result$OPERATOR_ID$MAN_NAME$OPERATOR_NAME$MP$CP$AP$TYPE" ,"map" ),
	COMPANY_SEARCH				("소속 회사 검색",			(byte)0x20	,"OPERATOR_ID$currentPage$pageSize$"	, "result$TOTAL$PAGE$LIST$MAKER_ID$MAKER_NAME" ,"list" ),
	ADMIN_ID_CHECK				("관리자 ID 중복확인",		(byte)0x21	,"OPERATOR_ID$userid"	, "result$" ,"map" ),
	ADMIN_REG					("관리자 등록",				(byte)0x22	,"OPERATOR_ID$userId$userName$region$mobile$email$phone$corp$fax$grade$position$companyName$password$device$cert$auth"	, "result" ,"map" ),	
	ADMIN_DETAIL_SEARCH			("관리자 상세 조회",			(byte)0x23	,"OPERATOR_ID$currentPage$pageSize$adminId"	, "result$TOTAL$PAGE$LIST$OPERATOR_ID$OPERATOR_NAME$ADDRESS$CELL_PHONE$EMAIL$PHONE$DEPARTMENT$FAX$OPER_POSITION$OPER_TITLE$MAKER_NAME$OPERATOR_PW$MP$CP$AP$" ,"list" ),
	ADMIN_MOD					("관리자 수정",				(byte)0x24	,"OPERATOR_ID$userId$userName$region$mobile$email$phone$corp$fax$grade$position$companyName$password$device$cert$auth"	, "result" ,"map" ),                                               
	ADMIN_LIST					("관리자 리스트 나열",			(byte)0x25	,"OPERATOR_ID$currentPage$pageSize"	               , "result$TOTAL$PAGE$LIST$OPERATOR_ID$OPERATOR_NAME$ADDRESS$CELL_PHONE$PHONE$DEPARTMENT$FAX$OPER_POSITION$OPER_TITLE$MAKER_NAME$" ,"list" ),
	ADMIN_DEL					("관리자 삭제",				(byte)0x26	,"OPERATOR_ID$userId$"	, "result" ,"map" ),
	//ASSIGN_AMDIN_SEARCH		("소속회사 관리자 검색",		(byte)0x27	,"OPERATOR_ID$currentPage$pageSize$searchKeyword"	, "result$TOTAL$PAGE$LIST$OPERATOR_ID$BRANCH_NAME$OPERATOR_NAME$ADDRESS$EMAIL$CELL_PHONE$PHONE$DEPARTMENT$FAX$OPER_POSITION$OPER_TITLE$MAKER_NAME$" ,"list" ),
	ASSIGN_AMDIN_SEARCH			("소속회사 관리자 검색",		(byte)0x27	,"OPERATOR_ID$currentPage$pageSize$searchKeyword"	, "result$TOTAL$PAGE$LIST$OPERATOR_ID$OPERATOR_NAME$ADDRESS$CELL_PHONE$PHONE$DEPARTMENT$FAX$OPER_POSITION$OPER_TITLE$MAKER_NAME$" ,"list" ),
	ADMIN_NAME_SEARCH			("관리자이름 검색",			(byte)0x28	,"OPERATOR_ID$currentPage$pageSize$searchKeyword"	, "result$TOTAL$PAGE$LIST$OPERATOR_ID$OPERATOR_NAME$ADDRESS$CELL_PHONE$PHONE$DEPARTMENT$FAX$OPER_POSITION$OPER_TITLE$MAKER_NAME$" ,"list" ),
	ADMIN_ID_SEARCH				("관리자 ID 검색",			(byte)0x29	,"OPERATOR_ID$currentPage$pageSize$searchKeyword"	, "result$TOTAL$PAGE$LIST$OPERATOR_ID$OPERATOR_NAME$ADDRESS$CELL_PHONE$PHONE$DEPARTMENT$FAX$OPER_POSITION$OPER_TITLE$MAKER_NAME$" ,"list" ),
	MAKER_CHECK					("제조사명 중복확인",			(byte)0x30	,"OPERATOR_ID$companyName"	, "result$" ,"map" ),
	POST_NUM_SEARCH				("우편번호 ",				(byte)0x3a	,"OPERATOR_ID$currentPage$pageSize$searchKeyword"	, "result$TOTAL$PAGE$LIST$ADDRESS$" ,"list" ),
	MAKER_ADD					("제조사 등록",				(byte)0x31	,"OPERATOR_ID$product_name$product_ssn$product_address$phone$fax$ceoname$homepage$"	, "result$" ,"map" ),
	MAKER_DETAIL_SEARCH			("제조사 상세 조회",			(byte)0x32	,"OPERATOR_ID$currentPage$pageSize$makerName"	, "result$TOTAL$PAGE$LIST$MAKER_NAME$BRN$ADDRESS$PHONE$FAX$CEO$HOMEPAGE$" ,"" ),
	MAKER_MOD					("제조사 정보 수정",			(byte)0x33	,"OPERATOR_ID$product_name$product_ssn$product_address$phone$fax$ceoname$homepage$"	,  "result$" ,"map" ),
	MAKER_LIST					("제조사 리스트 나열",			(byte)0x34	,"OPERATOR_ID$currentPage$pageSize$"	, "result$TOTAL$PAGE$LIST$MAKER_NAME$BRN$ADDRESS$PHONE$FAX$CEO$HOMEPAGE$MAKE_NO$" ,"list" ),
	MAKER_DEL					("제조사 삭제",				(byte)0x35	,"OPERATOR_ID$product_name"	, "result$" ,"map"  ),
	MAKER_NAME_SEARCH			("제조사명 검색",			(byte)0x36	,"OPERATOR_ID$currentPage$pageSize$searchKeyword"	, "result$TOTAL$PAGE$LIST$MAKER_ID$MAKER_NAME$BRN$ADDRESS$PHONE$FAX$CEO$HOMEPAGE$" ,"list" ),
	ASSIGN_MAKER_SEARCH			("소속회사 제조사 검색",		(byte)0x37	,"OPERATOR_ID$currentPage$pageSize$searchKeyword", "result$TOTAL$PAGE$LIST$MAKER_NAME$BRN$ADDRESS$PHONE$FAX$CEO$HOMEPAGE$" ,"list" ),
	MAKER_MANAGER_SEARCH		("제조사담당자명 검색",		(byte)0x38	,"OPERATOR_ID$currentPage$pageSize$searchKeyword"	,  "result$TOTAL$PAGE$LIST$MAKER_ID$MAKER_NAME$BRN$ADDRESS$PHONE$FAX$CEO$HOMEPAGE$" ,"list" ), 
	COM_REG_NUMBER_SERRCH		("사업자 등록번호 검색",		(byte)0x39	,"OPERATOR_ID$currentPage$pageSize$searchKeyword"	, "result$TOTAL$PAGE$LIST$MAKER_NAME$BRN$ADDRESS$PHONE$FAX$CEO$HOMEPAGE$MAKE_NO" ,"list" ),
	MAKER_LIST_3B				("제조사 리스트3B",		    (byte)0x3b	,"OPERATOR_ID$currentPage$pageSize$"	, "result$TOTAL$PAGE$LIST$MAKER_NAME$" ,"list" ),
	
	OPT_BY_DEVICE_SEARCH		("운영자 별 기기 검색",			(byte)0x40	,"OPERATOR_ID$currentPage$pageSize$adminID"	, "result$TOTAL$PAGE$LIST$MAKER_NAME$MS_NMAE$FW_NAME$CHIP_VERSION$REG_DATE$ISSUER_DATE$CERT_STATE$DN$" ,"list" ),
	MAKER_BY_SEARCH				("제조사 별 검색",			(byte)0x41	,"OPERATOR_ID$currentPage$pageSize$makerName$"	, "result$TOTAL$PAGE$LIST$MAKER_NAME$REG_DATE$CHIP_SERIAL$CHIP_NMAE$CHIP_VERSION$CERT_STATE$DN$" ,"list" ),
	DEVICE_Serial_Number_Search	("장치 시리얼 번호 검색",		(byte)0x42	,"OPERATOR_ID$currentPage$pageSize$makerName$serialNo"	, "result$TOTAL$PAGE$LIST$MAKER_NAME$MS_NMAE$FW_NAME$REG_DATE$ISSUER_DATE$CERT_STATE$DN$" ,"list" ),
	CHIP_NAME_SEARCH			("칩 이름 검색",				(byte)0x43	,"OPERATOR_ID$currentPage$pageSize$chip","result$TOTAL$PAGE$LIST$MAKER_NAME$MS_NMAE$FW_NAME$CHIP_VERSION$REG_DATE$ISSUER_DATE$CERT_STATE$DN$" ,"list"  ),
	CHIP_VERSION_SEARCH			("칩 버전 검색",				(byte)0x44	,"OPERATOR_ID$currentPage$pageSize$chipversion"	,"result$TOTAL$PAGE$LIST$MAKER_NAME$MS_NMAE$FW_NAME$REG_DATE$ISSUER_DATE$CERT_STATE$DN$" ,"list"  ),
	DEVICE_MOD					("기기 수정",				(byte)0x45	,"OPERATOR_ID$makerName$serialNo$", "result$" ,"map" ),
	DEVICE_DEL					("기기삭제",				(byte)0x46	,"OPERATOR_ID$MAKER_NAME$serialNo$"	, "result$" ,"map" ),
	DEVICE_CERT_ISSUE			("기기 인증서 발급",			(byte)0x47	,"OPERATOR_ID$MAKER_NAME$keyinfo"	, "result$" ,"map" ),
	DEVICE_CERT_REISSUE			("기기 인증서 재발급",			(byte)0x48	,"OPERATOR_ID$MAKER_NAME$keyinfo"	, "result$" ,"map" ),
	DEVICE_CERT_UPDATE			("기기 인증서 갱신",			(byte)0x49	,"OPERATOR_ID$MAKER_NAME$keyinfo"	, "result$" ,"map" ),
	DEVICE_CERT					("기기 인증서 폐지",			(byte)0x4a	,"OPERATOR_ID$MAKER_NAME$serialNo$"	, "result$" ,"map"  ),
	CONDITION_BY_SEARCH_4b		("조건 식 검색",				(byte)0x4b	,"OPERATOR_ID$currentPage$pageSize$MAKER_NAME$serialNo$device$chip$firmware$stats$date"	,"result$TOTAL$PAGE$LIST$MAKER_NAME$MS_SERIAL$MS_NMAE$CHIP_NMAE$FW_NAME$REG_DATE$ISSUER_DATE$CERT_STATE$DN$" ,"list"  ),
	CERT_SERIAL_NUMBER_SEARCH	("인증서 나열",				(byte)0x50	,"OPERATOR_ID$currentPage$pageSize$MAKER_NAME$serialNo$device$firmware$chip$stats$date$"	, "result$TOTAL$PAGE$LIST$DN$SERIAL_NO$STATUS$VALID_FROM$VALID_TO$MS_NMAE$MAN_NAME$" ,"list" ),
	CERT_DN_SEARCH				("인증서 DN 상세 조회",		(byte)0x51	,"OPERATOR_ID$currentPage$pageSize$DN$"	, "result$TOTAL$PAGE$LIST$DN$SERIAL_NO$STATUS$VALID_FROM$VALID_TO$MS_NMAE$" ,"list" ),
	CERT_REQUEST				("인증서 요청",				(byte)0x52	,"OPERATOR_ID$dn"	, "result$CERT" ,"map" ),
	AUDIT_SEARCH			    ("감사  조회",			    (byte)0x60	,"OPERATOR_ID$currentPage$pageSize$adminID$type$action$date"	, "result$TOTAL$PAGE$LIST$DI$OPERATOR_ID$AC$DE$DATE" ,"list" ),
	AUDIT_MON_SEARCH			("감사 월별 조회",			(byte)0x61	,""	, "" ,"" ),
	AUDIT_MAKER_SEARCH			("감사 제조사 별 조회",			(byte)0x62	,""	, "" ,"" ),
	CERT_PROCESS_DAY_SEARCH		("인증서 처리 일별 조회",		(byte)0x71	,"OPERATOR_ID$currentPage$pageSize$MAKER_NAME$device$date", "result$TOTAL$PAGE$LIST$MN$REG$IS$RN$RP$RK" ,"list" ),
	CERT_PROCESS_MON_SEARCH		("인증서 처리 월별 조회",		(byte)0x70	,"OPERATOR_ID$currentPage$pageSize$MAKER_NAME$device$date"	, "result$TOTAL$PAGE$LIST$DN$REG$IS$RN$RP$RK$" ,"list" ),
	CERT_STATUS_DAY_SEARCH		("인증서 상태 일별 조회",		(byte)0x72	,""	, "" ,"" ),
	CERT_STATUS_MON_SEARCH		("인증서 상태 월별 조회",		(byte)0x73	,""	, "" ,"" ),
	DEVICE_POLICY_LIST			("부여 정책 조회",		    (byte)0x80	,"OPERATOR_ID$currentPage$pageSize$makerName$device$firmware$chip$chipversion"	, "result$TOTAL$PAGE$LIST$POLICY_NAME$" ,"list" ),
	POLICY_LIST					("정책 리스트 조회",		    (byte)0x81	,"OPERATOR_ID$currentPage$pageSize"	, "result$TOTAL$PAGE$LIST$POLICY_NAME$" ,"list" ),
	POLICY_UPDATE				("정책 리스트 수정",		    (byte)0x82	,"OPERATOR_ID$makerName$device$firmware$chip$chipversion$police"	  , "result$" ,"map" ),
	DEVICE_90					("기기명 추가",		        (byte)0x90	,"OPERATOR_ID$makerName$value$"	, "result$" ,"map" ),
	DEVICE_91					("기기명 삭제",		        (byte)0x91	,"OPERATOR_ID$makerName$device$"	, "result$" ,"map" ),
	/*DEVICE_92					("기기명 리스트",		    (byte)0x92	,"OPERATOR_ID$currentPage$pageSize$makerName$"	, "result$TOTAL$PAGE$LIST$MN$" ,"list" ),*/
	FIRMWARE_93					("펌웨어 추가",		        (byte)0x93	,"OPERATOR_ID$makerName$device$value$"	, "result$" ,"map" ),
	FIRMWARE_94					("펌웨어 삭제",		        (byte)0x94	,"OPERATOR_ID$makerName$device$firmware$"	, "result$" ,"map" ),
	/*FIRMWARE_95					("펌웨어 리스트",		    (byte)0x95	,"OPERATOR_ID$currentPage$pageSize$makerName$device$"	, "result$$TOTAL$PAGE$LIST$FN" ,"list" ),*/

	CHIP_96						("칩이름 추가",		    	(byte)0x96	,"OPERATOR_ID$makerName$device$firmware$value$"	, "result$" ,"map" ),
	CHIP_97						("칩이름 삭제",		    	(byte)0x97	,"OPERATOR_ID$makerName$device$firmware$chip$"	, "result$" ,"map" ),
	/*CHIP_98						("칩이름  목록",		    	(byte)0x98	,"OPERATOR_ID$currentPage$pageSize$makerName$device$firmware$"	, "result$CN" ,"list" ),*/
	CHIP_99						("칩버전 추가가",		    (byte)0x99	,"OPERATOR_ID$makerName$device$firmware$chip$value"	, "result$" ,"map" ),
	CHIP_9a						("칩버젼 삭제",		    	(byte)0x9a	,"OPERATOR_ID$makerName$device$firmware$chip$chipversion"	, "result$" ,"map" ),
	/*CHIP_9b						("칩버젼목록",		    	(byte)0x9b	,"OPERATOR_ID$currentPage$pageSize$makerName$device$firmware$chip$"	, "result$CV" ,"list" ),*/
	
	CERT_PROCESS_REQUEST_SUCCESS("처리 성공 정보 조회",			(byte)0x9c	,"OPERATOR_ID$currentPage$pageSize$adminid$device$date", "result$TOTAL$PAGE$LIST$MN$IS$RN$RP$RK" ,"list" ),
	CERT_PROCESS_REQUEST_WAIT	("처리 대기 정보 조회",			(byte)0x9d	,"OPERATOR_ID$currentPage$pageSize$adminid$device$date", "result$TOTAL$PAGE$LIST$MN$IS$RN$RP$RK" ,"list" ),
	CERT_PROCESS_REQUEST_FAIL	("처리 실패 정보 조회",			(byte)0x9e	,"OPERATOR_ID$currentPage$pageSize$adminid$device$date", "result$TOTAL$PAGE$LIST$MN$IS$RN$RP$RK" ,"list" ),

	ADMIN_PW_MODIFY				("비밀번호 수정",		    (byte)0x01	,"OPERATOR_ID$currentPage$pageSize$makerName$device$firmware$chip$"	, "result$CV" ,"list" ),

	DEVICE_MODEL				("기기 정보 나열",			(byte)0xa1	,"OPERATOR_ID$currentPage$pageSize$makerName$", "result$TOTAL$PAGE$LIST$MAN_NAME$D_MODEL_NO$DEVICE_NAME$DEVICE_TYPE$DEVICE_MAN_NAME$OS", "list"),
	DEVICE_MODEL_DETAIL			("기기 정보 상제 조회",			(byte)0xa2	,"OPERATOR_ID$currentPage$pageSize$dModelNo$", "result$TOTAL$PAGE$LIST$MAN_NAME$D_MODEL_NO$DEVICE_NAME$DEVICE_TYPE$DEVICE_MAN_NAME$OS", "list"),
	DEVICE_MODEL_REG			("기기 정보 등록",			(byte)0xa0	,"OPERATOR_ID$manName$deviceName$deviceType$deviceManName$os$", "result$", "map"),
	DEVICE_MODEL_MOD			("기기 정보 수정",			(byte)0xa3	,"OPERATOR_ID$manName$dModelNo$deviceName$deviceType$deviceManName$os$", "result$", "map"),
	DEVICE_MODEL_DEL			("기기 정보 삭제",			(byte)0xa4	,"OPERATOR_ID$dModelNo$", "result$", "map"),
	
	DEVICE_FIRMWARE				("펌웨어 정보 나열",			(byte)0xa6	,"OPERATOR_ID$currentPage$pageSize$makerName$", "result$TOTAL$PAGE$LIST$MAN_NAME$FW_NO$FW_NAME$DEVICE_TYPE$FIRMWARE_MAN_NAME$FW_APPLY_DATE", "list"),
	DEVICE_FIRMWARE_DETAIL		("펌웨어 정보 상제 조회",		(byte)0xa7	,"OPERATOR_ID$currentPage$pageSize$fwNo$", "result$TOTAL$PAGE$LIST$MAN_NAME$FW_NO$FW_NAME$DEVICE_TYPE$FIRMWARE_MAN_NAME$OS$FW_APPLY_DATE", "list"),
	DEVICE_FIRMWARE_REG			("펌웨어 정보 등록",			(byte)0xa5	,"OPERATOR_ID$manName$fwName$deviceType$firmwareManName$fwApplyDate$", "result$", "map"),
	DEVICE_FIRMWARE_MOD			("펌웨어 정보 수정",			(byte)0xa8	,"OPERATOR_ID$manName$fwNo$fwName$deviceType$firmwareManName$fwApplyDate$", "result$", "map"),
	DEVICE_FIRMWARE_DEL			("펌웨어 정보 삭제",			(byte)0xa9	,"OPERATOR_ID$fwNo$", "result$", "map"),
	
	DEVICE_MCU					("MCU 정보 나열",			(byte)0xab	,"OPERATOR_ID$currentPage$pageSize$makerName$", "result$TOTAL$PAGE$LIST$MAN_NAME$MCU_NO$MCU_NAME$MCU_MAN_NAME$MCU_VERSION", "list"),
	DEVICE_MCU_DETAIL			("MCU 정보 상제 조회",		(byte)0xac	,"OPERATOR_ID$currentPage$pageSize$mcuNo$", "result$TOTAL$PAGE$LIST$MAN_NAME$MCU_NO$MCU_NAME$MCU_MAN_NAME$MCU_VERSION", "list"),
	DEVICE_MCU_REG				("MCU 정보 등록",			(byte)0xaa	,"OPERATOR_ID$manName$mcuName$mcuManName$mcuVersion$", "result$", "map"),
	DEVICE_MCU_MOD				("MCU 정보 수정",			(byte)0xad	,"OPERATOR_ID$manName$mcuNo$mcuName$mcuManName$mcuVersion$", "result$", "map"),
	DEVICE_MCU_DEL				("MCU 정보 삭제",			(byte)0xae	,"OPERATOR_ID$mcuNo$", "result$", "map"),
	
	DEVICE_TYPE					("기기 종류",				(byte)0xaf	,"OPERATOR_ID$", "result$TOTAL$PAGE$LIST$DEVICE_TYPE$DEVICE_TYPE", "list"),
	
	DEVICE_POLICY				("기기정책 정보 나열",			(byte)0xb1	,"OPERATOR_ID$currentPage$pageSize$makerName$", "result$TOTAL$PAGE$LIST$D_SERIAL_NO$MAN_NAME$D_MODEL_NO$FW_NO$MCU_NO$OPERATOR_ID$START_DATE$D_POLICY_CERT", "list"),
	DEVICE_POLICY_DETAIL		("기기정책 정보 상제 조회",		(byte)0xb2	,"OPERATOR_ID$currentPage$pageSize$dSerialNo$", "result$TOTAL$PAGE$LIST$D_SERIAL_NO$MAN_NAME$D_MODEL_NO$DEVICE_NAME$FW_NO$FW_NAME$MCU_NO$MCU_NAME$OPERATOR_ID$LRA_ID$START_DATE$D_POLICY_CERT", "list"),
	DEVICE_POLICY_REG			("기기정책 정보 등록",			(byte)0xb0	,"OPERATOR_ID$manName$dModelNo$deviceName$fwNo$fwName$mcuNo$mcuName$", "result$", "map"),
	DEVICE_POLICY_MOD			("기기정책 정보 수정",			(byte)0xb3	,"OPERATOR_ID$manName$dSerialNo$dModelNo$fwNo$mcuNo$dPolicyCert", "result$", "map"),
	DEVICE_POLICY_DEL			("기기정책 정보 삭제",			(byte)0xb4	,"OPERATOR_ID$dSerialNo$", "result$", "map"),
	
	DEVICE_b5					("기기 정보 단일 나열",			(byte)0xb5  ,"OPERATOR_ID$currentPage$pageSize$makerName$", "result$TOTAL$PAGE$LIST$D_MODEL_NO$DEVICE_NAME", "list"),
	DEVICE_b6					("펌웨어 정보 단일 나열",		(byte)0xb6  ,"OPERATOR_ID$currentPage$pageSize$makerName$device$", "result$TOTAL$PAGE$LIST$FW_NO$FW_NAME", "list"),
	DEVICE_b7					("MCU 정보 단일 나열",		(byte)0xb7  ,"OPERATOR_ID$currentPage$pageSize$makerName$device$firmware$", "result$TOTAL$PAGE$LIST$MCU_NO$MCU_NAME", "list"),
	
	FIRMWARE_HASH				("펌웨어 Hash 정보 나열",		(byte)0xc1	,"OPERATOR_ID$currentPage$pageSize$makerName$", "result$TOTAL$PAGE$LIST$MAN_NAME$FW_HASH_NO$FW_NO$FW_NAME$FW_HASH$START_DATE", "list"),
	FIRMWARE_HASH_DETAIL		("펌웨어 Hash 정보 상제 조회",	(byte)0xc2	,"OPERATOR_ID$currentPage$pageSize$fwHashNo$", "result$TOTAL$PAGE$LIST$MAN_NAME$FW_HASH_NO$FW_NO$FW_NAME$FW_HASH$START_DATE", "list"),
	FIRMWARE_HASH_REG			("펌웨어 Hash 정보 등록",		(byte)0xc0	,"OPERATOR_ID$manName$fwNo$fwName$fwHash$startDate$", "result$", "map"),
	FIRMWARE_HASH_MOD			("펌웨어 Hash 정보 수정",		(byte)0xc3	,"OPERATOR_ID$manName$fwHashNo$fwNo$fwName$fwHash$startDate$", "result$", "map"),
	FIRMWARE_HASH_DEL			("펌웨어 Hash 정보 삭제",		(byte)0xc4	,"OPERATOR_ID$fwHashNo$", "result$", "map")
	;





	String name;
	byte header;
	String reqProtocol;
	String resProtocol;
	String split ="\\$";
	String delimit ="$";
	String type ;
	Logger log = Logger.getLogger(OppProtocol.class);
	OppProtocol(String name,byte head, String reqProtocol, String resProtocol,String type) {
		this.name 		= name;
		this.header 		= head;
		this.reqProtocol 	= reqProtocol;
		this.resProtocol 	= resProtocol;
		this.type			= type;
	}
	
	public String getName(){
		return name;
	}
	
	public byte getCode(){
		return header;
	}
	
	public String getReqProtocol(){
		return reqProtocol;
	}
	
	public String getResProtocol(){
		return resProtocol;
	}
	
	public String getRequestMessage(Map param){
		String[] names= reqProtocol.split(split);
		StringBuffer buffer = new StringBuffer(500);
		for(String key :names){
			buffer.append(param.get(key)).append(delimit);
		}
		log.debug("RequestMessage ["+this.name+"] :"+buffer.toString());
		return buffer.toString();
		
	}
	public Map getResponseMessage(String message){
		log.debug("ResponseMessage ["+this.name+"] :"+message);
		Map result= new HashMap();
		String[] datas = message.split(split);
		String[] protocals = resProtocol.split(split);
		
		if(datas != null && datas[0].toLowerCase().startsWith("ok")){
			result.put(protocals[0], datas[0]);
			if(type.equals("list")){
				pushData(result,datas[1]);
				pushData(result,datas[2]);
				pushData(result,datas[3]);
				result.put("list", getList(datas,protocals));
			}else{
				setHashData(result,datas);
			}
		
		}else{
			if(datas.length==2){
				result.put("result",datas[0]);
				pushData(result,datas[1]);
			}else{
				result.put("result",datas[0]);
			}
		}
		return result;
		
	}
	
	
	 	
	private void pushData(Map result, String data) {
		String dd[] =equalString(data);
		
		result.put(dd[0],dd[1]);
		
	}

	 
	
	
	private void setHashData(Map result, String[] datas) {
		for(int i = 1; i<datas.length ; i++){
			String[] kk= equalString(datas[i]);
			result.put(kk[0],kk[1]);
			}
	}

	private List getList(String[] datas,String[] protocals){
		
		List ll = new ArrayList();
		Map temp  = new HashMap();;
		for(int count = 4 ; count<datas.length ; count ++){
			
			if((count-4)%(protocals.length-4)==0 && !temp.isEmpty()){
				ll.add(temp);
				temp  = new HashMap();
			}
			
			String v[]=equalString(datas[ count]);
			temp.put(  v[0],v[1]);
		}
		ll.add(temp);
		return ll;
	}
	
	private String[] equalString(String message){
		String[] rValue = new String[2];
		
		if(message != null){
			int in =message.indexOf("=");
		    
			if(in ==0){
				rValue[0]="ERRMSG";
				rValue[1]=message.substring(1,message.length());	
			}else if(in>0){
				rValue[0]=message.substring(0,in).trim();
				rValue[1]=message.substring(in+1,message.length());	
			}else if(in<0){
				rValue[0]="ERRMSG";
				rValue[1]=message;	
			}
			
		}else{
			rValue[0]="ERRMSG";
			rValue[1]="NONE";
		}
		
		return rValue;
		
	}
}



