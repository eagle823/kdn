package com.ksign.kdn.client;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


 
@Service
public final class RaOppConnection  {
	
	@Autowired
	ClientConnection client;

 Logger log = Logger.getLogger(RaOppConnection.class);
 
 public RaOppConnection() throws UnknownHostException, IOException{
	
 }
 
 private  void initClient() throws UnknownHostException, IOException{
	 if(client == null){
		 client =  new ClientConnection();
	 }
	 client.connect();
 }
 
 private  void threadStart() throws UnknownHostException, IOException{
	 initClient();
	 
	
 }
 

 private   OppResponse send(OppReqHandler oppReq,OppResponse res) throws IOException{
		
		
	 initClient();
	 
	 client.send(oppReq.getReqMessage(), res);
	 client.cleanConnection();
	 return res;
 }
 

 public Map oppRequest(OppProtocol reqcode,Map param) throws RaOppConnectionException{
	 	OppReqHandler oppReq = new OppReqHandler(reqcode, param);
		OppResponse oppRes = null;
		   
		    
		try {
			oppRes = this.send(oppReq, new OppResponse(reqcode));
			param =oppRes.getResponse();
			log.debug(param.toString());
		} catch (IOException e) {
			e.printStackTrace();
			throw new RaOppConnectionException( "통신 에러"); //사용자가 존재 하지 않습니다.
		}
		 // Operator operator
		 if(param == null ||param.get("result")== null){
			 throw new RaOppConnectionException( "등록된 정보가 없습니다. "); //사용자가 존재 하지 않습니다.
		 }
		 return param;
 }
 
 public class RaOppConnectionException extends Exception{
	 public RaOppConnectionException(String message){
		 super(message);
	 }
 }
 
}