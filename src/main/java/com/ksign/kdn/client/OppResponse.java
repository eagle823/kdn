package com.ksign.kdn.client;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

public class OppResponse {

	OppProtocol protocol;
	String resMessage;
	Logger log = Logger.getLogger(OppResponse.class);
	public OppResponse(OppProtocol protocol){
		this.protocol = protocol;
	}
	
	
	
	public void setOppProtocol(OppProtocol protocol){
		this.protocol = protocol;
	}
	
	
	public void setResponseMessage(String resPacket) {
		log.debug("############ resPacket :"+resPacket);
		resMessage =resPacket;
	}
	
	public Map getResponse(){
		if(resMessage != null)
			return protocol.getResponseMessage(resMessage);
		else
			return new HashMap();
	}
	

}
