package com.ksign.kdn.client;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.EOFException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.Socket;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class ClientConnection
{
	@Value("#{kdn['ra.ip']}") String m_host;
	@Value("#{kdn['ra.port']}") int m_port;
	
  private boolean conCheck = true;

  private String data = "";

  private boolean isDebug = false;
  private boolean m_bIsMultiRecord = true;

  private int m_connectTimeout = 0;

  private DataInputStream m_dis = null;
  private DataOutputStream m_dos = null;


  

  private int m_readTimeout = 90000;
  private Socket m_socket = null;

  private byte[] opCode = null;

  private String pKey = "";

  
  public ClientConnection(){
	  
  }
 
  

  public void cleanConnection()
  {
    try
    {
      if (this.m_dis != null) {
        this.m_dis.close();
      }
      if (this.m_dos != null) {
        this.m_dos.close();
      }
      if (this.m_socket != null) {
        this.m_socket.close();
      }
      this.conCheck = true;
    } catch (Exception e) {
      setData(e.getMessage());
    }
  }

  public void connect()
  {
    plainConnect();
  }

  private boolean createConnection()
  {
    try
    {
      this.m_socket = new Socket(this.m_host, this.m_port);
    }
    catch (IOException ex) {
      setData("Failed To Connect To Server " + this.m_host + " : " + this.m_port);
      return false;
    }
    catch (Exception ex)
    {
      setData("Failed To Connect To Server " + this.m_host + " : " + this.m_port);
      return false;
    }

    return true;
  }

  public String getData()
  {
    try
    {
      if ((this.data == null) || (this.data.equals(""))) {
        return "";
      }
      return new String(this.data.getBytes(), "EUC-KR");
    } catch (UnsupportedEncodingException ex) {
    	ex.printStackTrace();
    	return "";
    }
     
  }

  public byte[] getOpCode()
  {
    return this.opCode;
  }

  public String getPKey()
  {
    return this.pKey;
  }


  public void plainConnect()
    
  {
    this.conCheck = createConnection();

    if (!this.conCheck) {
      setData("서버와의 연결에 실패하였습니다.");
      System.out.println(this.conCheck);
      return;
    }
    try
    {
      if (this.m_readTimeout != 0) {
        this.m_socket.setSoTimeout(this.m_readTimeout);
      }
      this.m_dis = new DataInputStream(new BufferedInputStream(this.m_socket.getInputStream()));
      this.m_dos = new DataOutputStream(new BufferedOutputStream(this.m_socket.getOutputStream()));
    }
    catch (Exception ex1) {
      setData("failed to connect to server " + this.m_host + " : " + this.m_port);
      this.conCheck = false;
    }
  }

  private String readPacket(DataInputStream dis)
     
  {
    
    try
    {
      

      return readRecord(dis);
    }
    catch (EOFException e) {
      cleanConnection();

      setData(e.getMessage());
      
    } catch (IOException e) {
      cleanConnection();

      setData(e.getMessage());
    
    }
    catch (Exception ex) {
      setData(ex.getMessage());
    } 
    return null;
  }

  public String readPacket()
    
  {
    return readPacket(this.m_dis);
  }

  private String readRecord(DataInputStream dis)
    throws EOFException, IOException
  {
    int ch1 = dis.read();
    int ch2 = dis.read();
    int ch3 = dis.read();
    int ch4 = dis.read();
    int dataSize = (ch1 << 24) + (ch2 << 16) + (ch3 << 8) + (ch4 << 0);

    if (dataSize < 9)
    {
      return new String("nok$데이터가 수신되지 않았습니다.$");
    }

    System.out.println(" >>>>> RES_SIZE :: " + dataSize);

    byte[] buffer = new byte[dataSize];
    dis.readFully(buffer);

    return new String(buffer, 9, dataSize - 9, "euc-kr");
  }

  

  public boolean requestOPP(String opcode, String userInfo)
  {
    if (!this.conCheck) {
      System.out.println(this.conCheck);
      return false;
    }
    try
    {
      String result = "";
      String ops = opcode.substring(2);
      int op = Integer.parseInt(ops, 16);
      byte[] tmp = userInfo.getBytes();
      int cnt = tmp.length;

      byte[] packet = new byte[tmp.length + 9];

      packet[0] = (byte)op;
      packet[1] = 0;
      packet[2] = 0;
      packet[3] = 0;
      packet[4] = 1;

      packet[5] = (byte)(cnt >> 24);
      packet[6] = (byte)(cnt >> 16 & 0xFF);
      packet[7] = (byte)(cnt >> 8 & 0xFF);
      packet[8] = (byte)(cnt & 0xFF);

      for (int i = 9; i < packet.length; i++) {
        packet[i] = tmp[(i - 9)];
      }

      boolean res = sendPacket(packet);

      if (!res) {
        setData("데이터 전송이 실패하였습니다.");
        return false;
      }

      Thread.sleep(100L);

      String resPacket = readPacket();


      
      for (int i = 0; i < result.length(); i++) {
        if ((result.charAt(i) == 'n') && (result.charAt(i + 1) == 'o') && (result.charAt(i + 2) == 'k')) {
          result = result.substring(i);
          break;
        }
        if ((result.charAt(i) == 'o') && (result.charAt(i + 1) == 'k')) {
          result = result.substring(i);
          break;
        }
      }

      if (result.startsWith("ok")) {
        setData(result.substring(3));
        return true;
      }if (result.startsWith("nok")) {
        setData(result.substring(4));
        return false;
      }

      setData("알 수 없는 형식의 메시지가 수신되었습니다.");
      System.out.println("Unknown packet :: " + result);
      return false;
    }
    catch (Exception ex) {
      setData(ex.getMessage());
    }return false;
  }

  public void sendMessage(byte[] msg)
  {
    try
    {
      this.m_dos.write(msg);
    } catch (Exception e) {
      setData(e.getMessage());
    }
  }

  private boolean sendPacket(byte[] packet, DataOutputStream dos)
    
  {
    try
    {
      sendRecord(packet, dos);
      return true;
    }
    catch (IOException e)
    {
      cleanConnection();
      setData(e.getMessage());
       
    } catch (Exception ex) {
      cleanConnection();

      setData(ex.getMessage());
    } 
    return false;
  }

  public boolean sendPacket(byte[] packet)
     
  {
    sendPacket(packet, this.m_dos);
    return true;
  }

  private void sendRecord(byte[] record, DataOutputStream dos)
    throws IOException
  {
	   
	  
    dos.writeInt(record.length);
    dos.flush();
    /*try {
      Thread.sleep(100L);
    } catch (InterruptedException ex) {
    }*/
    dos.write(record);
    dos.flush();
  }

  public void setData(String data)
  {
    this.data = data;
  }

  public void setMultiRecord(boolean m_bIsMultiRecords)
  {
    this.m_bIsMultiRecord = m_bIsMultiRecords;
  }

  public void setOpCode(byte[] opCode)
  {
    this.opCode = opCode;
  }

  public void setServerInfo(String host, int port)
  {
    this.m_host = host;
    this.m_port = port;
  }

	public boolean send(byte[] reqMessage, OppResponse oppRes) {
		boolean res	= sendPacket(reqMessage);
		if (!res) {
	        setData("데이터 전송이 실패하였습니다.");
	        return false;
	      }

	     /* try {
			Thread.sleep(100L);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
	      String resPacket = readPacket();
	      oppRes.setResponseMessage(resPacket);
	      
	      return true;

	}
}