package com.ksign.kdn.client;

import java.util.Map;

public class OppReqHandler {
	OppProtocol protocol;
	
	private byte[] packet;

	public OppReqHandler(OppProtocol protocol,Map param){
		this.protocol = protocol;
		System.out.println("#####getRequestMessage :"+protocol.getRequestMessage(param));
		 byte[] tmp = protocol.getRequestMessage(param).getBytes();
	      int cnt = tmp.length;

	      packet = new byte[cnt + 9];

	      packet[0] = protocol.getCode();
	      packet[1] = 0;
	      packet[2] = 0;
	      packet[3] = 0;
	      packet[4] = 1;

	      packet[5] = (byte)(cnt >> 24);
	      packet[6] = (byte)(cnt >> 16 & 0xFF);
	      packet[7] = (byte)(cnt >> 8 & 0xFF);
	      packet[8] = (byte)(cnt & 0xFF);

	      for (int i = 9; i < packet.length; i++) {
	        packet[i] = tmp[(i - 9)];
	      }
	}
	
	public byte[] getReqMessage() {
		return packet;
	}

}
