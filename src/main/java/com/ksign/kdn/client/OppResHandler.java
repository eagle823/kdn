package com.ksign.kdn.client;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

public class OppResHandler {
private byte[] rsp = null;
private Map data;
private String charSet="EUC-KR";	
private final String delim ="$";
private final String eq ="=";
	public synchronized boolean handleResponse(byte[] rsp) {
		this.rsp = rsp;
		this.notify();
		return true;
	}
	
	public synchronized Map waitForResponse() {
		while(this.rsp == null) {
			try {
				this.wait();
			} catch (InterruptedException e) {
			}
		}
		
		if(rsp != null && rsp.length >13){
			

			try {
				setData( new  String(rsp, 13, rsp.length - 13, charSet));
			} catch (UnsupportedEncodingException e) {

				e.printStackTrace();
			}
		}
		
		return data;
		//System.out.println(new String(this.rsp,charSet));
	}

	private void setData(String protocal) {
		
		if(protocal != null ){
			data = new HashMap();
			if(protocal.indexOf(delim)>0){
				String[] datas =protocal.split(delim);
				int countName=0;
				 for(String d : datas){
					 if(d.indexOf(eq)>1){
						 String[] kv= d.split(eq);
						 data.put(kv[0], kv.length>1?kv[1]:"");
					 }else{
						 if(d.trim().length()>0)
						   data.put(countName+"", d); 
						 countName++;
					 }
				 }
				 
			}else{
				data.put("data", protocal);
			};
		}
		 
		
	}
}
