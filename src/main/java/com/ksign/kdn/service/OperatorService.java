package com.ksign.kdn.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ksign.kdn.client.OppProtocol;
import com.ksign.kdn.client.RaOppConnection;
import com.ksign.kdn.client.RaOppConnection.RaOppConnectionException;
import com.ksign.kdn.domain.PageResult;
import com.ksign.kdn.domain.Production;
import com.ksign.kdn.domain.WebUtil;
import com.ksign.kdn.form.CheckForm;
import com.ksign.kdn.form.SearchForm;
import com.ksign.kdn.util.ResultMessage;
import com.ksign.kdn.util.ResultMessage.Result;



@Service
public class OperatorService {

	@Autowired
	RaOppConnection raOpp;

	/*@Autowired
	ProductionMapper   productionMapper;*/

	Logger logger= Logger.getLogger(OperatorService.class);

	public Map loginCheck(String userid,String pwd) throws OperatorException{
		Map  param = new HashMap();
		param.put("userid", userid);		
		param.put("userpwd", pwd);



		try {
			param =raOpp.oppRequest(OppProtocol.IDPW_LOGIN, param);	
		} catch (RaOppConnectionException e) {
			e.printStackTrace();
			throw new OperatorException("User not Found"); //사용자가 존재 하지 않습니다.
		}


		// Operator operator
		if(param == null ||param.get("result")== null){
			throw new OperatorException("User not Found"); //사용자가 존재 하지 않습니다.
		}

		if(param.get("result").equals("ok") ){
			param.put("usertype", param.get("TYPE"));
			System.out.println("#$@%#$%B  " + param.toString());
			return param;
		}else{
			if(param.get("ERRMSG") != null)
				throw new OperatorException((String)param.get("ERRMSG")); //비밀 번호가 다릅니다.
			else
				throw new OperatorException("정의되지 않은 오류 발생"); //비밀 번호가 다릅니다.
		}

	}


	public PageResult operatorSearch(PageResult page){
		Map param = page.getSearchParam();
	 
		try {
			if(param.get("kind").toString().startsWith("companyName")&& !param.get("searchKeyword").toString().equals("")){
				param =raOpp.oppRequest(OppProtocol.MAKER_NAME_SEARCH, param);
			}else if(param.get("kind").toString().startsWith("managerName")&&!param.get("searchKeyword").toString().equals("")){
				param =raOpp.oppRequest(OppProtocol.MAKER_MANAGER_SEARCH, param);	
			}else if(param.get("kind").toString().startsWith("product_ssn")&&!param.get("searchKeyword").toString().equals("")){
				param =raOpp.oppRequest(OppProtocol.COM_REG_NUMBER_SERRCH, param);	
			}else{
				param = raOpp.oppRequest(OppProtocol.MAKER_LIST, param);
			}


		} catch (RaOppConnectionException e) {

			e.printStackTrace();
		}

		logger.debug("operatorSearch :"+param.toString());
		
		if(param == null){
			page.setTotalCount(0);
			page.setResultList(new ArrayList());
		}else if(param.get("result").equals("ok") ){
			if(param.get("list") !=null){
				List list = (List)param.get("list");
				System.out.println("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ " + list.toString());
				logger.debug("operatorSearch :"+list.toString());
				page.setTotalCount(param.get("TOTAL"));
				page.setResultList(list);
			}else{
				page.setTotalCount(0);
				page.setResultList(new ArrayList());
			}
		}else{
			page.setTotalCount(0);
			page.setResultList(new ArrayList());
		}


		return page;
	}


	public class OperatorException extends Exception{

		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		public OperatorException(String message){
			super(message);
		}


	}


	public Map managerSearch(SearchForm searForm) {
		logger.debug("#############vmanagerSearch :"+ searForm.toString());
		PageResult pageResult =WebUtil.getPageResult(searForm);

		logger.debug("#############vmanagerSearch :"+ pageResult.getSearchParam());
		Map param = new HashMap();
		param.put("empty", "");
		try {
			param =	raOpp.oppRequest(OppProtocol.ADMIN_LIST,param );
		} catch (RaOppConnectionException e) {

			e.printStackTrace();
		}
		return param;
	}


	public ResultMessage checkData(CheckForm check) {
		logger.debug("#############CheckForm :"+check.toString());
		ResultMessage message = null;
		try{
			if(check.getCheckValue().equals("")){
				throw new NullPointerException("제조사 명을 입력하시기 바랍니다.");	
			}
			Map param = new HashMap();
			param.put("companyName",check.getCheckValue());
			param.put("OPERATOR_ID",check.getOperatorId());
			param =	raOpp.oppRequest(OppProtocol.MAKER_CHECK, param);
			if(param != null && param.get("result")  != null&&param.get("result").equals("ok")){
				message = ResultMessage.OKMessage( "사용 가능 합니다.");
			}else{
				message = ResultMessage.NOKMessage( "같은 이름에 제조사가 존재 합니다.");

			}
		}catch(NullPointerException e){
			message = ResultMessage.NOKMessage(  e.getMessage());

		}catch(Exception e){
			message = ResultMessage.NOKMessage(  e.getMessage());
		}



		return message;
	}


	public ResultMessage saveForm(Production production, String key) {
		ResultMessage message = null;
		try{
			Map param = new HashMap();
			if(key.equals("add")){
				param =raOpp.oppRequest(OppProtocol.MAKER_ADD, production.toParamter());	

			}else if(key.equals("mod")){
				param =raOpp.oppRequest(OppProtocol.MAKER_MOD, production.toParamter()); 
			}else if(key.equals("del")){
				logger.debug(production.getDelList());
				logger.debug(production.getProduct_name());
				param =raOpp.oppRequest(OppProtocol.MAKER_DEL, production.toParamter()); 
			}

			message  =getResultMessage(param,key);
		}catch(Exception e){
			message = new ResultMessage(key,Result.FAIL,"에 실패 하였습니다.");
		}

		return message;
	}




	private ResultMessage getResultMessage(Map param,String key) {
		if(param.get("result")!= null && param.get("result").equals("ok")){
			return new ResultMessage(key,Result.SUCESS,"되었습니다");
		}else{
			return    new ResultMessage(key,Result.FAIL,"에 실패 하였습니다.");
		}
	}


	public Map loginCheckCert(String Dn, String signedData,String ip)  
			throws OperatorException{
		Map  param = new HashMap();
		param.put("dn", Dn);		
		param.put("userip", signedData);



		try {
			param =raOpp.oppRequest(OppProtocol.CERT_LOGIN, param);	
		} catch (RaOppConnectionException e) {
			e.printStackTrace();
			throw new OperatorException("User not Found"); //사용자가 존재 하지 않습니다.
		}


		// Operator operator
		if(param == null ||param.get("result")== null){
			throw new OperatorException("User not Found"); //사용자가 존재 하지 않습니다.
		}

		if(param.get("result").equals("ok") ){
			return param;
		}else{
			if(param.get("ERRMSG") != null)
				throw new OperatorException((String)param.get("ERRMSG")); //비밀 번호가 다릅니다.
			else
				throw new OperatorException("정의되지 않은 오류 발생"); //비밀 번호가 다릅니다.
		}
	}


	public Production getMakerDetail(Map param) throws RaOppConnectionException {

		param =raOpp.oppRequest(OppProtocol.MAKER_DETAIL_SEARCH, param);	
		;
		return new Production(param);
	}


	public String deletCompany(String[] list, Map param) {
		logger.debug(list);
		String fail ="";

		if(list  != null){
			logger.debug(list.length);

			for(int i =0 ;i <list.length ; i++){
				param.put("product_name", list[i]);
				try {
					Map result =raOpp.oppRequest(OppProtocol.MAKER_DEL,  param);
					if(result.get("result").equals("nok")){
						fail += list[i]+",";
					}
				} catch (RaOppConnectionException e) {
					fail += list[i]+",";
				}	
			}

		}



		return fail;
	}
}

