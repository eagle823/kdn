package com.ksign.kdn.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ksign.kdn.client.OppProtocol;
import com.ksign.kdn.client.RaOppConnection;
import com.ksign.kdn.client.RaOppConnection.RaOppConnectionException;
import com.ksign.kdn.domain.PageResult;
import com.ksign.kdn.form.SearchForm;
import com.ksign.kdn.util.StringUtil;

@Service
public class PopupService {
	Logger log = Logger.getLogger(PopupService.class);
	@Autowired
	RaOppConnection raOpp;
	
	public Map findCompany(	Map  param) throws PopupException{
		 
		/*param.put("pageSize", "0");*/		
		    
		try {
			
			param = raOpp.oppRequest(OppProtocol.COMPANY_SEARCH, param);
			 log.debug(param.toString());
		} catch (RaOppConnectionException  e) {
			throw new PopupException(e.getMessage()); //사용자가 존재 하지 않습니다.
		}
		 // Operator operator
		 if(param == null ||param.get("result")== null){
		  throw new PopupException("등록된 정보가 없습니다. "); //사용자가 존재 하지 않습니다.
		 }
		 
		 if(param.get("result").equals("ok") ){
			 List list =(List)param.get("list");
			 List renew = new ArrayList();
			 
			 for( int i  =0 ; i<list.size() ; i++){
				  Map t =(Map)list.get(i);
				  try{
				     t.put("MAKER_ID", t.get("MAKER_ID"));
				     t.put("MAKER_NAME", t.get("MAKER_NAME"));
				     renew.add(t);

				  }catch(Exception e){
					  e.printStackTrace();
					  if(t != null){
					  log.debug(t.toString());
					  }
				  }
				  
			  }
			 
			  param.put("list", renew);
			  param.put("paging", getPrintString(param));
			 
			 return param;
		 }else{
			 if(param.get("message") != null)
			 throw new PopupException((String)param.get("message")); //비밀 번호가 다릅니다.
			 else
				 throw new PopupException("정의되지 않은 오류 발생"); //비밀 번호가 다릅니다.
		 }
	}
	
public class PopupException extends Exception{
	public PopupException(String message){
		super(message);
	}
		
	}

public Map getData(SearchForm search ,String key) throws PopupException {
	
    if(key.equals("address")){
		return getAddressData(search);
    }else if(key.equals("company")){
    	return getCompanyData(search);
    }
	return null;
}
	

private Map getCompanyData(SearchForm search) throws PopupException {
	Map param = search.toMap();
	    /*param.put("pageSize", "0");*/		
    
	try {
		//ADMIN_NAME_SEARCH		
		//ADMIN_ID_SEARCH	
		if(search.getKind().equals("companyName") && !search.getSearchKeyword().trim().equals("")){
		   param.put("companyName", param.get("searchKeyword"));
		   param = raOpp.oppRequest(OppProtocol.MAKER_NAME_SEARCH, param);
		}else if(search.getKind().equals("managerName")  && !search.getSearchKeyword().trim().equals("")){
		  param.put("userName", param.get("searchKeyword"));
		  param = raOpp.oppRequest(OppProtocol.MAKER_MANAGER_SEARCH, param);
		}else{
		  param = raOpp.oppRequest(OppProtocol.COMPANY_SEARCH, param);
		}
		 log.debug(param.toString());
	} catch (RaOppConnectionException  e) {
		throw new PopupException(e.getMessage()); //사용자가 존재 하지 않습니다.
	}
	 // Operator operator
	 if(param == null ||param.get("result")== null){
	  throw new PopupException("등록된 정보가 없습니다. "); //사용자가 존재 하지 않습니다.
	 }
	 
	 if(param.get("result").equals("ok") ){
		 
		 List list =(List)param.get("list");
		 List renew = new ArrayList();
		 
		 for( int i  =0 ; i<list.size() ; i++){
			  Map t =(Map)list.get(i);
			  try{
				  System.out.println("QWER " + t.get("MAKER_NAME"));
			     t.put("MAKER_ID", t.get("MAKER_ID"));
			     t.put("MAKER_NAME", t.get("MAKER_NAME"));
			     renew.add(t);

			  }catch(Exception e){
				  e.printStackTrace();
				  if(t != null){
				  log.debug(t.toString());
				  }
			  }
			  
		  }
		 
		  param.put("list", renew);
		  param.put("paging", getPrintString(param));
		 
		 return param;
	 }else{
		 if(param.get("message") != null)
		 throw new PopupException((String)param.get("message")); //비밀 번호가 다릅니다.
		 else
			 throw new PopupException("정의되지 않은 오류 발생"); //비밀 번호가 다릅니다.
	 }
	 
}


private Map getAddressData(SearchForm search) throws PopupException {
	Map param =search.toMap(); 
	try {
		param = raOpp.oppRequest(OppProtocol.POST_NUM_SEARCH, param);
		
	} catch (RaOppConnectionException  e) {
		throw new PopupException(e.getMessage()); //사용자가 존재 하지 않습니다.
	}
	 // Operator operator
	 if(param == null ||param.get("result")== null){
	  throw new PopupException("등록된 정보가 없습니다. "); //사용자가 존재 하지 않습니다.
	 }
	 
	 if(param.get("result").equals("ok") ){
		  
		  List list =(List)param.get("list");
		  List renew = new ArrayList();
		 
		  for( int i  =0 ; i<list.size() ; i++){
			  Map t =(Map)list.get(i);
			  try{
			     String a = (String)t.get("ADDRESS");
			     String add[] = a.split("\\^");
			     log.debug("########## " +add.length);
			     if(add != null){
			    	 if( add.length==2){
					     t.put("post", add[0]);
					     t.put("address", add[1]);
			    	 }else if(add.length ==1){
			    		 t.put("post", add[0]); 
			    		 t.put("address", "");
			    	 }
			     }
			     
			     renew.add(t);
			     log.debug("########## " +list.toString());
			  }catch(Exception e){
				  e.printStackTrace();
				  if(t != null){
				  log.debug(t.toString());
				  }
			  }
			  
		  }
		 
		  param.put("list", renew);
		  param.put("paging", getPrintString(param));
		  
		 return param;
	 }else{
		 if(param.get("message") != null)
		 throw new PopupException((String)param.get("message")); //비밀 번호가 다릅니다.
		 else
			 throw new PopupException("정의되지 않은 오류 발생"); //비밀 번호가 다릅니다.
	 }
}


private String getPrintString(Map param){
	 PageResult p = new PageResult();
	  p.setTotalCount(param.get("TOTAL"));
	  p.setCurrentPage(Integer.parseInt(param.get("PAGE").toString()));
	  
	  String ps =StringUtil.getString(param, "LIST");
	  int pageSize =10;
	  try{
		  pageSize = Integer.parseInt(ps);
	  }catch(Exception e){}
			  
	  p.setPageSize(pageSize); 
	  
	  return p.getPagingPrint();
}
}

