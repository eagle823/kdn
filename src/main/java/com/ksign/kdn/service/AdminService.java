package com.ksign.kdn.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ksign.kdn.client.OppProtocol;
import com.ksign.kdn.client.RaOppConnection;
import com.ksign.kdn.client.RaOppConnection.RaOppConnectionException;
import com.ksign.kdn.domain.LoginSession;
import com.ksign.kdn.domain.PageResult;
import com.ksign.kdn.domain.Police;
import com.ksign.kdn.domain.Policy;
import com.ksign.kdn.domain.User;
import com.ksign.kdn.form.CheckForm;
import com.ksign.kdn.form.UserAddForm;
import com.ksign.kdn.service.CertService.CertServiceException;
import com.ksign.kdn.util.ResultMessage;


@Service
public class AdminService {
	Logger logger= Logger.getLogger(AdminService.class);
	
	
	@Autowired
	RaOppConnection raOpp;
	
	public ResultMessage checkID(CheckForm check,LoginSession session) {
		
		ResultMessage message = null;
		try{
			if(check.getCheckValue().equals("")){
			  throw new NullPointerException("사용자 ID을 입력하시기 바랍니다.");	
			}
			Map  param = new HashMap();
			param.put("userid",check.getCheckValue());		
			param.put("OPERATOR_ID",session.getOPERATOR_ID());		
			    
			try {
				param =raOpp.oppRequest(OppProtocol.ADMIN_ID_CHECK, param);	
			} catch (RaOppConnectionException e) {
				e.printStackTrace();
				message = ResultMessage.NOKMessage( "통신 에러"); //사용자가 존재 하지 않습니다.
			}
			 // Operator operator
			 if(param == null ||param.get("result")== null){
				 message = ResultMessage.NOKMessage( "등록된 정보가 없습니다. "); //사용자가 존재 하지 않습니다.
			 }
			 
			 if(param.get("result").equals("ok") ){
				 message = ResultMessage.OKMessage("사용 가능 합니다.");
			 }else{
				 if(param.get("ERRMSG") != null)
//					message = ResultMessage.NOKMessage( (String)param.get("ERRMSG")); //비밀 번호가 다릅니다.
				 	message = ResultMessage.NOKMessage("이미 사용중인 아이디 입니다."); //비밀 번호가 다릅니다.
				 else
					 message = ResultMessage.NOKMessage( "정의되지 않은 오류 발생"); //비밀 번호가 다릅니다.
			 }
			
		 
		}catch(NullPointerException e){
			message = ResultMessage.NOKMessage(  e.getMessage());
			 
		}catch(Exception e){
			message = ResultMessage.NOKMessage(  e.getMessage());
		}
		
		
		logger.debug("#############getMessage  :"+message.getMessage());
		return message;
	}
	
	public ResultMessage modifyPW(String userId, String confPw){
		
		ResultMessage message = null;
		try{
			if(userId.equals("")){
			  throw new NullPointerException("사용자 ID가 누락되었습니다.");	
			}
			Map  param = new HashMap();
			param.put("userid", userId);		
			param.put("confPw", confPw);		
			    
			try {
				param =raOpp.oppRequest(OppProtocol.ADMIN_PW_MODIFY, param);	
			} catch (RaOppConnectionException e) {
				e.printStackTrace();
				message = ResultMessage.NOKMessage( "통신 에러");
			}
			 // Operator operator
			 if(param == null ||param.get("result")== null){
				 message = ResultMessage.NOKMessage( "등록된 정보가 없습니다. ");
			 }
			 
			 if(param.get("result").equals("ok") ){
				 message = ResultMessage.OKMessage("수정 되었습니다.");
			 }else{
				 message = ResultMessage.NOKMessage( "정의되지 않은 오류 발생");
			 }
			
		 
		}catch(NullPointerException e){
			message = ResultMessage.NOKMessage(  e.getMessage());
			 
		}catch(Exception e){
			message = ResultMessage.NOKMessage(  e.getMessage());
		}
		
		logger.debug("#############getMessage  :"+message.getMessage());
		return message;
	}
	
	public   User    getAdminDetail(Map  param) throws AdminException {
		   try {
			   User user = new User();
				param =raOpp.oppRequest(OppProtocol.ADMIN_DETAIL_SEARCH, param);	
				Object obj = param.get("list");
				if(obj instanceof List){
					List list = (List)obj;
					if(list.size() >0){
						
						user.setUser((Map)list.get(0));
						return user;
					}
					
				}
					
				throw new AdminException("관리자 정보를 찾을 수 없습니다."); //사용자가 존재 하지 않습니다.
		   
		   } catch (RaOppConnectionException e) {
				e.printStackTrace();
				throw new AdminException("관리자 정보를 찾을 수 없습니다."); //사용자가 존재 하지 않습니다.
			}
	}


	public ResultMessage userAdd(UserAddForm userAddForm,LoginSession sesion,String key) {
		logger.debug("#############CheckForm :"+userAddForm.toString());
		ResultMessage message = null;
		try{
			  
			
			   Map param =userAddForm .toMap()	;
			   param.put("OPERATOR_ID", sesion.getOPERATOR_ID());
			   
			   logger.debug("############# user add param :"+param.toString());
					try {
						if(key.equals("add")){
						param =raOpp.oppRequest(OppProtocol.ADMIN_REG, param);
						}else{
							param =raOpp.oppRequest(OppProtocol.ADMIN_MOD, param);	
						}
					} catch (RaOppConnectionException e) {
						e.printStackTrace();
						message = ResultMessage.NOKMessage( e.getMessage()); //사용자가 존재 하지 않습니다.
					
					}
			   
			    
			 
			 if(param == null ||param.get("result")== null){
				 message = ResultMessage.NOKMessage( "등록된 정보가 없습니다. "); //사용자가 존재 하지 않습니다.
			 }
			 
			 if(param.get("result").equals("ok") ){
				 message =	 ResultMessage.OKMessage("등록 되었습니다.");
			 }else{
				 if(param.get("message") != null)
					 message = ResultMessage.NOKMessage( (String)param.get("message")); //비밀 번호가 다릅니다.
				 else
					 message = ResultMessage.NOKMessage( "정의되지 않은 오류 발생"); //비밀 번호가 다릅니다.
			 }
			
	
		}catch(Exception e){
			message = ResultMessage.NOKMessage(  e.getMessage());
		}
		return message;
	}

	 

	public PageResult userListSearch(PageResult pageResult) {
		
		Map param = pageResult.getSearchParam();
		param.put("empty","" );	
		logger.debug("#### Reqeust Param :"+param.toString()); 
		try {
			
			if(param.get("kind").toString().startsWith("companyName")&& !param.get("searchKeyword").toString().equals("")){
				param =raOpp.oppRequest(OppProtocol.ASSIGN_AMDIN_SEARCH, param);
			}else if(param.get("kind").toString().startsWith("managerName")&&!param.get("searchKeyword").toString().equals("")){
				param =raOpp.oppRequest(OppProtocol.ADMIN_NAME_SEARCH, param);	
			}else if(param.get("kind").toString().startsWith("managerId")&&!param.get("searchKeyword").toString().equals("")){
				param =raOpp.oppRequest(OppProtocol.ADMIN_ID_SEARCH, param);	
			}else{
				param =raOpp.oppRequest(OppProtocol.ADMIN_LIST, param);	
			}
		} catch (RaOppConnectionException e) {
			e.printStackTrace();
			param.put("list",new ArrayList() );	
		}
		logger.debug("#### Parma :"+param.toString());
	
		
		if(param.get("result").toString().toLowerCase().equals("ok")){
			pageResult.setTotalCount(param.get("TOTAL"));
			pageResult.setResultList((List)param.get("list"));
		}
				
		return pageResult;
		
		
	}
	
	public class AdminException extends Exception{
		public AdminException(String message){
			super(message);
		}
	}

	public String userDelete(HttpServletRequest request,Map  param )   {
		 
		String[] obj =request.getParameterValues("selecter");
		String fail="";
		if(obj  != null){
			
				  for(int i =0 ;i <obj.length ; i++){
					  param.put("userId", obj[i]);
					  try {
						  Map result  =raOpp.oppRequest(OppProtocol.ADMIN_DEL, param);
						if(result.get("result").equals("nok")){
							fail += obj[i]+",";
						}
					} catch (RaOppConnectionException e) {
						fail += obj[i]+",";
					}	
				  }
				
			}
		
		return fail;
		
	}
	//var _order =["companyName","device","firmware","chip","chipversion","police"];	 
	public Map serachPoliceData(Police police) {
		Map param =police.toParameter();
		try {
			if(police.getName().equals("device")){
				System.out.println("@@@@@@@@@@@@@@@@@@@@  ADDDDDDDDDDDD" + LoginSession.SESSIONNAME);
			param =raOpp.oppRequest(OppProtocol.DEVICE_b5, police.toParameter());
			}else if(police.getName().equals("firmware")){
				param =raOpp.oppRequest(OppProtocol.DEVICE_b6, police.toParameter());
			}else if(police.getName().equals("mcu")){
				param =raOpp.oppRequest(OppProtocol.DEVICE_b7, police.toParameter());
			}else if(police.getName().equals("police")){
				param =raOpp.oppRequest(OppProtocol.POLICY_LIST, police.toParameter());	
			}else if(police.getName().equals("mod")){
				param =raOpp.oppRequest(OppProtocol.POLICY_UPDATE, police.toParameter());	
			}
		} catch (RaOppConnectionException e) {
			 
			e.printStackTrace();
		}
		return param;
	}

	public PageResult getPolicyList(PageResult pageResult) throws RaOppConnectionException {
		
		Map param = pageResult.getSearchParam();
		param =raOpp.oppRequest(OppProtocol.DEVICE_POLICY, param);
	 
		if(param.get("result").toString().toLowerCase().equals("ok")){
			pageResult.setTotalCount(param.get("TOTAL"));
			pageResult.setResultList((List)param.get("list"));
		}
				
		return pageResult;
	}
	
	public Policy getPolicyDetail(Map param) throws AdminException{
		// TODO Auto-generated method stub
		Policy policy = new Policy();
		try {
			param = raOpp.oppRequest(OppProtocol.DEVICE_POLICY_DETAIL, param);
			Object obj = param.get("list");
			if(obj instanceof List){
				List list = (List)obj;
				if(list.size() > 0){
					policy.setPolicy((Map)list.get(0));
					return policy;
				}
			}
			throw new AdminException("정책 정보를 찾을 수 없습니다.");
		} catch (RaOppConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new AdminException("정책 정보를 찾을 수 없습니다.");
		}
	}
	
	public Map getPolicyCert(Map param){
		try {
			param =raOpp.oppRequest(OppProtocol.POLICY_LIST, param);
		} catch (RaOppConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return param;
	}
	
	public String policyDelete(HttpServletRequest request, Map param) {
		// TODO Auto-generated method stub
		String dSerialNo = request.getParameter("dSerialNo");
		String fail = "";
		
		param.put("dSerialNo", dSerialNo);
		try {
			Map result = raOpp.oppRequest(OppProtocol.DEVICE_POLICY_DEL, param);
			
			if(result.get("result").equals("nok")){
				fail = dSerialNo;
			}
		} catch (RaOppConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			fail = dSerialNo;
		}
		
		return fail;
	}
}
