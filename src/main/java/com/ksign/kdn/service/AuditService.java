package com.ksign.kdn.service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ksign.kdn.client.OppProtocol;
import com.ksign.kdn.client.RaOppConnection;
import com.ksign.kdn.client.RaOppConnection.RaOppConnectionException;
import com.ksign.kdn.domain.LoginSession;
import com.ksign.kdn.domain.PageResult;
import com.ksign.kdn.form.AuditForm;
import com.ksign.kdn.form.SearchForm;
import com.ksign.kdn.service.CertService.CertServiceException;

@Service
public class AuditService {
	
	
	Logger logger= Logger.getLogger(AdminService.class);
	@Autowired
	RaOppConnection raOpp;
	
	SimpleDateFormat day = new SimpleDateFormat("yyyy-MM-dd");
	SimpleDateFormat mon = new SimpleDateFormat("yyyy-MM");

	public PageResult getAuditList(AuditForm auditForm, LoginSession session) {
		
		Map param = auditForm.toMap();
		param.put("OPERATOR_ID",session.getOPERATOR_ID());
		PageResult pageResult = new PageResult();
		pageResult.setCurrentPage(auditForm.getCurrentPage());
		pageResult.setPageSize(auditForm.getPageSize());
		
		try {
			param = raOpp.oppRequest(OppProtocol.AUDIT_SEARCH, param);
		} catch (RaOppConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if(param.get("result").toString().toLowerCase().equals("ok")){
			pageResult.setTotalCount(param.get("TOTAL"));
			pageResult.setResultList((List)param.get("list"));
		}
		
		
		return pageResult;
	}
	//@Autowired
	//AuditMapper auditMapper;

	public PageResult getCertDayLog(PageResult pageResult, LoginSession session) {
		/*
		Map param = search.toMap();
		param.put("OPERATOR_ID",session.getOPERATOR_ID());

		if(param.get("date").equals(" ")){
			param.put("date", getDate());
		}else
		{
			param.put("date", search.getSdate() + "^" + search.getEdate());
		}
		PageResult pageResult = new PageResult();
		pageResult.setCurrentPage(search.getCurrentPage());
		pageResult.setPageSize(search.getPageSize());
		param.put("MAKER_NAME", session.getMAKER_NAME());
		
		try {
			param = raOpp.oppRequest(OppProtocol.CERT_PROCESS_DAY_SEARCH, param);
		} catch (RaOppConnectionException e) {
		 
			e.printStackTrace();
		}
		*/
		Map param = pageResult.getSearchParam();
	//	Map sParam = search.toMap();
		
		param.put("empty","" );	
		 
		try {
			/*
			if(sParam.get("date").equals(" ")){
				param.put("date", getDate());
			
			}else
			{
				param.put("date", search.getSdate() + "^" +search.getEdate());
			}
			*/
			param.put("date", pageResult.getsDate() + "^" + pageResult.geteDate());
			
			if(pageResult.getMakerName().equals(""))
			{	param.put("MAKER_NAME", session.getMAKER_NAME());
				
			}else{
				param.put("MAKER_NAME", pageResult.getMakerName());
			}
			
		
			param.put("device", pageResult.getDeviceName());
			logger.debug("#### Reqeust Param :"+param.toString());
			param =raOpp.oppRequest(OppProtocol.CERT_PROCESS_DAY_SEARCH, param);
			
	
		} catch (RaOppConnectionException e) {
			e.printStackTrace();
			param.put("list",new ArrayList() );	
		}
		logger.debug("#### Parma :"+param.toString());
		
		if(param.get("result").toString().toLowerCase().equals("ok")){
			pageResult.setTotalCount(param.get("TOTAL"));
			pageResult.setResultList((List)param.get("list"));
			pageResult.setOPERATOR_ID(session.getOPERATOR_ID());
		}
		return pageResult;
	}

	public PageResult getCertMonthLog(PageResult pageResult, LoginSession session) {
		Map param = pageResult.getSearchParam();
		//	Map sParam = search.toMap();
			
			param.put("empty","" );	
			 
			try {
				/*
				if(sParam.get("date").equals(" ")){
					param.put("date", getDate());
				
				}else
				{
					param.put("date", search.getSdate() + "^" +search.getEdate());
				}
				*/
				param.put("date", pageResult.getsDate() + "^" + pageResult.geteDate());
				if(pageResult.getMakerName().equals(""))
				{	param.put("MAKER_NAME", session.getMAKER_NAME());
					
				}else{
					param.put("MAKER_NAME", pageResult.getMakerName());
				}
				param.put("device", pageResult.getDeviceName());
				logger.debug("#### Reqeust Param :"+param.toString());
				param =raOpp.oppRequest(OppProtocol.CERT_PROCESS_MON_SEARCH, param);
				
		
			} catch (RaOppConnectionException e) {
				e.printStackTrace();
				param.put("list",new ArrayList() );	
			}
			logger.debug("#### Parma :"+param.toString());
			
			if(param.get("result").toString().toLowerCase().equals("ok")){
				pageResult.setTotalCount(param.get("TOTAL"));
				pageResult.setResultList((List)param.get("list"));
				pageResult.setOPERATOR_ID(session.getOPERATOR_ID());
			}
		
		return pageResult;
		
	}
	
	public PageResult getCertSuccessLog(SearchForm search, LoginSession session) {
		Map param = search.toMap();
		param.put("OPERATOR_ID",session.getOPERATOR_ID());

		if(param.get("date").equals(" ")){
			param.put("date", getDate());
		}else
		{
			param.put("date", search.getSdate() + "^" + search.getEdate());
		}
		PageResult pageResult = new PageResult();
		pageResult.setCurrentPage(search.getCurrentPage());
		pageResult.setPageSize(search.getPageSize());
		param.put("MAKER_NAME", session.getMAKER_NAME());
		
		try {
			param = raOpp.oppRequest(OppProtocol.CERT_PROCESS_REQUEST_SUCCESS, param);
		} catch (RaOppConnectionException e) {
		 
			e.printStackTrace();
		}
		
		if(param.get("result").toString().toLowerCase().equals("ok")){
			pageResult.setTotalCount(param.get("TOTAL"));
			pageResult.setResultList((List)param.get("list"));
			pageResult.setOPERATOR_ID(session.getOPERATOR_ID());
			System.out.println("@@@@@@@@@@@@@@@@@@@ pageResult " + pageResult.getOPERATOR_ID() + "@@@@ " + pageResult.getPageResult() + "//" + pageResult.getResultList().toString());
		}
		return pageResult;
	}
	
	public PageResult getCertWaitLog(SearchForm search, LoginSession session) {
		Map param = search.toMap();
		param.put("OPERATOR_ID",session.getOPERATOR_ID());

		if(param.get("date").equals(" ")){
			param.put("date", getDate());
		}else
		{
			param.put("date", search.getSdate() + "^" + search.getEdate());
		}
		PageResult pageResult = new PageResult();
		pageResult.setCurrentPage(search.getCurrentPage());
		pageResult.setPageSize(search.getPageSize());
		param.put("MAKER_NAME", session.getMAKER_NAME());
		
		try {
			param = raOpp.oppRequest(OppProtocol.CERT_PROCESS_REQUEST_WAIT, param);
		} catch (RaOppConnectionException e) {
		 
			e.printStackTrace();
		}
		
		if(param.get("result").toString().toLowerCase().equals("ok")){
			pageResult.setTotalCount(param.get("TOTAL"));
			pageResult.setResultList((List)param.get("list"));
			pageResult.setOPERATOR_ID(session.getOPERATOR_ID());
			System.out.println("@@@@@@@@@@@@@@@@@@@ pageResult " + pageResult.getOPERATOR_ID() + "@@@@ " + pageResult.getPageResult() + "//" + pageResult.getResultList().toString());
		}
		return pageResult;
	}
	
	public PageResult getCertFailLog(SearchForm search, LoginSession session) {
		Map param = search.toMap();
		param.put("OPERATOR_ID",session.getOPERATOR_ID());

		if(param.get("date").equals(" ")){
			param.put("date", getDate());
		}else
		{
			param.put("date", search.getSdate() + "^" + search.getEdate());
		}
		PageResult pageResult = new PageResult();
		pageResult.setCurrentPage(search.getCurrentPage());
		pageResult.setPageSize(search.getPageSize());
		param.put("MAKER_NAME", session.getMAKER_NAME());
		
		
		try {
			param = raOpp.oppRequest(OppProtocol.CERT_PROCESS_REQUEST_FAIL, param);
		} catch (RaOppConnectionException e) {
		 
			e.printStackTrace();
		}
		
		if(param.get("result").toString().toLowerCase().equals("ok")){
			pageResult.setTotalCount(param.get("TOTAL"));
			pageResult.setResultList((List)param.get("list"));
			pageResult.setOPERATOR_ID(session.getOPERATOR_ID());
			System.out.println("@@@@@@@@@@@@@@@@@@@ pageResult " + pageResult.getOPERATOR_ID() + "@@@@ " + pageResult.getPageResult() + "//" + pageResult.getResultList().toString());
		}
		return pageResult;
	}
	
	
	public Map getDeviceList(HttpServletRequest request, LoginSession session, PageResult pageResult)
			throws CertServiceException {

		Map param = new HashMap();
		Map pageParam = pageResult.getSearchParam();
		try {
	
			param.put("OPERATOR_ID", session.getOPERATOR_ID());
			param.put("currentPage", 1);
			param.put("pageSize", 10);
			
			if(pageResult.getMakerName().equals(""))
			{
				param.put("makerName", session.getMAKER_NAME());
			}else{
				param.put("makerName", pageResult.getMakerName());
			}
			
			
			System.out.println("@@@@@@@@@@@@@@@@@@ param @@@@@@@@@@@" + param.toString());
			
			param = raOpp.oppRequest(OppProtocol.DEVICE_b5, param);

			
		} catch (RaOppConnectionException e) {
			e.printStackTrace();
			//throw new AudicServiceException(e.getMessage());
		}
		return param;
	}
	
	private String getMonth() {
	Calendar c = Calendar.getInstance();
		
		String addeddate = mon.format(c.getTime()); 
		c.add(Calendar.MONTH, -10);
		
		return(day.format(c.getTime())+"^"+addeddate); 
	}

	private String getDate(){
		 
		Calendar c = Calendar.getInstance();
		
		String addeddate = day.format(c.getTime()); 
		c.add(Calendar.DATE, -9);
		
		return(day.format(c.getTime())+"^"+addeddate); 
	}

}
