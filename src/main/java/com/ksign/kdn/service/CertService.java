package com.ksign.kdn.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ksign.kdn.client.OppProtocol;
import com.ksign.kdn.client.RaOppConnection;
import com.ksign.kdn.client.RaOppConnection.RaOppConnectionException;
import com.ksign.kdn.domain.CertReq;
import com.ksign.kdn.domain.Device;
import com.ksign.kdn.domain.Firmware;
import com.ksign.kdn.domain.FirmwareHash;
import com.ksign.kdn.domain.LoginSession;
import com.ksign.kdn.domain.Mcu;
import com.ksign.kdn.domain.PageResult;
import com.ksign.kdn.domain.Police;
import com.ksign.kdn.domain.Policy;
import com.ksign.kdn.domain.WebUtil;
import com.ksign.kdn.form.CertSearchForm;
import com.ksign.kdn.util.ResultMessage;

@Service
public class CertService {
	Logger logger = Logger.getLogger(AdminService.class);
	@Autowired
	RaOppConnection raOpp;

	public String getPolicy() throws JsonGenerationException,
			JsonMappingException, IOException {
		ObjectMapper objectMapper = new ObjectMapper();

		Map param = new HashMap();
		param.put("empty", "");
		/*
		 * try { //param =raOpp.oppRequest(OppProtocol.CERT_POLICY, param);
		 * 
		 * } catch (RaOppConnectionException e) { // TODO Auto-generated catch
		 * block e.printStackTrace(); }
		 */
		String parmas = objectMapper.writeValueAsString(param);
		logger.debug("getPolicy :" + parmas);
		return parmas;
	}

	public PageResult certStatus(CertSearchForm form) {
		Map param = form.toMap();
		PageResult pageResult = new PageResult();
		pageResult.setCurrentPage(form.getCurrentPage());
		pageResult.setPageSize(form.getPageSize());
		;
		try {
			param = raOpp.oppRequest(OppProtocol.CONDITION_BY_SEARCH_4b, param);

			if (param.get("result").toString().toLowerCase().equals("ok")) {

				pageResult.setTotalCount(param.get("TOTAL"));
				pageResult.setResultList((List) param.get("list"));
			}

		} catch (RaOppConnectionException e) {
			e.printStackTrace();
		}
		return pageResult;
	}

	public String saveForm(CertReq certReq, String key) {

		String[] list = certReq.getIssueInfo();
		int ss = 0;
		int fail = 0;
		Map req = new HashMap();
		req.put("MAKER_NAME", certReq.getMAKER_NAME());
		req.put("OPERATOR_ID", certReq.getOPERATOR_ID());
		
		if(list == null)
		{
			System.out.println("@@@ its nullllll");
			
			return "잘못된 요청.";
		}
		
		try {
			for (String info : list) {
				try {

					req.put("keyinfo", info);
					System.out.println("@@@@@@@@@@@@@PPPP@@@@@@@@@@@ " + req.toString());
					Map param = new HashMap();
					if(key == "issued")
					{
						param = raOpp.oppRequest(OppProtocol.DEVICE_CERT_ISSUE, req);
					}else if(key == "reissue"){
						param = raOpp.oppRequest(OppProtocol.DEVICE_CERT_REISSUE, req);
					}else if(key == "update"){
						param = raOpp.oppRequest(OppProtocol.DEVICE_CERT_UPDATE, req);
					}
					
					
					if (param.get("result") != null
							&& param.get("result").equals("ok")) {
						ss++;
					} else {
						fail++;
					}
				} catch (Exception e) {
					e.printStackTrace();
					fail++;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return "성공[" + ss + "] 실패[" + fail + "] 신청 되었씁니다.";
	}

	/**
	 * 기기 정보 list
	 * @param pageResult
	 * @return
	 * @throws CertServiceException
	 */
	public Object getDeviceModelList(PageResult pageResult) throws CertServiceException {
		// TODO Auto-generated method stub
		
		Map param = pageResult.getSearchParam();
		try {
			param = raOpp.oppRequest(OppProtocol.DEVICE_MODEL, param);
			
		} catch (RaOppConnectionException e) {
			e.printStackTrace();
			throw new CertServiceException(e.getMessage());
		}
		
		if(param.get("result").toString().toLowerCase().equals("ok")){
			pageResult.setTotalCount(param.get("TOTAL"));
			pageResult.setResultList((List)param.get("list"));
		}
		logger.debug("#### Parma :"+param.toString());
		
		return pageResult;
	}
	
	/**
	 * 기기 상세 정보
	 * @param param
	 * @return
	 * @throws CertServiceException
	 */
	public Device getDeviceModelDetail(Map param) throws CertServiceException {
		// TODO Auto-generated method stub
		Device device = new Device();
		try {
			param = raOpp.oppRequest(OppProtocol.DEVICE_MODEL_DETAIL, param);
			Object obj = param.get("list");
			if(obj instanceof List){
				List list = (List)obj;
				if(list.size() > 0){
					device.setDevic((Map)list.get(0));
					return device;
				}
			}
			throw new CertServiceException("기기 정보를 찾을 수 없습니다.");
		} catch (RaOppConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new CertServiceException("기기 정보를 찾을 수 없습니다.");
		}
	}
	
	/**
	 * 기기정보 등록/수정
	 * @param deviceForm
	 * @param sesion
	 * @param key
	 * @return
	 */
	public ResultMessage deviceModelAdd(Device deviceForm,LoginSession sesion,String key) {
		logger.debug("#############CheckForm :"+deviceForm.toString());
		ResultMessage message = null;
		try{
			deviceForm.setOperatiorId(sesion.getOPERATOR_ID());
			deviceForm.setManName(sesion.getMAKER_NAME());
			Map param =deviceForm.toMap()	;
			
			logger.debug("############# device add param :"+param.toString());
			try {
				if(key.equals("add")){
					param =raOpp.oppRequest(OppProtocol.DEVICE_MODEL_REG, param);
				}else{
					param =raOpp.oppRequest(OppProtocol.DEVICE_MODEL_MOD, param);	
				}
			}catch (RaOppConnectionException e) {
				e.printStackTrace();
				message = ResultMessage.NOKMessage( e.getMessage());
			}
			 
			if(param == null ||param.get("result")== null){
				message = ResultMessage.NOKMessage( "등록된 정보가 없습니다. ");
			}
			
			if(param.get("result").equals("ok") ){
				message = ResultMessage.OKMessage("등록 되었습니다.");
			}else{
				if(param.get("message") != null){
					message = ResultMessage.NOKMessage( (String)param.get("message"));
				}else{
					message = ResultMessage.NOKMessage( "정의되지 않은 오류 발생");
				}
			}
		}catch(Exception e){
			message = ResultMessage.NOKMessage(e.getMessage());
		}
		
		return message;
	}
	/**
	 * 기기 정보 삭제
	 * @param request
	 * @param param
	 * @return
	 */
	public String deviceModelDelete(HttpServletRequest request, Map param) {
		// TODO Auto-generated method stub
		String dModelNo = request.getParameter("dModelNo");
		String fail = "";
		
		param.put("dModelNo", dModelNo);
		try {
			Map result = raOpp.oppRequest(OppProtocol.DEVICE_MODEL_DEL, param);
			
			if(result.get("result").equals("nok")){
				fail = dModelNo;
			}
		} catch (RaOppConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			fail = dModelNo;
		}
		
		return fail;
	}
	
	public Object getDeviceOsList(PageResult pageResult) throws CertServiceException {
		// TODO Auto-generated method stub
		Map param = pageResult.getSearchParam();
		try {
			param = raOpp.oppRequest(OppProtocol.DEVICE_b6, param);
			
		} catch (RaOppConnectionException e) {
			e.printStackTrace();
			throw new CertServiceException(e.getMessage());
		}
		
		if(param.get("result").toString().toLowerCase().equals("ok")){
			pageResult.setTotalCount(param.get("TOTAL"));
			pageResult.setResultList((List)param.get("list"));
		}
		logger.debug("#### Parma :"+param.toString());
		
		return pageResult;
	}
	
	/**
	 * 펌웨어 정보 list
	 * @param pageResult
	 * @return
	 * @throws CertServiceException
	 */
	public Object getDeviceFirmwareList(PageResult pageResult) throws CertServiceException {
		// TODO Auto-generated method stub
		Map param = pageResult.getSearchParam();
		try {
			param = raOpp.oppRequest(OppProtocol.DEVICE_FIRMWARE, param);
			
		} catch (RaOppConnectionException e) {
			e.printStackTrace();
			throw new CertServiceException(e.getMessage());
		}
		
		if(param.get("result").toString().toLowerCase().equals("ok")){
			pageResult.setTotalCount(param.get("TOTAL"));
			pageResult.setResultList((List)param.get("list"));
		}
		logger.debug("#### Parma :"+param.toString());
		
		return pageResult;
	}

	/**
	 * 펌웨어 상세 정보
	 * @param param
	 * @return
	 * @throws CertServiceException
	 */
	public Firmware getDeviceFirmwareDetail(Map param) throws CertServiceException {
		// TODO Auto-generated method stub
		Firmware firmware = new Firmware();
		try {
			param = raOpp.oppRequest(OppProtocol.DEVICE_FIRMWARE_DETAIL, param);
			Object obj = param.get("list");
			if(obj instanceof List){
				List list = (List)obj;
				if(list.size() > 0){
					firmware.setFirmware((Map)list.get(0));
					return firmware;
				}
			}
			throw new CertServiceException("펌웨어 정보를 찾을 수 없습니다.");
		} catch (RaOppConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new CertServiceException("펌웨어 정보를 찾을 수 없습니다.");
		}
	}
	
	/**
	 * 펌웨어정보 등록/수정
	 * @param firmwareForm
	 * @param sesion
	 * @param key
	 * @return
	 */
	public ResultMessage deviceFirmwareAdd(Firmware firmwareForm,LoginSession sesion,String key) {
		logger.debug("#############CheckForm :"+firmwareForm.toString());
		ResultMessage message = null;
		try{
			firmwareForm.setOperatiorId(sesion.getOPERATOR_ID());
			firmwareForm.setManName(sesion.getMAKER_NAME());
			Map param =firmwareForm.toMap()	;
			
			logger.debug("############# device add param :"+param.toString());
			try {
				if(key.equals("add")){
					param =raOpp.oppRequest(OppProtocol.DEVICE_FIRMWARE_REG, param);
				}else{
					param =raOpp.oppRequest(OppProtocol.DEVICE_FIRMWARE_MOD, param);	
				}
			}catch (RaOppConnectionException e) {
				e.printStackTrace();
				message = ResultMessage.NOKMessage( e.getMessage());
			}
			 
			if(param == null ||param.get("result")== null){
				message = ResultMessage.NOKMessage( "등록된 정보가 없습니다. ");
			}
			
			if(param.get("result").equals("ok") ){
				message = ResultMessage.OKMessage("등록 되었습니다.");
			}else{
				if(param.get("message") != null){
					message = ResultMessage.NOKMessage( (String)param.get("message"));
				}else{
					message = ResultMessage.NOKMessage( "정의되지 않은 오류 발생");
				}
			}
		}catch(Exception e){
			message = ResultMessage.NOKMessage(e.getMessage());
		}
		
		return message;
	}
	/**
	 * 펌웨어 정보 삭제
	 * @param request
	 * @param param
	 * @return
	 */
	public String deviceFirmwareDelete(HttpServletRequest request, Map param) {
		// TODO Auto-generated method stub
		String fwNo = request.getParameter("fwNo");
		String fail = "";
		
		param.put("fwNo", fwNo);
		try {
			Map result = raOpp.oppRequest(OppProtocol.DEVICE_FIRMWARE_DEL, param);
			
			if(result.get("result").equals("nok")){
				fail = fwNo;
			}
		} catch (RaOppConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			fail = fwNo;
		}
		
		return fail;
	}
	
	/**
	 * MCU 정보 list
	 * @param pageResult
	 * @return
	 * @throws CertServiceException
	 */
	public Object getDeviceMcuList(PageResult pageResult) throws CertServiceException {
		// TODO Auto-generated method stub
		Map param = pageResult.getSearchParam();
		try {
			param = raOpp.oppRequest(OppProtocol.DEVICE_MCU, param);
			
		} catch (RaOppConnectionException e) {
			e.printStackTrace();
			throw new CertServiceException(e.getMessage());
		}
		
		if(param.get("result").toString().toLowerCase().equals("ok")){
			pageResult.setTotalCount(param.get("TOTAL"));
			pageResult.setResultList((List)param.get("list"));
		}
		logger.debug("#### Parma :"+param.toString());
		
		return pageResult;
	}
	
	/**
	 * MCU 상세 정보
	 * @param param
	 * @return
	 * @throws CertServiceException
	 */
	public Mcu getDeviceMcuDetail(Map param) throws CertServiceException {
		// TODO Auto-generated method stub
		Mcu mcu = new Mcu();
		try {
			param = raOpp.oppRequest(OppProtocol.DEVICE_MCU_DETAIL, param);
			Object obj = param.get("list");
			if(obj instanceof List){
				List list = (List)obj;
				if(list.size() > 0){
					mcu.setMcu((Map)list.get(0));
					return mcu;
				}
			}
			throw new CertServiceException("MCU 정보를 찾을 수 없습니다.");
		} catch (RaOppConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new CertServiceException("MCU 정보를 찾을 수 없습니다.");
		}
	}
	
	/**
	 * MCU정보 등록/수정
	 * @param mcuForm
	 * @param sesion
	 * @param key
	 * @return
	 */
	public ResultMessage deviceMcuAdd(Mcu mcuForm,LoginSession sesion,String key) {
		logger.debug("#############CheckForm :"+mcuForm.toString());
		ResultMessage message = null;
		try{
			mcuForm.setOperatiorId(sesion.getOPERATOR_ID());
			mcuForm.setManName(sesion.getMAKER_NAME());
			Map param =mcuForm.toMap()	;
			
			logger.debug("############# device add param :"+param.toString());
			try {
				if(key.equals("add")){
					param =raOpp.oppRequest(OppProtocol.DEVICE_MCU_REG, param);
				}else{
					param =raOpp.oppRequest(OppProtocol.DEVICE_MCU_MOD, param);	
				}
			}catch (RaOppConnectionException e) {
				e.printStackTrace();
				message = ResultMessage.NOKMessage( e.getMessage());
			}
			 
			if(param == null ||param.get("result")== null){
				message = ResultMessage.NOKMessage( "등록된 정보가 없습니다. ");
			}
			
			if(param.get("result").equals("ok") ){
				message = ResultMessage.OKMessage("등록 되었습니다.");
			}else{
				if(param.get("message") != null){
					message = ResultMessage.NOKMessage( (String)param.get("message"));
				}else{
					message = ResultMessage.NOKMessage( "정의되지 않은 오류 발생");
				}
			}
		}catch(Exception e){
			message = ResultMessage.NOKMessage(e.getMessage());
		}
		
		return message;
	}
	/**
	 * MCU 정보 삭제
	 * @param request
	 * @param param
	 * @return
	 */
	public String deviceMcuDelete(HttpServletRequest request, Map param) {
		// TODO Auto-generated method stub
		String mcuNo = request.getParameter("mcuNo");
		String fail = "";
		
		param.put("mcuNo", mcuNo);
		try {
			Map result = raOpp.oppRequest(OppProtocol.DEVICE_MCU_DEL, param);
			
			if(result.get("result").equals("nok")){
				fail = mcuNo;
			}
		} catch (RaOppConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			fail = mcuNo;
		}
		
		return fail;
	}
	
	/**
	 * 기기정책 정보 list
	 * @param pageResult
	 * @return
	 * @throws CertServiceException
	 */
	public Object getDevicePolicyList(PageResult pageResult) throws CertServiceException {
		// TODO Auto-generated method stub
		Map param = pageResult.getSearchParam();
		try {
			param = raOpp.oppRequest(OppProtocol.DEVICE_POLICY, param);
			
		} catch (RaOppConnectionException e) {
			e.printStackTrace();
			throw new CertServiceException(e.getMessage());
		}
		
		if(param.get("result").toString().toLowerCase().equals("ok")){
			pageResult.setTotalCount(param.get("TOTAL"));
			pageResult.setResultList((List)param.get("list"));
		}
		logger.debug("#### Parma :"+param.toString());
		
		return pageResult;
	}
	
	/**
	 * 기기정책 상세 정보
	 * @param param
	 * @return
	 * @throws CertServiceException
	 */
	public Policy getDevicePolicyDetail(Map param) throws CertServiceException {
		// TODO Auto-generated method stub
		Policy policy = new Policy();
		try {
			param = raOpp.oppRequest(OppProtocol.DEVICE_POLICY_DETAIL, param);
			Object obj = param.get("list");
			if(obj instanceof List){
				List list = (List)obj;
				if(list.size() > 0){
					policy.setPolicy((Map)list.get(0));
					return policy;
				}
			}
			throw new CertServiceException("기기정책 정보를 찾을 수 없습니다.");
		} catch (RaOppConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new CertServiceException("기기정책 정보를 찾을 수 없습니다.");
		}
	}
	
	/**
	 * 기기정책정보 등록/수정
	 * @param policyForm
	 * @param sesion
	 * @param key
	 * @return
	 */
	public ResultMessage devicePolicyAdd(Policy policyForm,LoginSession sesion,String key) {
		logger.debug("#############CheckForm :"+policyForm.toString());
		ResultMessage message = null;
		try{
			policyForm.setOperatiorId(sesion.getOPERATOR_ID());
			policyForm.setManName(sesion.getMAKER_NAME());
			Map param =policyForm.toMap()	;
			
			logger.debug("############# devicePolicy add param :"+param.toString());
			try {
				if(key.equals("add")){
					param =raOpp.oppRequest(OppProtocol.DEVICE_POLICY_REG, param);
				}else{
					param =raOpp.oppRequest(OppProtocol.DEVICE_POLICY_MOD, param);	
				}
			}catch (RaOppConnectionException e) {
				e.printStackTrace();
				message = ResultMessage.NOKMessage( e.getMessage());
			}
			 
			if(param == null ||param.get("result")== null){
				message = ResultMessage.NOKMessage( "등록된 정보가 없습니다. ");
			}
			
			if(param.get("result").equals("ok") ){
				message = ResultMessage.OKMessage("등록 되었습니다.");
			}else{
				if(param.get("message") != null){
					message = ResultMessage.NOKMessage( (String)param.get("message"));
				}else{
					message = ResultMessage.NOKMessage( "정의되지 않은 오류 발생");
				}
			}
		}catch(Exception e){
			message = ResultMessage.NOKMessage(e.getMessage());
		}
		
		return message;
	}
	/**
	 * 기기정책 정보 삭제
	 * @param request
	 * @param param
	 * @return
	 */
	public String devicePolicyDelete(HttpServletRequest request, Map param) {
		// TODO Auto-generated method stub
		String dSerialNo = request.getParameter("dSerialNo");
		String fail = "";
		
		param.put("dSerialNo", dSerialNo);
		try {
			Map result = raOpp.oppRequest(OppProtocol.DEVICE_POLICY_DEL, param);
			
			if(result.get("result").equals("nok")){
				fail = dSerialNo;
			}
		} catch (RaOppConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			fail = dSerialNo;
		}
		
		return fail;
	}

	
	/**
	 * 펌웨어 HASH 정보 LIST
	 * @param pageResult
	 * @return
	 * @throws CertServiceException
	 */
	public Object getFirmwareHashList(PageResult pageResult) throws CertServiceException {
		// TODO Auto-generated method stub
		Map param = pageResult.getSearchParam();
		try {
			param = raOpp.oppRequest(OppProtocol.FIRMWARE_HASH, param);
			
		} catch (RaOppConnectionException e) {
			e.printStackTrace();
			throw new CertServiceException(e.getMessage());
		}
		
		if(param.get("result").toString().toLowerCase().equals("ok")){
			pageResult.setTotalCount(param.get("TOTAL"));
			pageResult.setResultList((List)param.get("list"));
		}
		logger.debug("#### Parma :"+param.toString());
		
		return pageResult;
	}
	
	/**
	 * 펌웨어 HASH 상세정보
	 * @param param
	 * @return
	 * @throws CertServiceException
	 */
	public FirmwareHash getFirmwareHashDetail(Map param) throws CertServiceException {
		// TODO Auto-generated method stub
		FirmwareHash firmwareHash = new FirmwareHash();

		try {
			param = raOpp.oppRequest(OppProtocol.FIRMWARE_HASH_DETAIL, param);
			Object obj = param.get("list");
			if(obj instanceof List){
				List list = (List)obj;
				if(list.size() > 0){
					firmwareHash.setFirmwareHash((Map)list.get(0));
					return firmwareHash;
				}
			}
			throw new CertServiceException("펌웨어 Hash 정보를 찾을 수 없습니다.");
		} catch (RaOppConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new CertServiceException("펌웨어 Hash 정보를 찾을 수 없습니다.");
		}
	}
	
	/**
	 * 펌웨어 Hash 정보 등록/수정
	 * @param firmwareHashForm
	 * @param sesion
	 * @param key
	 * @return
	 */
	public ResultMessage firmwareHashAdd(FirmwareHash firmwareHashForm,LoginSession sesion,String key) {
		logger.debug("#############CheckForm :"+firmwareHashForm.toString());
		ResultMessage message = null;
		try{
			firmwareHashForm.setOperatiorId(sesion.getOPERATOR_ID());
			firmwareHashForm.setManName(sesion.getMAKER_NAME());
			Map param =firmwareHashForm.toMap()	;
			
			logger.debug("############# devicePolicy add param :"+param.toString());
			try {
				if(key.equals("add")){
					param =raOpp.oppRequest(OppProtocol.FIRMWARE_HASH_REG, param);
				}else{
					param =raOpp.oppRequest(OppProtocol.FIRMWARE_HASH_MOD, param);	
				}
			}catch (RaOppConnectionException e) {
				e.printStackTrace();
				message = ResultMessage.NOKMessage( e.getMessage());
			}
			 
			if(param == null ||param.get("result")== null){
				message = ResultMessage.NOKMessage( "등록된 정보가 없습니다. ");
			}
			
			if(param.get("result").equals("ok") ){
				message = ResultMessage.OKMessage("등록 되었습니다.");
			}else{
				if(param.get("message") != null){
					message = ResultMessage.NOKMessage( (String)param.get("message"));
				}else{
					message = ResultMessage.NOKMessage( "정의되지 않은 오류 발생");
				}
			}
		}catch(Exception e){
			message = ResultMessage.NOKMessage(e.getMessage());
		}
		
		return message;
	}
	
	/**
	 * 펌웨어 Hash 정보 삭제
	 * @param request
	 * @param param
	 * @return
	 */
	public String firmwareHashDelete(HttpServletRequest request, Map param) {
		// TODO Auto-generated method stub
		String fwHashNo = request.getParameter("fwHashNo");
		String fail = "";
		
		param.put("fwHashNo", fwHashNo);
		try {
			Map result = raOpp.oppRequest(OppProtocol.FIRMWARE_HASH_DEL, param);
			
			if(result.get("result").equals("nok")){
				fail = fwHashNo;
			}
		} catch (RaOppConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			fail = fwHashNo;
		}
		
		return fail;
	}
	
	public Map getDeviceList(HttpServletRequest request, LoginSession session)
			throws CertServiceException {

		Map param = new HashMap();
		try {
			param = getMakerParam(request, param, session);
			param.put("pageSize", 0);
			System.out.println("@@@@@@@@@@@@@@@@@@ param @@@@@@@@@@@" + param.toString());
			param = raOpp.oppRequest(OppProtocol.DEVICE_b5, param);
			
		} catch (RaOppConnectionException e) {
			e.printStackTrace();
			throw new CertServiceException(e.getMessage());
		}
		return param;
	}

	private Map getMakerParam(HttpServletRequest request, Map param,
			LoginSession session) throws RaOppConnectionException,
			CertServiceException {

		param.put("OPERATOR_ID", session.getOPERATOR_ID());
		param.put("currentPage", 1);
		param.put("pageSize", 10);
		param.put("searchKeyword", session.getOPERATOR_ID());
		param.put("makerName", session.getMAKER_NAME());

		return param;
	}

	private Object getMakerName(Map param) throws CertServiceException {
		try {
			Object obj = param.get("list");

			if (obj != null) {
				List list = (List) obj;
				Map map = (Map) list.get(0);
				return map.get("MAKER_NAME");
			}
		} catch (Exception e) {
			throw new CertServiceException("제조사만 사용 가능한 메뉴 입니다");
		}
		throw new CertServiceException("제조사만 사용 가능한 메뉴 입니다");
	}

	public class CertServiceException extends Exception {
		CertServiceException(String message) {
			super(message);
		}
	}

	public Map serachPoliceData(Police police, HttpServletRequest request,
			LoginSession session) throws CertServiceException {
		Map param = police.toParameter();
		try {
			getMakerParam(request, param, session);
			param.put("pageSize", 0);
			System.out.println("@@@@@@@@@@@@@@@getMakerParam @@@@@@@@@@@@@@@@@" + param.toString() + "@@@@@@@" + param.get("companyName"));
		} catch (RaOppConnectionException e1) {

			e1.printStackTrace();
			throw new CertServiceException(e1.getMessage());
		}
		try {
		//	param.put("makerName", "제조사1");
			param = raOpp.oppRequest(getOppProtocol(police), param);
			System.out.println("@@@@@@@@@@@@@@@paramparamparam @@@@@@@@@@@@@@@@@" +param.toString());
		} catch (RaOppConnectionException e) {
			e.printStackTrace();
			throw new CertServiceException(e.getMessage());
		}
		return param;
	}

	private OppProtocol getOppProtocol(Police police)
			throws CertServiceException {

		System.out.println("@@@@@@@@@@@@@@@ @@@@@@@@@@@@@@@@@  " + police.getName());
		if (police.getOper().equals("list")) {
			if (police.getName().equals("device")) {
				return OppProtocol.DEVICE_b5;
			} else if (police.getName().equals("firmware")) {
				return OppProtocol.DEVICE_b6;
			} else if (police.getName().equals("mcu")) {
				return OppProtocol.DEVICE_b7;
			}
		} else if (police.getOper().equals("del")) {
			if (police.getName().equals("device")) {
				return OppProtocol.DEVICE_91;
			} else if (police.getName().equals("firmware")) {
				return OppProtocol.FIRMWARE_94;
			} else if (police.getName().equals("chip")) {
				return OppProtocol.CHIP_97;
			} else if (police.getName().equals("chipversion")) {
				return OppProtocol.CHIP_9a;
			}
		} else if (police.getOper().equals("add")) {
			if (police.getName().equals("device")) {
				return OppProtocol.DEVICE_90;
			} else if (police.getName().equals("firmware")) {
				return OppProtocol.FIRMWARE_93;
			} else if (police.getName().equals("chip")) {
				return OppProtocol.CHIP_96;
			} else if (police.getName().equals("chipversion")) {
				return OppProtocol.CHIP_99;
			}
		}

		throw new CertServiceException("지원하지 않는 종류 입니다. [" + police.getOper()
				+ "][" + police.getName() + "]");

	}

	public Object getMakerList(HttpServletRequest request, LoginSession session) {
		PageResult pr = WebUtil.getPageResult(request, session);
		Map param = pr.getSearchParam();
		try {
			param = raOpp.oppRequest(OppProtocol.MAKER_LIST, param);
		} catch (RaOppConnectionException e) {

			e.printStackTrace();
		}
		return param;
	}

	public PageResult certSerailSearch(PageResult result, LoginSession session) {

		Map param = result.getSearchParam();

		try {
			param = raOpp.oppRequest(OppProtocol.CERT_DN_SEARCH, param);

			if (param.get("result").toString().toLowerCase().equals("ok")) {

				result.setTotalCount(param.get("TOTAL"));
				result.setResultList((List) param.get("list"));
			}
		} catch (RaOppConnectionException e) {

			e.printStackTrace();
		}
		return result;
	}

	public PageResult conditionList(CertSearchForm form) {
		Map param = form.toMap();
		PageResult pageResult = new PageResult();
		pageResult.setCurrentPage(form.getCurrentPage());
		pageResult.setPageSize(form.getPageSize());
		;
		try {
			param = raOpp.oppRequest(OppProtocol.CONDITION_BY_SEARCH_4b,
					param);
			
			if (param.get("result").toString().toLowerCase().equals("ok")) {
				
				pageResult.setTotalCount(param.get("TOTAL"));
				pageResult.setResultList((List) param.get("list"));
			}
			
		} catch (RaOppConnectionException e) {
			e.printStackTrace();
		}
		return pageResult;
	}
	
	public PageResult certIssueList(CertSearchForm form) {
		Map param = form.toMap();
		PageResult pageResult = new PageResult();
		pageResult.setCurrentPage(form.getCurrentPage());
		pageResult.setPageSize(form.getPageSize());
		;
		try {
			param = raOpp.oppRequest(OppProtocol.CERT_SERIAL_NUMBER_SEARCH,
					param);

			if (param.get("result").toString().toLowerCase().equals("ok")) {

				pageResult.setTotalCount(param.get("TOTAL"));
				pageResult.setResultList((List) param.get("list"));
			}

		} catch (RaOppConnectionException e) {
			e.printStackTrace();
		}
		return pageResult;
	}

	public Map getCertificate(List dns, LoginSession session) {
		Map certs = new HashMap();
		Map param = new HashMap();
		List fail = new ArrayList();
		param.put("OPERATOR_ID", session.getOPERATOR_ID());
		if (dns != null && dns.size() > 0) {
			for (Object dn : dns) {
				param.put("dn", dn);
				Map re;
				try {
					re = raOpp.oppRequest(OppProtocol.CERT_REQUEST, param);
					if (re.get("result").toString().toLowerCase().equals("ok")) {
						certs.put(dn, re.get("CERT"));
					} else {
						fail.add(dn);
					}
				} catch (RaOppConnectionException e) {
					fail.add(dn);
				}

			}
			certs.put("failList", fail);
		}
		return certs;
	}
	
	public boolean resultRevocation(List dns, LoginSession session) {
		Map param = new HashMap();
		boolean result = false;
		param.put("OPERATOR_ID", session.getOPERATOR_ID());
		if(dns != null & dns.size() >0) {
			for (Object dn : dns) {
				String[] dnStr = dn.toString().split(",");
				String makerName = dnStr[2].substring(dnStr[2].indexOf("=")+1);
				String serialNo = dnStr[0].substring(dnStr[0].indexOf("=")+1);
				param.put("MAKER_NAME", makerName);
				param.put("serialNo", serialNo);
				Map re;
				try {
					re = raOpp.oppRequest(OppProtocol.DEVICE_CERT, param);
					if (re.get("result").toString().toLowerCase().equals("ok")) {
						result = true;
					}
				} catch (Exception e) {
					// TODO: handle exception
				}
			}
		}
		return result;
	}
	
	public boolean certDelete(List dns, LoginSession session) {
		Map param = new HashMap();
		boolean result = false;
		param.put("OPERATOR_ID", session.getOPERATOR_ID());
		if(dns != null & dns.size() >0) {
			for (Object dn : dns) {
				String[] dnStr = dn.toString().split(",");
				String makerName = dnStr[2].substring(dnStr[2].indexOf("=")+1);
				String serialNo = dnStr[0].substring(dnStr[0].indexOf("=")+1);
				param.put("MAKER_NAME", makerName);
				param.put("serialNo", serialNo);
				Map re;
				try {
					re = raOpp.oppRequest(OppProtocol.DEVICE_DEL, param);
					if (re.get("result").toString().toLowerCase().equals("ok")) {
						result = true;
					}
				} catch (Exception e) {
					// TODO: handle exception
				}
			}
		}
		return result;
	}
	
	//140401 추가
	
}
