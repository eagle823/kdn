package com.ksign.kdn.persistence;

import java.util.List;
import java.util.Map;

import com.ksign.kdn.domain.Audit;

public interface AuditMapper {
  Audit getAudit(String auditID);
	  
  	  void insertAudit(Audit audit);

	  void updateAudit(Audit audit);

	  void deleteAudit(String auditID);

	  int searchAuditCount(Map<String,String> parameter);
	  
	  List<Audit> searchAuditList(Map<String,String> parameter);
}
