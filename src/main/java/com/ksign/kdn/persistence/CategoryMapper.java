package com.ksign.kdn.persistence;

import java.util.List;
import java.util.Map;

import com.ksign.kdn.domain.Category;

public interface CategoryMapper {
	   
  	  Category getCategory(String categoryID);
	  
  	  void insertCategory(Category category);

	  void updateCategory(Category category);

	  void deleteCategory(String categoryID);

	  int searchCategoryCount(Map<String,String> parameter);
	  
	  List<Category> searchCategoryList(Map<String,String> parameter);
}
