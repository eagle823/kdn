package com.ksign.kdn.persistence;

import java.util.List;
import java.util.Map;

import com.ksign.kdn.domain.Production;

public interface ProductionMapper {
	   
  	  Production getProduction(String productionID);
      
  	  Production getProductionName(String product_name);
  	  
  	  void insertProduction(Production production);

	  void updateProduction(Production production);

	  void deleteProduction(String productionID);

	  int searchProductionCount(Map<String,String> parameter);
	  
	  List<Production> searchProductionList(Map<String,String> parameter);

	 
}
