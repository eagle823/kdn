package com.ksign.kdn.persistence;

import java.util.List;
import java.util.Map;

import com.ksign.kdn.domain.Operator;

public interface OperatorMapper {
	   
  	  Operator getOperator(String operatorID);
      
  	  Operator getOperatorName(String name);
  	  
  	  void insertOperator(Operator operator);

	  void updateOperator(Operator operator);

	  void deleteOperator(String operatorID);

	  int searchOperatorCount(Map<String,String> parameter);
	  
	  List<Operator> searchOperatorList(Map<String,String> parameter);

	int searchManagerCount(Map<String, String> searchParam);

	List<?> searchManagerList(Map<String, String> searchParam);
}
