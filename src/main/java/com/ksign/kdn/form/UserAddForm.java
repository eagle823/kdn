package com.ksign.kdn.form;

import java.util.HashMap;
import java.util.Map;

import com.ksign.kdn.util.StringUtil;

public class UserAddForm {
	
	private String userId = "";
	private String companyName = "";  //소속 회사명
	private String userName = "";     //사용자 명
	private String region = "";		///지역
	private String mobile1 = "";		//전화 번호
	private String mobile2 = "";
	private String mobile3 = "";
	private String email = "";		
	private String phone1 = "";
	private String phone2 = "";
	private String phone3 = "";
	private String branch = "";		//부서
	private String corp = "";		//팀
	private String password = "";	
	private String fax1 = "";	
	private String fax2 = "";	
	private String fax3 = "";	
	private String idcheck = "";	
	private String grade = "";	
	private String position = "";	
	private String device = "";	
	private String cert = "";	
	private String auth = "";	
	
/*	{"idcheck":"true",
		"userId":"dd",
		"userName":"심상학", companyName companyName
		"region":"",
		"mobile1":"010",
		"phone1":"02",
		"phone2":"1231",
		"phone3":"1231",
		"corp":"121231",
		"fax1":"123",
		"fax2":"123",
		"fax3":"3123","email":"3123123",
		"grade":"12312","position":"123123","password":"123123","companyName":"11111","device":"t","cert":"t","auth":"t"}
	
	*/
	
	
	public String getCompanyName() {
		return companyName;
	}
	public String getUserName() {
		return userName;
	}
	public String getRegion() {
		return region;
	}
	public String getMobile1() {
		return mobile1;
	}
	public String getMobile2() {
		return mobile2;
	}
	public String getMobile3() {
		return mobile3;
	}
	public String getEmail() {
		return email;
	}
	public String getPhone1() {
		return phone1;
	}
	public String getPhone2() {
		return phone2;
	}
	public String getPhone3() {
		return phone3;
	}
	public String getBranch() {
		return branch;
	}
	public String getCorp() {
		return corp;
	}
	public String getPassword() {
		return password;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public void setRegion(String region) {
		this.region = region;
	}
	public void setMobile1(String mobile1) {
		this.mobile1 = mobile1;
	}
	
	public void setMobile2(String mobile2) {
		this.mobile2 = mobile2;
	}
	
	public void setMobile3(String mobile3) {
		this.mobile3 = mobile3;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	public void setPhone1(String phone1) {
		this.phone1 = phone1;
	}
	
	public void setPhone2(String phone2) {
		this.phone2 = phone2;
	}
	
	public void setPhone3(String phone3) {
		this.phone3 = phone3;
	}
	
	public void setBranch(String branch) {
		this.branch = branch;
	}
	
	public void setCorp(String corp) {
		this.corp = corp;
	}
	
	public void setPassword(String password) {
		this.password = password;
	}
	@Override
	public String toString() {
		return "UserAddForm [companyName=" + companyName + ", userName="
				+ userName + ", region=" + region + ", mobile1=" + mobile1
				+ ", mobile2=" + mobile2 + ", mobile3=" + mobile3 + ", email="
				+ email + ", phone1=" + phone1 + ", phone2=" + phone2
				+ ", phone3=" + phone3 + ", branch=" + branch + ", corp="
				+ corp + ", password=" + password + "]";
	}
	public String getUserId() {
		return userId;
	}
	
	public void setUserId(String userId) {
		this.userId = userId;
	}
	
	public String  getMobile() {
		return mobile1+"-"+mobile2+"-"+mobile3;
	}
	
	public String  getPhone() {
		return phone1+"-"+phone2+"-"+phone3;
	}
	
	public Map toMap(){
		
		/*"userId
userName
region
mobile
email
phone
branch
fax
corp
grade
position
companyName
password
device
cert
auth	
		*/
		Map  param = new HashMap();
		param.put("userId",userId);		
		param.put("userName",userName);		
		param.put("region",region);	
		param.put("mobile",StringUtil.toCheckString(mobile1, "")+"-"+StringUtil.toCheckString(mobile2, "")+"-"+StringUtil.toCheckString(mobile3, "") );		
		param.put("email",email);		
		param.put("phone",StringUtil.toCheckString(phone1, "")+"-"+StringUtil.toCheckString(phone2, "")+"-"+StringUtil.toCheckString(phone3, ""));
		param.put("branch",StringUtil.toCheckString(branch, " "));		
		param.put("fax",StringUtil.toCheckString(fax1, "")+"-"+StringUtil.toCheckString(fax2, "")+"-"+StringUtil.toCheckString(fax3, ""));
		param.put("corp",StringUtil.toCheckString(corp, " "));		
		param.put("grade",StringUtil.toCheckString(grade, " "));		
		param.put("position",StringUtil.toCheckString(position, " "));		
		param.put("companyName",companyName );		
		param.put("password",password);		
		param.put("device",device);		
		param.put("cert",cert);		
		param.put("auth",auth);	
		return param;
		
		
		
	}
	public String getFax1() {
		return fax1;
	}
	public String getFax2() {
		return fax2;
	}
	public String getFax3() {
		return fax3;
	}
	public String getIdcheck() {
		return idcheck;
	}
	public String getGrade() {
		return grade;
	}
	public String getPosition() {
		return position;
	}
	public String getDevice() {
		return device;
	}
	public String getCert() {
		return cert;
	}
	public String getAuth() {
		return auth;
	}
	public void setFax1(String fax1) {
		this.fax1 = fax1;
	}
	public void setFax2(String fax2) {
		this.fax2 = fax2;
	}
	public void setFax3(String fax3) {
		this.fax3 = fax3;
	}
	public void setIdcheck(String idcheck) {
		this.idcheck = idcheck;
	}
	public void setGrade(String grade) {
		this.grade = grade;
	}
	public void setPosition(String position) {
		this.position = position;
	}
	public void setDevice(String device) {
		this.device = device;
	}
	public void setCert(String cert) {
		this.cert = cert;
	}
	public void setAuth(String auth) {
		this.auth = auth;
	}
}
