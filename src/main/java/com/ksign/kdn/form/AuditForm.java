package com.ksign.kdn.form;

import java.util.HashMap;
import java.util.Map;

import com.ksign.kdn.util.StringUtil;

public class AuditForm {
	private String adminid = ""; 
	private String type  = "";
	private String action  = "";
	private String sdate = "";
	private String edate  = "";
	private String currentPage = "1";
	private String pageSize ="10";
	
	
	
	public String getAdminid() {
		return adminid;
	}
	public String getType() {
		return type;
	}
	public String getAction() {
		return action;
	}
	public String getSdate() {
		return sdate;
	}
	public String getEdate() {
		return edate;
	}
	public void setAdminid(String adminid) {
		this.adminid = adminid;
	}
	public void setType(String type) {
		this.type = type;
	}
	public void setAction(String action) {
		this.action = action;
	}
	public void setSdate(String sdate) {
		this.sdate = sdate;
	}
	public void setEdate(String edate) {
		this.edate = edate;
	}
	
	public String getCurrentPage() {
		return currentPage;
	}
	public String getPageSize() {
		return pageSize;
	}
	public void setCurrentPage(String currentPage) {
		this.currentPage = currentPage;
	}
	public void setPageSize(String pageSize) {
		this.pageSize = pageSize;
	}
	
	public Map toMap(){
		Map param = new HashMap();
		param.put("currentPage", currentPage);
		param.put("pageSize", pageSize);
		param.put("adminID", StringUtil.toCheckString(adminid," "));
		param.put("type", StringUtil.toCheckString(type," "));
		param.put("action", StringUtil.toCheckString(action," "));
		String date = (sdate != null && sdate.length()>1&&edate != null && edate.length()>1)? sdate+"^"+edate:" ";
		param.put("date", date);
		 
		return param;
		
		/*PERATOR_ID
		$currentPage
		$pageSize
		$adminID
		$type
		$action
		$date*/
		
	}
	
	
}
