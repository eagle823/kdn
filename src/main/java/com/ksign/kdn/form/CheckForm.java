package com.ksign.kdn.form;

public class CheckForm {
	
	
	@Override
	public String toString() {
		return "CheckForm [checkName=" + checkName + ", checkValue="
				+ checkValue + "]";
	}
	private String checkName;
	private String checkValue;
	
	private String operatorId;
	
	public String getCheckName() {
		return checkName;
	}
	public void setCheckName(String checkName) {
		this.checkName = checkName;
	}
	public String getCheckValue() {
		return checkValue;
	}
	public void setCheckValue(String checkValue) {
		this.checkValue = checkValue;
	}
	public String getOperatorId() {
		return operatorId;
	}
	public void setOperatorId(String operatorId) {
		this.operatorId = operatorId;
	}
	
}
