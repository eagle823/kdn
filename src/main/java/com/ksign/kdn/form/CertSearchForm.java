package com.ksign.kdn.form;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.ksign.kdn.domain.LoginSession;
import com.ksign.kdn.domain.WebUtil;
import com.ksign.kdn.util.StringUtil;


public class CertSearchForm implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String OPERATOR_ID = "" ;
	private String currentPage = "" ;
	private String pageSize = "" ;
	private String MAKER_NAME = "" ;
	private String serialNo = "" ;
	private String device = "" ;
	private String firmware = "" ;
	private String chip = "" ;
	private String mcu = "" ;
	private String chipversion = "" ;
	private String stats = "T" ;
	private String date = "" ;
	private String sdate = "" ;
	private String edate = "" ;
	private String adminType ="";
	private String issuestate = "";
	private List makerList;

	public CertSearchForm(HttpServletRequest request,LoginSession session) {
		 
		sdate =	 WebUtil.getRequestString(request,"sdate","");
		edate =	 WebUtil.getRequestString(request,"edate","");
		
		if(!sdate.equals("")&&!edate.equals("")){
			date =sdate +"^"+edate;
		}
		stats =   WebUtil.getRequestString(request,"stats","T");
		device =	 WebUtil.getRequestString(request,"devicesName","");
		firmware =	 WebUtil.getRequestString(request,"firmwaresName","");
		mcu =	 WebUtil.getRequestString(request,"mcusName","");
		serialNo =	 WebUtil.getRequestString(request,"serialNo","");
		issuestate =	 WebUtil.getRequestString(request,"issuestate","");
		
		 
		currentPage =	 WebUtil.getRequestString(request,"currentPage","1");
		 pageSize = WebUtil.getRequestString(request,"pageSize","10");
		 OPERATOR_ID = session.getOPERATOR_ID();
		 adminType = session.getTYPE();
		 if(adminType== null ||adminType.equals("lra")){
			 MAKER_NAME = WebUtil.getRequestString(request,"companyName",""); 
		 }else{
		   MAKER_NAME =	session.getMAKER_NAME();
		 }
	}
	public String getOPERATOR_ID() {
		return OPERATOR_ID;
	}
	public String getCurrentPage() {
		return currentPage;
	}
	public String getPageSize() {
		return pageSize;
	}
	public String getMAKER_NAME() {
		return MAKER_NAME;
	}
	public String getSerialNo() {
		return serialNo;
	}
	public String getDevice() {
		return device;
	}
	public String getFirmware() {
		return firmware;
	}
	public String getChip() {
		return chip;
	}
	public String getChipversion() {
		return chipversion;
	}
	public String getStats() {
		return stats;
	}
	public String getDate() {
		return date;
	}
	public String getSdate() {
		return sdate;
	}
	public String getEdate() {
		return edate;
	}
	public void setOPERATOR_ID(String oPERATOR_ID) {
		OPERATOR_ID = oPERATOR_ID;
	}
	public void setCurrentPage(String currentPage) {
		this.currentPage = currentPage;
	}
	public void setPageSize(String pageSize) {
		this.pageSize = pageSize;
	}
	public void setMAKER_NAME(String mAKER_NAME) {
		MAKER_NAME = mAKER_NAME;
	}
	public void setSerialNo(String serialNo) {
		this.serialNo = serialNo;
	}
	public void setDevice(String device) {
		this.device = device;
	}
	public void setFirmware(String firmware) {
		this.firmware = firmware;
	}
	public void setChip(String chip) {
		this.chip = chip;
	}
	public void setChipversion(String chipversion) {
		this.chipversion = chipversion;
	}
	public void setStats(String stats) {
		this.stats = stats;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public void setSdate(String sdate) {
		this.sdate = sdate;
	}
	public void setEdate(String edate) {
		this.edate = edate;
	}
	public Map toMap() {
		Map  param= new HashMap();
		param.put("OPERATOR_ID",  OPERATOR_ID );
		param.put("currentPage", currentPage );
		param.put("pageSize",  pageSize);
		param.put("MAKER_NAME",	StringUtil.toCheckString( MAKER_NAME ," ")  );
		param.put("serialNo",  StringUtil.toCheckString( serialNo ," " ));
		param.put("device",    StringUtil.toCheckString( device ," " ));
		param.put("firmware",    StringUtil.toCheckString( firmware ," " )   );
		param.put("mcu",    StringUtil.toCheckString( mcu ," " )   );
		param.put("chip",   StringUtil.toCheckString( chip ," " ) );
		param.put("chipversion",   StringUtil.toCheckString( chipversion ," " ) );
		param.put("stats",   StringUtil.toCheckString( stats ," " )  );
		 if(edate != null &&edate.length()>2 && sdate != null && sdate.length()>2){
			param.put("date", sdate+"^"+edate);
		}else{
			param.put("date"," ");
		}
	 

		return param;
	}
	@Override
	public String toString() {
		return "CertSearchForm [OPERATOR_ID=" + OPERATOR_ID + ", currentPage="
				+ currentPage + ", pageSize=" + pageSize + ", MAKER_NAME="
				+ MAKER_NAME + ", serialNo=" + serialNo + ", device=" + device
				+ ", firmware=" + firmware + ", chip=" + chip + ", mcu=" + mcu
				+ ", chipversion=" + chipversion + ", stats=" + stats
				+ ", date=" + date + ", sdate=" + sdate + ", edate=" + edate
				+ "]";
	}
	public String getAdminType() {
		return adminType;
	}
	public void setAdminType(String adminType) {
		this.adminType = adminType;
	}
	public List getMakerList() {
		return makerList;
	}
	public void setMakerList(List makerList) {
		this.makerList = makerList;
	}
	public String getMcu() {
		return mcu;
	}
	public void setMcu(String mcu) {
		this.mcu = mcu;
	}
	public String getIssuestate() {
		return issuestate;
	}
	public void setIssuestate(String issuestate) {
		this.issuestate = issuestate;
	}
	
}
