package com.ksign.kdn.form;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.ksign.kdn.domain.LoginSession;
import com.ksign.kdn.util.StringUtil;

public class SearchForm {
	

	
	private String searchKeyword ="";
	private String  currentPage ="1";
	private String  kind ="";
	private String  pageResult ="";
	private String  pageSize ="10";
	private String adminid  =" ";
	private String type		=" ";
	private String action	=" ";
	private String sdate	=" ";
	private String edate	=" ";
	private String device	=" ";
	private List<String> dnList;
	private String operator_ID;
	
	public String getSearchKeyword() {
		return searchKeyword;
	}
	
	@Override
	public String toString() {
		return "SearchForm [searchKeyword=" + searchKeyword + ", currentPage="
				+ currentPage + ", kind=" + kind + ", pageResult=" + pageResult
				+ ", pageSize=" + pageSize + "]";
	}

	
	public Map toMap(){
		Map map  = new HashMap();
		map.put("currentPage", currentPage);
		map.put("kind", kind);
		map.put("searchKeyword", searchKeyword);
		map.put("pageSize", pageSize);

		map.put("type",  StringUtil.toCheckString(type, " ") );
		map.put("action", StringUtil.toCheckString(action, " "));
		map.put("adminid", StringUtil.toCheckString(adminid, " "));
		
		String date =(edate != null &&edate.length()>2 && sdate != null && sdate.length()>2)? (sdate+"^"+edate):" ";
		map.put("date",date);
		 
		map.put("device",StringUtil.toCheckString( device, " "));
		map.put("OPERATOR_ID",StringUtil.toCheckString( operator_ID, " "));
		return map;
	}
	public void setSearchKeyword(String searchKeyword) {
		this.searchKeyword = searchKeyword;
	}
	
	public String getCurrentPage() {
		return currentPage;
	}
	
	public void setCurrentPage(String currentPage) {
		this.currentPage = currentPage;
	}
	
	public String getKind() {
		return kind;
	}
	
	public void setKind(String kind) {
		this.kind = kind;
	}
	
	public String getPageResult() {
		return pageResult;
	}
	
	public void setPageResult(String pageResult) {
		this.pageResult = pageResult;
	}
	
	public String getPageSize() {
		return pageSize;
	}
	
	public void setPageSize(String pageSize) {
		this.pageSize = pageSize;
	}

	public String getAdminid() {
		return adminid;
	}

	public String getType() {
		return type;
	}

	public String getAction() {
		return action;
	}

	public String getSdate() {
		return sdate;
	}

	public String getEdate() {
		return edate;
	}

	public void setAdminid(String adminid) {
		this.adminid = adminid;
	}

	public void setType(String type) {
		this.type = type;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public void setSdate(String sdate) {
		this.sdate = sdate;
	}

	public void setEdate(String edate) {
		this.edate = edate;
	}

	public List<String> getDnList() {
		return dnList;
	}

	public void setDnList(List<String> dnList) {
		this.dnList = dnList;
	}

	public String getDevice() {
		return device;
	}

	public void setDevice(String device) {
		this.device = device;
	}

	public void setOperatorID(String operator_ID) {
		this.operator_ID =operator_ID;
		
	}

	

	 
}
