package com.ksign.kdn.web;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ksign.kdn.domain.CertReq;
import com.ksign.kdn.domain.Device;
import com.ksign.kdn.domain.Firmware;
import com.ksign.kdn.domain.FirmwareHash;
import com.ksign.kdn.domain.LoginSession;
import com.ksign.kdn.domain.Mcu;
import com.ksign.kdn.domain.PageResult;
import com.ksign.kdn.domain.Police;
import com.ksign.kdn.domain.Policy;
import com.ksign.kdn.domain.WebUtil;
import com.ksign.kdn.form.CertSearchForm;
import com.ksign.kdn.form.SearchForm;
import com.ksign.kdn.service.CertService;
import com.ksign.kdn.service.CertService.CertServiceException;
import com.ksign.kdn.service.PopupService;
import com.ksign.kdn.service.PopupService.PopupException;
import com.ksign.kdn.util.ResultMessage;


@Controller
public class CertController extends SessionController {

	SimpleDateFormat day = new SimpleDateFormat("yyyy-MM-dd");
	
	@Autowired
	CertService certService;

	@Autowired
	PopupService popupService;
	
	@RequestMapping("/cert/deviceModel/list")
	public String deviceModelList(
			@ModelAttribute(LoginSession.SESSIONNAME) LoginSession session,
			HttpServletRequest request,
			ModelMap map
			){
		
		try {
			PageResult pageResult = WebUtil.getPageResult(request, session);
			certService.getDeviceModelList(pageResult);

			map.put("pageResult", pageResult);
			map.put("session", session);
		} catch (Exception e) {
			map.put("message", "로그인 후 사용하실 수 있습니다.");
			return "/login/login";
		}

		return "/cert/deviceModel/deviceModelList";
	}

	@RequestMapping("/cert/deviceModel/view")
	public String deviceModelView(
			@RequestParam(value = "dModelNo", defaultValue = "") String dModelNo,
			@ModelAttribute(LoginSession.SESSIONNAME) LoginSession session,
			HttpServletRequest request,
			ModelMap map
			){
		
		Map param = new HashMap();
		param.put("dModelNo", dModelNo);
		param.put("OPERATOR_ID", session.getOPERATOR_ID());
		
		try {
			map.put("device", certService.getDeviceModelDetail(param));
		} catch (Exception e) {
			map.put("message", "상세 정보를 가져오는데 실패 하였습니다.");
			
			return "forward:/cert/deviceModel/list";
		}
		
		return "/cert/deviceModel/deviceModelView";
	}
	
	@RequestMapping("/cert/deviceModel/form/{key}")
	public String deviceModelForm(
			ModelMap map,
			@ModelAttribute(LoginSession.SESSIONNAME) LoginSession session,
			@RequestParam(value = "dModelNo", defaultValue = "") String dModelNo,
			@PathVariable(value = "key") String key, HttpServletRequest request
			){
		
		PageResult pageResult = WebUtil.getPageResult(request, session);
		Map param = pageResult.getSearchParam();
		
		if(key.equals("mod")){
			param.put("dModelNo", dModelNo);
			
			try {
				map.put("device", certService.getDeviceModelDetail(param));
			} catch (Exception e) {
				map.put("message", "상세 정보를 가져오는데 실패 하였습니다.");
				return "redirect:/cert/deviceModel/list";
			}
		}else if(key.equals("del")){
			String fail = certService.deviceModelDelete(request, param);
			
			if (fail.equals("")) {
				map.put("message", "삭제 되었습니다. ");
			} else {
				map.put("message", "다음 기기 삭제에 실패 하였습니다. [" + fail + "]");
			}
			
			return "forward:/cert/deviceModel/list";
		}/*else if(key.equals("add")){
			map.put("deviceType", certService.deviceTypeSelect());
		}*/
		
		
		map.put("key", key);
		return "/cert/deviceModel/deviceModelForm";
	}
	
	@RequestMapping(value = "/cert/deviceModel/save/{key}", method = RequestMethod.POST, headers = "Accept=*/*")
	public @ResponseBody
	ResultMessage deviceModelSave(
			@RequestBody Device deviceForm,
			@ModelAttribute(LoginSession.SESSIONNAME) LoginSession session,
			@PathVariable(value = "key") String key
			){
		
		return certService.deviceModelAdd(deviceForm, session, key);
	}
	
	@RequestMapping("/cert/deviceFirmware/list")
	public String deviceFirmware(
			@ModelAttribute(LoginSession.SESSIONNAME) LoginSession session,
			HttpServletRequest request,
			ModelMap map
			){
		
		try {
			PageResult pageResult = WebUtil.getPageResult(request, session);
			certService.getDeviceFirmwareList(pageResult);

			map.put("pageResult", pageResult);
			map.put("session", session);
		} catch (Exception e) {
			map.put("message", "로그인 후 사용하실 수 있습니다.");
			return "/login/login";
		}
		
		return "/cert/deviceFirmware/deviceFirmwareList";
	}
	
	@RequestMapping("/cert/deviceFirmware/view")
	public String deviceFirmwareView(
			@RequestParam(value = "fwNo", defaultValue = "") String fwNo,
			@ModelAttribute(LoginSession.SESSIONNAME) LoginSession session,
			HttpServletRequest request,
			ModelMap map
			){
		
		Map param = new HashMap();
		param.put("fwNo", fwNo);
		param.put("OPERATOR_ID", session.getOPERATOR_ID());
		
		try {
			map.put("firmware", certService.getDeviceFirmwareDetail(param));
		} catch (Exception e) {
			map.put("message", "상세 정보를 가져오는데 실패 하였습니다.");
			
			return "forward:/cert/deviceFirmware/list";
		}
		
		return "/cert/deviceFirmware/deviceFirmwareView";
	}
	
	@RequestMapping("/cert/deviceFirmware/form/{key}")
	public String deviceFirmwareForm(
			ModelMap map,
			@ModelAttribute(LoginSession.SESSIONNAME) LoginSession session,
			@RequestParam(value = "fwNo", defaultValue = "") String fwNo,
			@PathVariable(value = "key") String key, HttpServletRequest request
			){
		
		PageResult pageResult = WebUtil.getPageResult(request, session);
		Map param = pageResult.getSearchParam();
		if(key.equals("mod")){
			param.put("fwNo", fwNo);
			
			try {
				map.put("firmware", certService.getDeviceFirmwareDetail(param));
			} catch (Exception e) {
				map.put("message", "상세 정보를 가져오는데 실패 하였습니다.");
				return "redirect:/cert/deviceFirmware/list";
			}
		}else if(key.equals("del")){
			String fail = certService.deviceFirmwareDelete(request, param);

			if (fail.equals("")) {
				map.put("message", "삭제 되었습니다. ");
			} else {
				map.put("message", "다음 펌웨어 삭제에 실패 하였습니다. [" + fail + "]");
			}
			
			return "forward:/cert/deviceFirmware/list";
		}else{
			Firmware firmware = new Firmware();
			firmware.setFwApplyDate(getDate());
			map.put("firmware", firmware);
		}
		
		map.put("key", key);
		return "/cert/deviceFirmware/deviceFirmwareForm";
	}
	
	@RequestMapping(value = "/cert/deviceFirmware/save/{key}", method = RequestMethod.POST, headers = "Accept=*/*")
	public @ResponseBody
	ResultMessage deviceFirmwareSave(
			@RequestBody Firmware firmwareForm,
			@ModelAttribute(LoginSession.SESSIONNAME) LoginSession session,
			@PathVariable(value = "key") String key
			){
		
		return certService.deviceFirmwareAdd(firmwareForm, session, key);
	}
	
	@RequestMapping("/cert/deviceMcu/list")
	public String deviceMcu(
			@ModelAttribute(LoginSession.SESSIONNAME) LoginSession session,
			HttpServletRequest request,
			ModelMap map
			){
		
		try {
			PageResult pageResult = WebUtil.getPageResult(request, session);
			certService.getDeviceMcuList(pageResult);

			map.put("pageResult", pageResult);
			map.put("session", session);
		} catch (Exception e) {
			map.put("message", "로그인 후 사용하실 수 있습니다.");
			return "/login/login";
		}
		
		return "/cert/deviceMcu/deviceMcuList";
	}
	

	@RequestMapping("/cert/deviceMcu/view")
	public String deviceMcuView(
			@RequestParam(value = "mcuNo", defaultValue = "") String mcuNo,
			@ModelAttribute(LoginSession.SESSIONNAME) LoginSession session,
			HttpServletRequest request,
			ModelMap map
			){
		
		Map param = new HashMap();
		param.put("mcuNo", mcuNo);
		param.put("OPERATOR_ID", session.getOPERATOR_ID());
		
		try {
			map.put("mcu", certService.getDeviceMcuDetail(param));
		} catch (Exception e) {
			map.put("message", "상세 정보를 가져오는데 실패 하였습니다.");
			
			return "forward:/cert/deviceMcu/list";
		}
		
		return "/cert/deviceMcu/deviceMcuView";
	}
	
	@RequestMapping("/cert/deviceMcu/form/{key}")
	public String deviceMcuForm(
			ModelMap map,
			@ModelAttribute(LoginSession.SESSIONNAME) LoginSession session,
			@RequestParam(value = "mcuNo", defaultValue = "") String mcuNo,
			@PathVariable(value = "key") String key, HttpServletRequest request
			){
		
		PageResult pageResult = WebUtil.getPageResult(request, session);
		Map param = pageResult.getSearchParam();
		if(key.equals("mod")){
			param.put("mcuNo", mcuNo);
			
			try {
				map.put("mcu", certService.getDeviceMcuDetail(param));
			} catch (Exception e) {
				map.put("message", "상세 정보를 가져오는데 실패 하였습니다.");
				return "redirect:/cert/deviceMcu/list";
			}
		}else if(key.equals("del")){
			String fail = certService.deviceMcuDelete(request, param);

			if (fail.equals("")) {
				map.put("message", "삭제 되었습니다. ");
			} else {
				map.put("message", "다음 MCU 삭제에 실패 하였습니다. [" + fail + "]");
			}
			
			return "forward:/cert/deviceMcu/list";
		}
		
		map.put("key", key);
		return "/cert/deviceMcu/deviceMcuForm";
	}
	
	@RequestMapping(value = "/cert/deviceMcu/save/{key}", method = RequestMethod.POST, headers = "Accept=*/*")
	public @ResponseBody
	ResultMessage deviceMcuSave(
			@RequestBody Mcu mcuForm,
			@ModelAttribute(LoginSession.SESSIONNAME) LoginSession session,
			@PathVariable(value = "key") String key
			){
		
		return certService.deviceMcuAdd(mcuForm, session, key);
	}
	
	@RequestMapping("/cert/devicePolicy/list")
	public String devicePolicy(
			@ModelAttribute(LoginSession.SESSIONNAME) LoginSession session,
			HttpServletRequest request,
			ModelMap map
			){
		
		try {
			PageResult pageResult = WebUtil.getPageResult(request, session);
			certService.getDevicePolicyList(pageResult);

			map.put("pageResult", pageResult);
			map.put("session", session);
		} catch (Exception e) {
			map.put("message", "로그인 후 사용하실 수 있습니다.");
			return "/login/login";
		}
		
		return "/cert/devicePolicy/devicePolicyList";
	}
	
	@RequestMapping("/cert/devicePolicy/view")
	public String devicePolicyView(
			@RequestParam(value = "dSerialNo", defaultValue = "") String dSerialNo,
			@ModelAttribute(LoginSession.SESSIONNAME) LoginSession session,
			HttpServletRequest request,
			ModelMap map
			){
		
		Map param = new HashMap();
		param.put("dSerialNo", dSerialNo);
		param.put("OPERATOR_ID", session.getOPERATOR_ID());
		
		try {
			map.put("policy", certService.getDevicePolicyDetail(param));
		} catch (Exception e) {
			map.put("message", "상세 정보를 가져오는데 실패 하였습니다.");
			
			return "forward:/cert/devicePolicy/list";
		}
		
		return "/cert/devicePolicy/devicePolicyView";
	}
	
	// 140401 선점을 위해 먼저 선언
	@RequestMapping("/cert/devicePolicy/form/{key}2")
	public String devicePolicyForm2(
			ModelMap map,
			@ModelAttribute(LoginSession.SESSIONNAME) LoginSession session,
			@RequestParam(value = "dSerialNo", defaultValue = "") String dSerialNo,
			@RequestParam(value = "rmakerName", required=false) String rmakerName,
			@PathVariable(value = "key") String key,
			HttpServletRequest request
			) throws CertServiceException, PopupException{
		
		PageResult pageResult = WebUtil.getPageResult(request, session);
		if(rmakerName != null){
			pageResult.setMakerName(rmakerName);
		}
		
		PageResult pageResult1 = WebUtil.getPageResult(request, session);
		if(rmakerName != null){
			pageResult1.setMakerName(rmakerName);
		}
		
		PageResult pageResult2 = WebUtil.getPageResult(request, session);
		if(rmakerName != null){
			pageResult2.setMakerName(rmakerName);
		}
		
		PageResult pageResult3 = WebUtil.getPageResult(request, session);
		if(rmakerName != null){
			pageResult3.setMakerName(rmakerName);
		}
		
		map.put("companyList", popupService.findCompany(pageResult.getSearchParam()));
		map.put("deviceModelList", certService.getDeviceModelList(pageResult1));
		map.put("deviceFirmwareList", certService.getDeviceFirmwareList(pageResult2));
		map.put("deviceMcuList", certService.getDeviceMcuList(pageResult3));
				
		Map param = pageResult.getSearchParam();
		
		
		if(key.equals("mod")){
			param.put("dSerialNo", dSerialNo);
			
			try {
				map.put("policy", certService.getDevicePolicyDetail(param));
			} catch (Exception e) {
				map.put("message", "상세 정보를 가져오는데 실패 하였습니다.");
				return "redirect:/cert/devicePolicy/list2";
			}
		}else if(key.equals("del")){
			String fail = certService.devicePolicyDelete(request, param);

			if (fail.equals("")) {
				map.put("message", "삭제 되었습니다. ");
			} else {
				map.put("message", "다음 기기정책 삭제에 실패 하였습니다. [" + fail + "]");
			}
			
			return "forward:/cert/devicePolicy/list2";
		}
		
		if(rmakerName == null){
			rmakerName = session.getMAKER_NAME(); 
		}
		
		map.put("key", key);
		map.put("rmakerName", rmakerName);
		return "/cert/devicePolicy/devicePolicyForm2";
	}
	
	@RequestMapping("/cert/devicePolicy/form/{key}")
	public String devicePolicyForm(
			ModelMap map,
			@ModelAttribute(LoginSession.SESSIONNAME) LoginSession session,
			@RequestParam(value = "dSerialNo", defaultValue = "") String dSerialNo,
			@RequestParam(value = "rmakerName", required=false) String rmakerName,
			@PathVariable(value = "key") String key, HttpServletRequest request
			) throws CertServiceException, PopupException{
		
		PageResult pageResult = WebUtil.getPageResult(request, session);
		if(rmakerName != null){
			pageResult.setMakerName(rmakerName);
		}
		
		PageResult pageResult1 = WebUtil.getPageResult(request, session);
		if(rmakerName != null){
			pageResult1.setMakerName(rmakerName);
		}
		
		PageResult pageResult2 = WebUtil.getPageResult(request, session);
		if(rmakerName != null){
			pageResult2.setMakerName(rmakerName);
		}
		
		PageResult pageResult3 = WebUtil.getPageResult(request, session);
		if(rmakerName != null){
			pageResult3.setMakerName(rmakerName);
		}
		
		map.put("companyList", popupService.findCompany(pageResult.getSearchParam()));
		map.put("deviceModelList", certService.getDeviceModelList(pageResult1));
		map.put("deviceFirmwareList", certService.getDeviceFirmwareList(pageResult2));
		map.put("deviceMcuList", certService.getDeviceMcuList(pageResult3));
				
		Map param = pageResult.getSearchParam();
		
		
		if(key.equals("mod")){
			param.put("dSerialNo", dSerialNo);
			
			try {
				map.put("policy", certService.getDevicePolicyDetail(param));
			} catch (Exception e) {
				map.put("message", "상세 정보를 가져오는데 실패 하였습니다.");
				return "redirect:/cert/devicePolicy/list";
			}
		}else if(key.equals("del")){
			String fail = certService.devicePolicyDelete(request, param);

			if (fail.equals("")) {
				map.put("message", "삭제 되었습니다. ");
			} else {
				map.put("message", "다음 기기정책 삭제에 실패 하였습니다. [" + fail + "]");
			}
			
			return "forward:/cert/devicePolicy/list";
		}
		
		if(rmakerName == null){
			rmakerName = session.getMAKER_NAME(); 
		}
		
		map.put("key", key);
		map.put("rmakerName", rmakerName);
		return "/cert/devicePolicy/devicePolicyForm";
	}
	
	@RequestMapping(value = "/cert/devicePolicy/save/{key}", method = RequestMethod.POST, headers = "Accept=*/*")
	public @ResponseBody
	ResultMessage devicePolicySave(
			@RequestBody Policy policyForm,
			@ModelAttribute(LoginSession.SESSIONNAME) LoginSession session,
			@PathVariable(value = "key") String key
			){
		
		return certService.devicePolicyAdd(policyForm, session, key);
	}
	
	@RequestMapping("/cert/firmwareHash/list")
	public String firmwareHash(
			@ModelAttribute(LoginSession.SESSIONNAME) LoginSession session,
			HttpServletRequest request,
			ModelMap map
			){
		
		try {
			PageResult pageResult = WebUtil.getPageResult(request, session);
			certService.getFirmwareHashList(pageResult);

			map.put("pageResult", pageResult);
			map.put("session", session);
		} catch (Exception e) {
			map.put("message", "로그인 후 사용하실 수 있습니다.");
			return "/login/login";
		}
		
		return "/cert/firmwareHash/firmwareHashList";
	}
	
	@RequestMapping("/cert/firmwareHash/view")
	public String firmwareHashView(
			@RequestParam(value = "fwHashNo", defaultValue = "") String fwHashNo,
			@ModelAttribute(LoginSession.SESSIONNAME) LoginSession session,
			HttpServletRequest request,
			ModelMap map
			){
		
		Map param = new HashMap();
		param.put("fwHashNo", fwHashNo);
		param.put("OPERATOR_ID", session.getOPERATOR_ID());
		
		try {
			map.put("firmwareHash", certService.getFirmwareHashDetail(param));
		} catch (Exception e) {
			map.put("message", "상세 정보를 가져오는데 실패 하였습니다.");
			
			return "forward:/cert/firmwareHash/list";
		}
		
		return "/cert/firmwareHash/firmwareHashView";
	}
	
	@RequestMapping("/cert/firmwareHash/form/{key}")
	public String firmwareHashForm(
			ModelMap map,
			@ModelAttribute(LoginSession.SESSIONNAME) LoginSession session,
			@RequestParam(value = "fwHashNo", defaultValue = "") String fwHashNo,
			@PathVariable(value = "key") String key, HttpServletRequest request
			) throws CertServiceException, PopupException{
		
		PageResult pageResult = WebUtil.getPageResult(request, session);
		
		if(!key.equals("del")){
			PageResult pageResult1 = WebUtil.getPageResult(request, session);
			PageResult pageResult2 = WebUtil.getPageResult(request, session);
			
			map.put("firmwareHashList", certService.getFirmwareHashList(pageResult1));
			map.put("deviceFirmwareList", certService.getDeviceFirmwareList(pageResult2));
		}
				
		Map param = pageResult.getSearchParam();
		if(key.equals("mod")){
			param.put("fwHashNo", fwHashNo);
			
			try {
				map.put("firmwareHash", certService.getFirmwareHashDetail(param));
			} catch (Exception e) {
				map.put("message", "상세 정보를 가져오는데 실패 하였습니다.");
				return "redirect:/cert/firmwareHash/list";
			}
		}else if(key.equals("del")){
			String fail = certService.firmwareHashDelete(request, param);

			if (fail.equals("")) {
				map.put("message", "삭제 되었습니다. ");
			} else {
				map.put("message", "다음 기기정책 삭제에 실패 하였습니다. [" + fail + "]");
			}
			
			return "forward:/cert/firmwareHash/list";
		}
		
		map.put("key", key);
		return "/cert/firmwareHash/firmwareHashForm";
	}
	
	@RequestMapping(value = "/cert/firmwareHash/save/{key}", method = RequestMethod.POST, headers = "Accept=*/*")
	public @ResponseBody
	ResultMessage firmwareHashSave(
			@RequestBody FirmwareHash firmwareHashForm,
			@ModelAttribute(LoginSession.SESSIONNAME) LoginSession session,
			@PathVariable(value = "key") String key
			){
		
		return certService.firmwareHashAdd(firmwareHashForm, session, key);
	}
	
	@RequestMapping(value = "/cert/device/data"
			,method = RequestMethod.POST 
			,headers="Accept=*/*")
	public  @ResponseBody Map deviceData(@RequestBody Police police 
			,@ModelAttribute(LoginSession.SESSIONNAME) LoginSession session
			,HttpServletRequest request)		
	{
		
		Map value ;
		try {
			value =certService.serachPoliceData(police,request,session);
			System.out.println("@@@@@@@@@@@@@@@@@@@ /cert/device/data " +value);
		} catch (CertServiceException e) {
			value = new HashMap();
			value.put("message", e.getMessage());
		}
		return value;
		
	}
	
	@RequestMapping("/cert/issued")
	public String issued(ModelMap map  
			,@ModelAttribute(LoginSession.SESSIONNAME) LoginSession session
			,HttpServletRequest request){
		try {
			map.put("result", certService.getDeviceList(request,session));
		} catch (CertServiceException e) {
			map.put("message", e.getMessage());
		}
		return "/cert/issued";
	}
	
	@RequestMapping("/cert/reissue")
	public String reissue(ModelMap map  
			,@ModelAttribute(LoginSession.SESSIONNAME) LoginSession session
			,HttpServletRequest request){
		try {
			map.put("result", certService.getDeviceList(request,session));
		} catch (CertServiceException e) {
			map.put("message", e.getMessage());
		}
		return "/cert/reissue";
	}
	@RequestMapping("/cert/update")
	public String update(ModelMap map  
			,@ModelAttribute(LoginSession.SESSIONNAME) LoginSession session
			,HttpServletRequest request){
		try {
			map.put("result", certService.getDeviceList(request,session));
		} catch (CertServiceException e) {
			map.put("message", e.getMessage());
		}
		return "/cert/update";
	}
	@RequestMapping("/cert/revocation")
	public String revocation(ModelMap map  
			,@ModelAttribute(LoginSession.SESSIONNAME) LoginSession session
			,HttpServletRequest request){
		
		PageResult dateResult = WebUtil.getPageResult(request, session);
		CertSearchForm form = new CertSearchForm(request,session);
		
		// 날짜 초기화 (오늘 날짜로부터 한달 전까지 - 데이터의 값이 없을 때)
		if (dateResult.getsDate().equals(null)
				|| dateResult.getsDate().equals("")) {
			Calendar c = Calendar.getInstance();
			String addeddate = day.format(c.getTime());
			c.add(Calendar.MONTH, -1);
			
			dateResult.setsDate((day.format(c.getTime()) + "^" + addeddate).substring(0, 10));
			form.setSdate(dateResult.getsDate());
		}

		if (dateResult.geteDate().equals(null)
				|| dateResult.geteDate().equals("")) {
			Calendar c = Calendar.getInstance();
			String addeddate = day.format(c.getTime());
			
			dateResult.seteDate((day.format(c.getTime()) + "^" + addeddate).substring(11, 21));
			form.setEdate(dateResult.geteDate());
		} 

		try {
			map.put("devices", certService.getDeviceList(request,session));
			
			Police police = new Police();
			police.setOper("list");
			if(form.getFirmware() != null && !form.getFirmware().equals("")){
				police.setName("firmware");
				police.setDevice(request.getParameter("devices"));
				map.put("firmwares", certService.serachPoliceData(police,request,session));
			}

			if(form.getMcu() != null && !form.getMcu().equals("")){
				police.setName("mcu");
				police.setDevice(request.getParameter("devices"));
				police.setFirmware(request.getParameter("firmwares"));
				map.put("mcus", certService.serachPoliceData(police,request,session));
			}
			
		} catch (CertServiceException e) {
			// TODO Auto-generated catch block
			map.put("message", e.getMessage());
		}
		
		map.put("pageResult", certService.conditionList(form));
		map.put("searchDevices", form);
		
		map.put("dateResult", dateResult);
		
		return "/cert/revocation";
	}
	@RequestMapping("/cert/keymanagement")
	public String keymanagement(){
		return "/cert/keymanagement";
	}
	@RequestMapping("/cert/keygeneration")
	public String keygeneration(){
		return "/cert/keygeneration";
	}
	@RequestMapping("/cert/injection")
	public String injection(){
		return "/cert/injection";
	}
	@RequestMapping("/cert/management/status")
	public String status(ModelMap map , HttpServletRequest request
			  ,@ModelAttribute(LoginSession.SESSIONNAME) LoginSession session
			 ){
		
		PageResult dateResult = WebUtil.getPageResult(request, session);
		CertSearchForm form = new CertSearchForm(request,session);
		
		try {
			map.put("devices", certService.getDeviceList(request,session));
			
			Police police = new Police();
			police.setOper("list");
			if(form.getFirmware() != null && !form.getFirmware().equals("")){
				police.setName("firmware");
				police.setDevice(request.getParameter("devices"));
				map.put("firmwares", certService.serachPoliceData(police,request,session));
			}

			if(form.getMcu() != null && !form.getMcu().equals("")){
				police.setName("mcu");
				police.setDevice(request.getParameter("devices"));
				police.setFirmware(request.getParameter("firmwares"));
				map.put("mcus", certService.serachPoliceData(police,request,session));
			}
		} catch (CertServiceException e) {
			map.put("message", e.getMessage());
		}
		
		// 날짜 초기화 (오늘 날짜로부터 한달 전까지 - 데이터의 값이 없을 때)
		if (dateResult.getsDate().equals(null)
				|| dateResult.getsDate().equals("")) {
			Calendar c = Calendar.getInstance();
			String addeddate = day.format(c.getTime());
			c.add(Calendar.MONTH, -1);
			
			dateResult.setsDate((day.format(c.getTime()) + "^" + addeddate).substring(0, 10));
			form.setSdate(dateResult.getsDate());
		}
	
		if (dateResult.geteDate().equals(null)
				|| dateResult.geteDate().equals("")) {
			Calendar c = Calendar.getInstance();
			String addeddate = day.format(c.getTime());
			
			dateResult.seteDate((day.format(c.getTime()) + "^" + addeddate).substring(11, 21));
			form.setEdate(dateResult.geteDate());
		} 
		
		map.put("pageResult", certService.certStatus(form));
		map.put("searchDevices", form);
		
		return "/cert/management/status";
	}
	
	@RequestMapping("/cert/management/informationlisted")
	public String informationlisted(){
		return "/cert/management/informationlisted";
	}
	
	@RequestMapping("/cert/management/edit")
	public String edit(ModelMap map , HttpServletRequest request
			  ,@ModelAttribute(LoginSession.SESSIONNAME) LoginSession session,@RequestParam(value="DN",  defaultValue="")String DN ){
		
		PageResult pageResult = WebUtil.getPageResult(request, session);
		pageResult.appendParam("DN", DN);
		map.put("pageResult", certService.certSerailSearch(pageResult,session));
		System.out.println("@@@@@@@@@@@@@@@@@@ pR  " + certService.certSerailSearch(pageResult,session));
		return "/cert/management/edit";
	}
	@RequestMapping("/cert/management/remove")
	public String remove(){
		return "/cert/management/remove";
	}
	@RequestMapping("/cert/management/storage")
	public String storage(ModelMap map , HttpServletRequest request
			  ,@ModelAttribute(LoginSession.SESSIONNAME) LoginSession session
			 ){

		PageResult dateResult = WebUtil.getPageResult(request, session);
		CertSearchForm form = new CertSearchForm(request,session);
		
		try {
			map.put("devices", certService.getDeviceList(request,session));
			
			Police police = new Police();
			police.setOper("list");
			if(form.getFirmware() != null && !form.getFirmware().equals("")){
				police.setName("firmware");
				police.setDevice(request.getParameter("devices"));
				map.put("firmwares", certService.serachPoliceData(police,request,session));
			}

			if(form.getMcu() != null && !form.getMcu().equals("")){
				police.setName("mcu");
				police.setDevice(request.getParameter("devices"));
				police.setFirmware(request.getParameter("firmwares"));
				map.put("mcus", certService.serachPoliceData(police,request,session));
			}
		} catch (CertServiceException e) {
			map.put("message", e.getMessage());
		}
		
		// 날짜 초기화 (오늘 날짜로부터 한달 전까지 - 데이터의 값이 없을 때)
		if (dateResult.getsDate().equals(null)
				|| dateResult.getsDate().equals("")) {
			Calendar c = Calendar.getInstance();
			String addeddate = day.format(c.getTime());
			c.add(Calendar.MONTH, -1);
			
			dateResult.setsDate((day.format(c.getTime()) + "^" + addeddate).substring(0, 10));
			form.setSdate(dateResult.getsDate());
		}
	
		if (dateResult.geteDate().equals(null)
				|| dateResult.geteDate().equals("")) {
			Calendar c = Calendar.getInstance();
			String addeddate = day.format(c.getTime());
			
			dateResult.seteDate((day.format(c.getTime()) + "^" + addeddate).substring(11, 21));
			form.setEdate(dateResult.geteDate());
		} 
		
		map.put("pageResult", certService.certIssueList(form));
		map.put("searchDevices", form);
		return "/cert/management/storage";
	}
	
	@RequestMapping(value = "/cert/revocation/revo"
			, method = RequestMethod.POST)
	public @ResponseBody boolean resultRevo(@RequestBody SearchForm dns
			   ,@ModelAttribute(LoginSession.SESSIONNAME) LoginSession session			
			) {
		
		boolean returnMsg = certService.resultRevocation(dns.getDnList(),session);
		
		return returnMsg;
	}
	
	@RequestMapping(value="/cert/revocation/del"
			, method = RequestMethod.POST)
	public @ResponseBody boolean certDelete(@RequestBody SearchForm dns
			   ,@ModelAttribute(LoginSession.SESSIONNAME) LoginSession session) {
		
		boolean returnMsg = certService.certDelete(dns.getDnList(), session);
		
		return returnMsg;
	}

	@RequestMapping(value = "/cert/issued/save/{key}"
			,method = RequestMethod.POST 
			,headers="Accept=*/*")
	public String certSave(ModelMap map ,HttpServletRequest request  
			  ,@ModelAttribute(LoginSession.SESSIONNAME) LoginSession session
			,@RequestParam(value="oidList",  defaultValue="")String oidList 
			){
		
		 CertReq certReq = new CertReq();
		 certReq.setOidList(oidList);
		 certReq.setOPERATOR_ID(session.getOPERATOR_ID());
		certReq.setMAKER_NAME(session.getMAKER_NAME()); 

			if(certReq.getOPERATOR_ID().equals("")){
				 
				map.put("message", "로그인 후 사용하실 수 있습니다.");
				return "/login/login";
			}else{
				
				System.out.println("@@@@@@@@@@@@@@@@@@@@  %#$#@$" + LoginSession.SESSIONNAME);
				
				certService.saveForm(certReq,"issued");
			}
		
		return  "/cert/issued"; 

	}
	
	@RequestMapping(value = "/cert/reissue/save/{key}"
			,method = RequestMethod.POST 
			,headers="Accept=*/*")
	public String certReSave(ModelMap map ,HttpServletRequest request  
			  ,@ModelAttribute(LoginSession.SESSIONNAME) LoginSession session
			,@RequestParam(value="oidList",  defaultValue="")String oidList 
			){
		
		 CertReq certReq = new CertReq();
		 certReq.setOidList(oidList);
		 certReq.setOPERATOR_ID(session.getOPERATOR_ID());
		certReq.setMAKER_NAME(session.getMAKER_NAME()); 

			if(certReq.getOPERATOR_ID().equals("")){
				 
				map.put("message", "로그인 후 사용하실 수 있습니다.");
				return "/login/login";
			}else{
				certService.saveForm(certReq,"reissue");
			}
		
		return  "/cert/reissue"; 

	}

	@RequestMapping(value = "/cert/update/save/{key}"
			,method = RequestMethod.POST 
			,headers="Accept=*/*")
	public String certUpSave(ModelMap map ,HttpServletRequest request  
			  ,@ModelAttribute(LoginSession.SESSIONNAME) LoginSession session
			,@RequestParam(value="oidList",  defaultValue="")String oidList 
			){
		
		 CertReq certReq = new CertReq();
		 certReq.setOidList(oidList);
		 certReq.setOPERATOR_ID(session.getOPERATOR_ID());
		certReq.setMAKER_NAME(session.getMAKER_NAME()); 

			if(certReq.getOPERATOR_ID().equals("")){
				 
				map.put("message", "로그인 후 사용하실 수 있습니다.");
				return "/login/login";
			}else{
				certService.saveForm(certReq,"update");
			}
		
		return  "/cert/reissue"; 

	}
	
	@RequestMapping(value = "/cert/management/storage/{key}"
	,method = RequestMethod.POST 
	,headers="Accept=*/*")
	public  @ResponseBody Map certIssue(@RequestBody SearchForm dns
			   ,@ModelAttribute(LoginSession.SESSIONNAME) LoginSession session			
			){
		//String[] dns = request.getParameterValues("savecheck");
		dns.setOperatorID(session.getOPERATOR_ID());
		logger.debug(dns);
		logger.debug(session);
		logger.debug("$$$$$$$$$$$$$$$$$$$$$$$$$$");
		return certService.getCertificate(dns.getDnList(),session); 

	}

	private String getDate() {

		Calendar c = Calendar.getInstance();

		String addeddate = day.format(c.getTime());
		/*c.add(Calendar.DATE, -9);*/
		
		return addeddate;
	}	
	
	// 140401 추가
	@RequestMapping("/cert/management/storage2")
	public String storage2(ModelMap map , HttpServletRequest request
			  ,@ModelAttribute(LoginSession.SESSIONNAME) LoginSession session
			 ){

		PageResult dateResult = WebUtil.getPageResult(request, session);
		CertSearchForm form = new CertSearchForm(request,session);
		
		try {
			map.put("devices", certService.getDeviceList(request,session));
			
			Police police = new Police();
			police.setOper("list");
			if(form.getFirmware() != null && !form.getFirmware().equals("")){
				police.setName("firmware");
				police.setDevice(request.getParameter("devices"));
				map.put("firmwares", certService.serachPoliceData(police,request,session));
			}

			if(form.getMcu() != null && !form.getMcu().equals("")){
				police.setName("mcu");
				police.setDevice(request.getParameter("devices"));
				police.setFirmware(request.getParameter("firmwares"));
				map.put("mcus", certService.serachPoliceData(police,request,session));
			}
		} catch (CertServiceException e) {
			map.put("message", e.getMessage());
		}
		
		// 날짜 초기화 (오늘 날짜로부터 한달 전까지 - 데이터의 값이 없을 때)
		if (dateResult.getsDate().equals(null)
				|| dateResult.getsDate().equals("")) {
			Calendar c = Calendar.getInstance();
			String addeddate = day.format(c.getTime());
			c.add(Calendar.MONTH, -1);
			
			dateResult.setsDate((day.format(c.getTime()) + "^" + addeddate).substring(0, 10));
			form.setSdate(dateResult.getsDate());
		}
	
		if (dateResult.geteDate().equals(null)
				|| dateResult.geteDate().equals("")) {
			Calendar c = Calendar.getInstance();
			String addeddate = day.format(c.getTime());
			
			dateResult.seteDate((day.format(c.getTime()) + "^" + addeddate).substring(11, 21));
			form.setEdate(dateResult.geteDate());
		} 
		
		map.put("pageResult", certService.certIssueList(form));
		map.put("searchDevices", form);
		return "/cert/management/storage2";
	}
	
	@RequestMapping("/cert/devicePolicy/list2")
	public String devicePolicy2(
			@ModelAttribute(LoginSession.SESSIONNAME) LoginSession session,
			HttpServletRequest request,
			ModelMap map
			){
		
		try {
			PageResult pageResult = WebUtil.getPageResult(request, session);
			certService.getDevicePolicyList(pageResult);

			map.put("pageResult", pageResult);
			map.put("session", session);
		} catch (Exception e) {
			map.put("message", "로그인 후 사용하실 수 있습니다.");
			return "/login/login";
		}
		
		return "/cert/devicePolicy/devicePolicyList2";
	}
	
	@RequestMapping("/cert/devicePolicy/view2")
	public String devicePolicyView2(
			@RequestParam(value = "dSerialNo", defaultValue = "") String dSerialNo,
			@ModelAttribute(LoginSession.SESSIONNAME) LoginSession session,
			HttpServletRequest request,
			ModelMap map
			){
		
		Map param = new HashMap();
		param.put("dSerialNo", dSerialNo);
		param.put("OPERATOR_ID", session.getOPERATOR_ID());
		
		try {
			map.put("policy", certService.getDevicePolicyDetail(param));
		} catch (Exception e) {
			map.put("message", "상세 정보를 가져오는데 실패 하였습니다.");
			
			return "forward:/cert/devicePolicy/list2";
		}
		
		return "/cert/devicePolicy/devicePolicyView2";
	}
	
	@RequestMapping("/cert/issued2")
	public String issued2(ModelMap map  
			,@ModelAttribute(LoginSession.SESSIONNAME) LoginSession session
			,HttpServletRequest request){
		try {
			map.put("result", certService.getDeviceList(request,session));
		} catch (CertServiceException e) {
			map.put("message", e.getMessage());
		}
		return "/cert/issued2";
	}
	
	@RequestMapping("/cert/reissue2")
	public String reissue2(ModelMap map  
			,@ModelAttribute(LoginSession.SESSIONNAME) LoginSession session
			,HttpServletRequest request){
		try {
			map.put("result", certService.getDeviceList(request,session));
		} catch (CertServiceException e) {
			map.put("message", e.getMessage());
		}
		return "/cert/reissue2";
	}
	
	@RequestMapping("/cert/update2")
	public String update2(ModelMap map  
			,@ModelAttribute(LoginSession.SESSIONNAME) LoginSession session
			,HttpServletRequest request){
		try {
			map.put("result", certService.getDeviceList(request,session));
		} catch (CertServiceException e) {
			map.put("message", e.getMessage());
		}
		return "/cert/update2";
	}
	
	@RequestMapping("/cert/revocation2")
	public String revocation2(ModelMap map  
			,@ModelAttribute(LoginSession.SESSIONNAME) LoginSession session
			,HttpServletRequest request){
		
		PageResult dateResult = WebUtil.getPageResult(request, session);
		CertSearchForm form = new CertSearchForm(request,session);
		
		// 날짜 초기화 (오늘 날짜로부터 한달 전까지 - 데이터의 값이 없을 때)
		if (dateResult.getsDate().equals(null)
				|| dateResult.getsDate().equals("")) {
			Calendar c = Calendar.getInstance();
			String addeddate = day.format(c.getTime());
			c.add(Calendar.MONTH, -1);
			
			dateResult.setsDate((day.format(c.getTime()) + "^" + addeddate).substring(0, 10));
			form.setSdate(dateResult.getsDate());
		}

		if (dateResult.geteDate().equals(null)
				|| dateResult.geteDate().equals("")) {
			Calendar c = Calendar.getInstance();
			String addeddate = day.format(c.getTime());
			
			dateResult.seteDate((day.format(c.getTime()) + "^" + addeddate).substring(11, 21));
			form.setEdate(dateResult.geteDate());
		} 

		try {
			map.put("devices", certService.getDeviceList(request,session));
			
			Police police = new Police();
			police.setOper("list");
			if(form.getFirmware() != null && !form.getFirmware().equals("")){
				police.setName("firmware");
				police.setDevice(request.getParameter("devices"));
				map.put("firmwares", certService.serachPoliceData(police,request,session));
			}

			if(form.getMcu() != null && !form.getMcu().equals("")){
				police.setName("mcu");
				police.setDevice(request.getParameter("devices"));
				police.setFirmware(request.getParameter("firmwares"));
				map.put("mcus", certService.serachPoliceData(police,request,session));
			}
			
		} catch (CertServiceException e) {
			// TODO Auto-generated catch block
			map.put("message", e.getMessage());
		}
		
		map.put("pageResult", certService.conditionList(form));
		map.put("searchDevices", form);
		
		map.put("dateResult", dateResult);
		
		return "/cert/revocation2";
	}
	
	@RequestMapping("/cert/management/status2")
	public String status2(ModelMap map , HttpServletRequest request
			  ,@ModelAttribute(LoginSession.SESSIONNAME) LoginSession session
			 ){
		
		PageResult dateResult = WebUtil.getPageResult(request, session);
		CertSearchForm form = new CertSearchForm(request,session);
		
		try {
			map.put("devices", certService.getDeviceList(request,session));
			
			Police police = new Police();
			police.setOper("list");
			if(form.getFirmware() != null && !form.getFirmware().equals("")){
				police.setName("firmware");
				police.setDevice(request.getParameter("devices"));
				map.put("firmwares", certService.serachPoliceData(police,request,session));
			}

			if(form.getMcu() != null && !form.getMcu().equals("")){
				police.setName("mcu");
				police.setDevice(request.getParameter("devices"));
				police.setFirmware(request.getParameter("firmwares"));
				map.put("mcus", certService.serachPoliceData(police,request,session));
			}
		} catch (CertServiceException e) {
			map.put("message", e.getMessage());
		}
		
		// 날짜 초기화 (오늘 날짜로부터 한달 전까지 - 데이터의 값이 없을 때)
		if (dateResult.getsDate().equals(null)
				|| dateResult.getsDate().equals("")) {
			Calendar c = Calendar.getInstance();
			String addeddate = day.format(c.getTime());
			c.add(Calendar.MONTH, -1);
			
			dateResult.setsDate((day.format(c.getTime()) + "^" + addeddate).substring(0, 10));
			form.setSdate(dateResult.getsDate());
		}
	
		if (dateResult.geteDate().equals(null)
				|| dateResult.geteDate().equals("")) {
			Calendar c = Calendar.getInstance();
			String addeddate = day.format(c.getTime());
			
			dateResult.seteDate((day.format(c.getTime()) + "^" + addeddate).substring(11, 21));
			form.setEdate(dateResult.geteDate());
		} 
		
		map.put("pageResult", certService.certStatus(form));
		map.put("searchDevices", form);
		
		return "/cert/management/status2";
	}
}
