
package com.ksign.kdn.web;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.ksign.kdn.domain.LoginSession;
import com.ksign.kdn.service.OperatorService;
import com.ksign.kdn.service.OperatorService.OperatorException;


@Controller
public class LoginController {
	Logger log = Logger.getLogger(LoginController.class);
	@Autowired
	OperatorService operatorService;
	
	
	@RequestMapping("/login")
	public String login(){

		return "/login/login";
	}
	
	@RequestMapping("/login/check")
	public String check(ModelMap map,@RequestParam(value="pwd",  defaultValue="")String pwd,
			@RequestParam(value="id", required=false, defaultValue="")String id,
			@RequestParam(value="DN",  defaultValue="")String DN,
			@RequestParam(value="signed_data",  defaultValue="")String signedData,
			HttpServletRequest request){
		

		try {
			
			Map	operator  = null;
			if(DN.length()> 5){
				operator = operatorService.loginCheckCert(DN, signedData,request.getRemoteAddr());
			}else{
			    operator = operatorService.loginCheck(id, pwd);
			}
			if(operator != null && operator.get("result").equals("ok") ){
				request.getSession().setAttribute(LoginSession.SESSIONNAME,  new LoginSession(operator));
			}
		} catch (OperatorException e) {
			e.printStackTrace();
			map.put("id", id);
			map.put("message", e.getMessage());
			return "/login/login";
		}
		
		return "redirect:/info/information.kdn";
	}
	
	@RequestMapping("/login/logout")
	public String logout(HttpServletRequest request){
		try
		{
			request.getSession().setAttribute(LoginSession.SESSIONNAME, null);
			request.getSession().invalidate();
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		
		return "/login/login";
	}
	
}
