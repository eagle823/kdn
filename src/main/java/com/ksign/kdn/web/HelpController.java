package com.ksign.kdn.web;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import com.ksign.kdn.domain.LoginSession;

@Controller
public class HelpController extends SessionController {

	
	@RequestMapping("/help/faq")
	public String faq(
			@ModelAttribute(LoginSession.SESSIONNAME) LoginSession session
			,ModelMap map
			){
		
		map.put("session", session);
		return "/help/faqList";
	}
	
	@RequestMapping("/help/board")
	public String board(
			@ModelAttribute(LoginSession.SESSIONNAME) LoginSession session
			,ModelMap map
			){
		
		map.put("session", session);
		return "/help/board";
	}
}
