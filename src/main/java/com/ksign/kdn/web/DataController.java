package com.ksign.kdn.web;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.ksign.kdn.domain.LoginSession;


@Controller
public class DataController  extends SessionController {
	@RequestMapping("/data/{key}")
	public String regulations(
			@PathVariable(value="key")String key
			,@ModelAttribute(LoginSession.SESSIONNAME) LoginSession session
			,ModelMap map
			){
		
		map.put("session", session);
		return "/data/"+key;
	}
}
