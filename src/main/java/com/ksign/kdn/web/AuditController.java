package com.ksign.kdn.web;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import com.ksign.kdn.client.RaOppConnection;
import com.ksign.kdn.domain.LoginSession;
import com.ksign.kdn.domain.PageResult;
import com.ksign.kdn.domain.WebUtil;
import com.ksign.kdn.form.AuditForm;
import com.ksign.kdn.form.SearchForm;
import com.ksign.kdn.service.AuditService;

@Controller
public class AuditController extends SessionController {

	SimpleDateFormat day = new SimpleDateFormat("yyyy-MM-dd");
	SimpleDateFormat mon = new SimpleDateFormat("yyyy-MM");

	@Autowired
	AuditService auditService;

	@Autowired
	RaOppConnection raOpp;

	@RequestMapping("/audit/audit")
	public String audit(ModelMap map,
			@ModelAttribute(LoginSession.SESSIONNAME) LoginSession session,
			@ModelAttribute("audit") AuditForm auditForm,
			HttpServletRequest request) {
		
		PageResult pageResult = WebUtil.getPageResult(request, session);
		
		// 날짜 초기화 (오늘 날짜로부터 한달 전까지 - 데이터의 값이 없을 때)
		if (pageResult.getsDate().equals(null)
				|| pageResult.getsDate().equals("")) {
			Calendar c = Calendar.getInstance();
			String addeddate = day.format(c.getTime());
			c.add(Calendar.MONTH, -1);
			
			pageResult.setsDate((day.format(c.getTime()) + "^" + addeddate).substring(0, 10));
			auditForm.setSdate(pageResult.getsDate());
		}

		if (pageResult.geteDate().equals(null)
				|| pageResult.geteDate().equals("")) {
			Calendar c = Calendar.getInstance();
			String addeddate = day.format(c.getTime());
			
			pageResult.seteDate((day.format(c.getTime()) + "^" + addeddate).substring(11, 21));
			auditForm.setEdate(pageResult.geteDate());
		} 
		
		// 화면상 운영자ID 노출을 위한 제어문
		if(pageResult.getOPERATOR_ID().equals("") || pageResult.getOPERATOR_ID().equals(null))
		{
				pageResult.setOPERATOR_ID(session.getId());
		}
		
		// 해당 페이지 설정부분과 리스트 출력하는 부분 분리
		auditForm.setAdminid(pageResult.getOPERATOR_ID());
		auditService.getAuditList(auditForm, session);
		
		map.put("pageResult", pageResult);
		map.put("auditForm", auditForm);
		return "audit/audit";
	}

	@RequestMapping("/audit/cert/process")
	public String process(ModelMap map,
			@ModelAttribute(LoginSession.SESSIONNAME) LoginSession session,
			@ModelAttribute("table_search") SearchForm search,
			HttpServletRequest request) {

		PageResult pageResult = WebUtil.getPageResult(request, session);
		
		System.out.println("@@@@@@@@@@@@@@ PAGE@@@@@@@@@@@@@ "+ pageResult.getMakerName());
		
		//map.put("pageResult", pageResult);
		map.put("pageCode", "state");

		if (pageResult.getsDate().equals(null)
				|| pageResult.getsDate().equals("")) {
			pageResult.setsDate(getDate().substring(0,10));
		}

		if (pageResult.geteDate().equals(null)
				|| pageResult.geteDate().equals("")) {
			pageResult.seteDate(getDate().substring(11,21));
		}
		
		if(pageResult.getMakerName().equals("") || pageResult.getMakerName().equals(null))
		{
			pageResult.setMakerName(session.getMAKER_NAME());
		}else
		{
			pageResult.setMakerName(pageResult.getMakerName());
		}
		try
		{
			map.put("deviceInfo", auditService.getDeviceList(request, session, pageResult));
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		map.put("pageResult",auditService.getCertMonthLog(pageResult, session));
		return "/audit/process";
	}

	@RequestMapping("/audit/cert/state")
	public String state(ModelMap map,
			@ModelAttribute(LoginSession.SESSIONNAME) LoginSession session,
			@ModelAttribute("search") SearchForm search,
			HttpServletRequest request) {

		PageResult pageResult = WebUtil.getPageResult(request, session);
		
		//map.put("pageResult", pageResult);
		map.put("session", session);
		map.put("pageCode", "state");

		if (pageResult.getsDate().equals(null)
				|| pageResult.getsDate().equals("")) {
			pageResult.setsDate(getDate().substring(0,10));
		}

		if (pageResult.geteDate().equals(null)
				|| pageResult.geteDate().equals("")) {
			pageResult.seteDate(getDate().substring(11,21));
		}
		try
		{
			map.put("deviceInfo", auditService.getDeviceList(request, session, pageResult));
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		map.put("pageResult",auditService.getCertDayLog(pageResult, session));
		

		
		return "/audit/state";
	}

	@RequestMapping("/audit/cert/state/success")
	public String stateToRequestSuccess(ModelMap map,
			@ModelAttribute(LoginSession.SESSIONNAME) LoginSession session,
			@ModelAttribute("search") SearchForm search) {
		map.put("pageResult", auditService.getCertSuccessLog(search, session));
		map.put("session", session);
		map.put("pageCode", "success");
		return "/audit/state";
	}

	@RequestMapping("/audit/cert/state/wait")
	public String stateToRequestWait(ModelMap map,
			@ModelAttribute(LoginSession.SESSIONNAME) LoginSession session,
			@ModelAttribute("search") SearchForm search) {
		map.put("pageResult", auditService.getCertWaitLog(search, session));
		map.put("session", session);
		map.put("pageCode", "wait");
		return "/audit/state";
	}

	@RequestMapping("/audit/cert/state/fail")
	public String stateToRequestFail(ModelMap map,
			@ModelAttribute(LoginSession.SESSIONNAME) LoginSession session,
			@ModelAttribute("search") SearchForm search) {
		map.put("pageResult", auditService.getCertFailLog(search, session));
		map.put("session", session);
		map.put("pageCode", "fail");
		return "/audit/state";
	}

	private String getDate() {

		Calendar c = Calendar.getInstance();

		String addeddate = day.format(c.getTime());
		c.add(Calendar.DATE, -9);

		return (day.format(c.getTime()) + "^" + addeddate);
	}
}
