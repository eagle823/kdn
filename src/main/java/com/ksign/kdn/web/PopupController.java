package com.ksign.kdn.web;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ksign.kdn.domain.LoginSession;
import com.ksign.kdn.domain.PageResult;
import com.ksign.kdn.domain.WebUtil;
import com.ksign.kdn.form.SearchForm;
import com.ksign.kdn.service.PopupService;
import com.ksign.kdn.service.PopupService.PopupException;


@Controller
public class PopupController extends SessionController {
	
	@Autowired
	PopupService popupService;
	
	
	
	@RequestMapping("/popup/findCompany")
	public String findCompany(ModelMap map
			  ,@ModelAttribute(LoginSession.SESSIONNAME) LoginSession session
			,	HttpServletRequest request,@RequestParam(value="type",defaultValue="")String type ){
		 
			
			  PageResult pageResult = WebUtil.getPageResult(request,session);
		  
		
		Map res = null;
		try {
			res = popupService.findCompany(pageResult.getSearchParam());
		} catch (PopupException e) {
			map.put("message", e.getMessage());
			e.printStackTrace();
		}
		map.put("result", res);
		map.put("type", type);
		return "/popup/findCompany";
	}
	
	@RequestMapping("/popup/findAddress")
	public String findAddress(ModelMap map
			 ,@ModelAttribute(LoginSession.SESSIONNAME) LoginSession session
			 ,	HttpServletRequest request,@RequestParam(value="type",defaultValue="")String type ){
		 
			
			  PageResult pageResult = WebUtil.getPageResult(request,session);
		  
		 
		map.put("result", pageResult);
		map.put("type", type);
		return "/popup/findAddress";
	}
	
	
	@RequestMapping(value = "/popup/data/{key}", 
            method = RequestMethod.POST, 
            headers="Accept=*/*")
	public   @ResponseBody Map<String,Object> data(
			 @RequestBody SearchForm search
			 ,@ModelAttribute(LoginSession.SESSIONNAME) LoginSession session
			 ,@PathVariable(value="key")String key){
	
		search.setOperatorID(session.getOPERATOR_ID());
         Map map ;
         try{
         map = popupService.getData(search,key);
         }catch(Exception e){
        	 e.printStackTrace();
        	 map  = new HashMap();
        	 map.put("message", "정보를 가져오는데 실패 하였습니다.["+e.getMessage() +"]");
         }
		return map;
	}

}
