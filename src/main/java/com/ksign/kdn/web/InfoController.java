package com.ksign.kdn.web;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.ksign.kdn.domain.LoginSession;




@Controller
public class InfoController extends SessionController {
	Logger log = Logger.getLogger(InfoController.class);
	
	
	@RequestMapping("/info/{key}")
	public String info(
			@PathVariable(value="key")String key
			,@ModelAttribute(LoginSession.SESSIONNAME) LoginSession session
			,ModelMap map
			){
		
		map.put("session", session);
		return "/info/"+key;
	}
	
}
