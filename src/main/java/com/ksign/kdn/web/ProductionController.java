package com.ksign.kdn.web;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ksign.kdn.client.RaOppConnection.RaOppConnectionException;
import com.ksign.kdn.domain.LoginSession;
import com.ksign.kdn.domain.PageResult;
import com.ksign.kdn.domain.Production;
import com.ksign.kdn.domain.WebUtil;
import com.ksign.kdn.form.CheckForm;
import com.ksign.kdn.form.SearchForm;
import com.ksign.kdn.service.OperatorService;
import com.ksign.kdn.util.ResultMessage;


@Controller
public class ProductionController extends SessionController {

	@Autowired
	OperatorService operatorService;

	@RequestMapping("/production/company/form/{key}")
	public String compayForm(ModelMap map 
			,HttpServletRequest request 
			  ,@ModelAttribute(LoginSession.SESSIONNAME) LoginSession session
			,@RequestParam(value="makerName" ,defaultValue="")String makerName,
			@PathVariable(value="key")String key){
		if(key.equals("mod")){
			Map param = new HashMap();
			param.put("makerName", makerName);
			param.put("OPERATOR_ID", session.getOPERATOR_ID());
			
			try {
				map.put("production", operatorService.getMakerDetail(param));
			} catch (RaOppConnectionException e) {
				map.put("message", "상세 정보를 가져오는데 실패 하였습니다.");

				return "/product/companyList";
			}
		}else if(key.equals("del")){
			Map param = new HashMap();
			param.put("OPERATOR_ID", session.getOPERATOR_ID());
			String[] list =request.getParameterValues("delList");
			
			String fail ="";
			
			try{
			  fail = operatorService.deletCompany(list,param);
			}catch(Exception e){
				fail = "삭제 하지 못하였습니다.";
			}
				if(fail.equals("")){
					fail = "삭제 되었습니다.";
				}else{
					fail = fail  + " 는 실패 하였습니다.";
				}
			
				PageResult pageResult = WebUtil.getPageResult(request,session);
				operatorService.operatorSearch(pageResult);
				map.put("pageResult", pageResult);
			 
			map.put("message", fail)	 ;
			return "/product/companyList";//orgService.getDeptInoData(pagedListResult);
		}

		map.put("key", key);
		return "/product/companyForm";
	}

	@RequestMapping("/production/view")
	public String compayView( 
			@RequestParam(value="makerName" ,defaultValue="")String makerName,
			@ModelAttribute(LoginSession.SESSIONNAME) LoginSession session,
			HttpServletRequest request,
			ModelMap map
			){
		
		Map param = new HashMap();
		param.put("makerName", makerName);
		param.put("OPERATOR_ID", session.getOPERATOR_ID());
		try {
			map.put("production", operatorService.getMakerDetail(param));
		} catch (RaOppConnectionException e) {
			map.put("message", "상세 정보를 가져오는데 실패 하였습니다.");

			return "/product/companyList";
		}
		return "/product/companyView";
	}
	
	@RequestMapping(value = "/production/company/save/{key}"
			,method = RequestMethod.POST 
			,headers="Accept=*/*")
	public  @ResponseBody ResultMessage companSave(@RequestBody Production production
			  ,@ModelAttribute(LoginSession.SESSIONNAME) LoginSession session
			  ,@PathVariable(value="key")String key,HttpServletRequest request)		
	{
		production.setOPERATOR_ID(session.getOPERATOR_ID());
		return  operatorService.saveForm(production,key);//orgService.getDeptInoData(pagedListResult);

	}

	@RequestMapping({"/production/list","/product/list"})
	public String compayList(ModelMap map 
			,HttpServletRequest request 
			,@ModelAttribute(LoginSession.SESSIONNAME) LoginSession session
			){
		PageResult pageResult = WebUtil.getPageResult(request,session);
		
		operatorService.operatorSearch(pageResult);
		map.put("pageResult", pageResult);
		map.put("session", session);
		return "/product/companyList";
	}

	@RequestMapping(value = "/production/device/save/{key}"
			,method = RequestMethod.POST 
			,headers="Accept=*/*")
	public  @ResponseBody ResultMessage deviceSave(@RequestBody Production production,
			@PathVariable(value="key")String key	
			,@ModelAttribute(LoginSession.SESSIONNAME) LoginSession session
			,HttpServletRequest request )		
	{
		production.setOPERATOR_ID(session.getOPERATOR_ID());

		return  operatorService.saveForm(production,key);//orgService.getDeptInoData(pagedListResult);

	}

	@RequestMapping({"/production/deviceList","/product/deviceList"})
	public String deviceList(ModelMap map 
			,HttpServletRequest request 
			,@ModelAttribute(LoginSession.SESSIONNAME) LoginSession session){
		PageResult pageResult = WebUtil.getPageResult(request,session);

		operatorService.operatorSearch(pageResult);
		map.put("pageResult", pageResult);
		return "/product/deviceList";
	}

	@RequestMapping(value = "/production/list/data"
			,method = RequestMethod.POST 
			,headers="Accept=*/*")
	public  @ResponseBody Map managerInfoData(@RequestBody SearchForm pageResult,@ModelAttribute(LoginSession.SESSIONNAME) LoginSession session)		
	{
		return  operatorService.managerSearch(pageResult);//orgService.getDeptInoData(pagedListResult);

	}


	@RequestMapping(value = "/production/list/check"
			,method = RequestMethod.POST 
			,headers="Accept=*/*")
	public  @ResponseBody ResultMessage checkData(
			@RequestBody CheckForm check
			,@ModelAttribute(LoginSession.SESSIONNAME) LoginSession session
			)		
	{
		check.setOperatorId(session.getOPERATOR_ID());
		return  operatorService.checkData(check);//orgService.getDeptInoData(pagedListResult);

	}

}
