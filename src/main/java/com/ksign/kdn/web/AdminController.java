package com.ksign.kdn.web;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ksign.kdn.client.RaOppConnection.RaOppConnectionException;
import com.ksign.kdn.domain.LoginSession;
import com.ksign.kdn.domain.PageResult;
import com.ksign.kdn.domain.Police;
import com.ksign.kdn.domain.Policy;
import com.ksign.kdn.domain.WebUtil;
import com.ksign.kdn.form.CheckForm;
import com.ksign.kdn.form.UserAddForm;
import com.ksign.kdn.service.AdminService;
import com.ksign.kdn.service.CertService;
import com.ksign.kdn.service.AdminService.AdminException;
import com.ksign.kdn.util.ResultMessage;
import com.ksign.kdn.util.ResultMessage.Result;

@Controller
public class AdminController extends SessionController {

	@Autowired
	AdminService adminService;

	@Autowired
	CertService certService;
	
	@RequestMapping("/admin/list")
	public String userList(ModelMap map,
			@ModelAttribute(LoginSession.SESSIONNAME) LoginSession session,
			HttpServletRequest request) {
		try {

			PageResult pageResult = WebUtil.getPageResult(request, session);
			adminService.userListSearch(pageResult);

			map.put("pageResult", pageResult);
			map.put("session", session);
			System.out.println("@#$@$ " + session.getTYPE() + " ///// ");
		} catch (Exception e) {
			map.put("message", "로그인 후 사용하실 수 있습니다.");
			return "/login/login";
		}
		return "/admin/userList";
	}

	@RequestMapping("/admin/user/form/{key}")
	public String adminForm(ModelMap map,
			@ModelAttribute(LoginSession.
					SESSIONNAME) LoginSession session,
			@RequestParam(value = "adminId", defaultValue = "") String adminId,
			@PathVariable(value = "key") String key, HttpServletRequest request) {

		PageResult pageResult = WebUtil.getPageResult(request, session);
		Map param = pageResult.getSearchParam();
		if (key.equals("mod")) {
			param.put("adminId", adminId);

			try {
				map.put("user", adminService.getAdminDetail(param));
			} catch (AdminException e) {
				e.printStackTrace();
				map.put("message", "로그인 후 사용하실 수 있습니다.");
				return "/login/login";
			}
		} else if (key.equals("del")) {

			String fail = adminService.userDelete(request, param);
			if (fail.equals("")) {
				map.put("message", "삭제 되었습니다. ");
			} else {

				map.put("message", "다음 사용자 삭제에 실패 하였습니다. [" + fail + "]");

			}
			adminService.userListSearch(pageResult);
			map.put("pageResult", pageResult);

			return "/admin/userList";
		}
		map.put("key", key);
		return "/admin/userForm";
	}

	@RequestMapping("/admin/view")
	public String adminView(
			@RequestParam(value = "adminId", defaultValue = "") String adminId,
			@ModelAttribute(LoginSession.SESSIONNAME) LoginSession session,
			HttpServletRequest request, ModelMap map) {

		Map param = new HashMap();
		param.put("adminId", adminId);
		param.put("OPERATOR_ID", session.getOPERATOR_ID());
		try {
			map.put("user", adminService.getAdminDetail(param));
		} catch (AdminException e) {
			map.put("message", "상세 정보를 가져오는데 실패 하였습니다.");

			return "/admin/userList";
		}
		return "/admin/userView";
	}

	@RequestMapping("/admin/rating/list")
	public String rating(
			ModelMap map,
			@ModelAttribute(LoginSession.SESSIONNAME) LoginSession session,
			HttpServletRequest request,
			@RequestParam(value = "currentPage", required = false, defaultValue = "1") String currentPage) {
		
		try {
			PageResult pageResult = WebUtil.getPageResult(request, session);
			adminService.getPolicyList(pageResult);
			map.put("pageResult", pageResult);
		} catch (RaOppConnectionException e) {
			map.put("message", e.getMessage());
		}
		return "/admin/ratingList";
	}
	
	@RequestMapping("/admin/rating/form/{key}2")
	public String ratingForm2(
			ModelMap map,
			@ModelAttribute(LoginSession.SESSIONNAME) LoginSession session,
			@RequestParam(value = "dSerialNo", defaultValue = "") String dSerialNo,
			@PathVariable(value = "key") String key, HttpServletRequest request
			) throws AdminException{
		
		PageResult pageResult = WebUtil.getPageResult(request, session);
		Map param = pageResult.getSearchParam();
		
		key = key + "2";
		
		if(key.equals("mod2")){
			param.put("dSerialNo", dSerialNo);
			param.put("OPERATOR_ID", session.getOPERATOR_ID());
			
			try {
				map.put("certs", adminService.getPolicyCert(param));
				map.put("policy", adminService.getPolicyDetail(param));
			} catch (Exception e) {
				map.put("message", "상세 정보를 가져오는데 실패 하였습니다.");
				return "redirect:/admin/ratingList2";
			}
		}else if(key.equals("del2")){
			String fail = adminService.policyDelete(request, param);

			if (fail.equals("")) {
				map.put("message", "삭제 되었습니다. ");
			} else {
				map.put("message", "다음 기기정책 삭제에 실패 하였습니다. [" + fail + "]");
			}
			
			return "forward:/admin/rating/list2";
		}
		
		map.put("key", key);
		return "/admin/ratingForm2";
	}

	@RequestMapping("/admin/rating/form/{key}")
	public String ratingForm(
			ModelMap map,
			@ModelAttribute(LoginSession.SESSIONNAME) LoginSession session,
			@RequestParam(value = "dSerialNo", defaultValue = "") String dSerialNo,
			@PathVariable(value = "key") String key, HttpServletRequest request
			) throws AdminException{
		
		PageResult pageResult = WebUtil.getPageResult(request, session);
		Map param = pageResult.getSearchParam();
		
		if(key.equals("mod")){
			param.put("dSerialNo", dSerialNo);
			param.put("OPERATOR_ID", session.getOPERATOR_ID());
			
			try {
				map.put("certs", adminService.getPolicyCert(param));
				map.put("policy", adminService.getPolicyDetail(param));
			} catch (Exception e) {
				map.put("message", "상세 정보를 가져오는데 실패 하였습니다.");
				return "redirect:/admin/ratingList";
			}
		}else if(key.equals("del")){
			String fail = adminService.policyDelete(request, param);

			if (fail.equals("")) {
				map.put("message", "삭제 되었습니다. ");
			} else {
				map.put("message", "다음 기기정책 삭제에 실패 하였습니다. [" + fail + "]");
			}
			
			return "forward:/admin/rating/list";
		}
		
		map.put("key", key);
		return "/admin/ratingForm";
	}
	
	@RequestMapping("/admin/rating/view")
	public String ratingView(
			@RequestParam(value = "dSerialNo", defaultValue = "") String dSerialNo,
			@ModelAttribute(LoginSession.SESSIONNAME) LoginSession session,
			HttpServletRequest request, ModelMap map) {

		Map param = new HashMap();
		param.put("dSerialNo", dSerialNo);
		param.put("OPERATOR_ID", session.getOPERATOR_ID());
		try {
			map.put("policy", adminService.getPolicyDetail(param));
		} catch (AdminException e) {
			map.put("message", "상세 정보를 가져오는데 실패 하였습니다.");

			return "/admin/ratingList";
		}
		return "/admin/ratingView";
	}
	
	@RequestMapping(value = "/admin/rating/save/{key}", method = RequestMethod.POST, headers = "Accept=*/*")
	public @ResponseBody
	ResultMessage devicePolicySave(
			@RequestBody Policy policyForm,
			@ModelAttribute(LoginSession.SESSIONNAME) LoginSession session,
			@PathVariable(value = "key") String key
			){
		
		return certService.devicePolicyAdd(policyForm, session, key);
	}
	
	@RequestMapping(value="/admin/user/passwordCheck", method= RequestMethod.GET)
	public String passwordModForm(
			ModelMap map
			){

		return "/admin/passwordCheck";
	}
	
	@RequestMapping(value="/admin/user/passwordMod", method= RequestMethod.GET)
	public String passwordModForm(
			@RequestParam("userId") String userId,
			ModelMap map
			){
		
		ResultMessage message = new ResultMessage();
		message.setResult(Result.FAIL);
		
		map.put("userId", userId);
		map.put("flag", false);
		map.put("result", message);
		return "/admin/passwordForm";
	}

	@RequestMapping(value="/admin/user/passwordMod", method= RequestMethod.POST)
	public String passwordMod(
			@RequestParam("userId") String userId,
			@RequestParam("conf_pw") String confPw,
			ModelMap map
			){
		
		ResultMessage message = adminService.modifyPW(userId, confPw);
		
		map.put("flag", true);
		map.put("result", message);
		map.put("confPw", confPw);
		return "/admin/passwordForm";
	}
	
	@RequestMapping(value = "/admin/user/idcheck", method = RequestMethod.POST, headers = "Accept=*/*")
	public @ResponseBody
	ResultMessage checkData(@RequestBody CheckForm check,
			@ModelAttribute(LoginSession.SESSIONNAME) LoginSession session) {
		return adminService.checkID(check, session);

	}

	@RequestMapping(value = "/admin/user/save/{key}", method = RequestMethod.POST, headers = "Accept=*/*")
	public @ResponseBody
	ResultMessage checkData(@RequestBody UserAddForm userAddForm,
			@ModelAttribute(LoginSession.SESSIONNAME) LoginSession session,
			@PathVariable(value = "key") String key) {
		return adminService.userAdd(userAddForm, session, key);
	}

	@RequestMapping(value = "/admin/device/data", method = RequestMethod.POST, headers = "Accept=*/*")
	public @ResponseBody
	Map deviceData(@RequestBody Police police,
			@ModelAttribute(LoginSession.SESSIONNAME) LoginSession session) {

		police.setOPERATOR_ID(session.getOPERATOR_ID());
		return adminService.serachPoliceData(police);

	}
	
	@RequestMapping("/admin/rating/list2")
	public String rating2(
			ModelMap map,
			@ModelAttribute(LoginSession.SESSIONNAME) LoginSession session,
			HttpServletRequest request,
			@RequestParam(value = "currentPage", required = false, defaultValue = "1") String currentPage) {
		
		try {
			PageResult pageResult = WebUtil.getPageResult(request, session);
			adminService.getPolicyList(pageResult);
			map.put("pageResult", pageResult);
		} catch (RaOppConnectionException e) {
			map.put("message", e.getMessage());
		}
		return "/admin/ratingList2";
	}
	
	@RequestMapping("/admin/rating/view2")
	public String ratingView2(
			@RequestParam(value = "dSerialNo", defaultValue = "") String dSerialNo,
			@ModelAttribute(LoginSession.SESSIONNAME) LoginSession session,
			HttpServletRequest request, ModelMap map) {

		Map param = new HashMap();
		param.put("dSerialNo", dSerialNo);
		param.put("OPERATOR_ID", session.getOPERATOR_ID());
		try {
			map.put("policy", adminService.getPolicyDetail(param));
		} catch (AdminException e) {
			map.put("message", "상세 정보를 가져오는데 실패 하였습니다.");

			return "/admin/ratingList2";
		}
		return "/admin/ratingView2";
	}
}
