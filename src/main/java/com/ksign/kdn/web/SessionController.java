package com.ksign.kdn.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.web.HttpSessionRequiredException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.AbstractController;

import com.ksign.kdn.domain.LoginSession;

@SessionAttributes(value=LoginSession.SESSIONNAME,types=LoginSession.class)
public class SessionController extends AbstractController{
	protected LoginSession session;
	protected Logger logger= Logger.getLogger(SessionController.class);
	@Override
	protected ModelAndView handleRequestInternal(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		HttpSession sess = request.getSession();
		Object  obj = sess.getAttribute(LoginSession.SESSIONNAME);
		ModelAndView view = null;
		if(obj == null ){
			
			  view = new	 ModelAndView("/login/login");
			  view.addObject("messsage", "로그인이 필요합니다.");
		}else {
			session =(LoginSession)obj;
		}
		return null;
	}
	
	
	@ExceptionHandler(HttpSessionRequiredException.class)
    public ModelAndView handleHttpSessionRequiredException(Exception ex, HttpServletRequest request) throws Exception
    {
        logger.info("In the handleHttpSessionRequiredException Handler Method");
        ModelAndView  view = new	 ModelAndView("/login/login");
		  view.addObject("message", "로그인이 필요합니다.");
        return view;
    }
}
