package com.ksign.kdn.util;


public class ResultMessage {

	private String message;


	public enum Result{FAIL("nok","실패 하였습니다."),SUCESS("ok","완료 하였습니다.");
				public String message;
				private String code;
					Result(String code,String message){
						this.code =code;
						this.message = message;
					}
					
					public String getResult(){
						return this.code;
					}
					
	};

	private String goURL;
	private Type type;
	private Action action;
	private Result result;

	public enum Action{RELOAD,CLOSE,REDIRECT}
	public enum Type {add("add","등록 "),mod("mod","수정 "),del("del","삭제 ");
				private String message;
				private String code;
				Type(String code,String type){
					this.code =code;
					this.message =type;
				}
			   public Type getType(String code){
				   
				   for(Type e: Type.values()) {
					    if(e.code == code) {
					      return e;
					    }
					  }
				   return null;
			   }
	 
	
				public String getMessage(){
					return message;
				}

	} ;

	public ResultMessage(String code,Result result,String message)
	{
		this.type = getType(code);
		this.result = result;
		this.message =	this.type.getMessage() + message;
	}
	

	private Type getType(String code) {
		 for(Type e: Type.values()) {
			    if(e.code.equals(code)) {
			      return e;
			    }
			  }
		return null;
	}


	public ResultMessage(Type type,Result result)
	{
		this.type = type;
		this.result = result;
		this.message =	type.getMessage() + result.message;
	}
	
	public ResultMessage(Result result,String message){
		this.result = result;
		this.message =message ;
		}

	public ResultMessage(){};
	public static ResultMessage makeMessage(Result result,String message){
		return new ResultMessage( result,message );
	}

	public static ResultMessage  NOKMessage(String message){
		return	 new ResultMessage(  Result.FAIL,message );
	}

	public static ResultMessage OKMessage(String message){
		return  new ResultMessage( Result.SUCESS,message );
	}

	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getResult() {
		return result.getResult();
	}
	public void setResult(Result result) {
		this.result = result;
	}
	public String getGoURL() {
		return goURL;
	}
	public void setGoURL(String goURL) {
		this.goURL = goURL;
	}
	public Type getType() {
		return type;
	}
	public void setType(Type type) {
		this.type = type;
	}
	public Action getAction() {
		return action;
	}
	public void setAction(Action action) {
		this.action = action;
	}
}
