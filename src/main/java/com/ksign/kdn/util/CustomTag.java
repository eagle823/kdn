package com.ksign.kdn.util;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import javax.servlet.jsp.tagext.SimpleTagSupport;

public class CustomTag extends SimpleTagSupport {
	public static String urlEncode(String value, String charset) throws UnsupportedEncodingException {
		if(value == null || value.equals("")){
			return "";
		}
	    return URLEncoder.encode(value, charset);
	}
	
	public static String urlEncode(String value) throws UnsupportedEncodingException {
	    return URLEncoder.encode(value, "UTF-8");
	}
}
