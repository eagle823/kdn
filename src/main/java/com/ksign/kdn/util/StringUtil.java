package com.ksign.kdn.util;

import java.util.Map;

public class StringUtil {

	public static String getString(Map data, String key) {
		
		if(data != null){
			Object obj =data.get(key);
			if(obj != null){
				if (obj instanceof String) {
					return (String) obj;
				}
			}
		}
		return "";
	}

	public static String[] getList(Map data, String key, String split,
			int i) {
		String datas = getString(data,key);
		String[] value = new String [i];
		for( int j = 0 ; j<value.length ; j++){
			value[j] = "";
		}
		
		if(!data.equals("")){
	   String[] t = datas.split(split);
		   if(t.length ==i){
			   return t;
		   }
		}
		return value;
	}

	
	public static String toCheckString(String data, String va){
		return (data == null ||data.length()<1)?va:data;
			
	}
}
