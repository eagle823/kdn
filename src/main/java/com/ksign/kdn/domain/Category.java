package com.ksign.kdn.domain;

import java.io.Serializable;

public class Category implements Serializable{
	/**
	 *  
	 */
	private static final long serialVersionUID = 1L;
	
	
	public String getCategory_id() {
		return category_id;
	}
	public void setCategory_id(String category_id) {
		this.category_id = category_id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getNum() {
		return num;
	}
	public void setNum(String num) {
		this.num = num;
	}
	private String category_id ;
	private String name;
	private String  num;
	
	
	
}
