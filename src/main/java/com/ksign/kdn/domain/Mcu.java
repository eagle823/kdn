package com.ksign.kdn.domain;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import com.ksign.kdn.util.StringUtil;

public class Mcu implements Serializable {
	
	private static final long serialVersionUID = 1L;
	private String mcuNo = "";			// MCU 관리 번호
	private String manName = "";		// 등록 제조사명
	private String operatiorId = "";	// 로그인 운영자 ID
	private String lraId = "";			
	private String mcuName = "";		// MCU 명
	private String mcuManName = "";		// 제작 제조사명
	private String mcuVersion = "";		// MCI 버젼
	
	public void setMcu(Map mcu){
		mcuNo = StringUtil.getString(mcu, "MCU_NO");
		manName = StringUtil.getString(mcu, "MAN_NAME"); 
		operatiorId = StringUtil.getString(mcu, "OPERATIOR_ID");
		lraId = StringUtil.getString(mcu, "LRA_ID");
		mcuName = StringUtil.getString(mcu, "MCU_NAME");
		mcuManName = StringUtil.getString(mcu, "MCU_MAN_NAME");
		mcuVersion = StringUtil.getString(mcu, "MCU_VERSION");
	}
	
	@Override
	public String toString() {
		return "User [mcuNo=" + mcuNo + ", manName=" + manName
				+ ", operatiorId=" + operatiorId + ", lraId=" + lraId
				+ ", mcuName=" + mcuName + ", mcuManName=" + mcuManName
				+ ", mcuVersion=" + mcuVersion + "]";
	}

	public Map toMap(){
		Map  param = new HashMap();
		param.put("mcuNo", mcuNo);
		param.put("manName", manName);
		param.put("OPERATOR_ID", operatiorId);
		param.put("mcuName", mcuName);
		param.put("mcuManName", mcuManName);
		param.put("mcuVersion", mcuVersion);
		
		return param;
	}

	public String getMcuNo() {
		return mcuNo;
	}

	public void setMcuNo(String mcuNo) {
		this.mcuNo = mcuNo;
	}

	public String getManName() {
		return manName;
	}

	public void setManName(String manName) {
		this.manName = manName;
	}

	public String getOperatiorId() {
		return operatiorId;
	}

	public void setOperatiorId(String operatiorId) {
		this.operatiorId = operatiorId;
	}

	public String getLraId() {
		return lraId;
	}

	public void setLraId(String lraId) {
		this.lraId = lraId;
	}

	public String getMcuName() {
		return mcuName;
	}

	public void setMcuName(String mcuName) {
		this.mcuName = mcuName;
	}

	public String getMcuManName() {
		return mcuManName;
	}

	public void setMcuManName(String mcuManName) {
		this.mcuManName = mcuManName;
	}

	public String getMcuVersion() {
		return mcuVersion;
	}

	public void setMcuVersion(String mcuVersion) {
		this.mcuVersion = mcuVersion;
	}
	
}
