package com.ksign.kdn.domain;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Map;

public class LoginSession {

	public static final String SESSIONNAME = "KDN_LOGIN_SESSION";
	private String id;
	private String OPERATOR_ID;
	private String loginTime;
	private String OPERATOR_NAME;
	private String MAKER_NAME;
	private String TYPE;
	private String MP;
	private String CP;
	private String AP;

	public enum LoginType{LRA("lra"),MAKER("maker");
	private String code = "";

	LoginType(String code){
		this.code = code;
	}

	public String getCode(){
		return this.code;
	}
	}

	public LoginSession(Map	operator ){

		Calendar now = Calendar.getInstance();
		loginTime =new SimpleDateFormat("yyyy.MM.dd HH:mm:ss").format(now.getTime());
		id = (String)operator.get("OPERATOR_ID");
		OPERATOR_ID = (String)operator.get("OPERATOR_ID");
		OPERATOR_NAME = (String)operator.get("OPERATOR_NAME");
		MAKER_NAME 	= (String)operator.get("MAN_NAME");
		TYPE= (String) operator.get("TYPE");  
		MP= (String) operator.get("MP");
		CP= (String) operator.get("CP");
		AP= (String) operator.get("AP");
		
	}

	public static String getSessionname() {
		return SESSIONNAME;
	}

	public String getId() {
		return id;
	}

	public String getOPERATOR_ID() {
		return OPERATOR_ID;
	}

	public String getLoginTime() {
		return loginTime;
	}

	public String getOPERATOR_NAME() {
		return OPERATOR_NAME;
	}

	public String getMAKER_NAME() {
		return MAKER_NAME;
	}

	public String getTYPE() {
		return TYPE;
	}

	public String getMP() {
		return MP;
	}

	public String getCP() {
		return CP;
	}

	public String getAP() {
		return AP;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setOPERATOR_ID(String oPERATOR_ID) {
		OPERATOR_ID = oPERATOR_ID;
	}

	public void setLoginTime(String loginTime) {
		this.loginTime = loginTime;
	}

	public void setOPERATOR_NAME(String oPERATOR_NAME) {
		OPERATOR_NAME = oPERATOR_NAME;
	}

	public void setMAKER_NAME(String mAKER_NAME) {
		MAKER_NAME = mAKER_NAME;
	}

	public void setTYPE(String tYPE) {
		TYPE = tYPE;
	}

	public void setMP(String mP) {
		MP = mP;
	}

	public void setCP(String cP) {
		CP = cP;
	}

	public void setAP(String aP) {
		AP = aP;
	}



}
