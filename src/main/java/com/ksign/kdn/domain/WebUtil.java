package com.ksign.kdn.domain;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.ksign.kdn.form.SearchForm;

public class WebUtil {
	
	public static PageResult getPageResult(HttpServletRequest request,LoginSession session){
		 
		PageResult pageResult;
		 Object opage = request.getAttribute("pageResult");
		 
		 if(opage instanceof PageResult){
			 pageResult =(PageResult)opage;
		 }else{
			 pageResult = new PageResult();
		 }
		 
		 pageResult.setsDate(getRequestString(request,"sdate",""));
		 pageResult.seteDate(getRequestString(request,"edate",""));
		 pageResult.setKind(getRequestString(request,"kind",""));
		 pageResult.setSearchKeyword(getRequestString(request,"searchKeyword",""));
		 
		 pageResult.setCurrentPage(getRequestString(request,"currentPage","1"));
		 pageResult.setPageSize(getRequestInt(request,"pageSize",10));
		 pageResult.setOPERATOR_ID( session.getOPERATOR_ID());
		
		 if(getRequestString(request, "adminid", "").equals(""))
		 {
			 pageResult.setMakerName(session.getMAKER_NAME());
		 }else
		 {
			 pageResult.setMakerName(getRequestString(request, "adminid", ""));
			// session.setMAKER_NAME(pageResult.getMakerName());
		 }
		 if(getRequestString(request, "adminLoginid", "").equals("")){
			 pageResult.setOPERATOR_ID(session.getOPERATOR_ID());
		 }else{
			 pageResult.setOPERATOR_ID(getRequestString(request, "adminLoginid", ""));
		 }
		 if(getRequestString(request, "devices", "").equals(""))
		 {
			 pageResult.setDeviceName("");
		 }else
		 {
			 pageResult.setDeviceName(getRequestString(request, "devices", ""));
		 }

		 return pageResult;
	}
	
private static String getSessionString(HttpServletRequest request,String key,String value){
	HttpSession sess = request.getSession();
	Object  obj = sess.getAttribute(key);
	if(obj != null){
		return (String)obj;
	}else{
		return value;
	}
}
	
public static int getRequestInt(HttpServletRequest request,
			String key, int value) {
	 String obj = getRequestString(request,key,value+"");
	 if(obj != null){
		 
		 int i = 0;
		 try{
			 i=Integer.parseInt(obj);
		 }catch(Exception e){
			 i =value;
		 }
		 return i;
	 }else{
		 return value;
	 }
		 
	}

public static PageResult getPageResult(SearchForm search){
		
	 
		PageResult pageResult =toPageResult((Object) search.getPageResult());
	 
		 String  currentPage = search.getCurrentPage() ;
		 
		 pageResult.setCurrentPage(currentPage);
		 pageResult.setSearchKeyword(search.getSearchKeyword()) ;
		 pageResult.setKind(search.getKind()); 
		 
		 return pageResult;
	}

private static PageResult toPageResult(Object obj){
	if(obj instanceof PageResult){
		return (PageResult)obj;
	 }else{
		return  new PageResult();
	 }
}

public static String getRequestString(HttpServletRequest request,String key, String value){
	String obj = request.getParameter(key);
	 if(obj != null){
		 return  obj;
	 }else{
		 return value;
	 }
}

}
