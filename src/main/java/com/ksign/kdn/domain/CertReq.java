package com.ksign.kdn.domain;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CertReq {
private String	oidList ;
private String	chipName ;
private String	chipVersion ;
private String	deviceName ;
private String  operatorId;
private String  man_name;
private String MAKER_NAME = "";
private String OPERATOR_ID = "";


public String getOidList() {
	return oidList;
}
public String getChipName() {
	return chipName;
}
public String getChipVersion() {
	return chipVersion;
}
public String getDeviceName() {
	return deviceName;
}
public String getOperatorId() {
	return operatorId;
}
public String getMan_name() {
	return man_name;
}
public void setOidList(String oidList) {
	this.oidList = oidList;
}
public void setChipName(String chipName) {
	this.chipName = chipName;
}
public void setChipVersion(String chipVersion) {
	this.chipVersion = chipVersion;
}
public void setDeviceName(String deviceName) {
	this.deviceName = deviceName;
}
public void setOperatorId(String operatorId) {
	this.operatorId = operatorId;
}
public void setMan_name(String man_name) {
	this.man_name = man_name;
}

public Map toParams() {
	Map map = new HashMap();
	map.put("oidList", oidList);
	map.put("chipName", chipName);
	map.put("chipVersion", chipVersion);
	map.put("deviceName", deviceName);
	map.put("operatorId", operatorId);
	map.put("man_name", man_name);
	return map;
}


public String[] getIssueInfo(){
	String[]   list = null;
	if(oidList != null && oidList.length()>0){
		list =oidList.split("@@"); 
	}
	
	return list;
}

@Override
public String toString() {
	return "CertReq [oidList=" + oidList + ", chipName=" + chipName
			+ ", chipVersion=" + chipVersion + ", deviceName=" + deviceName
			+ ", operatorId=" + operatorId + ", man_name=" + man_name
			+ "]";
}
public String getOPERATOR_ID() {
	return OPERATOR_ID;
}
public void setOPERATOR_ID(String oPERATOR_ID) {
	OPERATOR_ID = oPERATOR_ID;
}
public String getMAKER_NAME() {
	return MAKER_NAME;
}
public void setMAKER_NAME(String mAKER_NAME) {
	MAKER_NAME = mAKER_NAME;
}
}
