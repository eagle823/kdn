package com.ksign.kdn.domain;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import com.ksign.kdn.util.StringUtil;

public class Firmware implements Serializable {
	
	private static final long serialVersionUID = 1L;
	private String fwNo = "";			// 펌웨어 관리 번호
	private String manName = "";		// 등록 제조사명
	private String operatiorId = "";	// 로그인 운영자 ID
	private String lraId = "";			
	private String deviceType = "";		// 기기 종류
	private String fwName = "";			// 펌웨어 명
	private String fwApplyDate = "";	// 펌웨어 도입일
	private String firmwareManName = "";	// 제작 제조사명
	
	public void setFirmware(Map firmware){
		fwNo = StringUtil.getString(firmware, "FW_NO");
		manName = StringUtil.getString(firmware, "MAN_NAME"); 
		operatiorId = StringUtil.getString(firmware, "OPERATIOR_ID");
		lraId = StringUtil.getString(firmware, "LRA_ID");
		deviceType = StringUtil.getString(firmware, "DEVICE_TYPE");
		fwName = StringUtil.getString(firmware, "FW_NAME");
		fwApplyDate = StringUtil.getString(firmware, "FW_APPLY_DATE");
		firmwareManName = StringUtil.getString(firmware, "FIRMWARE_MAN_NAME");
	}
	
	@Override
	public String toString() {
		return "User [fwNo=" + fwNo + ", manName=" + manName
				+ ", operatiorId=" + operatiorId + ", lraId=" + lraId
				+ ", deviceType=" + deviceType + ", fwName=" + fwName
				+ ", fwApplyDate=" + fwApplyDate + ", firmwareManName=" + firmwareManName +"]";
	}

	public Map toMap(){
		Map  param = new HashMap();
		param.put("fwNo", fwNo);
		param.put("manName", manName);
		param.put("OPERATOR_ID", operatiorId);
		param.put("deviceType", deviceType);
		param.put("fwName", fwName);
		param.put("fwApplyDate", fwApplyDate);
		param.put("firmwareManName", firmwareManName);
		
		return param;
	}

	public String getFwNo() {
		return fwNo;
	}

	public void setFwNo(String fwNo) {
		this.fwNo = fwNo;
	}

	public String getManName() {
		return manName;
	}

	public void setManName(String manName) {
		this.manName = manName;
	}

	public String getOperatiorId() {
		return operatiorId;
	}

	public void setOperatiorId(String operatiorId) {
		this.operatiorId = operatiorId;
	}

	public String getLraId() {
		return lraId;
	}

	public void setLraId(String lraId) {
		this.lraId = lraId;
	}

	public String getDeviceType() {
		return deviceType;
	}

	public void setDeviceType(String deviceType) {
		this.deviceType = deviceType;
	}

	public String getFwName() {
		return fwName;
	}

	public void setFwName(String fwName) {
		this.fwName = fwName;
	}

	public String getFwApplyDate() {
		return fwApplyDate;
	}

	public void setFwApplyDate(String fwApplyDate) {
		this.fwApplyDate = fwApplyDate;
	}

	public String getFirmwareManName() {
		return firmwareManName;
	}

	public void setFirmwareManName(String firmwareManName) {
		this.firmwareManName = firmwareManName;
	}
	
}
