package com.ksign.kdn.domain;

import java.io.Serializable;

public class Audit implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String event_no; 
	private String  type; 
	private String  type_sub; 

	private String  date_; 
	private String  operator_id; 
	private String  brief; 
	private String  detail; 
	private String  signature; 
	private String  ca_cert_serial_no; 
	private String  status; 
	private String  client_ip;
	private int[] requestStatistics = null;
	private int downloadCount = 0;





	public static Audit getInstance(){

		return new Audit();
	}

	public int getDownloadCount() {
		return downloadCount;
	}


	public void setDownloadCount(int downloadCount) {
		this.downloadCount = downloadCount;
	}



	public int[] getRequestStatistics() {
		return this.requestStatistics;
	}
	public void setRequestStatistics(int[] arg) {
		this.requestStatistics = arg;
	}
	public String getType_sub() {
		return type_sub;
	}

	public void setType_sub(String type_sub) {
		this.type_sub = type_sub;
	}

	public String getEvent_no() {
		return event_no;
	}
	public void setEvent_no(String event_no) {
		this.event_no = event_no;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getDate_() {
		return date_;
	}
	public void setDate_(String date_) {
		this.date_ = date_;
	}
	public String getOperator_id() {
		return operator_id;
	}
	public void setOperator_id(String operator_id) {
		this.operator_id = operator_id;
	}
	public String getBrief() {
		return brief;
	}
	public void setBrief(String brief) {
		this.brief = brief;
	}
	public String getDetail() {
		return detail;
	}
	public void setDetail(String detail) {
		this.detail = detail;
	}
	public String getSignature() {
		return signature;
	}
	public void setSignature(String signature) {
		this.signature = signature;
	}
	public String getCa_cert_serial_no() {
		return ca_cert_serial_no;
	}
	public void setCa_cert_serial_no(String ca_cert_serial_no) {
		this.ca_cert_serial_no = ca_cert_serial_no;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getClient_ip() {
		return client_ip;
	}
	public void setClient_ip(String client_ip) {
		this.client_ip = client_ip;
	}


	private void setDetail(){
		
		
		if(this.getType() == null||this.getType_sub() == null){
			return ;
		}
		
		if (this.getType().equals("request")) {
			type = "CMP";
			if ( this.getType_sub().equals("request")) {
				detail = detail + "인증서 발급 요청 SERIAL[" + this.ca_cert_serial_no  + "] / DATE[" +this.date_   + "] / IP[" + this.client_ip + "]";
				brief = "Certificate Issue Request";
			} else if ( this.getType_sub().equals("reissue")) { 
				detail = detail + "인증서 재발급 요청 SERIAL:[" +this.ca_cert_serial_no  + "] / DATE[" +this.date_   + "] / IP[" + this.client_ip + "]";
				brief = "Certificate ReIssue Request";
			} else if ( this.getType_sub().equals("update")) {
				detail = detail + "인증서 갱신 요청 SERIAL:[" +this.ca_cert_serial_no  + "] / DATE[" +this.date_   + "] / IP[" + this.client_ip + "]";
				brief = "Certificate Update Request";
			} else if ( this.getType_sub().equals("revocate")) {
				detail = detail + "인증서 폐지 요청 SERIAL:[" +this.ca_cert_serial_no  + "] / DATE[" +this.date_   + "] / IP[" + this.client_ip + "]";
				brief = "Certificate Revocate Request";
			} else if ( this.getType_sub().equals("total")) {
				detail = detail + "인증서 발급 요청 결과  요청건수:[" + getRequestStatistics()[0] + "], 성공:[" + getRequestStatistics()[1] + "], 실패:[" + getRequestStatistics()[2] + "] / IP[" + this.client_ip + "]";
				brief = "Certificate Issue Request";
			} else if ( this.getType_sub().equals("reissuetotal")) {
				detail = detail + "인증서 재발급 요청 결과  요청건수:[" + getRequestStatistics()[0] + "], 성공:[" + getRequestStatistics()[1] + "], 실패:[" + getRequestStatistics()[2] + "] / IP[" + this.client_ip + "]";
				brief = "Certificate ReIssue Request";
			} else if ( this.getType_sub().equals("updatetotal")) {
				detail = detail + "인증서 갱신 요청 결과  요청건수:[" + getRequestStatistics()[0] + "], 성공:[" + getRequestStatistics()[1] + "], 실패:[" + getRequestStatistics()[2] + "] / IP[" + this.client_ip + "]";
				brief = "Certificate Update Request";
			} else if ( this.getType_sub().equals("revocatetotal")) {
				detail = detail + "인증서 폐지 요청 결과  요청건수:[" + getRequestStatistics()[0] + "], 성공:[" + getRequestStatistics()[1] + "], 실패:[" + getRequestStatistics()[2] + "] / IP[" + this.client_ip + "]";
				brief = "Certificate Revocate Request";
			}
		} else if (this.getType().equals("download")) {
			type = "CMP";
			detail = detail + "인증서 다운로드 : Operator[" + this.operator_id + "] / COUNT[" + getDownloadCount() + "] / IP[" + this.client_ip + "]";
			brief = "Certificate Download";
		} else if (this.getType().equals("login")) {
			if ( this.getType_sub().equals("login")) {
				type = "Login";
				detail = detail + "로그인 시도:[" + this.operator_id + "] / 시간 : [" +this.date_   + "] / IP[" + this.client_ip + "]";
				brief = "Operator Login";
			} else if ( this.getType_sub().equals("logout")) {
				type = "Logout";
				detail = detail + "로그아웃:[" + this.operator_id + "] / 시간 : [" +this.date_   + "] / IP[" + this.client_ip + "]";
				brief = "Operator Logout";
			}
		}
	}
}
