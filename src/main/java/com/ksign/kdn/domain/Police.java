package com.ksign.kdn.domain;

import java.util.HashMap;
import java.util.Map;

public class Police {
	private String companyName = "" ;
	private String device = "" ;
	private String firmware = "" ;
	private String mcu = "" ;
	private String police = "" ;
	private String OPERATOR_ID="";
	private String oper ="";
	private String name ="";
	private String value ="";
	private String parent ="";
	
	public String getCompanyName() {
		return companyName;
	}
	public String getDevice() {
		return device;
	}
	public String getFirmware() {
		return firmware;
	}
	public String getMcu() {
		return mcu;
	}
	public void setMcu(String mcu) {
		this.mcu = mcu;
	}
	public String getPolice() {
		return police;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public void setDevice(String device) {
		this.device = device;
	}
	public void setFirmware(String firmware) {
		this.firmware = firmware;
	}
	public void setPolice(String police) {
		this.police = police;
	}
	public String getOPERATOR_ID() {
		return OPERATOR_ID;
	}
	public void setOPERATOR_ID(String oPERATOR_ID) {
		OPERATOR_ID = oPERATOR_ID;
	}
	
	
	public Map toParameter() {
		Map param =new HashMap();
		param.put("makerName", companyName);
		param.put("device", device);
		param.put("firmware", firmware);
		param.put("mcu", mcu);
		param.put("police", police);
		param.put("OPERATOR_ID", OPERATOR_ID);
		param.put("currentPage", "1");
		param.put("pageSize", "0");
		param.put("value", value);
		param.put("parent", parent);
		return param;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getOper() {
		return oper;
	}
	public void setOper(String oper) {
		this.oper = oper;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public String getParent() {
		return parent;
	}
	public void setParent(String parent) {
		this.parent = parent;
	}
}
