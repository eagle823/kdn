package com.ksign.kdn.domain;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import com.ksign.kdn.util.StringUtil;

public class Device implements Serializable {
	
	private static final long serialVersionUID = 1L;
	private String dModelNo = "";		// 기기 관리 번호
	private String manName = "";		// 등록 제조사명
	private String operatiorId = "";	// 로그인 운영자 ID
	private String lraId = "";			
	private String deviceType = "";		// 기기 종류
	private String deviceName = "";		// 기기명(모델명)
	private String deviceManName = "";	// 제작 제조사명
	private String os = "";				// 탑제 os
	
	public void setDevic(Map devic){
		dModelNo = StringUtil.getString(devic, "D_MODEL_NO");
		manName = StringUtil.getString(devic, "MAN_NAME"); 
		operatiorId = StringUtil.getString(devic, "OPERATIOR_ID");
		lraId = StringUtil.getString(devic, "LRA_ID");
		deviceType = StringUtil.getString(devic, "DEVICE_TYPE");
		deviceName = StringUtil.getString(devic, "DEVICE_NAME");
		deviceManName = StringUtil.getString(devic, "DEVICE_MAN_NAME");
		os = StringUtil.getString(devic, "OS");
	}
	
	@Override
	public String toString() {
		return "User [dModelNo=" + dModelNo + ", manName=" + manName
				+ ", operatiorId=" + operatiorId + ", lraId=" + lraId
				+ ", deviceType=" + deviceType + ", deviceName=" + deviceName
				+ ", deviceManName=" + deviceManName + ", os=" + os + "]";
	}

	public Map toMap(){
		Map  param = new HashMap();
		param.put("dModelNo", dModelNo);
		param.put("manName", manName);
		param.put("OPERATOR_ID", operatiorId);
		param.put("deviceType", deviceType);
		param.put("deviceName", deviceName);
		param.put("deviceManName", deviceManName);
		param.put("os", os);
		
		return param;
	}
	
	public String getdModelNo() {
		return dModelNo;
	}

	public void setdModelNo(String dModelNo) {
		this.dModelNo = dModelNo;
	}

	public String getManName() {
		return manName;
	}

	public void setManName(String manName) {
		this.manName = manName;
	}

	public String getOperatiorId() {
		return operatiorId;
	}

	public void setOperatiorId(String operatiorId) {
		this.operatiorId = operatiorId;
	}

	public String getLraId() {
		return lraId;
	}

	public void setLraId(String lraId) {
		this.lraId = lraId;
	}

	public String getDeviceType() {
		return deviceType;
	}

	public void setDeviceType(String deviceType) {
		this.deviceType = deviceType;
	}

	public String getDeviceName() {
		return deviceName;
	}

	public void setDeviceName(String deviceName) {
		this.deviceName = deviceName;
	}

	public String getDeviceManName() {
		return deviceManName;
	}

	public void setDeviceManName(String deviceManName) {
		this.deviceManName = deviceManName;
	}

	public String getOs() {
		return os;
	}

	public void setOs(String os) {
		this.os = os;
	}
	
}
