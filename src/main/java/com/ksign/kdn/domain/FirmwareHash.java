package com.ksign.kdn.domain;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import com.ksign.kdn.util.StringUtil;

public class FirmwareHash implements Serializable {
	
	private static final long serialVersionUID = 1L;
	private String fwHashNo = "";		// 펌웨어 Hash 관리 번호
	private String manName = "";		// 등록 제조사명
	private String operatiorId = "";	// 로그인 운영자 ID
	private String lraId = "";			
	private String fwNo = "";			// 펌웨어 관리 번호
	private String fwName = "";			// 펌웨어 명
	private String startDate = "";		// 펌웨어 Hash 적용일
	private String fwHash = "";			// 펌웨어 Hash 값
	
	public void setFirmwareHash(Map firmwareHash){
		fwHashNo = StringUtil.getString(firmwareHash, "FW_HASH_NO");
		manName = StringUtil.getString(firmwareHash, "MAN_NAME"); 
		operatiorId = StringUtil.getString(firmwareHash, "OPERATOR_ID");
		lraId = StringUtil.getString(firmwareHash, "LRA_ID");
		fwNo = StringUtil.getString(firmwareHash, "FW_NO");
		fwName = StringUtil.getString(firmwareHash, "FW_NAME");
		startDate = StringUtil.getString(firmwareHash, "START_DATE");
		fwHash = StringUtil.getString(firmwareHash, "FW_HASH");
	}
	
	@Override
	public String toString() {
		return "User [fwHashNo=" + fwHashNo + ", manName=" + manName
				+ ", operatiorId=" + operatiorId + ", lraId=" + lraId
				+ ", fwName=" + fwName + ", fwName=" + fwName
				+ ", startDate=" + startDate + ", fwHash=" + fwHash + "]";
	}

	public Map toMap(){
		Map  param = new HashMap();
		param.put("fwHashNo", fwHashNo);
		param.put("manName", manName);
		param.put("OPERATOR_ID", operatiorId);
		param.put("fwNo", fwNo);
		param.put("fwName", fwName);
		param.put("startDate", startDate);
		param.put("fwHash", fwHash);
		
		return param;
	}

	public String getFwHashNo() {
		return fwHashNo;
	}

	public void setFwHashNo(String fwHashNo) {
		this.fwHashNo = fwHashNo;
	}

	public String getManName() {
		return manName;
	}

	public void setManName(String manName) {
		this.manName = manName;
	}

	public String getOperatiorId() {
		return operatiorId;
	}

	public void setOperatiorId(String operatiorId) {
		this.operatiorId = operatiorId;
	}

	public String getLraId() {
		return lraId;
	}

	public void setLraId(String lraId) {
		this.lraId = lraId;
	}

	public String getFwNo() {
		return fwNo;
	}

	public void setFwNo(String fwNo) {
		this.fwNo = fwNo;
	}

	public String getFwName() {
		return fwName;
	}

	public void setFwName(String fwName) {
		this.fwName = fwName;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getFwHash() {
		return fwHash;
	}

	public void setFwHash(String fwHash) {
		this.fwHash = fwHash;
	}

}
