package com.ksign.kdn.domain;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PageResult {

	private List<?> resultList;
	
	private int pageSize = 10;
	
    private Map params;
	private int pageGroupSize = 10;
	private int nextPage   = 0;
	private int currentPage = 1;
	private String kind;
	private String searchKeyword;
	private int totalCount= 0;	
	private String pagingPrint; 
	private String link = "<a class=\"paging\" href=\"javascript:goPage('";
	private int endRow;
	private int startRow;
	private String OPERATOR_ID;
	private String makerName;
	
	private String  pageResult =""; // bas64 object
	private String  currentpage ="1";// String currentPage
	
	private String sDate="";
	private String eDate="";
	
	private String deviceName = "";
	
	public String getDeviceName() {
		return deviceName;
	}

	public void setDeviceName(String deviceName) {
		this.deviceName = deviceName;
	}

	public void appendParam(String key,String value){
		add(key,value);
	}
	
	private void add(String key, String value) {
		if( params ==  null){
			setPamameters();
		}
		params.put(key, value);
	}

	
	public synchronized void  setPamameters(){
		params = new HashMap();
	}
	
	public String getOPERATOR_ID() {
		return OPERATOR_ID;
	}

	public void setOPERATOR_ID(String oPERATOR_ID) {
		OPERATOR_ID = oPERATOR_ID;
	}

	
	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}
	
	public String getPageResult() {
		return pageResult;
	}

	public void setPageResult(String pageResult) {
		this.pageResult = pageResult;
	}

	public String getCurrentpage() {
		return currentpage;
	}

	

	public PageResult(){}
	
	public PageResult(int totalPage,String currentPage){
		this.totalCount = totalPage;
		setCurrentPage(currentPage);
	}
	
	public List<?> getResultList() {
		return resultList;
	}

	public void setResultList(List<?> resultList) {
		this.resultList = resultList;
	}

	public int getCurrentPage() {
		return currentPage;
	}

	public void setCurrentPage(int currentPage) {
		this.currentPage = currentPage;
	}

	
	public void setCurrentPage(String currentPage) {
		if(currentPage != null){
			try{
			this.currentPage = Integer.parseInt(currentPage);
			}catch(NumberFormatException e){
				this.currentPage =1;
			}
		}
	}
	public String getKind() {
		return kind;
	}

	public void setKind(String kind) {
		this.kind = kind;
	}

	public String getSearchKeyword() {
		return searchKeyword;
	}

	public void setSearchKeyword(String searchKeyword) {
		this.searchKeyword = searchKeyword;
	}

	public int getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(int totalCount) {
		this.totalCount = totalCount;
	}
	
	public void setTotalCount(Object totalCount) {
		try{
			 
				this.totalCount = Integer.parseInt(totalCount.toString());
			 
		}catch(Exception e){
			this.totalCount =0;
		}
		 
	}
	
	public String getPagingPrint(){
		 

		StringBuffer buffer = new StringBuffer();
		
	int	 numPageGroup = (int) Math.ceil((double)currentPage/pageGroupSize);

		int pageGroupCount = totalCount/(pageSize*pageGroupSize)+( totalCount % (pageSize*pageGroupSize) == 0 ? 0 : 1);
		int startPage = pageGroupSize*(numPageGroup-1)+1;
		int endPage= startPage + pageGroupSize-1;
		
		int pageCount =0;			 
		pageCount =totalCount / pageSize + ( totalCount % pageSize == 0 ? 0 : 1);
		
		if(endPage > pageCount)endPage = pageCount;
		
		if(totalCount > 0){
			if(numPageGroup > 1){
				buffer.append(link).append((numPageGroup-2)*pageGroupSize+1 ).append("')\">[이전]</a>").append("&nbsp;");  
			}

			for(int i =startPage ; i <= endPage; i ++){
				if(currentPage== i){
					buffer.append("<font color='#708090'>").append(i).append("</font>");
				}else{
					buffer.append(link).append(i).append("')\">[<font color='#191970'>").append(i).append("</font>]</a>");
				}
				buffer.append("&nbsp;");
			}
			if(numPageGroup < pageGroupCount){
				buffer.append(link).append(numPageGroup*pageGroupSize+1).append("')\">[다음]</a>");
			}



		}

		return buffer.toString();
	}

	public int getEndRow(){
		 endRow =(currentPage * pageSize>totalCount?totalCount:currentPage * pageSize);
		return endRow ;
	}
	
	public int getStartRow(){
	 startRow = ((currentPage - 1) * pageSize + 1);
	 return startRow ;
	}
	
	public Map getSearchParam() {
	 
		add("kind", this.kind);
		add("endRow", this.getEndRow()+"");
		add("startRow", this.getStartRow()+"");
		add("searchKeyword", this.searchKeyword);
		add("currentPage", this.currentPage+"");
		add("pageSize", this.pageSize+"");
		add("OPERATOR_ID", this.OPERATOR_ID);
		add("makerName", this.makerName);
		return  params;
	}
	
	public HashMap<String ,Object> toHashMap(){
		HashMap<String,Object> map= new HashMap<String, Object>();
		map.put("page", this);
		return map;
	}

	public void setPageSize(String pageSize) {
		try{
			this.pageSize = Integer.parseInt(pageSize);
			}catch(NumberFormatException e){
				this.pageSize =10;
			}
		
	}

	public String getMakerName() {
		return makerName;
	}

	public void setMakerName(String makerName) {
		this.makerName = makerName;
	}

	public String getsDate() {
		return sDate;
	}

	public void setsDate(String sDate) {
		this.sDate = sDate;
	}

	public String geteDate() {
		return eDate;
	}

	public void seteDate(String eDate) {
		this.eDate = eDate;
	}
	
	
}
