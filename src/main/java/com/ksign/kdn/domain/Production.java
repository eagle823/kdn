package com.ksign.kdn.domain;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import com.ksign.kdn.util.StringUtil;

public class Production  implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String   product_id; 
	 private String  product_name; 
	 private String  product_address; 
	 private String  product_ssn; 
	 private String  product_model; 
	 private String  product_operator; 
	 private String  product_oper_email; 
	 private String  phone;
	 private String homepage ="";
	 private String fax1 ="";
	 private String fax2 ="";
	 private String fax3 ="";
	 private String phone1 ="";
	 private String phone2 ="";
	 private String phone3 ="";
	 private String ceoname ="";
	 private String product_ssn1 ="";
	 private String product_ssn2 ="";
	 private String product_ssn3 ="";
	 private String OPERATOR_ID ="";
	 private String[] delList;
	 public Production(){}
	 
	 
	public Production(Map param) {
		product_name  =  StringUtil.getString(param,"MAKER_NAME") ;
		String[] t  = StringUtil.getList(param,"BRN","-",3) ;
		product_ssn1 = t[0];
		product_ssn2 = t[1];
		product_ssn3 = t[2];
		ceoname = StringUtil.getString(param,"CEO") ;
		product_address = StringUtil.getString(param,"ADRESS") ;
		homepage = StringUtil.getString(param,"HOMEPAGE") ;
		  t  = StringUtil.getList(param,"PHONE","-",3) ;
		phone1 = t[0];
		phone2 = t[1];
		phone3 = t[2];
		  t  = StringUtil.getList(param,"FAX","-",3) ;
			fax1 = t[0];
			fax2 = t[1];
			fax3 = t[2];
	}
	
	
	
	public String getProduct_id() {
		return product_id;
	}
	public void setProduct_id(String product_id) {
		this.product_id = product_id;
	}
	public String getProduct_name() {
		return product_name;
	}
	public void setProduct_name(String product_name) {
		this.product_name = product_name;
	}
	public String getProduct_address() {
		return product_address;
	}
	public void setProduct_address(String product_address) {
		this.product_address = product_address;
	}
	public String getProduct_ssn() {
		return product_ssn;
	}
	public void setProduct_ssn(String product_ssn) {
		this.product_ssn = product_ssn;
	}
	public String getProduct_model() {
		return product_model;
	}
	public void setProduct_model(String product_model) {
		this.product_model = product_model;
	}
	public String getProduct_operator() {
		return product_operator;
	}
	public void setProduct_operator(String product_operator) {
		this.product_operator = product_operator;
	}
	public String getProduct_oper_email() {
		return product_oper_email;
	}
	public void setProduct_oper_email(String product_oper_email) {
		this.product_oper_email = product_oper_email;
	}
	 
	public String getHomepage() {
		return homepage;
	}
	public String getFax1() {
		return fax1;
	}
	public String getFax2() {
		return fax2;
	}
	public String getFax3() {
		return fax3;
	}
	public String getCeoname() {
		return ceoname;
	}
	public String getProduct_ssn1() {
		return product_ssn1;
	}
	public String getProduct_ssn2() {
		return product_ssn2;
	}
	public String getProduct_ssn3() {
		return product_ssn3;
	}
	public void setHomepage(String homepage) {
		this.homepage = homepage;
	}
	public void setFax1(String fax1) {
		this.fax1 = fax1;
	}
	public void setFax2(String fax2) {
		this.fax2 = fax2;
	}
	public void setFax3(String fax3) {
		this.fax3 = fax3;
	}
	public void setCeoname(String ceoname) {
		this.ceoname = ceoname;
	}
	public void setProduct_ssn1(String product_ssn1) {
		this.product_ssn1 = product_ssn1;
	}
	public void setProduct_ssn2(String product_ssn2) {
		this.product_ssn2 = product_ssn2;
	}
	public void setProduct_ssn3(String product_ssn3) {
		this.product_ssn3 = product_ssn3;
	}
	public String getOPERATOR_ID() {
		return OPERATOR_ID;
	}
	public void setOPERATOR_ID(String oPERATOR_ID) {
		OPERATOR_ID = oPERATOR_ID;
	}
	
	public Map toParamter() {
		Map param = new HashMap();
		
		param.put("OPERATOR_ID", OPERATOR_ID );
		param.put("product_name",product_name  );
		param.put("product_ssn", product_ssn1+"-"+ product_ssn2+"-"+product_ssn3);
		param.put("product_address",  product_address);
		param.put("ceoname", ceoname );
		param.put("fax", fax1+"-"+fax2+"-"+fax3 );
		param.put("homepage",homepage );
		param.put("phone", phone1+"-"+phone2+"-"+phone3 );
		 return param;
	}
	@Override
	public String toString() {
		return "Production [product_id=" + product_id + ", product_name="
				+ product_name + ", product_address=" + product_address
				+ ", product_ssn=" + product_ssn + ", product_model="
				+ product_model + ", product_operator=" + product_operator
				+ ", product_oper_email=" + product_oper_email
				+ ", phone=" +phone + ", homepage="
				+ homepage + ", fax1=" + fax1 + ", fax2=" + fax2 + ", fax3="
				+ fax3 + ", ceoname=" + ceoname + ", product_ssn1="
				+ product_ssn1 + ", product_ssn2=" + product_ssn2
				+ ", product_ssn3=" + product_ssn3 + ", OPERATOR_ID="
				+ OPERATOR_ID + "]";
	}
	public String getPhone3() {
		return phone3;
	}
	public void setPhone3(String phone3) {
		this.phone3 = phone3;
	}
	public String getPhone2() {
		return phone2;
	}
	public void setPhone2(String phone2) {
		this.phone2 = phone2;
	}
	public String getPhone1() {
		return phone1;
	}
	public void setPhone1(String phone1) {
		this.phone1 = phone1;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}


	public String[] getDelList() {
		return delList;
	}


	public void setDelList(String[] delList) {
		this.delList = delList;
	}
	 
	 
}
