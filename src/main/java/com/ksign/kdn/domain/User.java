package com.ksign.kdn.domain;

import java.io.Serializable;
import java.util.Map;

import com.ksign.kdn.util.StringUtil;

public class User implements Serializable{
	 /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String companyName	 = "" ;
	private String userId	 = "" ;
	private String userName	 = "" ;
	private String region	 = "" ;
	private String mobile1	 = "" ;
	private String mobile2	 = "" ;
	private String mobile3	 = "" ;
	private String phone1	 = "" ;
	private String phone2	 = "" ;
	private String phone3	 = "" ;
	private String corp	 = "" ;
	private String fax1 	 = "" ;
	private String fax2	 = "" ;
	private String fax3	 = "" ;
	private String email	 = "" ;
	private String position	 = "" ;
	private String grade	 = "" ;
	private String password	 = "" ;
	private String maker	 = "" ;
	private String device	 = "" ;
	private String cert	 = "" ;
	private String auth	= "" ;
	
	
	public void setUser(Map user){
		//OPERATOR_ID$OPERATOR_NAME$ ADDRESS$CELL_PHONE$PHONE$DEPARTMENT$FAX$OPER_POSITION$OPER_TITLE$MAKER_NAME$OPERATOR_PW$MP$CP$AP$"
	
	userId  =  StringUtil.getString(user,"OPERATOR_ID") ;
	userName = StringUtil.getString(user,"OPERATOR_NAME") ;
	region = StringUtil.getString(user,"ADDRESS") ;
	String[] t = StringUtil.getList(user,"CELL_PHONE","-",3) ;
	mobile1 =t[0];
	mobile2 =t[1];
	mobile3 =t[2];
	  t = StringUtil.getList(user,"PHONE","-",3) ;
	phone1 =t[0];
	phone2 =t[1];
	phone3 =t[2];
	t = StringUtil.getList(user,"FAX","-",3) ;
	fax1 =t[0];
	fax2 =t[1];
	fax3 =t[2];
	email =  StringUtil.getString(user,"EMAIL") ;
	corp = StringUtil.getString(user,"DEPARTMENT") ;
	position= StringUtil.getString(user,"OPER_TITLE");
	maker =StringUtil.getString(user,"MAKER_NAME");
	password =StringUtil.getString(user,"OPERATOR_PW");
	grade =StringUtil.getString(user,"OPER_POSITION");
	device =StringUtil.getString(user,"MP");
	cert =StringUtil.getString(user,"CP");
	auth =StringUtil.getString(user,"AP");
	 
	}
	
	public String getGrade() {
		return grade;
	}

	public void setGrade(String grade) {
		this.grade = grade;
	}

	@Override
	public String toString() {
		return "User [companyName=" + companyName + ", userId=" + userId
				+ ", userName=" + userName + ", region=" + region
				+ ", mobile1=" + mobile1 + ", mobile2=" + mobile2
				+ ", mobile3=" + mobile3 + ", phone1=" + phone1 + ", phone2="
				+ phone2 + ", phone3=" + phone3 + ", corp=" + corp + ", fax1="
				+ fax1 + ", fax2=" + fax2 + ", fax3=" + fax3 + ", email="
				+ email + ", position=" + position + ", password=" + password
				+ ", maker=" + maker + ", device=" + device + ", cert=" + cert
				+ ", auth=" + auth + "]";
	}
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getRegion() {
		return region;
	}
	public void setRegion(String region) {
		this.region = region;
	}
	public String getMobile1() {
		return mobile1;
	}
	public void setMobile1(String mobile1) {
		this.mobile1 = mobile1;
	}
	public String getMobile2() {
		return mobile2;
	}
	public void setMobile2(String mobile2) {
		this.mobile2 = mobile2;
	}
	public String getMobile3() {
		return mobile3;
	}
	public void setMobile3(String mobile3) {
		this.mobile3 = mobile3;
	}
	public String getPhone1() {
		return phone1;
	}
	public void setPhone1(String phone1) {
		this.phone1 = phone1;
	}
	public String getPhone2() {
		return phone2;
	}
	public void setPhone2(String phone2) {
		this.phone2 = phone2;
	}
	public String getPhone3() {
		return phone3;
	}
	public void setPhone3(String phone3) {
		this.phone3 = phone3;
	}
	public String getCorp() {
		return corp;
	}
	public void setCorp(String corp) {
		this.corp = corp;
	}
	public String getFax1() {
		return fax1;
	}
	public void setFax1(String fax1) {
		this.fax1 = fax1;
	}
	public String getFax2() {
		return fax2;
	}
	public void setFax2(String fax2) {
		this.fax2 = fax2;
	}
	public String getFax3() {
		return fax3;
	}
	public void setFax3(String fax3) {
		this.fax3 = fax3;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPosition() {
		return position;
	}
	public void setPosition(String position) {
		this.position = position;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getMaker() {
		return maker;
	}
	public void setMaker(String maker) {
		this.maker = maker;
	}
	public String getDevice() {
		return device;
	}
	public void setDevice(String device) {
		this.device = device;
	}
	public String getCert() {
		return cert;
	}
	public void setCert(String cert) {
		this.cert = cert;
	}
	public String getAuth() {
		return auth;
	}
	public void setAuth(String auth) {
		this.auth = auth;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}
