package com.ksign.kdn.domain;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import com.ksign.kdn.util.StringUtil;

public class Policy implements Serializable {
	
	private static final long serialVersionUID = 1L;
	private String dSerialNo = "";		// 기기 정책 관리 번호
	private String manName = "";		// 등록 제조사명
	private String operatiorId = "";	// 로그인 운영자 ID
	private String lraId = "";			
	private String dModelNo = "";		// 기기 모델 관리 번호
	private String deviceName = "";		// 기기 명
	private String fwNo = "";			// 펌웨어 관리 번호
	private String fwName = "";			// 펌웨어 명
	private String mcuNo = "";			// MCU 관리 번호
	private String mcuName = "";		// MCU 명
	private String startDate = "";		// 정책적용일
	private String dPolicyCert = "";	// 정책명
	
	public void setPolicy(Map policy){
		dSerialNo = StringUtil.getString(policy, "D_SERIAL_NO");
		manName = StringUtil.getString(policy, "MAN_NAME"); 
		operatiorId = StringUtil.getString(policy, "OPERATOR_ID");
		lraId = StringUtil.getString(policy, "LRA_ID");
		dModelNo = StringUtil.getString(policy, "D_MODEL_NO");
		deviceName = StringUtil.getString(policy, "DEVICE_NAME");
		fwNo = StringUtil.getString(policy, "FW_NO");
		fwName = StringUtil.getString(policy, "FW_NAME");
		mcuNo = StringUtil.getString(policy, "MCU_NO");
		mcuName = StringUtil.getString(policy, "MCU_NAME");
		startDate = StringUtil.getString(policy, "START_DATE");
		dPolicyCert = StringUtil.getString(policy, "D_POLICY_CERT");
	}
	
	@Override
	public String toString() {
		return "User [dSerialNo=" + dSerialNo + ", manName=" + manName
				+ ", operatiorId=" + operatiorId + ", lraId=" + lraId
				+ ", dModelNo=" + dModelNo + ", deviceName=" + deviceName
				+ ", fwName=" + fwName + ", fwName=" + fwName
				+ ", mcuNo=" + mcuNo + ", mcuName=" + mcuName
				+ ", startDate=" + startDate + ", dPolicyCert=" + dPolicyCert + "]";
	}

	public Map toMap(){
		Map  param = new HashMap();
		param.put("dSerialNo", dSerialNo);
		param.put("manName", manName);
		param.put("OPERATOR_ID", operatiorId);
		param.put("dModelNo", dModelNo);
		param.put("deviceName", deviceName);
		param.put("fwNo", fwNo);
		param.put("fwName", fwName);
		param.put("mcuNo", mcuNo);
		param.put("mcuName", mcuName);
		param.put("startDate", startDate);
		param.put("dPolicyCert", dPolicyCert);
		
		return param;
	}

	public String getdSerialNo() {
		return dSerialNo;
	}

	public void setdSerialNo(String dSerialNo) {
		this.dSerialNo = dSerialNo;
	}

	public String getManName() {
		return manName;
	}

	public void setManName(String manName) {
		this.manName = manName;
	}

	public String getOperatiorId() {
		return operatiorId;
	}

	public void setOperatiorId(String operatiorId) {
		this.operatiorId = operatiorId;
	}

	public String getLraId() {
		return lraId;
	}

	public void setLraId(String lraId) {
		this.lraId = lraId;
	}

	public String getdModelNo() {
		return dModelNo;
	}

	public void setdModelNo(String dModelNo) {
		this.dModelNo = dModelNo;
	}

	public String getDeviceName() {
		return deviceName;
	}

	public void setDeviceName(String deviceName) {
		this.deviceName = deviceName;
	}

	public String getFwNo() {
		return fwNo;
	}

	public void setFwNo(String fwNo) {
		this.fwNo = fwNo;
	}

	public String getFwName() {
		return fwName;
	}

	public void setFwName(String fwName) {
		this.fwName = fwName;
	}

	public String getMcuNo() {
		return mcuNo;
	}

	public void setMcuNo(String mcuNo) {
		this.mcuNo = mcuNo;
	}

	public String getMcuName() {
		return mcuName;
	}

	public void setMcuName(String mcuName) {
		this.mcuName = mcuName;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getdPolicyCert() {
		return dPolicyCert;
	}

	public void setdPolicyCert(String dPolicyCert) {
		this.dPolicyCert = dPolicyCert;
	}
	
}
