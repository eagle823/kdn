package com.ksign.kdn.domain;

import java.io.Serializable;
import java.util.Date;


public class Operator implements Serializable{
	
	/** 
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String reg_num;  
	private Date   reg_date; 
	private String   deactivated;  
	private String   operator_id;  
	private String   category_id;  
	private String   corp_id; 
	private String   branch_id;  
	private String   type;  
	private String   name;  
	private String   email;  
	private String    phone;  
	private String   phone2;  
	private String   address;  
	private String   operator_pw;
	
	
	public String getReg_num() {
		return reg_num;
	}
	public void setReg_num(String reg_num) {
		this.reg_num = reg_num;
	}
	public Date getReg_date() {
		return reg_date;
	}
	public void setReg_date(Date reg_date) {
		this.reg_date = reg_date;
	}
	public String getDeactivated() {
		return deactivated;
	}
	public void setDeactivated(String deactivated) {
		this.deactivated = deactivated;
	}
	public String getOperator_id() {
		return operator_id;
	}
	public void setOperator_id(String operator_id) {
		this.operator_id = operator_id;
	}
	public String getCategory_id() {
		return category_id;
	}
	public void setCategory_id(String category_id) {
		this.category_id = category_id;
	}
	public String getCorp_id() {
		return corp_id;
	}
	public void setCorp_id(String corp_id) {
		this.corp_id = corp_id;
	}
	public String getBranch_id() {
		return branch_id;
	}
	public void setBranch_id(String branch_id) {
		this.branch_id = branch_id;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getPhone2() {
		return phone2;
	}
	public void setPhone2(String phone2) {
		this.phone2 = phone2;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getOperator_pw() {
		return operator_pw;
	}
	public void setOperator_pw(String operator_pw) {
		this.operator_pw = operator_pw;
	} 
}
