<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles"             prefix="tiles"      %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"              prefix="c"          %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"               prefix="fmt"        %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions"         prefix="fn"         %>
<script type="text/javascript" src="${request.contextPath}/js/product/productadd.js"></script>
<style>
<!--
.listselected{background-color:#336699;}
-->
</style>
<script type="text/javascript">
<!--
$(document).ready(function(){
	  
	  $("a").click(function(){
		  if($(this).text()=='등록'||$(this).text()=='수정'){
			var submitData = $("#companyForm" ).serializeArray();
			  var formObject = {};
			  $.each(submitData,
			      function(i, v) {
			          formObject[v.name] = v.value;
			      });
			 
			   productAdd(formObject,'${key}');
			   
		  }else if($(this).text()=='취소'){
			  
		  }else if($(this).text()=='우편번호 검색'){
			  ShowModalPopup('${request.contextPath}/popup/findAddress.kdn','500','600');
		  } 
	  });
	  
	});
//-->
</script>
<form id="companyForm">
	<div id="contents">
		<div id="navi">
			<c:choose>
				<c:when test="${key != 'mod' }">
					<img class="title" src="${request.contextPath}/images/title_0302.gif" alt="신규등록" />
					<a href="/info/information.kdn">홈</a> > <a href="/production/list.kdn">제조사 관리</a> > <a href="/production/list.kdn">제조사 정보 관리</a> > <a href="/production/company/form/add.kdn">신규등록</a>
				</c:when>
				<c:otherwise>
					<img class="title" src="${request.contextPath}/images/title_0309.gif" alt="제조사 수정" />
					<a href="/info/information.kdn">홈</a> > <a href="/production/list.kdn">제조사 관리</a> > <a href="/production/list.kdn">제조사 정보 관리</a> > 제조사 수정
				</c:otherwise>
			</c:choose>
		</div>	  
		<p class="subtitle">제조사 정보</p>  
		<div class="table_wrap">
			<table class="board_write">
				<colgroup>
					<col width="15%" />
					<col width="35%" />
					<col width="15%" />
					<col width="35%" />
				</colgroup>
				<tr> 
					<th>제조사명</th>
					<td>
						<c:choose>
							<c:when test="${key != 'mod' }">
								<input id="product_name" name="product_name" type="text" value="${production.product_name}" /> 
								<input id="namecheck" type="hidden" value="false"/> <a class="inner_btn btntext" href="javascript:checkName();">중복확인</a>
							</c:when>
							<c:otherwise>
								${production.product_name}
								<input id="product_name" name="product_name" type="hidden" value="${production.product_name}" />
							</c:otherwise>
						</c:choose>
					</td>
					<th>사업자등록번호</th>
					<td>
						<c:choose>
							<c:when test="${key != 'mod' }">
								<input id="product_ssn1" name="product_ssn1" maxlength="3" size="6" type="text" value="${production.product_ssn1}" /> -
								<input id="product_ssn2" maxlength="2" size="5" name="product_ssn2" type="text" maxlength="2" value="${production.product_ssn2}" /> -
								<input id="product_ssn3" maxlength="5" size="8"name="product_ssn3" type="text" value="${production.product_ssn3}" />
							</c:when>
							<c:otherwise>
								${production.product_ssn1} - ${production.product_ssn2} - ${production.product_ssn3}
								<input id="product_ssn1" name="product_ssn1" type="hidden" value="${production.product_ssn1}" />
								<input id="product_ssn2" name="product_ssn2" type="hidden" value="${production.product_ssn2}" />
								<input id="product_ssn3" name="product_ssn3" type="hidden" value="${production.product_ssn3}" />
							</c:otherwise>
						</c:choose>
					</td>
				</tr>
				<tr>
					<th>주소</th>
					<td colspan="3">
						<input name="product_address" id="product_address" type="text" size="60" readonly="readonly" value="${production.product_address}"/>&nbsp;<a class="inner_btn btntext" href="#none">우편번호 검색</a>
					</td>
				</tr>
				<tr>
					<th>대표자명</th>
					<td><input name="ceoname" id="ceoname" type="text" maxlength="30" value="${production.ceoname}"/></td>
					<th>홈페이지 주소</th>
					<td><input type="text" name="homepage" id="homepage" value="${production.homepage}"/></td>
				</tr>
				<tr> 
					<th>Fax</th>
					<td>
						<input id="fax1" name="fax1" type="text" maxlength="4" size="4"  value="${production.fax1}"/> - 
						<input maxlength="4" size="4" id="fax2" name="fax2" type="text" value="${production.fax2}"/> - 
						<input id="fax3"  maxlength="4" size="4" name="fax3" type="text" value="${production.fax3}"/>
					</td>
					<th>대표 전화 번호</th>
					<td>
						<input id="phone1" name="phone1" type="text" maxlength="4" size="4" value="${production.phone1}"/> - 
						<input maxlength="4" size="4" id="phone2" name="phone2" type="text" value="${production.phone2}"/> - 
						<input id="phone3" maxlength="4" size="4" name="phone3" type="text" value="${production.phone3}">
					</td>
				</tr>
			</table>
		</div>
		<div class="btn_center">
			<a class="css_btn btntext" href="#none"><c:out value="${(key == 'mod' ? '수정':'등록')}"/></a>       
			<a class="css_btn btntext" href="/production/list.kdn">취소</a>
		</div>
	</div>
</form>