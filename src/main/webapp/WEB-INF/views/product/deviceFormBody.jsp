<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles"             prefix="tiles"      %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"              prefix="c"          %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"               prefix="fmt"        %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions"         prefix="fn"         %>
<script type="text/javascript" src="${request.contextPath}/js/product/productadd.js"></script>
<style>
<!--
.listselected{background-color:#336699;}
-->
</style>
    <div id="contents">
    	<div id="navi">
	    	<img src="${request.contextPath}/images/title_0306.gif" class="title" />
	    	<a href="#">홈</a> > 인증서 발급 관리 > 기기인증서 발급
    	</div>
        
        <p class="subtitle">인증서 검색 조건</p>
        
       	 <div class="table_wrap">
              <table class="table_search">
                <caption>검색</caption>
                    <colgroup>
                    <col width="15%" />
                    <col width="85%" />
                    </colgroup>
                    <tr>
                        <td><input name="" type="checkbox" value=""> 일별</td>
                        <td><input name="" type="text" value=""></td>
                    </tr>
                    <tr>
                        <td><input name="" type="checkbox" value=""> 월별</td>
                        <td><input name="" type="text" value=""></td>
                    </tr>
                    <tr>
                        <td colspan="2"><input name="" type="checkbox" value=""> 기기 종류
                        <select name="select" id="select">
                        	<option value="">기기 선택</option>
                        </select>
                        <img src="../images/blank.gif" height="1" width="25">
                        <input name="" type="checkbox" value=""> 제조사 
                        <select name="select" id="select2">
                        	<option value="">제조사 선택</option>
                        </select>
                        <img src="../images/blank.gif" height="1" width="25">
                        <input name="" type="checkbox" value=""> 시리얼 번호 <input name="" type="text" value=""></td>
                    </tr>
           </table> 
	   </div>
        
        <p class="subtitle">인증서 리스트</p>
    
   		 <div class="table_wrap">
            <table class="basic_list">
            <caption>인증서 리스트</caption>
             	<colgroup>
                <col width="" />
                <col width="" />
                <col width="" />
                <col width="" />
                <col width="" />
                <col width="" />
           		</colgroup>
              <tr>
                <th>선택</th>
                <th>시리얼 번호</th>
                <th>DN</th>
                <th>유효기간 시작</th>
                <th>유효기간 만료</th>
                <th>등록일자</th>
              </tr>
              <tr>
                <td><input name="" type="checkbox" value=""></td>
                <td>sn-1234</td>
                <td>cn=rm=123121113</td>
                <td>2013-05-20 12:15:00</td>
                <td>2014-05-20 12:15:00</td>
                <td>2013-05-20 12:15:00</td>
              </tr>
              <tr>
                <td><input name="" type="checkbox" value=""></td>
                <td>sn-1234</td>
                <td>cn=rm=123121113</td>
                <td>2013-05-20 12:15:00</td>
                <td>2014-05-20 12:15:00</td>
                <td>2013-05-20 12:15:00</td>
              </tr>
              <tr>
                <td><input name="" type="checkbox" value=""></td>
                <td>sn-1234</td>
                <td>cn=rm=123121113</td>
                <td>2013-05-20 12:15:00</td>
                <td>2014-05-20 12:15:00</td>
                <td>2013-05-20 12:15:00</td>
              </tr>
              <tr>
                <td><input name="" type="checkbox" value=""></td>
                <td>sn-1234</td>
                <td>cn=rm=123121113</td>
                <td>2013-05-20 12:15:00</td>
                <td>2014-05-20 12:15:00</td>
                <td>2013-05-20 12:15:00</td>
              </tr>
              <tr>
                <td><input name="" type="checkbox" value=""></td>
                <td>sn-1234</td>
                <td>cn=rm=123121113</td>
                <td>2013-05-20 12:15:00</td>
                <td>2014-05-20 12:15:00</td>
                <td>2013-05-20 12:15:00</td>
              </tr>
            </table>
		</div>
        <div class="page_num"> << < 1 2 3 4 5 > >> </div>
        <div class="btn_center">
        <a class="css_btn btntext" href="#">모든 인증서 저장</a>
        <a class="css_btn btntext" href="#">확인</a>
        <a class="css_btn btntext" href="#">취소</a>
        </div>
    </div>
   