<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles"             prefix="tiles"      %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"              prefix="c"          %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"               prefix="fmt"        %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions"         prefix="fn"         %>
<script>
<!--
$(document).ready(function(){

	$("a").click(function(){
		  if($(this).text()=='검색'){
			 
			  $("form" ).attr('action','${request.contextPath}/production/list.kdn')
			           .attr('method','post').submit();
			//  $("form" ).attr('action','${request.contextPath}/production/list.kdn').submit();
			   
		  }else if($(this).text()=='삭제'){
			  if($('input[name="delList"]').is(":checked")){
				  if(confirm("삭제 하시겠습니까?")){
					  $("form" ).attr('action','${request.contextPath}/production/company/form/del.kdn')
			            .attr('method','post').submit();
				  }
			  }else{
				  alert("선택하신 게시물이 없습니다.");
			  }
		};
	  });
	  
	  $('#pageSize').change(function(){
		  $("form" ).attr('action','${request.contextPath}/production/list.kdn')
          .attr('method','post').submit();
	  });
	  
	});
	
	if('${message}'!=''){
		alert('${message}');
	}
	
	function goPage( current){
		$('#currentPage').val(current);
		 $("form" ).attr('action','${request.contextPath}/production/list.kdn')
	         .attr('method','post').submit();
	};
//-->
</script>
<form method="post">
<input type="hidden"  name="currentPage" id="currentPage" value='${pageResult.currentPage}' >
	<div id="contents">
		<div id="navi">
		    <img class="title" src="${request.contextPath}/images/title_0301.gif" alt="제조사 등록 현황" />
			<a href="/info/information.kdn">홈</a> &gt; <a href="/production/list.kdn">제조사 관리</a> &gt; <a href="/production/list.kdn">제조사 정보 관리</a> &gt; 제조사 등록현황
		</div>
		<p class="subtitle">제조사 검색</p>
		<div class="table_wrap">
			<table class="board_write">
			<caption>검색 조건</caption>
			<colgroup>
				<col width="15%" />
				<col width="85%" />
			</colgroup>
			<tr>
				<th>검색 조건</th>
				<td>
				<select name="kind" id="kind">
					<option value="companyName" ${pageResult.kind=='companyName'?'selected="selected"':'' }>제조사 이름</option>
					<option value="managerName" ${pageResult.kind=='managerName'?'selected="selected"':'' }>담당자 이름</option>
					<option value="product_ssn" ${pageResult.kind=='product_ssn'?'selected="selected"':'' }>사업자 등록 번호</option>
				</select>
				<input name="searchKeyword" id="searchKeyword" type="text" value="${pageResult.searchKeyword}"><a class="inner_btn btntext" href="#none">검색</a></td>
			</tr>
			</table>
		</div>
		<p class="subtitle_left">제조사 정보</p>
		<div class="list_pages">
			건수/페이지 : 
			<select name="pageSize" id="pageSize" >
				<option value='5' ${pageResult.pageSize=='5'?'selected="selected"':'' }>5</option>
				<option value='10' ${pageResult.pageSize=='10'||pageResult.pageSize==''?'selected="selected"':'' } >10</option>
				<option value='20' ${pageResult.pageSize=='20'?'selected="selected"':'' }>20</option>
			</select>
			Total : ${pageResult.totalCount} 개
		</div> 
		<div class="table_wrap">
			<table class="basic_list">
				<caption>관리자목록</caption>
				<colgroup>
					<col width="" />
					<col width="" />
					<col width="" />
					<col width="" />
					<col width="" />
				</colgroup>
				<tr>  	 	 	
					<th>제조사 명</th>
					<th>사업자 등록번호</th>
					<th>대표자명</th>
					<th>대표번호</th>
					<th>홈페이지</th>
				</tr>
				<c:if test="${not empty pageResult.resultList}">
					<c:forEach var="result" items="${pageResult.resultList}" varStatus="status">
						<c:url value="/production/view.kdn" var="viewLink">  
							<c:param name="makerName" value="${result.MAKER_NAME}"></c:param>  
						</c:url>
						<c:url value="/production/company/form/mod.kdn" var="tutorialLink">  
							<c:param name="makerName" value="${result.MAKER_NAME}"></c:param>  
						</c:url>
						<tr style="cursor:pointer;" onClick="location.href='${viewLink }'" onmouseover="this.style.background='#f1f1f1';" onmouseout="this.style.background='#ffffff';">
							<td><c:out value="${result.MAKER_NAME}"/>
							<td><c:out value="${result.BRN}"/></td>
							<td>${result.CEO}</td>
							<td>${result.PHONE}</td>
							<td>${result.HOMEPAGE}</td>
						</tr>
					</c:forEach>
				</c:if>
				<c:if test= "${empty pageResult.resultList}" >
				<tr>
					<td colspan="4">조회된 제조사 정보가 없습니다.</td>
				</tr>
			</c:if>
			</table>
		</div>
		<div class="btn_center">
			<div class="page_num"><c:if test= "${not empty pageResult.resultList}" >${pageResult.pagingPrint} </c:if></div>
		</div>
	</div>
</form>
