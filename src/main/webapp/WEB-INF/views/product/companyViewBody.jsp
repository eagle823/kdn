<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles"             prefix="tiles"      %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"              prefix="c"          %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"               prefix="fmt"        %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions"         prefix="fn"         %>
<script type="text/javascript" src="${request.contextPath}/js/product/productadd.js"></script>
<style>
<!--
.listselected{background-color:#336699;}
-->
</style>
<script>
$(document).ready(function(){

	$("a").click(function(){
		if($(this).text()=='삭제'){
			if(confirm("삭제 하시겠습니까?")){
				$("form" ).attr('action','${request.contextPath}/production/company/form/del.kdn').submit();
			}
		};
	});
});
</script>
<form action="" method="post">
	<input type="hidden" name="delList" value="${production.product_name}" />
</form>
<div id="contents">
	<div id="navi">
		<img class="title" src="${request.contextPath}/images/title_0301.gif" alt="제조사 등록 현황" />
		<a href="/info/information.kdn">홈</a> &gt; <a href="/production/list.kdn">제조사 관리</a> &gt; <a href="/production/list.kdn">제조사 정보 관리</a> &gt; 제조사 등록현황
	</div>	  
	<p class="subtitle">제조사 정보</p>  
	<div class="table_wrap">
		<table class="board_write">
			<colgroup>
				<col width="15%" />
				<col width="35%" />
				<col width="15%" />
				<col width="35%" />
			</colgroup>
			<tr> 
				<th>제조사명</th>
				<td >${production.product_name}</td>
				<th>사업자등록번호</th>
				<td>${production.product_ssn1} - ${production.product_ssn2} - ${production.product_ssn3}
				</td>
			</tr>
			<tr>
				<th>주소</th>
				<td colspan="3">${production.product_address}</td>
			</tr>
			<tr>
				<th>대표 자명</th>
				<td>${production.ceoname}</td>
				<th>홈페이지 주소</th>
				<td>${production.homepage}</td>
			</tr>
			<tr> 
				<th>Fax</th>
				<td>${production.fax1} - ${production.fax2} - ${production.fax3}</td>
				<th>대표 전화 번호</th>
				<td>${production.phone1} - ${production.phone2} - ${production.phone3}</td>
			</tr>
		</table>
	</div>
	<div class="btn_center">
		<c:url value="/production/company/form/mod.kdn" var="tutorialLink">  
			<c:param name="makerName" value="${production.product_name}"></c:param>  
		</c:url>
		<a class="css_btn btntext" href="${tutorialLink }">수정</a>
		<a class="css_btn btntext" href="#">삭제</a>
		<a class="css_btn btntext" href="/production/list.kdn">목록</a>
	</div>
</div>