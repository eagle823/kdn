<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles"             prefix="tiles"      %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"              prefix="c"          %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"               prefix="fmt"        %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions"         prefix="fn"         %>
     <div id="contents">
    	<div id="navi">
    		<img src="${request.contextPath}/images/title_0401.gif" class="title" />
    		<a href="#">홈 </a> &gt; 제조사 관리&gt;  기기 관리
    	</div>
        
  	    <p class="subtitle">기기 검색</p>        
        <div class="table_wrap">
            <table class="board_write">
            <caption>기기검색</caption>
             <colgroup>
                <col width="15%" />
                <col width="35%" />
                <col width="15%" />
                <col width="35%" />
            </colgroup>
              <tr>
                <th>기기 시리얼 번호</th>
                <td colspan="3"><input id="company" type="text"></td>
              </tr>
              <tr>
                <th>칩 이름</th>
                <td><input id="company" type="text"></td>
                <th>칩 보안모듈</th>
                <td><input id="company" type="text"> <a class="inner_btn btntext" href="#">검색</a></td>
              </tr>
            </table>
		</div>
        
  	    <p class="subtitle">기기 목록</p>        
        
        <div class="table_wrap">
            <table class="basic_list">
            <caption>기기목록</caption>
             	<colgroup>
                <col width="" />
                <col width="" />
                <col width="" />
                <col width="" />
                <col width="" />
                <col width="" />
           		</colgroup>
              <tr>
                <th>선택</th>
                <th>제조사 명</th>
                <th>기기 시리얼 번호</th>
                <th>칩 이름</th>
                <th>칩 버전</th>
                <th>인증서 상태</th>
              </tr>
              <tr>
                <td><input name="" type="checkbox" value=""></td>
                <td>company</td>
                <td><a href="#">asdf-0001</a></td>
                <td>asdfas</td>
                <td>1.0.1</td>
                <td>발급</td>
              </tr>
              <tr>
                <td><input name="" type="checkbox" value=""></td>
                <td>company</td>
                <td><a href="#">asdf-0001</a></td>
                <td>asdfas</td>
                <td>1.0.1</td>
                <td>발급</td>
              </tr>
              <tr>
                <td><input name="" type="checkbox" value=""></td>
                <td>company</td>
                <td><a href="#">asdf-0001</a></td>
                <td>asdfas</td>
                <td>1.0.1</td>
                <td>발급</td>
              </tr>
              <tr>
                <td><input name="" type="checkbox" value=""></td>
                <td>company</td>
                <td><a href="#">asdf-0001</a></td>
                <td>asdfas</td>
                <td>1.0.1</td>
                <td>발급</td>
              </tr>
              <tr>
                <td><input name="" type="checkbox" value=""></td>
                <td>company</td>
                <td><a href="#">asdf-0001</a></td>
                <td>asdfas</td>
                <td>1.0.1</td>
                <td>발급</td>
              </tr>
            </table>
		</div>
        <div class="page_num"> << < 1 2 3 4 5 > >> </div>
        <div class="btn_center">
        <a class="css_btn btntext" href="#">등록</a>
        <a class="css_btn btntext" href="#">삭제</a>
        <a class="css_btn btntext" href="#">수정</a>
        <a class="css_btn btntext" href="#">취소</a>
        </div>
	</div>
   