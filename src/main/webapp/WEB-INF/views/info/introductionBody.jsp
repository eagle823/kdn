<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%request.setCharacterEncoding("UTF-8");%>
 <div id="contents">
    	<div id="navi"><img src="${request.contextPath}/images/title_0104.gif" class="title" /><a href="/info/information.kdn">홈</a> > <a href="/info/information.kdn">센터소개</a> > <a href="/info/introduction.kdn">인증업무안내</a></div>
        
   	    <p class="subtitle">인증센터 시스템 운영/관리</p>
   	    <p class="pl20">한전KDN는 소속 기관에 대해 GPKI인증서비스를 안정적으로 제공하기 위해 핵심인증시스템을 구축하고 운영/관리합니다</p>
   	    <p class="subtitle">핵심인증시스템 운영</p>
   	    <p class="pl20">인증시스템(CA): 사용자 공개키에 대한 인증서를 생성,갱신,폐지하는 인증업무 수행<br>
						등록시스템(RA): 개인용,특수목적용(업무용), 전자관인용(기관용),서버용 사용자에 대한 등록업무 수행<br>
						키생성시스템(KGS): 인증센터 인즈서 발급 업무 수행을 위한 인증시스템(CA)용 키 생성 업무 수행<br>
						디렉토리 시스템(DS): 인증서 및 CRL 게시 관리<br>
						인증센터 홈페이지 시스템</p>
   	    <p class="subtitle">인증센터 인적, 물리적 보안 통제 관리</p>
   	    <p class="pl20">네트워크 보안<br> 
						시스템 보안<br>
						운영 보안<br>
						물리적 접근 보안</p>
        
    </div>
   