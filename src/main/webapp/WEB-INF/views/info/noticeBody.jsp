<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%request.setCharacterEncoding("UTF-8");%>
 <div id="contents">
    <div id="navi"><img src="${request.contextPath}/images/title_0103.gif" class="title" /><a href="/info/information.kdn">홈</a> > <a href="/info/information.kdn">센터소개</a> > <a href="/info/notice.kdn">공지사항</a></div>
    <div class="table_wrap">
      <table class="basic_list">
        <caption>
          공지사항
        </caption>
        <colgroup>
          <col width="10%" />
          <col>
          <col width="15%" />
        </colgroup>
        <tr>
          <th>번호</th>
          <th>제목</th>
          <th>등록일</th>
        </tr>
        <tr>
          <td>100</td>
          <td class="title"><a href="/info/noticeView.kdn?notice=1">12월 정기점검 안내</a></td>
          <td>2013-12-15</td>
        </tr>
        <tr>
          <td>100</td>
          <td class="title"><a href="/info/noticeView.kdn?notice=2">12월 정기점검 안내</a></td>
          <td>2013-12-15</td>
        </tr>
        <tr>
          <td>100</td>
          <td class="title"><a href="/info/noticeView.kdn?notice=3">12월 정기점검 안내</a></td>
          <td>2013-12-15</td>
        </tr>
        <tr>
          <td>100</td>
          <td class="title"><a href="/info/noticeView.kdn?notice=4">12월 정기점검 안내</a></td>
          <td>2013-12-15</td>
        </tr>
        <tr>
          <td>100</td>
          <td class="title"><a href="/info/noticeView.kdn?notice=5">12월 정기점검 안내</a></td>
          <td>2013-12-15</td>
        </tr>
        <tr>
          <td>100</td>
          <td class="title"><a href="/info/noticeView.kdn?notice=6">12월 정기점검 안내</a></td>
          <td>2013-12-15</td>
        </tr>
        <tr>
          <td>100</td>
          <td class="title"><a href="/info/noticeView.kdn?notice=7">12월 정기점검 안내</a></td>
          <td>2013-12-15</td>
        </tr>
      </table>
    </div>
    <div class="page_num"> << < 1 2 3 4 5 > >> </div>
  </div>