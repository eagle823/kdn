<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles"             prefix="tiles"      %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"              prefix="c"          %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"               prefix="fmt"        %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions"         prefix="fn"         %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>KDN_스마트그리드기기 인증운영시스템</title>
<link href="${request.contextPath}/css/default.css" rel="stylesheet" type="text/css">
<link href="${request.contextPath}/css/jquery-ui.min.css" rel="stylesheet" type="text/css">
 <!--<![if lt IE 9]>-->
      <script type="text/javascript" src="${request.contextPath}/js/json2.js"></script>
   <!--<![endif]-->
<script type="text/javascript" src="${request.contextPath}/js/jquery-1.10.1.min.js"></script>
<script type="text/javascript" src="${request.contextPath}/js/jquery.serializeJSON.min.js"></script>
<script type="text/javascript">
if (!Array.prototype.indexOf)
{
  Array.prototype.indexOf = function(elt /*, from*/)
  {
    var len = this.length >>> 0;

    var from = Number(arguments[1]) || 0;
    from = (from < 0)
         ? Math.ceil(from)
         : Math.floor(from);
    if (from < 0)
      from += len;

    for (; from < len; from++)
    {
      if (from in this &&
          this[from] === elt)
        return from;
    }
    return -1;
  };
}
	
	
	
</script>

</head>

<body>
	<tiles:insertAttribute name="popup"/>
	<div style="width:100%; text-align:center;">
		<div id="header"><tiles:insertAttribute name="top"/></div>
		
		<div id="container">
			
		    <div id="left">
		    <div id="login_status"><tiles:insertAttribute name="login_statu"/></div>
		    <tiles:insertAttribute name="left"/></div>
		    <!-- <div id="contents"> -->
		    <tiles:insertAttribute name="body"/>    	
			<!-- </div> -->
		    <div id="footer"><tiles:insertAttribute name="footer"/></div> 
		</div> 
	</div>
</body>
</html>
