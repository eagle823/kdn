<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
 <link rel="stylesheet" href="${request.contextPath}/css/menu/megafish.css" media="screen">
    <style>
      html, body, .wrap {
        width: 50em;
        height: 100%;
        padding: 0;
        margin: 0;
      }
      .wrap {
        padding: 1em;
        height: auto;
        min-height: 90%;
      }
      h1 {
        font-size: 1.125em;
      }
      .sf-mega ul {
        list-style-type: auto;
        margin: 0;
        padding-left: 1.2em;
      }
      .sf-mega li {
        margin-left: 0;
      }
      .sf-mega h2 {
        font-size: 1em;
        margin: .5em 0;
        color: #13a;
      }
      a:focus, a:hover, a:active {
        text-decoration: none;
      }
    </style>
    <script src="${request.contextPath}/js/jquery-1.10.1.min.js"></script>
    <script src="${request.contextPath}/js/menu/hoverIntent.js"></script>
    <script src="${request.contextPath}/js/menu/superfish.js"></script>
    

<a href="/"><img src="${request.contextPath}/images/top_logo.gif" width="344" height="36" class="top_logo" />
</a>
<script>
(function($){ //create closure so we can safely use $ as alias for jQuery

    $(document).ready(function(){
      
      var exampleOptions = {
        speed: 'fast'
      }
      // initialise plugin
      var example = $('#example').superfish(exampleOptions);

      // buttons to demonstrate Superfish's public methods
      $('.destroy').on('click', function(){
        example.superfish('destroy');
      });

      $('.init').on('click', function(){
        example.superfish(exampleOptions);
      });

      $('.open').on('click', function(){
        example.children('li:first').superfish('show');
      });

      $('.close').on('click', function(){
        example.children('li:first').superfish('hide');
      });
    });

  })(jQuery);

</script>
    
  <ul class="sf-menu" id="example">
       <li class="current"><a href="/info/information.kdn">센터 소개</a> </li>
        <li class="current"><a href="/admin/list.kdn">관리자 관리</a>
		<div class="sf-mega">
			<div class="sf-mega-section">
				<ul>
					<li class="current"><a href="/admin/list.kdn">관리자 정보 관리</a></li>
				</ul>
			</div>
			<div class="sf-mega-section">
				<ul>
					<li class="current"><a href="/admin/levelList.kdn">관리자 정책 관리</a></li>
				</ul>
			</div>
		</div>
		</li>
		<li class="current"><a href="/production/list.kdn">제조사 정보 관리</a> 
		<div class="sf-mega">
            <div class="sf-mega-section">             
              <ul>
                <li class="current"><a href="/production/list.kdn">제조사 정보 관리</a></li>                
              </ul>
            </div>
          </div>
		</li>
		<li class="current"><a href="/cert/deviceModel.kdn">기기 정보 관리</a>
		
		<div class="sf-mega">
			<div class="sf-mega-section">
				<ul>
					<li class="current"><a href="/cert/deviceModel.kdn">기기 모델관리</a></li>
				</ul>
			</div>
			<div class="sf-mega-section">
				<ul>
					<li class="current"><a href="/cert/issued.kdn">기기 신청 관리</a></li>
				</ul>
			</div>
			<div class="sf-mega-section">
				<ul>
					<li class="current"><a href="/cert/keygeneration.kdn">기기 키 관리</a></li>
				</ul>
			</div>
		</div>
		
		 </li>
		<li class="current"><a href="/cert/management/status.kdn">인증서 관리</a> 
		
		<div class="sf-mega">
           <div class="sf-mega-section">
				<ul>
					<li class="current"><a href="/cert/management/status.kdn">인증서 조회</a></li>
				</ul>
			</div>
			<div class="sf-mega-section">
				<ul>
					<li class="current"><a href="/cert/management/storage.kdn">인증서 저장</a></li>
				</ul>
			</div>
		</div>
		</li>
		<li class="current"><a href="/audit/audit.kdn">감사 관리</a>
		<div class="sf-mega">
           <div class="sf-mega-section">
				<ul>
					<li class="current"><a href="followed.html">감사 관리</a></li>
				</ul>
			</div>
			<div class="sf-mega-section">
				<ul>
					<li class="current"><a href="followed.html">통계 관리</a></li>
				</ul>
			</div>
		</div>
		 </li>
		<li class="current"><a href="/data/regulations.kdn">자료실</a> </li>
		<li class="current"><a href="/help/faq.kdn">도움말</a> </li>
  </ul>		
  
   
        
