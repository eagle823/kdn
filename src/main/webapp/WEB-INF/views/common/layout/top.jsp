<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<script type="text/javascript" src="${request.contextPath}/js/admin/common.js"></script>
<script>
function rollover(id){
 for(i=1;i<=8;i++){
  j=eval("img"+i); 
  k=eval("menu"+i); 
  if(i==id){
   j.src="${request.contextPath}/images/top_0"+i+"_over.gif";
   k.style.visibility="visible";}
 else{
   j.src="${request.contextPath}/images/top_0"+i+".gif";
   k.style.visibility="hidden";}
  }
 }
</script>
<style>
#menu1{position:absolute;font-size:11px;visibility:hidden; padding-left:35px; margin-top:6px;}
#menu2{position:absolute;font-size:11px;visibility:hidden; padding-left:152px; margin-top:6px;}
#menu3{position:absolute;font-size:11px;visibility:hidden; padding-left:276px; margin-top:6px;}
#menu4{position:absolute;font-size:11px;visibility:hidden; padding-left:390px; margin-top:6px;}
#menu5{position:absolute;font-size:11px;visibility:hidden; padding-left:524px; margin-top:6px;}
#menu6{position:absolute;font-size:11px;visibility:hidden; padding-left:653px; margin-top:6px;}
#menu7{position:absolute;font-size:11px;visibility:hidden; padding-left:781px; margin-top:6px;}
#menu8{position:absolute;font-size:11px;visibility:hidden; padding-left:900px; margin-top:6px;}
body {margin:0; color: #333; font-family:"돋움", "Dotum", AppleGothic, Sans-serif; font-size: 12px;background-image:url(/images/back_topsub.gif); background-repeat:repeat-x;}
</style>

<div class="pt10 al"><a href="${request.contextPath}/info/information.kdn">
<img src="${request.contextPath}/images/top_logo.gif" width="344" height="36" class="top_logo" /></a></div>
<div class="top_nav">
	<a href="${request.contextPath}/info/information.kdn"><img src="${request.contextPath}/images/top_01.gif" id="img1" border="0" onmouseover="rollover(1)" alt="센터소개" /></a><c:if test="${KDN_LOGIN_SESSION.TYPE eq 'lra'}"><a href="${request.contextPath}/admin/list.kdn"><img src="${request.contextPath}/images/top_02.gif" id="img2" border="0" onmouseover="rollover(2)" alt="관리자 관리" /></a><a href="${request.contextPath}/production/list.kdn"><img src="${request.contextPath}/images/top_03.gif" id="img3" border="0" onmouseover="rollover(3)" alt="제조사 관리" /></a></c:if><c:if test="${KDN_LOGIN_SESSION.MP eq 't'}"><a href="${request.contextPath}/cert/deviceModel/list.kdn"><img src="${request.contextPath}/images/top_04.gif" id="img4" border="0" onmouseover="rollover(4)" alt="기기 정보 관리" /></a></c:if><c:if test="${KDN_LOGIN_SESSION.CP eq 't'}"><a href="${request.contextPath}/cert/management/status.kdn"><img src="${request.contextPath}/images/top_05.gif" id="img5" border="0" onmouseover="rollover(5)" alt="인증서 관리" /></a></c:if><c:if test="${KDN_LOGIN_SESSION.AP eq 't'}"><a href="${request.contextPath}/audit/audit.kdn"><img src="${request.contextPath}/images/top_06.gif" id="img6" border="0" onmouseover="rollover(6)" alt="감사 관리" /></a></c:if><a href="${request.contextPath}/data/regulations.kdn"><img src="${request.contextPath}/images/top_07.gif" id="img7" border="0" onmouseover="rollover(7)" alt="자료실" /></a><a href="${request.contextPath}/help/faq.kdn"><img src="${request.contextPath}/images/top_08.gif" id="img8" border="0" onmouseover="rollover(8)" alt="도움말" /></a>
</div>

    <div class="topsub">
    
    <div id=menu1>
        <a href="/info/information.kdn" class="pr8 topsub">개요</a>
        <a href="/info/address.kdn" class="pr8 topsub">주소 및 약도</a>
        <a href="/info/notice.kdn" class="pr8 topsub">공지사항</a>
        <a href="/info/introduction.html" class="pr8 topsub">인증업무 안내</a>
    </div>
	<div id=menu2>
        <a href="/admin/list.kdn" class="pr8 topsub">관리자 정보 관리</a>
        <a href="/admin/rating/list2.kdn" class="pr8 topsub">관리자 정책 관리</a>
        <a href="/admin/rating/list.kdn" class="pr8 topsub">펌웨어 관리</a>
    </div>
    <div id=menu3>
    	<a href="/production/list.kdn" class="pr8 topsub">제조사 정보 관리</a>
    </div>
	<div id=menu4>
        <a href="/cert/deviceModel/list.kdn" class="pr8 topsub">기기 모델 관리</a>
        <a href="/cert/devicePolicy/list.kdn" class="pr8 topsub">펌웨어 관리</a>
    </div>
	<div id=menu5>
        <a href="/cert/management/status2.kdn" class="pr8 topsub">인증서 조회</a>
        <a href="/cert/management/storage2.kdn" class="pr8 topsub">인증서 저장</a>
        <a href="/cert/management/status.kdn" class="pr8 topsub">펌웨어 관리</a>
    </div>
	<div id=menu6>
        <a href="/audit/audit.kdn" class="pr8 topsub">감사기록 관리</a>
        <a href="/audit/cert/process.kdn" class="pr8 topsub">통계 관리</a>
    </div>
	<div id=menu7>
        <a href="/data/regulations.kdn" class="pr8 topsub">관련 법규</a>
        <a href="/data/glossary.kdn" class="pr8 topsub">용어 설명</a>
        <a href="/data/relatedLinks.kdn" class="pr8 topsub">관련 사이트</a>
    </div>
	<div id=menu8>
        <a href="/help/faq.kdn" class="pr8 topsub">FAQ</a>
        <a href="/help/board.kdn" class="pr8 topsub">Q&A</a>
    </div>
    
    
</div>
   
        
</div>