<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<style type="text/css">
/* 원하는 만큼 투명도 처리 : opacity=50 */
.alpha50 {-moz-opacity:0.5;filter:Alpha(opacity=90);background-color : #d3d3d3;z-index:200;}
</style>

<script type="text/javascript">

function ShowModalPopup(url,popWidth,popHeight) {
	make_back();  
	layer_open(url,popWidth,popHeight);
	
}



/*
 * @content : 배경을 만든 후 레이어 새창을 열어줌
 */
function open_window() {
	make_window();
}

/* @content : fnGetBodyInnerSize(), fnGetBodyScroll() 조합
 * @return : 브라우저 크기및 스크롤값을 객체로 리턴
 */
function fnGetBodySize()
{
	var oBody  = new Object;
	var oBodySize = fnGetBodyInnerSize();
	var oBodyScroll = fnGetBodyScroll();

	oBody.width  = oBodySize.width;
	oBody.height = oBodySize.height;
	oBody.top  = oBodyScroll.top;
	oBody.left  = oBodyScroll.left;

	return oBody; 
}

/* @content : 돌려주는 값 : 객체(가로크기=정수, 세로크기=정수)
 *            동작 브라우저 : 모든 브라우저 /  확인 브라우저 : IE 8.0, FF 2.0
 * @return : 브라우저의 크기(가로, 세로)
 */
function fnGetBodyInnerSize()
{
	var oBody = new Object;

	if ( self.innerWidth) // IE 외 모든 브라우저
	{
		oBody.width = self.innerWidth;
		oBody.height = self.innerHeight;
	}
	else if ( document.documentElement && document.documentElement.clientWidth) // Explorer 6 Strict
	{  
		oBody.width = document.documentElement.clientWidth;
		oBody.height = document.documentElement.clientHeight;
	}
	else if ( document.body )  // IE 브라우저
	{  
		oBody.width = document.body.clientWidth;
		oBody.height = document.body.clientHeight;
	}

	return oBody;
}

// 브라우저의 스크롤 된 위치 값을 얻는다
// 동작 브라우저 : 모든 브라우저
// 확인 브라우저 : IE 6.0, FF 2.0
// 돌려주는 값 : 객체[가로스크롤값(정수), 세로스크롤값(정수)]
function fnGetBodyScroll()
{
	var x,y;
	var oScroll = new Object;

	if (self.pageYOffset) // IE 외 모든 브라우저
	{
		x = self.pageXOffset;
		y = self.pageYOffset;
	}
	else if (document.documentElement && document.documentElement.scrollTop) // Explorer 6 Strict
	{
		x = document.documentElement.scrollLeft;
		y = document.documentElement.scrollTop;
	}
	else if (document.body)  // IE 브라우저
	{
		x = document.body.scrollLeft;
		y = document.body.scrollTop;
	}
	oScroll.left = x;
	oScroll.top = y;

	return oScroll;
}

/*
 * 팝업배경 만들기
 */
function make_back()
{
	var oBack = document.getElementById ('ScheduleLayerBack');

	if(!oBack)
	{
		oBack = document.createElement('div');
		oBack.setAttribute('id','ScheduleLayerBack');
		oBack.name = 'ScheduleLayerBack';
		oBack.className = 'alpha50';
		oBack.style.position = 'absolute';
		oBack.style.left = '0px';
		oBack.style.top = '0px';
		oBack.style.zIndex = 100;
		document.body.appendChild(oBack);
		oBack.onclick = function(){fnHideSchedule();};
	}

	var oBody = fnGetBodySize();

	if(oBody.height > document.body.scrollHeight)
	{
		oBack.style.height = oBody.height+'px';
	}
	else
	{
		oBack.style.height = document.body.scrollHeight+'px';
	}


//	oBack.style.width = '1600px';
	oBack.style.width = oBody.width + 'px';

	oBack.style.display = '';
	document.body.width = '100%';
}

function HideModalPopup(){
	
	fnHideSchedule();
}
/*
 * 레이어 창 안보이게 처리
 */
function fnHideSchedule()
{
	var oBack = document.getElementById("ScheduleLayerBack");
	var oSchedule = document.getElementById("ScheduleLayer");

	if(oBack)
	{
		oBack.style.display = 'none';
		oSchedule.style.display = 'none';
	}
}

 
function layer_open(url,width,height)
{
	var oSchedule = document.getElementById('ScheduleLayer');

	if ( !oSchedule )
	{
		var oIFrame = document.createElement('IFRAME');

		oIFrame.setAttribute('id', 'ScheduleFrame');
		oIFrame.src = url;		/* moving url */
		oIFrame.width = width+'px';
		oIFrame.height = height+'px';
		oIFrame.frameBorder = 'no';
		oIFrame.scrolling = 'no';

		oSchedule = document.createElement('div');
		oSchedule.setAttribute('id', 'ScheduleLayer');
		oSchedule.style.position = 'absolute';
		oSchedule.style.width = width+'px';
		oSchedule.style.height = height+'px';
		oSchedule.style.zIndex = 200;
		oSchedule.style.backgroundColor = '#FFFFFF';

		oSchedule.appendChild(oIFrame);

		document.body.appendChild(oSchedule);

		oSchedule = null;
		var oSchedule = document.getElementById('ScheduleLayer');
	}

	var oBody = fnGetBodySize();
 //alert(oBody.top +" : "+ oBody.left+" : "+ oBody.width+" : "+ oBody.height);

		var  wp  =  oBody.width/2- width/2;
		var   hp = oBody.height/2-height/2;
		oSchedule.style.top =hp;
		oSchedule.style.left =wp;
/*
	if ( oBody.top < 443 )
	{
		if (navigator.appName == "Netscape") 
		{
			oSchedule.style.top = '473px'; 

		}
		else
		{
			//alert(navigator.appName);
			oSchedule.style.top = (oBody.top+30)+'px'; 
			oSchedule.style.top = (oBody.height/2)+'px';
		}
	}
	else
	{
		oSchedule.style.top = (oBody.top+30)+'px';

	}
	//alert(oSchedule.style.top);
	oSchedule.style.left = ((oBody.width / 2) - 250) + 'px';  */
	oSchedule.style.display = '';
}

 

<!--
var _ModalPopupBackgroundID = 'kdn_layer_popup_custom';

function ShowModalPopup2(url,popWidth,popHeight) {
	 
    var isIE6 = (navigator.appVersion.toLowerCase().indexOf('msie 6') > 0);

    var popupID = "#dvPopup";
    $(popupID).height(popHeight);
    $(popupID).width(popWidth);
    var popupMarginTop =popHeight / 2;
    var popupMarginLeft = popWidth / 2;
   
    $("#urlPath").attr("src",url);

    
    
    $(popupID).css({
        'left': '50%',
        'z-index': 9999
    });

    $("#urlPath").height(popHeight-50);
    $("#urlPath").width(popWidth-10);
    if (isIE6) {
   	
        $(popupID).css({
            'top': $(document).scrollTop(),
            'margin-top': $(window).height() / 2 - popupMarginTop,
            'margin-left': -popupMarginLeft,
            'display': 'block',
            'position': 'absolute'
        });
        $(popupID).height(popHeight);
        $(popupID).width(popWidth);
        $('.pop-container').css({
            'width': '100%' ,
            'height':'90%'              
        });
    } else {
        $(popupID).css({
            'top': '50%',
            'margin-top': -popupMarginTop,
            'margin-left': -popupMarginLeft,
            'display': 'block',
            'position': 'fixed'
        });
    }

    var backgroundSelector = $('<div id="' + _ModalPopupBackgroundID + '" ></div>');

    backgroundSelector.appendTo('body');

    //Set CSS for modal background. Set z-index of background lower than popup window.
    backgroundSelector.css({    	
        'width': $(document).width(),
        'height': $(document).height(),
        'display': 'none',
        'background-color': '#555555',
        'position': 'absolute',
        'top': 0,
        'left': 0,
        'z-index': 9990
    });

    backgroundSelector.fadeTo('fast', 0.8);
}

function HideModalPopup2(modalPopupID) {
   
     $("#urlPath").attr("src","");
    $("#dvPopup").css('display', 'none');    
    $("#" + _ModalPopupBackgroundID).remove();
   
}
//-->
</script>
 <style>

	iframe.select_bug {position: absolute;top:0;left:0;z-index:-1;opacity:0;filter:progid:DXImageTransform.Microsoft.Alpha(opacity=0);}
       *html #dvPopup
       {
       	top:expression(eval(document.documentElement.scrollTop)) !important;
       }
  
	.layer {display:none; position:fixed; _position:absolute; top:0; left:0; width:100%; height:100%; z-index:100;}
	.layer .bg {position:absolute; top:0; left:0; width:100%; height:100%; background:#000; opacity:.5; filter:alpha(opacity=50);}
	.layer .pop-layer {display:block;}

	.pop-layer {display:none; position: absolute; top: 50%; left: 50%;    width:100%;  background-color:#fff;  z-index: 10; }	
	.pop-layer .pop-container {padding: 5px 5px;   width: 99%; height: 92%; }
	.pop-layer p.ctxt {color: #666; line-height: 1px;}
	.pop-layer .btn-r {width: 100%; margin:5px 0 5px; padding-bottom: 5px; border-top: 1px solid #DDD; text-align:right;}

	a.cbtn {display:inline-block;     margin:5px 20px 5px 10px;         height:25px; padding:0 14px 0; border:1px solid #304a8a; background-color:#3f5a9d; font-size:13px; color:#fff; line-height:25px;}	
	a.cbtn:hover {border: 1px solid #091940; background-color:#1f326a; color:#fff;}
</style>
<!--[if IE]>
    <style>
        .layer .pop-layer{
            display:block !important;
        }
    </style>
<![endif]-->

	<div id="dvPopup" style="display:none;" class="pop-layer"  >
	  
	       <div class="pop-container" >
				<div class="pop-conts" style=" width: 100%; height: 100%;">
	              <iframe src="" id="urlPath" style="width: 100%; height: 100%;"></iframe>			
	               
					<div class="btn-r">
									<a class="cbtn"   href="#" onclick="HideModalPopup(); return false;">닫기</a>
					</div>
		   </div>
		 </div>
	</div>
	<div id="mask" style="display: none;"></div>
     <div id="popup" style="display: none;">
          <div class="container" id="insertHere">
               <div class="span-1 append-23 last">
                    <p><a href="#" onclick="closePopup();">Close</a></p>
               </div>
          </div>
     </div>