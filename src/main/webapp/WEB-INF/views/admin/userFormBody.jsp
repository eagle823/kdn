<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<script type="text/javascript"
	src="${request.contextPath}/js/admin/admin.js"></script>
<script type="text/javascript">
<!--
	$(document)
			.ready(
					function() {

						$("a")
								.click(
										function() {
											if ($(this).text() == '등록') {
												submitData();
											} else if ($(this).text() == '수정') {
												window
														.open(
																"${request.contextPath}/admin/user/passwordCheck.kdn",
																"winPwCk",
																"width=400px, height=150");
											} else if ($(this).text() == '중복확인') {
												checkid();
											} else if ($(this).text() == '소속회사 검색') {
												ShowModalPopup(
														'${request.contextPath}/popup/findCompany.kdn',
														'500', '600');
											} else if ($(this).text() == '제조사 검색') {
												ShowModalPopup(
														'${request.contextPath}/popup/findCompany.kdn',
														'500', '600');
											} else if ($(this).text() == '취소') {
												history.back();
											} else if ($(this).text() == '비밀번호수정') {
												var userId = $('#userId').val();
												window
														.open(
																"${request.contextPath}/admin/user/passwordMod.kdn?userId="
																		+ userId,
																"winPw",
																"width=400px, height=150");
											} else if ($(this).text() == '비밀번호 초기화') {
												$('#password').val('8888');
												alert("초기화 되었습니다.");
											} 
										});
					});
	
	function submitData() {
		var submitData = $("#userAddForm").serializeArray();
		var formObject = {};
		$.each(submitData, function(i, v) {
			formObject[v.name] = v.value;
		});

		userAdd('${key}', formObject);
	}
//-->
</script>

    <div id="contents">
    	<div id="navi">
    	<img class="title" src="${request.contextPath}/images/title_0201.gif" />
    	<a href="/info/information.kdn">홈</a> > <a href="/admin/list.kdn">관리자 관리</a> > <a href="/admin/list.kdn">관리자 정보 관리</a> > <a href="/admin/user/form/add.kdn">신규등록</a>
        </div>
     <form id="userAddForm"action="">
     <input type="hidden" name="idcheck" id="idcheck" value="${(key=='add'?'false':'true')}">
  	    <p class="subtitle">정보 입력</p>
        <div class="table_wrap">
            <table class="board_write">
            <caption>상세 정보 입력</caption>
             <colgroup>
                <col width="15%" />
                <col width="35%" />
                <col width="15%" />
                <col width="35%" />
            </colgroup>
              <tr>
                <th>* 관리자 ID</th>
                <td>
                	<c:choose>
                		<c:when test="${ key =='add'}">
                			<input id="userId" name="userId" value ='${user.userId }' type="text" style="ime-mode:disabled" class="inputw100" />
                			<c:if test="${key=='add'}" ><a class="inner_btn btntext" href="#">중복확인</a></c:if>
                		</c:when>
                		<c:otherwise>
                			${user.userId }
                			<input id="userId" name="userId"
									value='${user.userId }' type="hidden"  class="inputw100" />
							</c:otherwise>
						</c:choose></td>
					<th>* 관리자명</th>
					<td><input id="userName" name="userName"
						value='${user.userName }' type="text"  class="inputw100" /></td>
				</tr>
				<tr>
					<th>지역</th>
					<td><select name="region" id="region">
							<option selected="selected" value="">지역 선택</option>
							<option value="서울특별시"
								${user.region=='서울특별시'?'selected="selected"':'' }>
								서울특별시</option>
							<option value="광주광역시"
								${user.region=='광주광역시'?'selected="selected"':'' }>
								광주광역시</option>
							<option value="부산광역시"
								${user.region=='부산광역시'?'selected="selected"':'' }>
								부산광역시</option>
							<option value="인천광역시"
								${user.region=='인천광역시'?'selected="selected"':'' }>
								인천광역시</option>
							<option value="대구광역시"
								${user.region=='대구광역시'?'selected="selected"':'' }>
								대구광역시</option>
							<option value="대전광역시"
								${user.region=='대전광역시'?'selected="selected"':'' }>
								대전광역시</option>
							<option value="울산광역시"
								${user.region=='울산광역시'?'selected="selected"':'' }>
								울산광역시</option>
							<option value="강원도"
								${user.region=='강원도'?'selected="selected"':'' }>강원도</option>
							<option value="경기도"
								${user.region=='경기도'?'selected="selected"':'' }>경기도</option>
							<option value="경상남도"
								${user.region=='경상남도'?'selected="selected"':'' }>경상남도</option>
							<option value="경상북도"
								${user.region=='경상북도'?'selected="selected"':'' }>경상북도</option>
							<option value="전라남도"
								${user.region=='전라남도'?'selected="selected"':'' }>전라남도
							</option>
							<option value="전라북도"
								${user.region=='전라북도'?'selected="selected"':'' }>전라북도
							</option>
							<option value="충청남도"
								${user.region=='충청남도'?'selected="selected"':'' }>충청남도</option>
							<option value="충청북도"
								${user.region=='충청북도'?'selected="selected"':'' }>충청북도
							</option>
							<option value="제주도"
								${user.region=='제주도'?'selected="selected"':'' }>제주도</option>

					</select></td>
					<th>휴대폰 번호</th>
					<td><select name="mobile1">
							<option ${user.mobile1=='010'?'selected="selected"':'' }
								value="010">010</option>
							<option ${user.mobile1=='011'?'selected="selected"':'' }
								value="011">011</option>
							<option ${user.mobile1=='016'?'selected="selected"':'' }
								value="016">016</option>
							<option ${user.mobile1=='017'?'selected="selected"':'' }
								value="017">017</option>
							<option ${user.mobile1=='018'?'selected="selected"':'' }
								value="018">018</option>
							<option ${user.mobile1=='019'?'selected="selected"':'' }
								value="019">019</option>

					</select>-<input id="mobile2" name="mobile2" type="text" maxlength="4"
						size="4" value='${user.mobile2 }' />-<input id="mobile3"
						name="mobile3" type="text" maxlength="4" size="4"
						value='${user.mobile3 }' /></td>
				</tr>
				<tr>
					<th>* 전화 번호</th>
					<td><select name="phone1">
							<option value="02"
								${user.phone1=='02 '?'selected="selected"':'' }>02</option>
							<option value="031"
								${user.phone1=='031'?'selected="selected"':'' }>031</option>
							<option value="032"
								${user.phone1=='032'?'selected="selected"':'' }>032</option>
							<option value="033"
								${user.phone1=='033'?'selected="selected"':'' }>033</option>
							<option value="041"
								${user.phone1=='041'?'selected="selected"':'' }>041</option>
							<option value="042"
								${user.phone1=='042'?'selected="selected"':'' }>042</option>
							<option value="043"
								${user.phone1=='043'?'selected="selected"':'' }>043</option>
							<option value="051"
								${user.phone1=='051'?'selected="selected"':'' }>051</option>
							<option value="052"
								${user.phone1=='052'?'selected="selected"':'' }>052</option>
							<option value="053"
								${user.phone1=='053'?'selected="selected"':'' }>053</option>
							<option value="054"
								${user.phone1=='054'?'selected="selected"':'' }>054</option>
							<option value="055"
								${user.phone1=='055'?'selected="selected"':'' }>055</option>
							<option value="061"
								${user.phone1=='061'?'selected="selected"':'' }>061</option>
							<option value="062"
								${user.phone1=='062'?'selected="selected"':'' }>062</option>
							<option value="063"
								${user.phone1=='063'?'selected="selected"':'' }>063</option>
							<option value="064"
								${user.phone1=='064'?'selected="selected"':'' }>064</option>
							<option value="070"
								${user.phone1=='070'?'selected="selected"':'' }>070</option>
					</select> - <input class="input" type="text" maxlength="4" size="4"
						name="phone2" id="phone2" value='${user.phone2 }'> - <input
						class="input" type="text" maxlength="4" size="4" name="phone3"
						id="phone3" value='${user.phone3 }'></td>
					<th>부서명</th>
					<td><input class="inputw100" type="text" name="corp" id="corp"
						value='${user.corp }'></td>
				</tr>
				<tr>
					<th>FAX</th>
					<td><input class="input" type="text" maxlength="4" size="4"
						name="fax1" id="fax1" value='${user.fax1 }'>-<input
						class="input" type="text" maxlength="4" size="4" name="fax2"
						id="fax2" value='${user.fax2 }'>-<input class="input"
						type="text" maxlength="4" size="4" name="fax3" id="fax3"
						value='${user.fax3 }'></td>
					<th>이메일</th>
					<td><input class="inputw100" type="text" maxlength="50" 
						name="email" id="email" value='${user.email }'></td>
				</tr>
				<tr>
					<th>직급</th>
					<td><input class="inputw100" type="text" maxlength="50" 
                    name="grade" id="grade" value='${user.grade }'></td>
					<th>직책</th>
					<td><input class="inputw100" type="text" maxlength="50" 
                    name="position" id="position" value='${user.position }'></td>
				</tr>
				<tr>
					<th>* 비밀번호</th>
					<td><c:choose>
							<c:when test="${key == 'add' }">
								<input class="inputw100" type="password" maxlength="50" name="password" id="password" />
							</c:when>
							<c:otherwise>
								<input class="inputw100" type="hidden" name="password" id="password" value="${user.password }" />
								<c:choose>
									<c:when test="${KDN_LOGIN_SESSION.TYPE ne 'lra'}">
										<a class="inner_btn btntext" id="pwMod" href="#">비밀번호수정</a>
									</c:when>
									<c:otherwise>
										<a class="inner_btn btntext" id="pwReset" href="#">비밀번호 초기화</a>
									</c:otherwise>
								</c:choose>
							</c:otherwise>
						</c:choose></td>
					<th>* 제조사</th>
					<td><c:choose>
							<c:when test="${ key=='add'}">
								<input class="inputw100" type="text" readonly="readonly"
									maxlength="50" name="companyName" id="companyName"
									value='${user.maker }' />
								<a class="inner_btn btntext" href="#">제조사 검색</a>
							</c:when>
							<c:otherwise>
               			${user.maker }
               			<input id="companyName" name="companyName"
									value='${user.maker }' type="hidden"  class="inputw100" />
							</c:otherwise>
						</c:choose></td>
				</tr>
			</table>
		</div>
		<c:if test="${KDN_LOGIN_SESSION.TYPE eq 'lra'}">
		<p class="subtitle">권한 부여</p>
		<div class="table_wrap">
			<table class="board_write">
				<caption>권한 부여</caption>
				<colgroup>
					<col width="20%" />
					<col width="13%" />
					<col width="20%" />
					<col width="13%" />
					<col width="20%" />
					<col width="13%" />
				</colgroup>
				<tr>
					<th>기기 관리</th>
					<td><input id="device" name="device" value='t' type="checkbox"
						${user.device=='t'?'checked="checked"':'' }></td>
					<th>인증서 관리</th>
					<td><input id="cert" name="cert" value='t' type="checkbox"
						${user.cert=='t'?'checked="checked"':'' }></td>
					<th>감사 관리</th>
					<td><input id="auth" name="auth" value='t' type="checkbox"
						${user.auth=='t'?'checked="checked"':'' }></td>
				</tr>
			</table>
		</div>
		</c:if>
	</form>
	<div class="btn_center">
		<a class="css_btn btntext" href="#">${key=='mod'?'수정':'등록'}</a> <a
			class="css_btn btntext" href="#">취소</a>
	</div>
</div>
