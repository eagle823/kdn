<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles"             prefix="tiles"      %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"              prefix="c"          %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"               prefix="fmt"        %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions"         prefix="fn"         %>

<script type="text/javascript">
$(document).ready(function(){
	  
	$("a").click(function(){
		if($(this).text()=='삭제'){
			if(confirm("삭제 하시겠습니까?")){
				$("form" ).attr('action','${request.contextPath}/admin/rating/form/del.kdn').submit();
			}
		}
	});
});
</script>
<form action="" method="post">
	<input type="hidden" name="dSerialNo" value="${policy.dSerialNo }" />
</form>
<div id="contents">
	<div id="navi">
		<img class="title" src="${request.contextPath}/images/title_0203.gif" alt="정책 관리" />
		<a href="/info/information.kdn">홈</a> &gt; <a href="/admin/rating/list.kdn">관리자 관리</a> &gt; <a href="/admin/rating/list.kdn">관리자 정책관리</a> &gt; 정책 관리
	</div>
	<p class="subtitle">상세 정보</p>
	<div class="table_wrap">
	<table class="board_write">
		<caption>상세 정보</caption>
		<colgroup>
			<col width="15%" />
			<col width="35%" />
			<col width="15%" />
			<col width="35%" />
		</colgroup>
		<tr>
			<th>기기정책관리번호</th>
			<td>${policy.dSerialNo }</td>
			<th>제조사명</th>
			<td>${policy.manName }</td>
		</tr>
		<tr>
			<th>기기 관리번호</th>
			<td>${policy.dModelNo }</td>
			<th>기기명</th>
			<td>${policy.deviceName }</td>
		</tr>
		<tr>
			<th>펌웨어 관리번호</th>
			<td>${policy.fwNo }</td>
		    <th>펌웨어명</th>
			<td>${policy.fwName }</td>
		</tr>
		<tr>
			<th>MCU 관리번호</th>
			<td>${policy.mcuNo }</td>
		    <th>MCU명</th>
			<td>${policy.mcuName }</td>
		</tr>
		<tr>
			<th>관리자ID</th>
			<td>${policy.lraId }</td>
		    <th>정책 적용일</th>
			<td>${policy.startDate }</td>
		</tr>
		<tr>
			<th>정책명</th>
			<td colspan="3">${policy.dPolicyCert }</td>
		</tr>
	</table>
	</div>
	<div class="btn_center">
		<c:url value="/admin/rating/form/mod.kdn" var="tutorialLink">  
			<c:param name="dSerialNo" value="${policy.dSerialNo }"></c:param>  
		</c:url>
		<a class="css_btn btntext" href="${tutorialLink }">수정</a>
		<a class="css_btn btntext" href="#">삭제</a>
		<a class="css_btn btntext" href="/admin/rating/list">취소</a>
	</div>
</div>
  