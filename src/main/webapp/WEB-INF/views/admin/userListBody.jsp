<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles"             prefix="tiles"      %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"              prefix="c"          %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"               prefix="fmt"        %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions"         prefix="fn"         %>
<script type="text/javascript" src="${request.contextPath}/js/admin/admin.js"> </script>
<script type="text/javascript">

$(document).ready(function(){
	
	if('${session.TYPE}' != 'lra'){
		window.location = "/info/information.kdn";
	}
	
	$("a").click(function(){
		if($(this).text()=='검색'){
			$("form" ).attr('action','${request.contextPath}/admin/list.kdn').submit();
		}else if($(this).text()=='삭제'){
			if($('input[name="selecter"]').is(":checked")){
				if(confirm("삭제 하시겠습니까?")){
					$("form" ).attr('action','${request.contextPath}/admin/user/form/del.kdn').submit();
				}
			}else{
				alert("선택하신 게시물이 없습니다.");
			}
		}
	});
	  
	$('#pageSize').change(function(){
		$("form" ).attr('action','${request.contextPath}/admin/list.kdn')
			.attr('method','post').submit();
	});
});
	
if('${message}'!=''){
	alert('${message}');
}
	
function goPage( current){
	$('#currentPage').val(current);
	 $("form" ).attr('action','${request.contextPath}/admin/list.kdn')
         .attr('method','post').submit();
};

	
</script>
    <div id="contents">
    	<div id="navi">
    	<img class="title" src="${request.contextPath}/images/title_0201.gif" />
    	<a href="/info/information.kdn">홈</a> > <a href="/admin/list.kdn">관리자 관리</a> > <a href="/admin/list.kdn">관리자 정보 관리</a> > <a href="/admin/list.kdn">관리 등록현황</a>
    	</div>
    	<form method="post">
        <input type="hidden"  name="currentPage" id="currentPage" value='${pageResult.currentPage}' >  
        <p class="subtitle">소속회사  검색</p>
        <div class="table_wrap">
            <table class="board_write">
            <caption>검색 조건</caption>
             <colgroup>
                <col width="15%" />
                <col>
            </colgroup>
              <tr>
                <th>검색 조건</th>
                <td><select name="kind" id="kind">
                        	<option value="companyName"  ${pageResult.kind=='companyName'?'selected="selected"':'' }>제조사명</option>
                        	<option value="managerName"  ${pageResult.kind=='managerName'?'selected="selected"':'' }>관리자명</option>
                        	<option value="managerId"   ${pageResult.kind=='managerId'?'selected="selected"':'' }>관리자 ID </option>
                        </select>&nbsp;<input name='searchKeyword' id="searchKeyword" type="text" value="${pageResult.searchKeyword}"> <a class="inner_btn btntext" href="#">검색</a></td>
              </tr>
            </table>
		</div>
	 
	  <p class="subtitle_left">관리자 목록</p>   
	  <div class="list_pages">건수/페이지 : <select name="pageSize" id="pageSize"  class="selectws">
	  <option value='5' ${pageResult.pageSize=='5'?'selected="selected"':'' }>5</option>
	  <option value='10' ${pageResult.pageSize=='10'||pageResult.pageSize==''?'selected="selected"':'' } >10</option>
	  <option value='20' ${pageResult.pageSize=='20'?'selected="selected"':'' }>20</option>
	  				</select>    Total : ${pageResult.totalCount} 개</div> 
        <div class="table_wrap">
            <table class="basic_list">
            <caption>관리자목록</caption>
             	<colgroup>
	                <col width="" />
	                <col width="" />
	                <col width="" />
	                <col width="" />
	                <col width="" />
	                <col width="" />
	                <col width="" />
           		</colgroup>
              <tr>
                <th>제조사명</th>
                <th>관리자 ID</th>
                <th>관리자명</th>
                <th>부서명</th>
                <th>직급</th>
                <th>전화 번호</th>
                <th>휴대폰 번호 </th>
              </tr>
             <c:if test= "${not empty pageResult.resultList}" >
            
             <c:forEach var="result" items="${pageResult.resultList}" varStatus="status">
             	<c:url value="/admin/view.kdn" var="viewLink">  
					<c:param name="adminId" value="${result.OPERATOR_ID}"></c:param>  
				</c:url>
				<c:url value="/admin/user/form/mod.kdn" var="tutorialLink">  
					<c:param name="adminId" value="${result.OPERATOR_ID}"></c:param>  
				</c:url>
    			<tr style="cursor:pointer;" onClick="location.href='${viewLink }'" onmouseover="this.style.background='#f1f1f1';" onmouseout="this.style.background='#ffffff';">
                <td><c:out value="${result.MAKER_NAME}"/></td>
                <td><c:out value="${result.OPERATOR_ID}"/></td>
                <td>${result.OPERATOR_NAME}</td>                
                <td>${result.DEPARTMENT}</td>
                <td>${result.OPER_POSITION}</td>
                <td>${result.PHONE}</td>
                <td>${result.CELL_PHONE}</td>
              </tr>
              </c:forEach>
               </c:if>
               <c:if test= "${empty pageResult.resultList}" >
	              <tr>
	                <td colspan="7">조회된 관리자 정보가 없습니다.</td>
	              </tr>
              </c:if>
            </table>
		</div>
		</form>
		<div class="page_num"> <c:if test= "${not empty pageResult.resultList}" >${pageResult.pagingPrint} </c:if></div>
	</div>
   