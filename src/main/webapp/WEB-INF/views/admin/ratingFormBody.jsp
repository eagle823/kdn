<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles"             prefix="tiles"      %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"              prefix="c"          %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"               prefix="fmt"        %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions"         prefix="fn"         %>

<script type="text/javascript">
$(document).ready(function(){
	
	$("a").click(function(){
		if($(this).text()=='등록' || $(this).text()=='수정'){
			submitData();
		}
	});
});

function submitData() {
	var submitData = $("#ratingForm").serializeArray();
	var formObject = {};
	$.each(submitData, function(i, v) {
		formObject[v.name] = v.value;
	});

	mcuAdd('${key}', formObject);
}

function mcuAdd(key,form){

	$.ajax({
		type : "POST",
		contentType: "application/json; charset=UTF-8",
		url : "/admin/rating/save/"+key+".kdn",
		data :  JSON.stringify(form), //데이터JSON.stringify
		dataType : "json",
		success : function(resultData){
			
			var type = (key =='add'? '등록' :'수정');
			
			if(resultData['result'] == 'nok'){
				alert(type+"에 실패 하였습니다.["+resultData['message']+"]");
				
			}else {
				alert(type +"되었습니다.");
				window.location = "/cert/devicePolicy/list.kdn";
			}
		},
		error : function(e) {
			alert("error : " + e);
		}
	});
}
</script>
<div id="contents">
	<div id="navi">
		<img class="title" src="${request.contextPath}/images/title_0203.gif" alt="정책 관리" />
		<a href="/info/information.kdn">홈</a> &gt; <a href="/admin/rating/list.kdn">관리자 관리</a> &gt; <a href="/admin/rating/list.kdn">관리자 정책관리</a> &gt; 정책 관리
	</div>
	<p class="subtitle">상세 정보</p>
	<form id="ratingForm"action="">
	<div class="table_wrap">
	<table class="board_write">
		<caption>상세 정보</caption>
		<colgroup>
			<col width="15%" />
			<col width="35%" />
			<col width="15%" />
			<col width="35%" />
		</colgroup>
		<tr>
			<th>기기정책관리번호</th>
			<td>
				${policy.dSerialNo }
				<input type="hidden" name="dSerialNo" value="${policy.dSerialNo }" />
			</td>
			<th>제조사명</th>
			<td>
				${policy.manName }
				<input type="hidden" name="manName" value="${policy.manName }" />
			</td>
		</tr>
		<tr>
			<th>기기 관리번호</th>
			<td>
				${policy.dModelNo }
				<input type="hidden" name="dModelNo" value="${policy.dModelNo }" />
			</td>
			<th>기기명</th>
			<td>
				${policy.deviceName }
				<input type="hidden" name="deviceName" value="${policy.deviceName }" />
			</td>
		</tr>
		<tr>
			<th>펌웨어 관리번호</th>
			<td>
				${policy.fwNo }
				<input type="hidden" name="fwNo" value="${policy.fwNo }" />
			</td>
		    <th>펌웨어명</th>
			<td>
				${policy.fwName }
				<input type="hidden" name="fwName" value="${policy.fwName }" />
			</td>
		</tr>
		<tr>
			<th>MCU 관리번호</th>
			<td>
				${policy.mcuNo }
				<input type="hidden" name="mcuNo" value="${policy.mcuNo }" />
			</td>
		    <th>MCU명</th>
			<td>
				${policy.mcuName }
				<input type="hidden" name="mcuName" value="${policy.mcuName }" />
			</td>
		</tr>
		<tr>
			<th>관리자ID</th>
			<td>
				${policy.lraId }
				<input type="hidden" name="lraId" value="${policy.lraId }" />
			</td>
		    <th>정책 적용일</th>
			<td>
				${policy.startDate }
				<input type="hidden" name="startDate" value="${policy.startDate }" />
			</td>
		</tr>
		<tr>
			<th>정책명</th>
			<td colspan="3">
				<select name="dPolicyCert" id="dPolicyCert">
					<c:forEach items="${certs.list }" var="item">
						<option value="${item.POLICY_NAME }"<c:if test="${item.POLICY_NAME == policy.dPolicyCert }"> selected="selected"</c:if>>${item.POLICY_NAME }</option>
					</c:forEach>
				</select>
			</td>
		</tr>
	</table>
	</div>
	</form>
	<div class="btn_center">
		<a class="css_btn btntext" href="#">등록</a>
		<a class="css_btn btntext" href="/admin/rating/list">취소</a>
	</div>
</div>
  