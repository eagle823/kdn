<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>비밀번호 변경</title>
<link href="${request.contextPath}/css/default.css" rel="stylesheet" type="text/css">
 <!--<![if lt IE 9]>-->
      <script type="text/javascript" src="${request.contextPath}/js/json2.js"></script>
   <!--<![endif]-->
<script type="text/javascript" src="${request.contextPath}/js/jquery-1.10.1.min.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
		$('#now_pw').focus();
		var p = opener.document.getElementById("password").value;
		$("a").click(function(){
			if($(this).text()=='확인'){
				if($('#now_pw').val() == ""){
					alert("비밀번호를 입력하세요.");
					$('#now_pw').focus();
					return;
				}else if(p != $('#now_pw').val()){
					alert("비밀번호가 정확하지 않습니다.");
					$('#now_pw').focus();
					return;
				}else{
					$(opener.location).attr("href", "javascript:submitData();");
					self.close();
				}
			}else if($(this).text()=='취소'){
				self.close();
			}
		});
	});
</script>
</head>
<body>
<p class="subtitle">비밀번호 확인</p>
	<div class="table_wrap">
	<table class="board_write">
	<caption>상세 정보 입력</caption>
		<colgroup>
			<col width="15%" />
			<col width="35%" />
			<col width="15%" />
			<col width="35%" />
		</colgroup>
		<tr>
			<th>비밀번호</th>
			<td><input type="password" name="now_pw" id="now_pw" /></td>
		</tr>
	</table>
	</div>
	<div class="btn_center">
		<a class="css_btn btntext" href="#">확인</a>
		<a class="css_btn btntext" href="#">취소</a>
	</div>
</body>
</html>