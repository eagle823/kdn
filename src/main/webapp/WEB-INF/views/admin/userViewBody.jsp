<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles"             prefix="tiles"      %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"              prefix="c"          %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"               prefix="fmt"        %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions"         prefix="fn"         %>

<script type="text/javascript">
$(document).ready(function(){
	  
	$("a").click(function(){
		if($(this).text()=='삭제'){
			if(confirm("삭제 하시겠습니까?")){
				$("form" ).attr('action','${request.contextPath}/admin/user/form/del.kdn').submit();
			}
		}
	});
});
</script>
<form action="" method="post">
	<input type="hidden" name="selecter" value="${user.userId }" />
</form>
<div id="contents">
	<div id="navi">
		<img class="title" src="${request.contextPath}/images/title_0201.gif" />
		<a href="/info/information.kdn">홈</a> &gt; <a href="/admin/list.kdn">관리자 관리</a> &gt; <a href="/admin/list.kdn">관리자 정보 관리</a> &gt; 정보수정
	</div>
<p class="subtitle">정보 입력</p>
<div class="table_wrap">
<table class="board_write">
	<caption>상세 정보 입력</caption>
	<colgroup>
		<col width="15%" />
		<col width="35%" />
		<col width="15%" />
		<col width="35%" />
	</colgroup>
	<tr>
	  <th>관리자 ID</th>
	  <td>${user.userId }</td>
		<th>관리자 이름</th>
		<td>${user.userName }</td>
	</tr>
	<tr>
		<th>지역</th>
		<td>${user.region }</td>
	    <th>휴대폰 번호</th>
	    <td>${user.mobile1 } - ${user.mobile2 } - ${user.mobile3 }</td>
	</tr>
	<tr>
		<th>전화 번호</th>
		<td>${user.phone1 } - ${user.phone2 } - ${user.phone3 }</td>
		<th>부서명</th>
		<td>${user.corp }</td>
	</tr>
	<tr>
		<th>FAX</th>
		<td>${user.fax1 } - ${user.fax2 } - ${user.fax3 }</td>
		<th>이메일</th>
		<td>${user.email }</td>
	</tr>
	<tr>
		<th>직급</th>
		<td>${user.grade }</td>
		<th>직책</th>
		<td>${user.position }</td>
	</tr>
	<tr>
		<th>제조사</th>
		<td rowspan="3">${user.maker }</td>
	</tr>
</table>
</div>
<p class="subtitle">권한 부여</p>
<div class="table_wrap">
	<table class="board_write">
		<caption>권한 부여</caption>
		<colgroup>
			<col width="20%" />
			<col width="13%" />
			<col width="20%" />
			<col width="13%" />
			<col width="20%" />
			<col width="13%" />
		</colgroup>
		<tr>
			<th>기기 관리</th>
			<td>${user.device=='t'?'부여':'미부여' }</td>
			<th>인증서 관리</th>
			<td>${user.cert=='t'?'부여':'미부여' }</td>
			<th>감사 관리</th>
			<td>${user.auth=='t'?'부여':'미부여' }</td>
		</tr>
	</table>
</div>
	<div class="btn_center">
		<c:url value="/admin/user/form/mod.kdn" var="tutorialLink">  
			<c:param name="adminId" value="${user.userId }"></c:param>  
		</c:url>
		<a class="css_btn btntext" href="${tutorialLink }">수정</a>
		<a class="css_btn btntext" href="#">삭제</a>
		<a class="css_btn btntext" href="/admin/list.kdn">취소</a>
	</div>
</div>
  