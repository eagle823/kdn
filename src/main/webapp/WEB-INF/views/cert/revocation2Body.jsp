<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%request.setCharacterEncoding("UTF-8");%>
<script src="${request.contextPath}/js/jquery.ui.core.min.js"></script>
<script src="${request.contextPath}/js/jquery.ui.widget.min.js"></script>
<script src="${request.contextPath}/js/jquery.ui.datepicker.min.js"></script>
<script src="${request.contextPath}/js/jquery.ui.datepicker-kr.js"></script>
<script type="text/javascript" src="${request.contextPath}/js/cert/issue.js"></script>
<script type="text/javascript" src="${request.contextPath}/js/cert/model.js"></script>
<script type="text/javascript" src="${request.contextPath}/js/cert/revocation.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	$(document).ready(function(){
		$('#pageSize').change(function(){
			$("form" ).attr('action','${request.contextPath}/cert/revocation2.kdn').attr('method','post').submit();
		});
	});
	
	if('${message}'!=''){
		alert('${message}');
	}

	function goPage(current){
		$('#currentPage').val(current);
		$("form" ).attr('action','${request.contextPath}/cert/revocation2.kdn').attr('method','post').submit();
	};
	
	$( "#datepicker" ).datepicker( $.datepicker.regional[ "kr" ] );

	$( "#sdate" ).datepicker({
		defaultDate: "-1m",
		changeMonth: true,
		changeYear: true,
		onClose: function( selectedDate ) {
			$( "#edate" ).datepicker( "option", "minDate", selectedDate );
		}
	});
	
	$("#edate").datepicker({
		changeMonth: true,
		changeYear: true,
		onClose: function( selectedDate ) {
			$( "#sdate" ).datepicker( "option", "maxDate", selectedDate );
		}
		
	});
	
	var _order =["devices","firmwares","mcus"];	  
	var _ordername =["device","firmware","mcu"];
	$('select').change(function(){
		var name =$(this).attr('name');
		 if($(this).val() != ''){
			var inp =  _order.indexOf(name);
			if(inp+1 < _order.length){
			$('#'+_order[inp+1]).attr("disabled",false);
			 deviceControl(_ordername[inp+1],'list');
			}
		 };
		 $('#'+_order[inp]+'Name').val($(this).children("option:selected").text());        
    });
	
	$("a").click(function(){
		if($(this).text()=='인증서 폐지'){
			var dnlist = new Array();
			$("input:checkbox[name='savecheck']:checked").each(function(){
				dnlist.push( $(this).val());
			});
			if(dnlist.length != 0){
				if(confirm("선택하신 "+dnlist.length+"개 인증서를 폐지 하시겠습니까?")){
					revocationCert(dnlist);
				}
			} else {
				alert("폐지할 인증서를 선택해 주세요.");
			}
		}else if($(this).text()=='제조사검색'){
			  $( "#companyName" ).val("");
			   ShowModalPopup('${request.contextPath}/popup/findCompany.kdn?type=device','500','600');
			 // ShowModalPopup('${request.contextPath}/popup/findCompany.kdn?type=device','500','600');
		  }else if($(this).text()=='검색'){
			  $("form" ).attr('action','${request.contextPath}/cert/revocation.kdn').attr('method','post').submit();
		  }else if($(this).text()=='기기 삭제') {
			  var dnlist = new Array();
				$("input:checkbox[name='savecheck']:checked").each(function(){
					dnlist.push( $(this).val());
				});
				if(dnlist.length != 0){
					if(confirm("선택하신 "+dnlist.length+"개 기기를 삭제 하시겠습니까?")){
						certDelete(dnlist);
					}
				} else {
					alert("삭제할 기기를 선택해 주세요.");
				}
		  }
	});
});

function goPage(current){
	$('#currentPage').val(current);
	 $("form" ).attr('action','${request.contextPath}/cert/revocation.kdn')
         .attr('method','post').submit();
};

$.fn.extend({
	 device:function(){
	 
	  if($('#companyName').val() != ''){
		  deviceControl('device','list');
		   }
	 }
});
</script>
<form method="post">
	<input type="hidden"  name="currentPage" id="currentPage" value="${pageResult.currentPage }" >
	<input type="hidden" name="devicesName" id="devicesName" value="${searchDevices.device }" /> 
	<input type="hidden" name="firmwaresName" id="firmwaresName" value="${searchDevices.firmware }" /> 
	<input type="hidden" name="mcusName" id="mcusName" value="${searchDevices.mcu }" /> 
    <div id="contents">
    	<div id="navi">
    	  <img class="title" src="${request.contextPath}/images/title_0404.gif" alt="기기 인증서 폐지">
    	<a href="/info/information.kdn">홈</a> &gt; <a href="/cert/deviceModel.kdn">기기 정보 관리</a> &gt; <a href="/cert/issued.kdn">기기 신청 관리</a> &gt; <a href="/cert/revocation.kdn">폐지 신청</a>    	
    	</div>
        <p class="subtitle">기기 검색 조건</p>
        <div class="table_wrap">
              <table class="table_search">
                    <colgroup>
                    <col width="15%">
                    <col width="35%">
                    <col width="15%">
                    <col width="35%">
                    </colgroup>
                    <tr>
                    	<th>제조사명</th>
                    	<td><input type="text" name ="companyName" id='companyName' size='20' value='${searchDevices.MAKER_NAME}'>&nbsp;<a class="inner_btn btntext" href="#">제조사검색</a></td>
                    	<th>기기 시리얼 번호</th>
                    	<td><input type="text" name ="serialNo" id='serialNO' class="inputw100" value="${searchDevices.serialNo }"></td>
                    </tr>
                    <tr>
                        <th>기기명</th>
                        <td><select name='devices' id='devices'>
                                <option value=''>선택</option>
                                <c:if test="${not empty devices.list}">
                                    <c:forEach var="result" items="${ devices.list}"
                                        varStatus="status">
                                        <option value='${result.D_MODEL_NO}'<c:if test="${searchDevices.device ==  result.DEVICE_NAME}"> selected="selected"</c:if>>${result.DEVICE_NAME}</option>
                                    </c:forEach>
                                </c:if>
                        </select></td>
						<th>펌웨어 명</th>
                        <td>
                        	<select name='firmwares' id='firmwares'<c:if test="${empty firmwares.list}"> disabled="disabled"</c:if>>
                        		<option value="">선택</option>
                        		<c:if test="${not empty firmwares.list}">
                                    <c:forEach var="result" items="${ firmwares.list}"
                                        varStatus="status">
                                        <option value='${result.FW_NO}'<c:if test="${searchDevices.firmware ==  result.FW_NAME}"> selected="selected"</c:if>>${result.FW_NAME}</option>
                                    </c:forEach>
                                </c:if>
                        	</select>
                        </td>
                    </tr>
                    <tr>
						<th>MCU 명</th>
						<td colspan="3">
							<select name='mcus' id='mcus'<c:if test="${empty mcus.list}"> disabled="disabled"</c:if>>
								<option value="">선택</option>
								<c:if test="${not empty mcus.list}">
                                    <c:forEach var="result" items="${ mcus.list}"
                                        varStatus="status">
                                        <option value='${result.MCU_NO}'<c:if test="${searchDevices.mcu == result.MCU_NAME}"> selected="selected"</c:if>>${result.MCU_NAME}</option>
                                    </c:forEach>
                                </c:if>
							</select>
						</td>
                    </tr>
           </table> 
	   </div>
        <p class="subtitle">인증서 검색 조건</p>
        <div class="table_wrap">
              <table class="table_search">
                    <colgroup>
                    <col width="15%">
                    <col width="">
                    </colgroup>
                    <tr>
                    	<th>발급 상태</th>
                    	<td><input type="radio" checked="checked" name='stats' value="T"<c:if test="${searchDevices.stats != 'F'}"> checked="checked"</c:if>>발급
                    	<input type="radio" name='stats' value="F"<c:if test="${searchDevices.stats == 'F'}"> checked="checked"</c:if>>미발급</td>
                    </tr>
                    <tr>
                        <th>발급날짜</th>
                        <td><input type="text" name='sdate' id='sdate' value='${dateResult.sDate}' > ~ <input type="text" name='edate' id='edate' value='${dateResult.eDate}'> <a class="inner_btn btntext ar" href="#">검색</a></td>
                    </tr>
           </table> 
	   </div>
        <p class="subtitle_left">기기 리스트</p>
    	<div class="list_pages">
			Total : ${pageResult.totalCount} 개</div> 
   		 <div class="table_wrap">
		<table class="basic_list">
			<caption>기기 리스트</caption>
			<colgroup>
				<col width="">
				<col width="">
				<col width="">
				<col width="">
				<col width="">
				<col width="">
				<col width="">
			</colgroup>
			<tr>
				<th>선택</th>
				<th>제조사명</th>
				<th>기기 시리얼 번호</th>
				<th>기기명</th>
				<th>MCU 명</th>
				<th>인증서 상태</th>
				<th>발급 날짜</th>
			</tr>
			<c:if test= "${not empty pageResult.resultList}" >
				<c:forEach var="result" items="${pageResult.resultList}" varStatus="status">
					<c:url value="${request.contextPath}/cert/management/edit.kdn" var="tutorialLink">  
						<c:param name="DN" value="${result.DN}"></c:param>  
					</c:url>
					<tr onmouseover="this.style.background='#f1f1f1';" onmouseout="this.style.background='#ffffff';">
						<td><input name="savecheck" type="checkbox" value="${result.DN}"></td>
						<td style="cursor:pointer;" onClick="location.href='${tutorialLink }'" >${result.MAKER_NAME}</td>
						<td style="cursor:pointer;" onClick="location.href='${tutorialLink }'" >
                             ${result.MS_SERIAL}
						</td>
						<td style="cursor:pointer;" onClick="location.href='${tutorialLink }'" >${result.MAKER_NAME }</td>
						<td style="cursor:pointer;" onClick="location.href='${tutorialLink }'" >${result.CHIP_NMAE}</td>
						<td style="cursor:pointer;" onClick="location.href='${tutorialLink }'" >${result.CERT_STATE}</td>
						<td style="cursor:pointer;" onClick="location.href='${tutorialLink }'" >${result.REG_DATE }</td>
					</tr>
				</c:forEach>
			</c:if>
			<c:if test= "${empty pageResult.resultList}" >
				<tr>
					<td colspan="7">조회된 인증서 정보가 없습니다.</td>
				</tr>
			</c:if>
		</table>
	</div>
	<div class="page_num"> <c:if test= "${not empty pageResult.resultList}" >${pageResult.pagingPrint} </c:if> </div>
	<div class="btn_center">
		<a class="css_btn btntext" href="#">기기 삭제</a>
		<a class="css_btn btntext" href="#">인증서 폐지</a>
	</div>
</div>
</form>
