<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles"             prefix="tiles"      %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"              prefix="c"          %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"               prefix="fmt"        %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions"         prefix="fn"         %>

<script type="text/javascript">
$(document).ready(function(){
	  
	$("a").click(function(){
		if($(this).text()=='삭제'){
			if(confirm("삭제 하시겠습니까?")){
				$("form" ).attr('action','${request.contextPath}/cert/deviceFirmware/form/del.kdn').submit();
			}
		}
	});
});
</script>
<form action="" method="post">
	<input type="hidden" name="fwNo" value="${firmware.fwNo }" />
</form>
<div id="contents">
	<div id="navi">
		<img class="title" src="${request.contextPath}/images/title_0414.gif" alt="펌웨어 관리" />
		<a href="/info/information.kdn">홈</a> &gt; <a href="/cert/deviceModel/list.kdn">기기 정보 관리</a> &gt; <a href="/cert/deviceModel/list.kdn">기기 모델 관리</a> &gt; 펌웨어 관리
	</div>
	<p class="subtitle">상세 정보</p>
	<div class="table_wrap">
	<table class="board_write">
		<caption>상세 정보</caption>
		<colgroup>
			<col width="15%" />
			<col width="35%" />
			<col width="15%" />
			<col width="35%" />
		</colgroup>
		<tr>
			<th>펌웨어관리 번호</th>
			<td>${firmware.fwNo }</td>
			<th>등록 제조사명</th>
			<td>${firmware.manName }</td>
		</tr>
		<tr>
			<th>펌웨어명</th>
			<td>${firmware.fwName }</td>
		    <th>기기 종류</th>
		    <td>${firmware.deviceType }</td>
		</tr>
		<tr>
			<th>펌웨어 제조사명</th>
			<td>${firmware.firmwareManName }</td>
			<th>펌웨어 도입 일</th>
			<td>${firmware.fwApplyDate }</td>
		</tr>
	</table>
	</div>
	<div class="btn_center">
		<c:url value="/cert/deviceFirmware/form/mod.kdn" var="tutorialLink">  
			<c:param name="fwNo" value="${firmware.fwNo }"></c:param>  
		</c:url>
		<a class="css_btn btntext" href="${tutorialLink }">수정</a>
		<a class="css_btn btntext" href="#">삭제</a>
		<a class="css_btn btntext" href="/cert/deviceFirmware/list">취소</a>
	</div>
</div>
  