<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles"             prefix="tiles"      %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"              prefix="c"          %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"               prefix="fmt"        %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions"         prefix="fn"         %>

<script src="${request.contextPath}/js/jquery.ui.core.min.js"></script>
<script src="${request.contextPath}/js/jquery.ui.widget.min.js"></script>
<script src="${request.contextPath}/js/jquery.ui.datepicker.min.js"></script>
<script src="${request.contextPath}/js/jquery.ui.datepicker-kr.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	$( "#datepicker" ).datepicker( $.datepicker.regional[ "kr" ] );
	$( "#fwApplyDate" ).datepicker({
	 	changeMonth: true,
		changeYear: true
	});
	 
	$("a").click(function(){
		if($(this).text()=='등록' || $(this).text()=='수정'){
			if($("#fwName").val() == ""){
				alert("펌웨어 명(버젼 포함)을 입력하세요.");
				$("#fwName").focus();
				return false;
			}
			
			if($("#deviceType").val() == ""){
				alert("기기 종류를 선택하세요.");
				$("#deviceType").focus();
				return false;
			}
			
			if($("#deviceManName").val() == ""){
				alert("펌웨어 제조사명을 입력하세요.");
				$("#deviceManName").focus();
				return false;
			}
			
			if($("#fwApplyDate").val() == ""){
				alert("펌웨어 도입일을 입력하세요.");
				$("#fwApplyDate").focus();
				return false;
			}
			
			submitData();
		}
	});
});

function submitData() {
	var submitData = $("#firmwareForm").serializeArray();
	var formObject = {};
	$.each(submitData, function(i, v) {
		formObject[v.name] = v.value;
	});

	firmwareAdd('${key}', formObject);
}

function firmwareAdd(key,form){

	$.ajax({
		type : "POST",
		contentType: "application/json; charset=UTF-8",
		url : "/cert/deviceFirmware/save/"+key+".kdn", //호출 URL  /production/company/save/add.kdn
		data :  JSON.stringify(form), //데이터JSON.stringify
		dataType : "json",
		success : function(resultData){
			
			var type = (key =='add'? '등록' :'수정');
			
			if(resultData['result'] == 'nok'){
				alert(type+"에 실패 하였습니다.["+resultData['message']+"]");
				
			}else {
				alert(type +"되었습니다.");
				window.location = "/cert/deviceFirmware/list.kdn";
			}
		},
		error : function(e) {
			alert("error : " + e);
		}
	});
}
</script>
<div id="contents">
	<div id="navi">
		<img class="title" src="${request.contextPath}/images/title_0414.gif" alt="펌웨어 관리" />
		<a href="/info/information.kdn">홈</a> &gt; <a href="/cert/deviceModel/list.kdn">기기 정보 관리</a> &gt; <a href="/cert/deviceModel/list.kdn">기기 모델 관리</a> &gt; 펌웨어 관리
	</div>
	<p class="subtitle">상세 정보</p>
	<div class="table_wrap">
	<form id="firmwareForm"action="">
	<input type="hidden" name="fwNo" value="${firmware.fwNo }" />
		<table class="board_write">
			<caption>상세 정보</caption>
			<colgroup>
				<col width="15%" />
				<col width="35%" />
				<col width="15%" />
				<col width="35%" />
			</colgroup>
			<tr>
				<th>펌웨어 명(버젼 포함)</th>
				<td><input type="text" name="fwName" id="fwName" value="${firmware.fwName }" /></td>
			    <th>기기 종류</th>
			    <td>
			    	<c:choose>
			    		<c:when test="${key == 'mod' }">
			    			${firmware.deviceType }
			    			<input type="hidden" name="deviceType" value="${firmware.deviceType }" />
			    		</c:when>
			    		<c:otherwise>
			    			<select name="deviceType" id="deviceType" >
					    		<option value="">선택하세요</option>
					    		<option value="FRTU">FRTU</option>
					    		<option value="DCU">DCU</option>
					    		<option value="METER">METER</option>
					    	</select>
			    		</c:otherwise>
			    	</c:choose>
			    </td>
			</tr>
			<tr>
				<th>펌웨어 제조사명</th>
				<td><input type="text" name="firmwareManName" id="firmwareManName" value="${firmware.firmwareManName }" /></td>
				<th>펌웨어 도입일</th>
				<td><input type="text" name="fwApplyDate" id="fwApplyDate" value="${firmware.fwApplyDate }" /></td>
			</tr>
		</table>
	</form>
	</div>
	<div class="btn_center">
		<a class="css_btn btntext" href="#">${key=='mod'?'수정':'등록'}</a>
		<a class="css_btn btntext" href="/cert/deviceFirmware/list">취소</a>
	</div>
</div>
 