<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles"             prefix="tiles"      %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"              prefix="c"          %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"               prefix="fmt"        %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions"         prefix="fn"         %>
<script type="text/javascript">
$(document).ready(function(){
	  
	$("a").click(function(){
		if($(this).text()=='등록' || $(this).text()=='수정'){
			if($("#deviceName").val() == ""){
				alert("기기명(모델명)을 입력하세요.");
				$("#deviceName").focus();
				return false;
			}
			
			if($("#deviceType").val() == ""){
				alert("기기 종류를 선택하세요.");
				$("#deviceType").focus();
				return false;
			}
			
			if($("#deviceManName").val() == ""){
				alert("기기 제조사명을 입력하세요.");
				$("#deviceManName").focus();
				return false;
			}
			
			if($("#os").val() == ""){
				alert("탐제 OS를 입력하세요.");
				$("#os").focus();
				return false;
			}
			
			submitData();
		}
	});
});

function submitData() {
	var submitData = $("#deviceForm").serializeArray();
	var formObject = {};
	$.each(submitData, function(i, v) {
		formObject[v.name] = v.value;
	});

	deviceAdd('${key}', formObject);
}

function deviceAdd(key,form){

	$.ajax({
		type : "POST",
		contentType: "application/json; charset=UTF-8",
		url : "/cert/deviceModel/save/"+key+".kdn", //호출 URL  /production/company/save/add.kdn
		data :  JSON.stringify(form), //데이터JSON.stringify
		dataType : "json",
		success : function(resultData){
			
			var type = (key =='add'? '등록' :'수정');
			
			if(resultData['result'] == 'nok'){
				alert(type+"에 실패 하였습니다.["+resultData['message']+"]");
				
			}else {
				alert(type +"되었습니다.");
				window.location = "/cert/deviceModel/list.kdn";
			}
		},
		error : function(e) {
			alert("error : " + e);
		}
	});
}
</script>
<div id="contents">
	<div id="navi">
		<img class="title" src="${request.contextPath}/images/title_0413.gif" alt="기기 관리" />
		<a href="/info/information.kdn">홈</a> &gt; <a href="/cert/deviceModel/list.kdn">기기 정보 관리</a> &gt; <a href="/cert/deviceModel/list.kdn">기기 모델 관리</a> &gt; 기기 관리
	</div>
	<p class="subtitle">상세 정보</p>
	<div class="table_wrap">
	<form id="deviceForm"action="">
	<input type="hidden" name="dModelNo" value="${device.dModelNo }" />
		<table class="board_write">
			<caption>상세 정보</caption>
			<colgroup>
				<col width="15%" />
				<col width="35%" />
				<col width="15%" />
				<col width="35%" />
			</colgroup>
			<tr>
				<th>기기명(모델명)</th>
				<td><input type="text" name="deviceName" id="deviceName" value="${device.deviceName }" /></td>
			    <th>기기 종류</th>
			    <td>
			    	<c:choose>
			    		<c:when test="${key == 'mod' }">
			    			${device.deviceType }
			    			<input type="hidden" name="deviceType" value="${device.deviceType }" />
			    		</c:when>
			    		<c:otherwise>
			    			<select name="deviceType" id="deviceType" >
					    		<option value="">선택하세요</option>
					    		<option value="FRTU">FRTU</option>
					    		<option value="DCU">DCU</option>
					    		<option value="METER">METER</option>
					    	</select>
			    		</c:otherwise>
			    	</c:choose>
			    </td>
			</tr>
			<tr>
				<th>기기 제조사명</th>
				<td><input type="text" name="deviceManName" id="deviceManName" value="${device.deviceManName }" /></td>
				<th>탐제 OS</th>
				<td><input type="text" name="os" id="os" value="${device.os }" /></td>
			</tr>
		</table>
	</form>
	</div>
	<div class="btn_center">
		<a class="css_btn btntext" href="#">${key=='mod'?'수정':'등록'}</a>
		<a class="css_btn btntext" href="/cert/deviceModel/list">취소</a>
	</div>
</div>
 