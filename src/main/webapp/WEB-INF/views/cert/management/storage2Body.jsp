<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%request.setCharacterEncoding("UTF-8");%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles"             prefix="tiles"      %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"              prefix="c"          %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"               prefix="fmt"        %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions"         prefix="fn"         %>
<script type="text/javascript" src="${request.contextPath}/js/cert/model.js"></script>
<script type="text/javascript" src="${request.contextPath}/js/cert/storage.js"></script>

<script src="${request.contextPath}/js/jquery.ui.core.min.js"></script>
<script src="${request.contextPath}/js/jquery.ui.widget.min.js"></script>
<script src="${request.contextPath}/js/jquery.ui.datepicker.min.js"></script>
<script src="${request.contextPath}/js/jquery.ui.datepicker-kr.js"></script>
<script type="text/javascript">
 

<!--
var rows = new Array();
$(document).ready(function(){
	var type = '${KDN_LOGIN_SESSION.TYPE }';	
	
	 $('#companyName').click(function(){
		 if(type == 'lra'){
		   $( "#companyName" ).val("");
		   ShowModalPopup('${request.contextPath}/popup/findCompany.kdn?type=device','500','600');
		 }
	 });
	 
	 $( "#datepicker" ).datepicker( $.datepicker.regional[ "kr" ] );
	 $( "#sdate" ).datepicker({
	 	defaultDate: "+1w",
	 	changeMonth: true,
	 	onClose: function( selectedDate ) {
	 		$( "#edate" ).datepicker( "option", "minDate", selectedDate );
	 	}
	 });
	 $("#edate").datepicker({
	 	defaultDate: "+1w",
	 	changeMonth: true,
	 	onClose: function( selectedDate ) {
	 		$( "#sdate" ).datepicker( "option", "maxDate", selectedDate );
	 	}
	 	
	 });
	 
	 var _order =["devices","mcus"];	  
		var _ordername =["device","mcu"];	  
	
	
	$('select').change(function(){
		var name =$(this).attr('name');
		 if($(this).val() != ''){
			var inp =  _order.indexOf(name);
			
			if(inp+1 < _order.length){
			$('#'+_order[inp+1]).attr("disabled",false);
			 deviceControl(_ordername[inp+1],'list');
			}
		 };
		 $('#'+_order[inp]+'Name').val($(this).children("option:selected").text());
    });	  
    
    $("a").click(function(){
		  if($(this).text()=='등록'||$(this).text()=='수정'){
			var submitData = $("#companyForm" ).serializeArray();
			  var formObject = {};
			  $.each(submitData,
			      function(i, v) {
			          formObject[v.name] = v.value;
			      });
			 
			   productAdd(formObject,'${key}');
			   
		  }else if($(this).text()=='제조사검색'){
			  $( "#companyName" ).val("");
			   ShowModalPopup('${request.contextPath}/popup/findCompany.kdn?type=device','500','600');
		  }else if($(this).text()=='검색'){
			  $("form" ).attr('action','${request.contextPath}/cert/management/storage.kdn').submit();
		  }else if($(this).text()=='모든 인증서 저장'){
			  var total ='${pageResult.totalCount}';
				  if(confirm("전체 "+total+"개 인증서를 저장 하시겠습니까?")){
					//  $("form" ).attr('action','${request.contextPath}/cert/management/storage/allsave.kdn').submit(); 
					  
					 };
		  }else if($(this).text()=='선택 인증서 저장'){
			  
			  var dnlist = new Array();

			  $("input:checkbox[name='savecheck']:checked").each(function(){
				 dnlist.push( $(this).val());
			  });
			 if(confirm("선택하신 "+dnlist.length+"개 인증서를 저장 하시겠습니까?")){
				 saveCert(dnlist); // $("form" ).attr('action','${request.contextPath}/cert/management/storage/save.kdn').submit();
			 };
			  
		  }
		  
	  });
	
});


function goPage( current){
	$('#currentPage').val(current);
	 $("form" ).attr('action','${request.contextPath}/cert/management/storage2.kdn')
      .attr('method','post').submit();
};

$.fn.extend({
	 device:function(){
	 
	  if($('#companyName').val() != ''){
		  deviceControl('device','list');
		   }
	 }
});

function saveFile(fileList){
	document.KDNApplet.SaveFiles(fileList);
}



	-->
	
	
	</script>
	<form method="post">

<input type="hidden" name="currentPage" id="currentPage" value="${pageResult.currentPage}">
<input type="hidden" name="devicesName" id="devicesName" value="${searchDevices.device }" /> 
<input type="hidden" name="firmwaresName" id="firmwaresName" value="없음" /> 
<input type="hidden" name="mcusName" id="mcusName" value="${searchDevices.mcu }" />
    <div id="contents">
    	<div id="navi"><img src="${request.contextPath}/images/title_0411.gif" class="title" />
    	<a href="/info/information.kdn">홈</a> &gt; <a href="/cert/management/status.kdn">인증서 관리</a> &gt; 인증서 저장 
    	</div>
           
      <p class="subtitle">기기 검색 조건</p>
        <div class="table_wrap">
              <table class="table_search">
                    <colgroup>
                    <col width="15%" />
                    <col width="35%" />
                    <col width="15%" />
                    <col width="35%" />
                    </colgroup>
                    <tr>
                    	<th>제조사명</th>
                    	<td><input type="text" name ="companyName" id='companyName' size='20' value='${searchDevices.MAKER_NAME }' readonly="readonly"><c:if test="${KDN_LOGIN_SESSION.TYPE eq 'lra'}"> &nbsp;<a class="inner_btn btntext" href="#">제조사검색</a></c:if></td>
                    	<th>기기 시리얼 번호</th>
                    	<td><input type="text" name ="serialNo" id='serialNo' size='20' value='${searchDevices.serialNo }'></td>
                    </tr>
                    <tr>
				<th> 기기명</th>
				<td colspan="3">
					<select name='devices' id='devices'>
						<option value=''>선택</option>
					<c:if test="${not empty devices.list}">
						<c:forEach var="result" items="${devices.list}" varStatus="status">
						<option value='${result.D_MODEL_NO}'<c:if test="${searchDevices.device ==  result.DEVICE_NAME}"> selected="selected"</c:if>>${result.DEVICE_NAME}</option>
						</c:forEach>
					</c:if>
					</select>
				</td>
			</tr>
			<tr>
				<th>MCU 명</th>
				<td colspan="3">
					<select name='mcus' id='mcus'<c:if test="${empty mcus.list}"> disabled="disabled"</c:if>>
						<option value="">선택</option>
							<c:if test="${not empty mcus.list}">
								<c:forEach var="result" items="${ mcus.list}" varStatus="status">
									<option value='${result.MCU_NO}'<c:if test="${searchDevices.mcu == result.MCU_NAME}"> selected="selected"</c:if>>${result.MCU_NAME}</option>
								</c:forEach>
							</c:if>
					</select>
				</td>
			</tr>
           </table> 
	   </div>
         <p class="subtitle">인증서 검색 조건</p>
        <div class="table_wrap">
              <table class="table_search">
                    <colgroup>
                    <col width="15%" />
                    <col width="85%" />
                    </colgroup>
                    <tr>
                    	<th >발급 상태</th>
                    	<td>
                    		<input type="radio" name='stats' value="T"<c:if test="${searchDevices.stats != 'F' }"> checked="checked"</c:if>/>발급
                    	    <input type="radio" name='stats' value='F'<c:if test="${searchDevices.stats == 'F' }"> checked="checked"</c:if>/>미발급   
						</td>
                    </tr>
                    <tr>
                        <th>발급날짜</th>
                        <td><input type="text" name='sdate' id='sdate' value='${searchDevices.sdate}' > ~ <input type="text" name='edate'  value='${searchDevices.edate}'id='edate'> <a class="inner_btn btntext" href="#">검색</a></td>
                    </tr>
           </table> 
	   </div>
         <p class="subtitle_left">인증서 리스트</p>
      <div class="list_pages">건수/페이지 : <select name="pageSize" id="pageSize" class="selectws">
	  <option value='5' ${pageResult.pageSize=='5'?'selected="selected"':'' }>5</option>
	  <option value='10' ${pageResult.pageSize=='10'||pageResult.pageSize==''?'selected="selected"':'' } >10</option>
	  <option value='20' ${pageResult.pageSize=='20'?'selected="selected"':'' }>20</option>
	  				</select>    Total : ${pageResult.totalCount} 개</div>      
   		 <div class="table_wrap">
            <table class="basic_list">
            <caption>인증서 리스트</caption>
             	<colgroup>
                <col width="" />
                <col width="" />
                <col width="" />
                <col width="" />
                <col width="" />
                <col width="" />
           		</colgroup>
              <tr>
                <th>선택</th>
                <th>시리얼 번호</th>
                <th>DN</th>
                <th>유효기간 시작</th>
                <th>유효기간 만료</th>
                <th>제조사명</th>
              </tr>
            
             <c:if test= "${not empty pageResult.resultList}" >
            
             <c:forEach var="result" items="${pageResult.resultList}" varStatus="status">
	              <tr onmouseover="this.style.background='#f1f1f1';" onmouseout="this.style.background='#ffffff';">
	                <td><input name="savecheck" type="checkbox" value="${result.DN}"></td>
	                <td>${result.SERIAL_NO}</td>
	                <td>${result.DN} </td>
	                <td>${result.VALID_FROM}</td>
	                <td>${result.VALID_TO}</td>
	                <td>${result.MAN_NAME}</td>
	              </tr>
              </c:forEach>
              </c:if>
              <c:if test= "${empty pageResult.resultList}" >
	              <tr>
	                <td colspan="7">조회된 인증서 정보가 없습니다.</td>
	              </tr>
              </c:if>
            </table>
		</div>
        <div class="page_num"> <c:if test= "${not empty pageResult.resultList}" >${pageResult.pagingPrint} </c:if> </div>
        <div class="btn_center">
        <a class="css_btn btntext" href="#">모든 인증서 저장</a>
        <a class="css_btn btntext" href="#">선택 인증서 저장</a>
        
        </div>
    </div>
    
        </form>
  <!--[if !IE]>-->
      <object ID="KDNApplet" classid="java:kdn.applet.KDNApplet.class" 
              type="application/x-java-applet"
              height="0" width="0" >
          <param name="codebase" value="/js/" />
          <param NAME="archive" value="KDN_APPLET.jar">
          <param name="code" value="kdn.applet.KDNApplet" />
           <param NAME="scriptable" VALUE="true">
			<param NAME="mayscript" VALUE="true">
			<param NAME="codebase_lookup" value="false">
      <!--<![endif]-->
        <object ID="KDNApplet"  classid="clsid:8AD9C840-044E-11D1-B3E9-00805F499D93" 
                height="0" width="0" > 
          <param name="codebase" value="/js/" />
          <param NAME="archive" value="KDN_APPLET.jar"/>
          <param name="code" value="kdn.applet.KDNApplet" />
           <param NAME="scriptable" VALUE="true" />
			<param NAME="mayscript" VALUE="true"/>
			<param NAME="codebase_lookup" value="false"/>
        </object> 
      <!--[if !IE]>-->
      </object>
    