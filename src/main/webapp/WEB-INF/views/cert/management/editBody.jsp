<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%request.setCharacterEncoding("UTF-8");%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles"             prefix="tiles"      %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"              prefix="c"          %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"               prefix="fmt"        %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions"         prefix="fn"         %>
    <div id="contents">
    	<div id="navi"><img src="${request.contextPath}/images/title_0407.gif" class="title" /><a href="/info/information.kdn">홈</a> > <a href="/cert/management/status.kdn">인증서 관리</a> > <a href="/cert/management/status.kdn">인증서 조회</a></div>
         
        <%-- <p class="subtitle">기기 상세 정보 입력</p>
        <div class="table_wrap">
              <table class="table_search">
                    <colgroup>
                    <col width="15%" />
                    <col width="35%" />
                    <col width="15%" />
                    <col width="35%" />
                    </colgroup>
                    <tr>
                    	<th>제조사명</th>
                    	<td><input type="text" name="companyName" id='companyName' size='20' value='${form.MAKER_NAME}'></td>
                    	<th>기기 시리얼 번호</th>
                    	<c:if test= "${not empty pageResult.resultList}" >
                    	<c:forEach var="result" items="${pageResult.resultList}" varStatus="status" end="1">
                    	<td> <input type="text" name ="serialNo" id='serialNo' size='20' value='${result.SERIAL_NO}' readonly="readonly">  </td>
                   		  </c:forEach>
                   		</c:if>
                    </tr>
                    <tr>
                        <th> 기기명</th>
                        <c:if test= "${not empty pageResult.resultList}" >
                    	<c:forEach var="result" items="${pageResult.resultList}" varStatus="status" end="1">
                        <td><input type="text" name ="devices" id='devices' size='20' value='${result.MS_NAME}'></td>
                        </c:forEach>
                   		</c:if>
                        <th> 펌웨어 이름 </th>
                        <td><input type="text" name ="firmwares" id='firmwares' size='20' value='${form.serialNo}'></td>
                    </tr>
                    <tr>
                        <th>  칩이름 </th>
                        <td><select name='chips' id='chips' disabled="disabled"><option value="">선택</option></select></td>
                       <th>칩버젼  </th>
                        <td>
                         <select name='chipversions' id='chipversions' disabled="disabled"><option value="">선택</option></select></td>
                    </tr>
           </table> 
	   </div> --%>
        
     <p class="subtitle_left">인증서 리스트</p>
      <div class="list_pages">건수/페이지 : <select name="pageSize" id="pageSize" class="selectws">
	  <option value='5' ${pageResult.pageSize=='5'?'selected="selected"':'' }>5</option>
	  <option value='10' ${pageResult.pageSize=='10'||pageResult.pageSize==''?'selected="selected"':'' } >10</option>
	  <option value='20' ${pageResult.pageSize=='20'?'selected="selected"':'' }>20</option>
	  				</select>    Total : ${pageResult.totalCount} 개</div>  
   		 <div class="table_wrap">
            <table class="basic_list">
            <caption>인증서 리스트</caption>
             	<colgroup>
                <col width="" />
                <col width="" />
                <col width="" />
                <col width="" />
                <col width="" />
           		</colgroup>
              <tr>
                <th>시리얼 번호</th>
                <th>DN</th>
                <th>유효기간 시작</th>
                <th>유효기간 만료</th>
                <th>상태</th>
              </tr>
               <c:if test= "${not empty pageResult.resultList}" >
            
             <c:forEach var="result" items="${pageResult.resultList}" varStatus="status">
              <tr>
                <td>${result.SERIAL_NO}</td>
                <td>${result.DN}</td>
                <td>${result.VALID_FROM}</td>
                <td>${result.VALID_TO}</td>
                <td>${result.STATUS}</td>
              </tr>
              </c:forEach>
              </c:if>
            <c:if test= "${empty pageResult.resultList}" >
	              <tr>
	                <td colspan="6" class="nolist">조회된 인증서 정보가 없습니다.</td>
	              </tr>
              </c:if>
            </table>
		</div>
        <div class="page_num"> <c:if test= "${not empty pageResult.resultList}" >${pageResult.pagingPrint} </c:if> </div>
        <div class="btn_center">
        <a class="css_btn btntext" href="/cert/management/status.kdn">목록</a>
        </div>
    </div>
    