<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%request.setCharacterEncoding("UTF-8");%>
    <div id="contents">
    	<div id="navi">홈 > 인증서 발급 관리 > 재발급</div>
        <p>기기인증서 발급</p>
        <p class="subtitle">인증서 검색 조건</p>
        
        <p>&nbsp;</p>
        <p>&nbsp;</p>
        
        <p class="subtitle">인증서 리스트</p>
    
   		 <div class="table_wrap">
            <table class="basic_list">
            <caption>인증서 리스트</caption>
             	<colgroup>
                <col width="" />
                <col width="" />
                <col width="" />
                <col width="" />
                <col width="" />
                <col width="" />
           		</colgroup>
              <tr>
                <th>선택</th>
                <th>시리얼 번호</th>
                <th>DN</th>
                <th>유효기간 시작</th>
                <th>유효기간 만료</th>
                <th>등록일자</th>
              </tr>
              <tr>
                <td><input name="" type="checkbox" value=""></td>
                <td>sn-1234</td>
                <td>cn=rm=123121113</td>
                <td>2013-05-20 12:15:00</td>
                <td>2014-05-20 12:15:00</td>
                <td>2013-05-20 12:15:00</td>
              </tr>
              <tr>
                <td><input name="" type="checkbox" value=""></td>
                <td>sn-1234</td>
                <td>cn=rm=123121113</td>
                <td>2013-05-20 12:15:00</td>
                <td>2014-05-20 12:15:00</td>
                <td>2013-05-20 12:15:00</td>
              </tr>
              <tr>
                <td><input name="" type="checkbox" value=""></td>
                <td>sn-1234</td>
                <td>cn=rm=123121113</td>
                <td>2013-05-20 12:15:00</td>
                <td>2014-05-20 12:15:00</td>
                <td>2013-05-20 12:15:00</td>
              </tr>
              <tr>
                <td><input name="" type="checkbox" value=""></td>
                <td>sn-1234</td>
                <td>cn=rm=123121113</td>
                <td>2013-05-20 12:15:00</td>
                <td>2014-05-20 12:15:00</td>
                <td>2013-05-20 12:15:00</td>
              </tr>
              <tr>
                <td><input name="" type="checkbox" value=""></td>
                <td>sn-1234</td>
                <td>cn=rm=123121113</td>
                <td>2013-05-20 12:15:00</td>
                <td>2014-05-20 12:15:00</td>
                <td>2013-05-20 12:15:00</td>
              </tr>
            </table>
		</div>
        <div class="btn_center">
        <a class="css_btn btntext" href="#">등록</a>
        <a class="css_btn btntext" href="#">삭제</a>
        <a class="css_btn btntext" href="#">수정</a>
        <a class="css_btn btntext" href="#">취소</a>
        </div>
    </div>
    