<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles"             prefix="tiles"      %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"              prefix="c"          %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"               prefix="fmt"        %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions"         prefix="fn"         %>

<script type="text/javascript">
$(document).ready(function(){

	$("#manName").change(function() {
		location.href= "/cert/devicePolicy/form/add.kdn?rmakerName="+$(this).children("option:selected").text();
   	});
	
	$("a").click(function(){
		if($(this).text()=='등록' || $(this).text()=='수정'){
			if($("#manName").val() == null){
				alert("제조사 명을 선택하세요.");
				$("#manName").focus();
				return false;
			}

			if($("#dModelNo").val() == null){
				alert("기기 명을 선택하세요.");
				$("#dModelNo").focus();
				return false;
			}
			
			if($("#fwNo").val() == null){
				alert("펌웨어 명을 선택하세요.");
				$("#fwNo").focus();
				return false;
			}
			
			if($("#mcuNo").val() == null){
				alert("MCU 명을 선택하세요.");
				$("#mcuNo").focus();
				return false;
			}
			
			$('select').each(function(){
				var classtag = "."+$(this).attr("name");
				$(classtag).val($(this).children("option:selected").text());
			});
			
			submitData();
		}
	});
});

function submitData() {
	var submitData = $("#policyForm").serializeArray();
	var formObject = {};
	$.each(submitData, function(i, v) {
		formObject[v.name] = v.value;
	});

	mcuAdd('${key}', formObject);
}

function mcuAdd(key,form){

	$.ajax({
		type : "POST",
		contentType: "application/json; charset=UTF-8",
		url : "/cert/devicePolicy/save/"+key+".kdn",
		data :  JSON.stringify(form), //데이터JSON.stringify
		dataType : "json",
		success : function(resultData){
			
			var type = (key =='add'? '등록' :'수정');
			
			if(resultData['result'] == 'nok'){
				alert(type+"에 실패 하였습니다.["+resultData['message']+"]");
				
			}else {
				alert(type +"되었습니다.");
				window.location = "/cert/devicePolicy/list.kdn";
			}
		},
		error : function(e) {
			alert("error : " + e);
		}
	});
}
</script>
<div id="contents">
	<div id="navi">
		<img class="title" src="${request.contextPath}/images/title_0416.gif" alt="기기 정책 등록" />
		<a href="/info/information.kdn">홈</a> &gt; <a href="/cert/deviceModel/list.kdn">기기 정보 관리</a> &gt; <a href="/cert/deviceModel/list.kdn">기기 모델 관리</a> &gt; 기기 정책 등록
	</div>
	<p class="subtitle">상세 정보</p>
	<div class="table_wrap">
	<form id="policyForm"action="">
	<input type="hidden" name="mcuNo" value="${mcu.mcuNo }" />
		<table class="board_write">
			<caption>상세 정보</caption>
			<colgroup>
				<col width="" />
				<col width="" />
				<col width="" />
				<col width="" />
			</colgroup>
			<tr>
				<th>제조사 명</th>
			    <th>기기 명</th>
			    <th>펌웨어 명</th>
			    <th>MCU 명</th>
			</tr>
			<tr>
				<td>
					<select name="manName" id="manName" size="15" style="width:100%">
						<c:forEach var="company" items="${companyList.list }">
							<option value="${company.MAKER_ID }"<c:if test="${rmakerName == company.MAKER_NAME}"> selected="selected"</c:if>>${company.MAKER_NAME }</option>
						</c:forEach>
					</select>
				</td>
				<td>
					<select name="dModelNo" id="dModelNo" size="15" style="width:100%">
						<c:forEach var="device" items="${deviceModelList.resultList }">
							<option value="${device.D_MODEL_NO }">${device.DEVICE_NAME }</option>
						</c:forEach>
					</select>
					<input type="hidden" name="deviceName" class="dModelNo" value="" />
				</td>
				<td>
					<select name="fwNo" id="fwNo" size="15" style="width:100%">
						<c:forEach var="firmware" items="${deviceFirmwareList.resultList }">
							<option value="${firmware.FW_NO }">${firmware.FW_NAME }</option>
						</c:forEach>
					</select>
					<input type="hidden" name="fwName" class="fwNo" value="" />
				</td>
				<td>
					<select name="mcuNo" id="mcuNo" size="15" style="width:100%">
						<c:forEach var="mcu" items="${deviceMcuList.resultList }">
							<option value="${mcu.MCU_NO }">${mcu.MCU_NAME }</option>
						</c:forEach>
					</select>
					<input type="hidden" name="mcuName" class="mcuNo" value="" />
				</td>
			</tr>
		</table>
	</form>
	</div>
	<div class="btn_center">
		<a class="css_btn btntext" href="#">등록</a>
		<a class="css_btn btntext" href="/cert/devicePolicy/list">취소</a>
	</div>
</div>
 