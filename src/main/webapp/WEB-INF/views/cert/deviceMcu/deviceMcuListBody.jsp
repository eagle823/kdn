<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles"             prefix="tiles"      %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"              prefix="c"          %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"               prefix="fmt"        %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions"         prefix="fn"         %>
<script type="text/javascript" src="${request.contextPath}/js/admin/admin.js"> </script>
<script type="text/javascript">
$(document).ready(function(){
	$('#pageSize').change(function(){
		$("form" ).attr('action','${request.contextPath}/cert/deviceMcu/list.kdn').attr('method','post').submit();
	});
});
	
if('${message}'!=''){
	alert('${message}');
}

function goPage(current){
	$('#currentPage').val(current);
	$("form" ).attr('action','${request.contextPath}/cert/deviceMcu/list.kdn').attr('method','post').submit();
};

</script>
<div id="contents">
	<div id="navi">
		<img class="title" src="${request.contextPath}/images/title_0415.gif" alt="MCU 관리" />
		<a href="/info/information.kdn">홈</a> &gt; <a href="/cert/deviceModel/list.kdn">기기 정보 관리</a> &gt; <a href="/cert/deviceModel/list.kdn">기기 모델 관리</a> &gt; MCU 관리
	</div>
	<form method="post">
	<input type="hidden"  name="currentPage" id="currentPage" value='${pageResult.currentPage}' >  
	<p class="subtitle_left">MCU 목록</p>   
	<div class="list_pages">
		건수/페이지 : <select name="pageSize" id="pageSize" class="selectws">
		<option value='5' ${pageResult.pageSize=='5'?'selected="selected"':'' }>5</option>
		<option value='10' ${pageResult.pageSize=='10'||pageResult.pageSize==''?'selected="selected"':'' } >10</option>
		<option value='20' ${pageResult.pageSize=='20'?'selected="selected"':'' }>20</option>
		</select>    Total : ${pageResult.totalCount} 개</div> 
	<div class="table_wrap">
		<table class="basic_list">
			<caption>MCU 목록</caption>
			<colgroup>
				<col width="" />
				<col width="" />
				<col width="" />
				<col width="" />
				<col width="" />
			</colgroup>
			<tr>
				<th>MCU 관리 번호</th>
				<th>MCU 명</th>
				<th>등록 제조사명</th>
				<th>MCU 제조사명</th>
				<th>MCU 버전</th>
			</tr>
			<c:if test= "${not empty pageResult.resultList }" >
				<c:forEach var="result" items="${pageResult.resultList }" varStatus="status">
				<c:url value="/cert/deviceMcu/view.kdn" var="viewLink">  
					<c:param name="mcuNo" value="${result.MCU_NO }"></c:param>  
				</c:url>
				<tr style="cursor:pointer;" onClick="location.href='${viewLink }'" onmouseover="this.style.background='#f1f1f1';" onmouseout="this.style.background='#ffffff';">
					<td>${result.MCU_NO }</td>
					<td>${result.MCU_NAME }</td>
					<td>${result.MAN_NAME }</td>
					<td>${result.MCU_MAN_NAME }</td>
					<td>${result.MCU_VERSION }</td>
				</tr>
				</c:forEach>
			</c:if>
			<c:if test= "${empty pageResult.resultList }" >
				<tr>
					<td colspan="5">조회된 MCU 정보가 없습니다.</td>
				</tr>
			</c:if>
		</table>
	</div>
	</form>
	<div class="page_num"> <c:if test= "${not empty pageResult.resultList }" >${pageResult.pagingPrint }</c:if></div>
	<div class="btn_center">
		<a class="css_btn btntext" href="/cert/deviceMcu/form/add.kdn">등록</a>
	</div>
</div>