<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles"             prefix="tiles"      %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"              prefix="c"          %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"               prefix="fmt"        %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions"         prefix="fn"         %>

<script type="text/javascript">
$(document).ready(function(){
	  
	$("a").click(function(){
		if($(this).text()=='삭제'){
			if(confirm("삭제 하시겠습니까?")){
				$("form" ).attr('action','${request.contextPath}/cert/deviceMcu/form/del.kdn').submit();
			}
		}
	});
});
</script>
<form action="" method="post">
	<input type="hidden" name="mcuNo" value="${mcu.mcuNo }" />
</form>
<div id="contents">
	<div id="navi">
		<img class="title" src="${request.contextPath}/images/title_0415.gif" alt="MCU 관리" />
		<a href="/info/information.kdn">홈</a> &gt; <a href="/cert/deviceModel/list.kdn">기기 정보 관리</a> &gt; <a href="/cert/deviceModel/list.kdn">기기 모델 관리</a> &gt; MCU 관리
	</div>
	<p class="subtitle">상세 정보</p>
	<div class="table_wrap">
	<table class="board_write">
		<caption>상세 정보</caption>
		<colgroup>
			<col width="15%" />
			<col width="35%" />
			<col width="15%" />
			<col width="35%" />
		</colgroup>
		<tr>
			<th>MCU 관리번호</th>
			<td>${mcu.mcuNo }</td>
			<th>등록 제조사명</th>
			<td>${mcu.manName }</td>
		</tr>
		<tr>
			<th>MCU 명</th>
			<td>${mcu.mcuName }</td>
		    <th>MCU 제조사명</th>
			<td>${mcu.mcuManName }</td>
		</tr>
		<tr>
			<th>MCU 버전</th>
			<td colspan="3">${mcu.mcuVersion }</td>
		</tr>
	</table>
	</div>
	<div class="btn_center">
		<c:url value="/cert/deviceMcu/form/mod.kdn" var="tutorialLink">  
			<c:param name="mcuNo" value="${mcu.mcuNo }"></c:param>  
		</c:url>
		<a class="css_btn btntext" href="${tutorialLink }">수정</a>
		<a class="css_btn btntext" href="#">삭제</a>
		<a class="css_btn btntext" href="/cert/deviceMcu/list">취소</a>
	</div>
</div>
  