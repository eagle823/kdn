<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/views/common/common.jsp" %>
<%request.setCharacterEncoding("UTF-8");%>

<script type="text/javascript" src="${request.contextPath}/js/cert/issue.js"></script>
<script type="text/javascript" src="${request.contextPath}/js/cert/model.js"></script>
<script type="text/javascript">

var rows = new Array();
$(document).ready(function(){
	
	var _order =["devices","mcus"];	  
	var _ordername =["device","mcu"];	  
	$('select').change(function(){
		var name =$(this).attr('name');
		 if($(this).val() != ''){
			var inp =  _order.indexOf(name);
			if(inp+1 < _order.length){
			$('#'+_order[inp+1]).attr("disabled",false);
			 deviceControl(_ordername[inp+1],'list');
			}
			
		 };
        
    });	  
	
	
	  $("a").click(function(){
		  if($(this).text()=='파일검색'){
			  if(!checkVal('devices',"",'기기를 선택 하여 주시기 바랍니다.'))	return false;
			  if(!checkVal('mcus',"",'MCU를 선택 하여 주시기 바랍니다.'))	return false;
			  var device =$("#devices").children("option:selected").text();
			  var firmware =$("#firmwares").val();
			  var mcu =$("#mcus").children("option:selected").text();
			  
				path = document.KDNApplet.GetFilePath();
				if(path==null || path=="") {
					alert("경로를 읽지 못했습니다.");
					return;
				}
				$('#path').val(path);
				
				result = document.KDNApplet.GetFileList(path); // 폴더 내에 파일 리스트 축출
				
				// 초기화를 위한 부분
				var keyLists;
				 keyLists = result.split('@@');
				 var table =$('#keyList');
				 table.empty();
				 var trs ="";
				 var allList = "";
				 for(var i=0;i<=keyLists.length; i=i+3){
					 if(keyLists.length > i+3){
						 rows.push([keyLists[i],keyLists[i+1],keyLists[i+2]]);
						 allList += keyLists[i]+'$'+device+'$'+firmware+'$'+mcu+'$'+keyLists[i+1]+'$'+keyLists[i+2].split(" ")[0]+'@@';
					 }
				 }
				 
				
				 $('#oidList').val(allList);
				 
				 for(var i=0;i<=keyLists.length; i=i+3){
					 if(keyLists.length > i+3){
						 trs += '<tr><td ><input name="ch" id="ch" type="checkbox" value="'+keyLists[i+1]+'"></td>'
					     + '<td>'+device+'</td><td>'+keyLists[i]+'</td>'					 
					     + '<td>'+mcu+'</td>'
						 + '<td>'+ keyLists[i+2]+'</td>'
						 +'</tr>' ;
					 };
					 	
				 };
				    
				 table.append(trs); 
		  }/* else if($(this).text()=='모든 인증서 저장'){
			  
				 $('form').submit();
		  } */else if($(this).text()=='신청'){
			  var oidList="";
				 $('#keyList tr').each(function(){
					 if($(this).find("input").is(":checked")){
						 
						 oidList += $(this).find("td:eq(2)").text()
						            +'$'+$(this).find("td:eq(1)").text()
						 			+'$'+$(this).find("td:eq(3)").text()
						 			+'$'+$(this).find("td:eq(4)").text()
						 			+'$'+ $(this).find("input").val()
						 			+'$'+$(this).find("td:eq(5)").text().split(" ")[0]+'@@';
					 
					 }
				  });
				 
				
				 if(oidList != ""){
					 $('#oidList').val(oidList);
					 $('form').submit();
				 };
				 
		  };
	  });
	  
	});
	
 function getText(element,v){
	return $(element).find("td:eq("+v+")").text();
	 
 };
 if('${message}'!=''){
	 alert('${message}');
 }
 </script>
<form action="/cert/issued/save/add.kdn" method="post">
<input type="hidden" id="oidList" name="oidList">
     <div id="contents">
    	<div id="navi"><img src="${request.contextPath}/images/title_0403.gif" class="title">
    	<a href="/info/information.kdn">홈</a> > <a href="/cert/deviceModel.kdn">기기 정보 관리</a> > <a href="/cert/issued.kdn">기기 신청 관리</a> > <a href="/cert/update.kdn">갱신 신청</a>
    	</div>
        
        <p class="subtitle">기기 상세  정보</p>
        
       	 <div class="table_wrap">
              <table class="table_search">
                    <colgroup>
                    <col width="25%">
                    <col width="25%">
                    <col width="25%">
                    <col width="25%">
                    </colgroup>
                    <tr>
                        <th> 기기 모델</th>
							<td><select name='devices' id='devices'>
									<option value=''>선택</option>
									<c:if test="${not empty result.list}">
										<c:forEach var="result" items="${ result.list}"
											varStatus="status">
											<option value='${result.D_MODEL_NO}'>${result.DEVICE_NAME}</option>
										</c:forEach>
									</c:if>
							</select></td>
					<th> MCU  </th>
                        <td><select name='mcus' id='mcus' disabled="disabled"><option value="">선택</option></select></td>
                    </tr>
           </table> 
           <input type="hidden" name="firmwares" id="firmwares" value="" />
	   </div>
         <p class="subtitle">파일 경로</p>
        
       	 <div class="table_wrap">
              <table class="table_search">
                <caption>검색</caption>
                    <colgroup>
                    <col width="15%">
                    <col width="85%">
                    </colgroup>
                    <tr>
                        <td> 파일 경로</td>
                         
                        <td>
                         <input name="path" type="text" id="path"  readonly="readonly">
                          <a class="css_btn btntext" href="#">파일검색</a>
                        </td>
                    </tr>
                    
           </table> 
	   </div>
        <p class="subtitle">키 정보 </p>
    
   		 <div class="table_wrap">
            <table class="basic_list" >
            <caption>키정보</caption>
             	<colgroup>
                <col width="">
                <col width="">
                <col width="">
                <col width="">
                <col width="">                      
           		</colgroup>
              <tr>
                <th>선택</th>
                <th>기기명</th>
                <th>시리얼 번호</th>
                <th>MCU</th>               
                <th>생성일자</th>               
              </tr>
              <tbody id='keyList'>
              <tr>
               <td colspan="5">읽은 키정보가 존재 하지 않습니다.</td>              
              </tr>
              </tbody>
            </table>
		</div>
		
        <div class="btn_center">
        <a class="css_btn btntext" href="#">신청</a>
       <!--   <a class="css_btn btntext" href="#">수정</a> -->
        <a class="css_btn btntext" href="#">취소</a>
        </div>
        
              </form>    <!--[if !IE]>-->
      <object ID="KDNApplet" classid="java:kdn.applet.KDNApplet.class" 
              type="application/x-java-applet"
              height="0" width="0" >
          <param name="codebase" value="/js/">
          <param NAME="archive" value="KDN_APPLET.jar">
          <param name="code" value="kdn.applet.KDNApplet">
           <param NAME="scriptable" VALUE="true">
			<param NAME="mayscript" VALUE="true">
			<param NAME="codebase_lookup" value="false">
      <!--<![endif]-->
        <object ID="KDNApplet"  classid="clsid:8AD9C840-044E-11D1-B3E9-00805F499D93" 
                height="0" width="0" > 
          <param name="codebase" value="/js/">
          <param NAME="archive" value="KDN_APPLET.jar">
          <param name="code" value="kdn.applet.KDNApplet">
           <param NAME="scriptable" VALUE="true">
			<param NAME="mayscript" VALUE="true">
			<param NAME="codebase_lookup" value="false">
        </object> 
      <!--[if !IE]>-->
      </object>
      <!--<![endif]-->
        
        
    </div>
    