<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles"             prefix="tiles"      %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"              prefix="c"          %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"               prefix="fmt"        %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions"         prefix="fn"         %>

<script src="${request.contextPath}/js/jquery.ui.core.min.js"></script>
<script src="${request.contextPath}/js/jquery.ui.widget.min.js"></script>
<script src="${request.contextPath}/js/jquery.ui.datepicker.min.js"></script>
<script src="${request.contextPath}/js/jquery.ui.datepicker-kr.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	$("#datepicker").datepicker( $.datepicker.regional[ "kr" ] );
	$("#startDate").datepicker({
	 	changeMonth: true,
		changeYear: true
	});
	 
	$('#fwNo').change(function(){
		//$(this).val();
		//$(this).children("option:selected").text();
		$('#fwNameText').text($(this).val());
		$('#fwName').val($(this).children("option:selected").text());
	});
	
	$("a").click(function(){
		if($(this).text()=='등록' || $(this).text()=='수정'){
			if($("#fwNo").val() == ""){
				alert("펌웨어 명을 선택하세요.");
				$("#fwNo").focus();
				return false;
			}
			
			if($("#fwHash").val() == ""){
				alert("펌웨어 Hash값을 입력하세요.");
				$("#fwHash").focus();
				return false;
			}
			
			if($("#startDate").val() == ""){
				alert("펌웨어 Hash 적용 일을 입력하세요.");
				$("#startDate").focus();
				return false;
			}
			
			submitData();
		}
	});
});

function submitData() {
	var submitData = $("#firmwareHashForm").serializeArray();
	var formObject = {};
	$.each(submitData, function(i, v) {
		formObject[v.name] = v.value;
	});

	firmwareAdd('${key}', formObject);
}

function firmwareAdd(key,form){

	$.ajax({
		type : "POST",
		contentType: "application/json; charset=UTF-8",
		url : "/cert/firmwareHash/save/"+key+".kdn", //호출 URL  /production/company/save/add.kdn
		data :  JSON.stringify(form), //데이터JSON.stringify
		dataType : "json",
		success : function(resultData){
			
			var type = (key =='add'? '등록' :'수정');
			
			if(resultData['result'] == 'nok'){
				alert(type+"에 실패 하였습니다.["+resultData['message']+"]");
				
			}else {
				alert(type +"되었습니다.");
				window.location = "/cert/firmwareHash/list.kdn";
			}
		},
		error : function(e) {
			alert("error : " + e);
		}
	});
}
</script>
<div id="contents">
	<div id="navi">
		<img class="title" src="${request.contextPath}/images/title_0417.gif" alt="펌웨어 검증 관리" />
		<a href="/info/information.kdn">홈</a> &gt; <a href="/cert/deviceModel/list.kdn">기기 정보 관리</a> &gt; <a href="/cert/deviceModel/list.kdn">기기 모델 관리</a> &gt; 펌웨어 검증 관리
	</div>
	<p class="subtitle">상세 정보</p>
	<div class="table_wrap">
	<form id="firmwareHashForm"action="">
	<input type="hidden" name="fwHashNo" value="${firmwareHash.fwHashNo }" />
	<input type="hidden" name="fwName" id="fwName" value="${firmwareHash.fwName }" />
		<table class="board_write">
			<caption>상세 정보</caption>
			<colgroup>
				<col width="15%" />
				<col width="35%" />
				<col width="15%" />
				<col width="35%" />
			</colgroup>
			<tr>
				<th>펌웨어 명</th>
				<td>
					<select name="fwNo" id="fwNo">
						<option value="">선택</option>
					<c:forEach items="${deviceFirmwareList.resultList }" var="item">
						<option value="${item.FW_NO }"<c:if test="${firmwareHash.fwNo == item.FW_NO}"> selected="selected"</c:if>>${item.FW_NAME }</option>
					</c:forEach>
					</select>
				</td>
			    <th>펌웨어 관리번호</th>
			    <td id="fwNameText">
			    	${firmwareHash.fwNo }
			    </td>
			</tr>
			<tr>
				<th>펌웨어 Hash값</th>
				<td><input type="text" name="fwHash" id="fwHash" value="${firmwareHash.fwHash }" /><a class="inner_btn btntext" href="#">검색</a></td>
				<th>펌웨어 Hash 적용 일</th>
				<td><input type="text" name="startDate" id="startDate" value="${firmwareHash.startDate }" /></td>
			</tr>
		</table>
	</form>
	</div>
	<div class="btn_center">
		<a class="css_btn btntext" href="#">${key=='mod'?'수정':'등록'}</a>
		<a class="css_btn btntext" href="/cert/firmwareHash/list">취소</a>
	</div>
</div>
 