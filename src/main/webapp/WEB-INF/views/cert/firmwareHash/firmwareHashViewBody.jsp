<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles"             prefix="tiles"      %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"              prefix="c"          %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"               prefix="fmt"        %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions"         prefix="fn"         %>

<script type="text/javascript">
$(document).ready(function(){
	  
	$("a").click(function(){
		if($(this).text()=='삭제'){
			if(confirm("삭제 하시겠습니까?")){
				$("form" ).attr('action','${request.contextPath}/cert/firmwareHash/form/del.kdn').submit();
			}
		}
	});
});
</script>
<form action="" method="post">
	<input type="hidden" name="fwHashNo" value="${firmwareHash.fwHashNo }" />
</form>
<div id="contents">
	<div id="navi">
		<img class="title" src="${request.contextPath}/images/title_0417.gif" alt="펌웨어 검증 관리" />
		<a href="/info/information.kdn">홈</a> &gt; <a href="/cert/deviceModel/list.kdn">기기 정보 관리</a> &gt; <a href="/cert/deviceModel/list.kdn">기기 모델 관리</a> &gt; 펌웨어 검증 관리
	</div>
	<p class="subtitle">상세 정보</p>
	<div class="table_wrap">
	<input type="hidden" name="fwHashNo" value="${firmwareHash.fwHashNo }" />
	<input type="hidden" name="fwName" id="fwName" value="${firmwareHash.fwName }" />
		<table class="board_write">
			<caption>상세 정보</caption>
			<colgroup>
				<col width="15%" />
				<col width="35%" />
				<col width="15%" />
				<col width="35%" />
			</colgroup>
			<tr>
				<th>펌웨어 명</th>
				<td>${firmwareHash.fwName }</td>
			    <th>펌웨어 관리번호</th>
			    <td id="fwNameText">${firmwareHash.fwNo }</td>
			</tr>
			<tr>
				<th>펌웨어 Hash값</th>
				<td>${firmwareHash.fwHash }</td>
				<th>펌웨어 Hash 적용 일</th>
				<td>${firmwareHash.startDate }</td>
			</tr>
		</table>
	</div>
	<div class="btn_center">
		<c:url value="/cert/firmwareHash/form/mod.kdn" var="tutorialLink">  
			<c:param name="fwHashNo" value="${firmwareHash.fwHashNo }"></c:param>  
		</c:url>
		<a class="css_btn btntext" href="${tutorialLink }">수정</a>
		<a class="css_btn btntext" href="#">삭제</a>
		<a class="css_btn btntext" href="/cert/firmwareHash/list">취소</a>
	</div>
</div>
  