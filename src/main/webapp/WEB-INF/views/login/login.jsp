<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles"             prefix="tiles"      %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"              prefix="c"          %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"               prefix="fmt"        %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions"         prefix="fn"         %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="X-UA-Compatible" content="IE=dege" />
<title>스마트그리드기기 인증운영시스템</title>
<link href="${request.contextPath}/css/login.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="${request.contextPath}/js/jquery-1.10.1.min.js"></script>
<script type="text/javascript">
$(function() {
	$('#id').focus();
	$('#pwd').keyup(function(e){
	    if(e.keyCode == 13)
	    {
	        $("#form_login").submit();
	    }
	});
});

function login_submit()	{
	var form_login = document.form_login;

	if (form_login.id.value==""){
		alert("Enter your ID");
		form_login.id.focus();
		return ;
	}
	if (form_login.pwd.value==""){
		alert("Enter your password");
		form_login.pwd.focus();
		return ;
	}
	form_login.action="/login/check.kdn";
	form_login.submit();
}
if('${message}' != ''){
	alert( '${message}');
}

function CheckAX() {
	var installed = false;
	try {
		xObj = new ActiveXObject("AxKDN.AxKDN.1");
		if (xObj)
			installed = true;
		else
			installed = false;
	} catch (ex) {
		installed = false;
	}
	return installed;
}

function Login(){
	var dn, ret;
	var ssn;
	var signeddata;

	if( !CheckAX() ){
		alert("클라이언트 프로그램이 설치되지 않아 수행할 수 없습니다.");
		return;
	}

	dn = document.AxKCASE.SelectCert();
	if ((dn == null) || (dn == "")){
		if(document.AxKCASE.GetErrorCode() != -1)
			alert(document.AxKCASE.GetErrorContent());
		return;
	}
	signeddata = document.AxKCASE.SignedData(dn, "", dn);
	if( signeddata == null || signeddata == "" )
	{
		errmsg = document.AxKCASE.GetErrorContent();
		errcode = document.AxKCASE.GetErrorCode();
		alert( "인증서 로그인 오류 :"+errmsg );
		return;
	}

	document.getElementById("DN").value = dn;
	document.SignedData.DN.value= dn;
	document.SignedData.signed_data.value = signeddata;
	document.SignedData.action="/login/check.kdn";
	//document.SignedData.target="result";
	document.SignedData.submit();
}
</script>
</head>

<body>
<div id="header"><img src="${request.contextPath}/images/top_logo.gif" width="344" height="36"></div>
<form action="/login/check.kdn"  name="form_login" id="form_login" method="post">
<div id="container">
    <div id="login_cert_area">
    	<div id="login_cert_btn">
            <a href="#"><img src="${request.contextPath}/images/btn_login01.gif" alt="인증서안내" class="p2"></a>
            <a href="#"><img src="${request.contextPath}/images/btn_login02.gif" alt="인증서FAQ" class="p2"></a>    
            <a href="${request.contextPath}/info/01.jsp"><img src="${request.contextPath}/images/btn_login03.gif" alt="센터소개" class="p2"></a>
            <a href="#"><img src="${request.contextPath}/images/btn_login04.gif" alt="공지사항" class="p2"></a>
        </div>
            <a href="javascript:Login()"><img src="${request.contextPath}/images/btn_login05.gif" alt="인증서 로그인" class="p2"></a>
     </div>
     <div class="comment_cert"><span class="red">주의하세요!</span><br><br>
     인증서의 안전한 이용을 위해 USB 등 이동식 저장장치에 보관하시고<br>
     웹 사이트 메일이나 P2P, 웹하드 등에 보관하지 마십시오.<br></div>
     <div id="login_area">
     
        <table class="login_input fl">
          <tr>
            <td class="login_title ar">아이디</td>
            <td><input id="id" name ="id" value="${id}" style="ime-mode:disabled; font-size: 11px;color: #666666; border: 1px #666666 solid;	background-color: #FFFFFF; width:100px;"></td>
          </tr>
          <tr>
            <td class="login_title ar">비밀번호</td>
            <td><input id="pwd" name="pwd" type="password"/></td>
          </tr>
        </table>    
        <a href="#" onclick="login_submit()"><img src="${request.contextPath}/images/btn_login06.gif" alt="아이디 로그인" class="fr pl2"></a>    
    </div>   
    <!--div class="comment"><span class="red">주의하세요!</span><br><br>
     인증서의 안전한 이용을 위해 USB 등 이동식 저장장치에 보관하시고<br>
     웹 사이트 메일이나 P2P, 웹하드 등에 보관하지 마십시오.<br></div-->
            
    <object id="AxKCASE"  classid="CLSID:952496E6-52BD-41ed-A47D-D05D3D2A370E"codebase="/js/AxKDN(3.5.3.4).cab#Version=3,5,3,4">
</object>      
 </div>     
  <div id="footer">
  <br><br>
<div class="logo_footer"><img src="/images/logo_footer.gif"></div>
<div class="p5"><a href="#">센터소개</a> | <a href="#">고객센터</a> | <a href="#">관련법규</a> | <a href="#">문의하기</a><br>
Copyright ⓒ KEPCO KDN Co., Ltd. All Rights reserved.<br>
(우)137-862 서울 서초구 효령로 72길 60(Hyoryeong-ro 72 Gil. 60) 대표전화 : 02-6262-6114</div>
</div>          
  </form>   

<form method="post" name="SignedData" id="SignedData" >
	<input type="hidden" value="" id="signed_data" name="signed_data" size="0">
	<input type="hidden" value="" id="ssn" name="ssn" size="0">
	<input type="hidden" value="" id="DN" name="DN" size="0">
</form>

</body>
</html>
