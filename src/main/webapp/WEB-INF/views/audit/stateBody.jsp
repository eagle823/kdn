<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<script src="${request.contextPath}/js/jquery.ui.core.min.js"></script>
<script src="${request.contextPath}/js/jquery.ui.widget.min.js"></script>
<script src="${request.contextPath}/js/jquery.ui.datepicker.min.js"></script>
<script src="${request.contextPath}/js/jquery.ui.datepicker-kr.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		$('#pageSize').change(function(){
			$("form" ).attr('action','${request.contextPath}/audit/cert/state.kdn').attr('method','post').submit();
		});
	});

	$(function() {
		/*
		$( "#datepicker" ).datepicker( $.datepicker.regional[ "kr" ] );
		$( "#sdate" ).datepicker({
			defaultDate: "+1w",
			changeMonth: true,
			numberOfMonths: 2,
			onClose: function( selectedDate ) {
				$( "#edate" ).datepicker( "option", "minDate", selectedDate );
			}
		});
		$("#edate").datepicker({
			defaultDate: "+1w",
			changeMonth: true,
			numberOfMonths: 2,
			onClose: function( selectedDate ) {
				$( "#sdate" ).datepicker( "option", "maxDate", selectedDate );
			}
			
		});
		 */
	
		$("#datepicker").datepicker($.datepicker.regional["kr"]);
		$("#sdate").datepicker({
			changeMonth : true,
			changeYear : true,
			onClose : function(selectedDate) {
				$("#edate").datepicker("option", "minDate", selectedDate);
			}
		});
		$("#edate").datepicker({
			changeMonth : true,
			changeYear : true,
			onClose : function(selectedDate) {

				$("#sdate").datepicker("option", "maxDate", selectedDate);
			}

		});

		$("a")
				.click(
						function() {
							if ($(this).text() == '검색') {
								/*
								  var _se;
								  _se = document.getElementById("adminid").value + "$" + document.getElementById("deviceModel").value + "$"
								  + document.getElementById("sdate").value + "$"+ document.getElementById("edate").value;
								 */
								if ('${pageCode}' == 'state') {

									$("form")
											.attr('action',
													'${request.contextPath}/audit/cert/state.kdn')
											.submit();
								} else if ('${pageCode}' == 'wait') {
									$("form")
											.attr('action',
													'${request.contextPath}/audit/cert/state/wait.kdn')
											.submit();
								} else if ('${pageCode}' == 'success') {
									$("form")
											.attr('action',
													'${request.contextPath}/audit/cert/state/success.kdn')
											.submit();
								} else if ('${pageCode}' == 'fail') {
									$("form")
											.attr('action',
													'${request.contextPath}/audit/cert/state/fail.kdn')
											.submit();
								}

							}
							if ($(this).text() == '제조사 검색') {
								ShowModalPopup(
										'${request.contextPath}/popup/findCompany.kdn',
										'500', '600');
							}
						});
	});

	if ('${message}' != '') {
		alert('${message}');
	}

	function goPage(current) {
		$('#currentPage').val(current);
		$("form").attr('action', '${request.contextPath}/audit/cert/state.kdn')
				.attr('method', 'post').submit();
	};

	function changeText() {
		document.getElementById('adminid').value = document
				.getElementById('companyName').value;
		
		$("form")
		.attr('action',
				'${request.contextPath}/audit/cert/state.kdn')
		.submit();
	};

</script>

<div id="contents">
	<div id="navi">
		<img src="/images/title_0505.gif" class="title" /> <a
			href="/info/information.kdn">홈</a> &gt; <a href="/audit/audit.kdn">감사
			관리</a> &gt; <a href="/audit/cert/process.kdn">통계</a> &gt;

		<c:if test="${pageCode == 'wait'}">
			<a href="/audit/cert/state/wait.kdn">처리 요청 대기 정보</a>
		</c:if>
		<c:choose>
			<c:when test="${pageCode == 'state'}">
				<a href="/audit/cert/state.kdn">일별 통계</a>
			</c:when>
			<c:when test="${pageCode == 'success'}">
				<a href="/audit/cert/state/success.kdn">처리 성공 정보</a>
			</c:when>
			<c:when test="${pageCode == 'fail'}">
				<a href="/audit/cert/state/fail.kdn">처리 실패 정보</a>
			</c:when>
		</c:choose>

	</div>

	<form method="post">
		<input type="hidden" name="currentPage" id="currentPage"
			value='${pageResult.currentPage}'>
		<p class="subtitle">통계 검색 조건</p>
		<div class="table_wrap">
			<table class="table_search">
				<colgroup>
					<col width="15%" />
					<col width="35%" />
					<col width="15%" />
					<col width="35%" />
				</colgroup>
				<tr>
					<th>제조사명</th>
					<td>
						<input type="text" name='adminid' id='adminid' readonly="readonly" value='${pageResult.makerName}'>
						<c:if test="${session.TYPE == 'lra'}"><a class="inner_btn btntext" href="#">제조사 검색</a></c:if>
					</td>
					<th>기기모델</th>
					<td><select name='devices' id='devices' style="width: 100px" >
						<option value=''>선택</option>
						<c:if test="${not empty deviceInfo.list}">
							<c:forEach var="result" items="${ deviceInfo.list}"
								varStatus="status">
								<option value='${result.DEVICE_NAME}'<c:if test="${pageResult.deviceName == result.DEVICE_NAME}"> selected="selected"</c:if>>${result.DEVICE_NAME}</option>
							</c:forEach>
						</c:if>
				</select>
					</td>

				</tr>
				<tr>
					<th>발급날짜</th>
					<td colspan="2"><input type="text" name='sdate' id='sdate'
						value='${pageResult.sDate}'> ~ <input type="text"
						name='edate' id='edate' value='${pageResult.eDate }'></td>
					<td><a class="inner_btn btntext" href="#">검색</a></td>
				</tr>
			</table>
		</div>

		<p class="subtitle_left">통계 조회 결과 리스트</p>
		<div class="list_pages">
			건수/페이지 : <select name="pageSize" id="pageSize">
				<option value='5'
					${pageResult.pageSize=='5'?'selected="selected"':'' }>5</option>
				<option value='10'
					${pageResult.pageSize=='10'||pageResult.pageSize==''?'selected="selected"':'' }>10</option>
				<option value='20'
					${pageResult.pageSize=='20'?'selected="selected"':'' }>20</option>
			</select> Total : ${pageResult.totalCount} 개
		</div>
		<div class="table_wrap">
			<table class="basic_list">
				<caption>감사 정보 리스트</caption>
				<colgroup>
					<col width="" />
					<col width="" />
					<col width="" />
					<col width="" />
					<col width="" />
					<col width="" />
				</colgroup>

				<tr>
					<th>일별</th>
					<th>등록 수</th>
					<th>발급 수</th>
					<th>재발급 수</th>
					<th>갱신 수</th>
					<th>폐지 수</th>
				</tr>

				<c:if test="${not empty pageResult.resultList}">

					<c:forEach var="result" items="${pageResult.resultList}"
						varStatus="status">
						<tr>
							<td>${result.DAY}</td>
							<td>${result.REG}</td>
							<td>${result.IS}</td>
							<td>${result.RN}</td>
							<td>${result.RP}</td>
							<td>${result.RK}</td>

						</tr>
					</c:forEach>
				</c:if>
				<c:if test="${empty pageResult.resultList}">
					<tr>
						<td colspan="6">조회된 인증서 정보가 없습니다.</td>
					</tr>
				</c:if>
			</table>
		</div>
	</form>
	<div class="page_num">
		<c:if test="${not empty pageResult.resultList}">${pageResult.pagingPrint} </c:if>
	</div>


</div>