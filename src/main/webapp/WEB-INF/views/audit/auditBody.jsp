<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles"             prefix="tiles"      %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"              prefix="c"          %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"               prefix="fmt"        %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions"         prefix="fn"         %>
 <script src="${request.contextPath}/js/jquery.ui.core.min.js"></script>
	<script src="${request.contextPath}/js/jquery.ui.widget.min.js"></script>
	<script src="${request.contextPath}/js/jquery.ui.datepicker.min.js"></script>
	<script src="${request.contextPath}/js/jquery.ui.datepicker-kr.js"></script>
<script type="text/javascript">

var managerAction = ["등록","수정","삭제"];
var certAction = ["등록","발급","재발급","갱신","폐지","저장"];
$(document).ready(function(){
	$("#type").change(function(){
		var opt ="";
		if($(this).val()=='인증서'){
			$("#action").empty();
			for( var i =0 ; i<certAction.length; i++){
				opt +="<option value='"+certAction[i]+"'>"+certAction[i]+"</option>";
			}
			$("#action").append(opt);
		}else{
			$("#action").empty();
			for( var i =0 ; i<managerAction.length; i++){
				opt +="<option value='"+managerAction[i]+"'>"+managerAction[i]+"</option>";
			}
			$("#action").append(opt);
		}
	});
});

function goPage(current){
	$('#currentPage').val(current);
	 $("form" ).attr('action','${request.contextPath}/audit/audit.kdn')
         .attr('method','post').submit();
};


$(function() {
	$( "#datepicker" ).datepicker( $.datepicker.regional[ "kr" ] );

	$( "#sdate" ).datepicker({
		defaultDate: "-1m",
		changeMonth: true,
		changeYear: true,
		onClose: function( selectedDate ) {
			$( "#edate" ).datepicker( "option", "minDate", selectedDate );
		}
	});
	
	$("#edate").datepicker({
		changeMonth: true,
		changeYear: true,
		onClose: function( selectedDate ) {
			$( "#sdate" ).datepicker( "option", "maxDate", selectedDate );
		}
		
	});
	
	$("a").click(function(){
		  if($(this).text()=='검색'){
			  $("form").attr('action','${request.contextPath}/audit/audit.kdn').submit();
		  }
	});
	
	$('#pageSize').change(function(){
		$("form" ).attr('action','${request.contextPath}/audit/audit.kdn').attr('method','post').submit();
	});

});

</script>
      <div id="contents">
    	<div id="navi"><img src="/images/title_0501.gif" class="title" />
    	<a href="/info/information.kdn">홈</a> &gt; <a href="/audit/audit.kdn">감사 관리</a> &gt; 감사기록 
    	</div>
    	<form method="post">  	
    	<input type="hidden" name="currentPage" id="currentPage"
			value='${pageResult.currentPage}'>
     <p class="subtitle">감사 검색 조건</p>
        <div class="table_wrap">
              <table class="table_search">
                    <colgroup>
                    <col width="15%" />
                    <col width="20%" />
                    <col width="15%" />
                    <col width="20%" />
                    <col width="15%" />
                    <col width="15%" />
                    
                    </colgroup>
                    <tr>
                    	<th >운영자 ID</th>
                    	<td><input type="text" name='adminLoginid' value="${pageResult.OPERATOR_ID}"<c:if test="${session.TYPE != 'lra'}">readonly="readonly"</c:if> /></td>
                    	<th>구분</th>                    	
                  	    <td>
                  	    	<select name='type' id='type' > 
		                  	    <option value=" ">선택</option>
		                  	    <option value="관리자"<c:if test="${auditForm.type  == '관리자' }"> selected="selected"</c:if>>관리자</option>
		                  	    <option value="제조사"<c:if test="${auditForm.type  == '제조사' }"> selected="selected"</c:if>>제조사</option>
		                  	    <option value="인증서"<c:if test="${auditForm.type  == '인증서' }"> selected="selected"</c:if>>인증서</option>
                  	    	</select>
                  	    </td>
                    	<th>행위 </th> 
                   	    <td><select name='action' id='action'>
                   	    <option value="">선택</option>
                   	    <option value="등록"<c:if test="${auditForm.action  == '등록' }"> selected="selected"</c:if>>등록</option>
                  	    <option value="수정"<c:if test="${auditForm.action  == '수정' }"> selected="selected"</c:if>>수정</option>
                  	    <option value="삭제"<c:if test="${auditForm.action  == '삭제' }"> selected="selected"</c:if>>삭제</option>                  	   
                   	    </select></td> 
                    </tr>
                    <tr>
                        <th>발급날짜</th>
                        <td colspan="5"><input type="text" name='sdate' id='sdate' value='${pageResult.sDate}' > ~ <input type="text" name='edate' id='edate' value='${pageResult.eDate}'><a class="inner_btn btntext" href="#">검색</a></td>
                    </tr>
           </table> 
	   </div>
      <p class="subtitle_left ">감사 조회 결과 리스트</p>
     <div class="list_pages">건수/페이지 : <select name="pageSize" id="pageSize" class="selectws">
	  <option value='5' ${pageResult.pageSize=='5'?'selected="selected"':'' }>5</option>
	  <option value='10' ${pageResult.pageSize=='10'||pageResult.pageSize==''?'selected="selected"':'' } >10</option>
	  <option value='20' ${pageResult.pageSize=='20'?'selected="selected"':'' }>20</option>
	  				</select>    Total : ${pageResult.totalCount} 개</div> 
   		 <div class="table_wrap">
            <table class="basic_list">
            <caption>감사 정보 리스트</caption>
             	<colgroup>
                <col width="" />
                <col width="" />
                <col width="" />
                <col width="" />
                <col width="" />                 
           		</colgroup>
           		
              <tr>
                <th>구분</th>
                <th>운영자 ID</th>
                <th>행위</th>
                <th>상세정보</th>
                <th>일자</th>
              </tr>
              
               <c:if test= "${not empty pageResult.resultList}" >
            
             <c:forEach var="result" items="${pageResult.resultList}" varStatus="status">
	              <tr>
	                <td>${result.DI}</td>
	                <td>${result.OPERATOR_ID} </td>
	                <td>${result.AC}</td>
	                <td>${result.DE}</td>
	                <td>${result.DATE}</td>
	              </tr>
              </c:forEach>
              </c:if>
              <c:if test= "${empty pageResult.resultList}" >
	              <tr>
	                <td colspan="7">조회된 인증서 정보가 없습니다.</td>
	              </tr>
              </c:if>
            </table>
		</div>
		</form>
        <div class="page_num"> <c:if test= "${not empty pageResult.resultList}" >${pageResult.pagingPrint} </c:if> </div>
                      
     
     </div>