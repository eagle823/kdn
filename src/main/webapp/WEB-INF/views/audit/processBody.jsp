<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib uri="http://tiles.apache.org/tags-tiles"             prefix="tiles"      %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"              prefix="c"          %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"               prefix="fmt"        %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions"         prefix="fn"         %>
    <script src="${request.contextPath}/js/jquery.ui.core.min.js"></script>
	<script src="${request.contextPath}/js/jquery.ui.widget.min.js"></script>
	<script src="${request.contextPath}/js/jquery.ui.datepicker.min.js"></script>
	<script src="${request.contextPath}/js/jquery.ui.datepicker-kr.js"></script>
<script type="text/javascript">

var managerAction = ["등록","수정","삭제"];
var certAction = ["등록","발급","재발급","갱신","폐지","저장"];



$(function() {
		
	 $( "#datepicker" ).datepicker( $.datepicker.regional[ "kr" ] );
	 $( "#sdate" ).datepicker({
	 	changeMonth: true,
		changeYear: true,
	 	onClose: function( selectedDate ) {
	 		$( "#edate" ).datepicker( "option", "minDate", selectedDate );
	 	}
	 });
	 $("#edate").datepicker({
	 	changeMonth: true,
		changeYear: true,
	 	onClose: function( selectedDate ) {
	 		$( "#sdate" ).datepicker( "option", "maxDate", selectedDate );
	 	}
	 	
	 });
	 
	$('select').change(function(){
		 $('#deviceName').val($(this).children("option:selected").text());
    });
	 
	/*
	 var nowDate = new Date();
	    var nowYear = nowDate.getYear();
	    var nowMonth = nowDate.getMonth() + 1;
	    var nowDay = nowDate.getDate();
	    
	    var beforeMonth = nowDate.getMonth() - 2;
	    var beforeYear = nowDate.getYear();

	    if (nowMonth < 10) {
	        nowMonth = "0" + nowMonth;
	    }
	    if (nowDay < 10) {
	        nowDay = "0" + nowDay;
	    }
	    
	    if(beforeMonth < 1)
	   	{
	    	beforeMonth = 12 - parseInt(beforeMonth);
	    	beforeYear = parseInt(beforeYear) - 1;
	   	}
	    
	    if(beforeMonth == 12)
	    {
	    	beforeYear = parseInt(beforeYear) - 1;
	   	}
	    
	    
	    if (beforeMonth < 10) {
	    	beforeMonth = "0" + beforeMonth;
	    }
	    
	    var thisDate = nowYear + "-" + nowMonth + "-" + nowDay;
	    var beforeDate = beforeYear + "-" + beforeMonth + "-" + nowDay;
	    
	    document.getElementById('edate').value = thisDate;
	    document.getElementById('sdate').value = beforeDate;
	    document.getElementById('adminid').value = '${session.MAKER_NAME}';
		*/
	/*
	$( "#datepicker" ).datepicker( $.datepicker.regional[ "kr" ] );
	$( "#sdate" ).datepicker({
		defaultDate: "+1w",
		changeMonth: true,
		numberOfMonths: 2,
		onClose: function( selectedDate ) {
			$( "#edate" ).datepicker( "option", "minDate", selectedDate );
		}
	});
	$("#edate").datepicker({
		defaultDate: "+1w",
		changeMonth: true,
		numberOfMonths: 2,
		onClose: function( selectedDate ) {
			$( "#sdate" ).datepicker( "option", "maxDate", selectedDate );
		}
		
	});*/
	
	$("a").click(function(){
		  if($(this).text()=='검색'){
			/*
			  var _se;
			  _se = document.getElementById("adminid").value + "$" + document.getElementById("deviceModel").value + "$"
			  + document.getElementById("sdate").value + "$"+ document.getElementById("edate").value;
			*/
			  $("form").attr('action','${request.contextPath}/audit/cert/process.kdn').submit();
		  }
		  
		  if($(this).text() == '제조사 검색'){
				ShowModalPopup(
						'${request.contextPath}/popup/findCompany.kdn',
						'500', '600');
			}
	});
	
	$('#pageSize').change(function(){
		$("form" ).attr('action','${request.contextPath}/audit/cert/process.kdn').attr('method','post').submit();
	});
});

function goPage( current){
	$('#currentPage').val(current);
	 $("form" ).attr('action','${request.contextPath}/audit/cert/process.kdn')
         .attr('method','post').submit();
};

function changeText(){
	  document.getElementById('adminid').value =  document.getElementById('companyName').value;
	  
	  $("form")
		.attr('action',
				'${request.contextPath}/audit/cert/process.kdn')
		.submit();
};


</script>
   <div id="contents">
      <div id="navi">
      <img src="/images/title_0504.gif" class="title" />
      <a href="/info/information.kdn">홈</a> &gt; <a href="/audit/audit.kdn">감사 관리</a> &gt; <a href="/audit/cert/process.kdn">통계</a> &gt; 월별 통계
      </div>
     <form method="post">  	
     <input type="hidden" name="currentPage" id="currentPage"
			value='${pageResult.currentPage}' />
     <input type="hidden" name="deviceName" id="deviceName" value="${pageResult.deviceName }" />
     <p class="subtitle">통계 검색 조건</p>
        <div class="table_wrap">
                <table class="table_search">
                    <colgroup>
                    <col width="15%" />
                    <col width="35%" />
                    <col width="15%" />
                    <col width="35%" />
                    </colgroup>
                    <tr>
                    	<th >제조사명</th>

                    	<td>
                    		<input type="text" name='adminid' id='adminid' readonly="readonly"  value =  '${pageResult.makerName }' />
							<c:if test="${session.TYPE == 'lra'}">
								<a class="inner_btn btntext" href="#">제조사 검색</a>
								<input type="hidden" name="companyName" id="companyName" onchange="javascript:changeText();"/>
							</c:if>
                    	</td>
                    	<th>기기모델</th>
                  	   <td><select name='devices' id='devices' style="width: 100px" >
						<option value=''>선택</option>
						<c:if test="${not empty deviceInfo.list}">
							<c:forEach var="result" items="${ deviceInfo.list}"
								varStatus="status">
								<option value='${result.DEVICE_NAME}'<c:if test="${pageResult.deviceName == result.DEVICE_NAME}"> selected="selected"</c:if>>${result.DEVICE_NAME}</option>
							</c:forEach>
						</c:if>
				</select> </td>
                    	
                    </tr>
                    <tr>
                        <th>발급날짜</th>
                        <td colspan="2"><input type="text" name='sdate' id='sdate' value='${pageResult.sDate }'/> ~ <input type="text" name='edate' id='edate' value='${pageResult.eDate }'/>   </td>
                        <td ><a class="inner_btn btntext"  href="#">검색</a></td>
                    </tr>
                    <c:if test="${session.TYPE == 'lra'}">
                    <tr>
				</tr>
				</c:if>
           </table>  
	   </div>
      <p class="subtitle_left">통계  조회 결과 리스트</p>
    <div class="list_pages">건수/페이지 : <select name="pageSize" id="pageSize">
	  <option value='5' ${pageResult.pageSize=='5'?'selected="selected"':'' }>5</option>
	  <option value='10' ${pageResult.pageSize=='10'||pageResult.pageSize==''?'selected="selected"':'' } >10</option>
	  <option value='20' ${pageResult.pageSize=='20'?'selected="selected"':'' }>20</option>
	  				</select>    Total : ${pageResult.totalCount} 개</div> 
   		 <div class="table_wrap">
            <table class="basic_list">
            <caption>감사 정보 리스트</caption>
             	<colgroup>
                <col width="" />
                <col width="" />
                <col width="" />
                <col width="" />
                <col width="" /> 
                 <col width="" />                   
           		</colgroup>
           		
              <tr>
                <th>월별</th>
                <th>등록 수</th>
                <th>발급 수</th>
                 <th>재발급 수</th>
                <th>갱신 수</th>
                <th>폐지 수</th>
              </tr>
                  <c:if test= "${not empty pageResult.resultList}" >
            
             <c:forEach var="result" items="${pageResult.resultList}" varStatus="status">
	              <tr>
	                <td>${result.MONTH}</td>
	                <td>${result.REG} </td>
	                <td>${result.IS}</td>
	                <td>${result.RN} </td>
	                <td>${result.RP}</td>
	                <td>${result.RK}</td>
	              </tr>
              </c:forEach>
              </c:if>
              <c:if test= "${empty pageResult.resultList}" >
	              <tr>
	                <td colspan="6">조회된 인증서 정보가 없습니다.</td>
	              </tr>
              </c:if>
            </table>
		</div>
		</form>
        <div class="page_num"> <c:if test= "${not empty pageResult.resultList}" >${pageResult.pagingPrint} </c:if> </div>
</div>