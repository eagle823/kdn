<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%request.setCharacterEncoding("UTF-8");%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles"             prefix="tiles"      %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"              prefix="c"          %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"               prefix="fmt"        %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions"         prefix="fn"         %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr" />
<title></title>
<link href="/css/default.css" rel="stylesheet" type="text/css">
<!--<![if lt IE 9]>-->
<script type="text/javascript" src="${request.contextPath}/js/json2.js"></script>
<!--<![endif]-->
<script type="text/javascript" src="${request.contextPath}/js/jquery-1.10.1.min.js"></script>
<style type="text/css">
#popup {float:left;  width:100%; display:inline;}
.pop_table_wrap { width:100%; }
</style>
<script type="text/javascript">
$(document).ready(function(){
	  
	  $("a").click(function(){
		  if($(this).text()=='선택'){
			  var str =$('#addr').val();
			  if(str  ==""){
				  alert("주소를 선택 하여주십시요");
			  }else{
					$('#product_address',parent.document).val(str);
					 window.parent.HideModalPopup(); 
			  }
		  }else if($(this).text()=='취소'){
			  window.parent.HideModalPopup();
		  }else if($(this).text()=='검색'){
			  searchAddress(1);
		  }
	  });
	  
	});
 
if('${message}' != ''){
	alert('${message}');
}


function searchAddress( page){
	 if(page == null || page == undefined ||page ==''){
		 page = 1;
	 }
	 
	 if($('#searchKeyword').val() == ""){
			alert("검색어를 입력해 주세요.");
			$('#searchKeyword').focus();
	}else{
		var form={
				pageSize :10,
				currentPage :page,
				searchKeyword : $('#searchKeyword').val()			 
		}; 
		
		$.ajax({
			type : "POST",
			contentType: "application/json; charset=utf-8",
			url : "/popup/data/address.kdn", //호출 URL
			data : JSON.stringify(form), //데이터
			dataType : "json",
			success : function(resultData){
				if(typeof resultData['list'] === 'undefined'){
					alert('주소가 존재 하지 않습니다.');
				}else{
					if (resultData['list'].length >0) {
						addTable(resultData['list']);
						printPaging(resultData['paging']);
					} else {
						alert('주소가 존재 하지 않습니다.');
					}
				}
				
			},
			error : function(x, o, e) {
				alert(x.status + " : "+ o +" : "+e);
			}
		});
	}
}


function addTable( list ){
	
	var tb =$("#zipinfo");
	tb.empty();
	var row="";
	$.each(list,function(index,value){
		
		 row += '<tr>	<td ><input type="radio" name="check_value"  onclick="javascript:doSelect(\''+value['post']+'\',\''+value['address']+'\')"></td>'
      	           +'<td >'+value['post']+'</td>'
      	           +'<td >'+value['address']+'</td></tr>';
      	        
      	      
	});
	tb.append(row);
} 

function doSelect(id, name) {
  $('#addr').val("("+id+")"+name);
}

function goPage(pageNum) { 
	searchAddress(pageNum);
}

function printPaging(page){//total,cpage
	$('.page_num').empty();
	$('.page_num').html(page);
	
};

</script>
</head>
<body>
<input type="hidden" name="addr" id="addr" value="">
    <div id="popup">  
    <p class="subtitle">주소 검색  검색</p>
        <div class="table_wrap">
            <table class="board_write">
            <caption>검색 조건</caption>
             <colgroup>
                <col width="15%" />
                <col>
            </colgroup>
              <tr>
                <th>검색 조건</th>
                <td><select name="kind" id="kind">
                        	<option value="companyName">동이름</option>
                        </select>&nbsp;<input name='searchKeyword' id="searchKeyword" type="text"> <a class="inner_btn btntext" href="#">검색</a></td>
              </tr>
            </table>
		</div>
     <p class="subtitle">주소목록</p>  
    
        <div class="pop_table_wrap">
            <table class="basic_list" >
            <caption>주소 목록</caption>
             	<colgroup>
                <col width="" />
                <col width="" />
                <col width="" />                
           		</colgroup>
              <tr>	 	
                <th>선택</th>
                <th>우편번호</th>
                <th>주소</th>               
              </tr>
              <tbody id="zipinfo">
                   <tr>
	                <td colspan="3">찾을 동을 검색 해주시기 바랍니다. </td>
	              </tr>
	         </tbody>            
            </table>
		</div>
		<div class="page_num"> </div>
        <div class="btn_center">       
        <a class="css_btn btntext" href="#">선택</a>       
        <a class="css_btn btntext" href="#">취소</a>
        </div>
	</div>    
</body>
</html>
