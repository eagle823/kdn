<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%request.setCharacterEncoding("UTF-8");%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles"             prefix="tiles"      %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"              prefix="c"          %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"               prefix="fmt"        %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions"         prefix="fn"         %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr" />
<title></title>
<link href="/css/default.css" rel="stylesheet" type="text/css">
<!--<![if lt IE 9]>-->
<script type="text/javascript" src="${request.contextPath}/js/json2.js"></script>
<!--<![endif]-->
<script type="text/javascript" src="${request.contextPath}/js/jquery-1.10.1.min.js"></script>
<style type="text/css">
#popup {float:left;  width:100%; display:inline;}
.pop_table_wrap { width:100%; }
</style>
<script type="text/javascript">
$(document).ready(function(){
    
    $("a").click(function(){
        if($(this).text()=='선택'){
            
            if($(':radio[name="sel"]:checked').length < 1){
                alert('제조사를 선택 해주세요.');
            }else{
                $('#companyName',parent.document).val($(':radio[name="sel"]:checked').val());
                $('#adminid',parent.document).val($(':radio[name="sel"]:checked').val());
                if('${type}'=='device'){
                	
                   parent.$.fn.device();
                }
                window.parent.HideModalPopup();
            }
        }else if($(this).text()=='취소'){
            window.parent.HideModalPopup();
        }else if($(this).text()=='검색'){
        	if($('#searchKeyword').val() == ""){
                alert("검색어를 입력해 주세요.");
                $('#searchKeyword').focus();
            }else{
            	searchComapny(1);
            }
        };
    });
    
  });

if('${message}' != ''){
	alert('${message}');
}

function searchComapny(page){
    if(page == null || page == undefined ||page ==''){
        page = 1;
      }
      
         var form={
                 pageSize :"10",
                 currentPage :page,
                 kind : $('#kind').val(),             
                 searchKeyword : $('#searchKeyword').val()             
         }; 
         $.ajax({
             type : "POST",
             contentType: "application/json; charset=utf-8",
             url : "/popup/data/company.kdn", //호출 URL
             data : JSON.stringify(form), //데이터
             dataType : "json",
             success : function(resultData){

                 if(typeof resultData['list'] == 'undefined'){
                     alert('회사가  존재 하지 않습니다.');
                 }else{
                     if (resultData['list'].length >0) {
                         addTable(resultData['list']);
                         printPaging(resultData['paging']);
                     } else {
					alert('회사가  존재 하지 않습니다.');
				}
			}

		},
		error : function(x, o, e) {
			alert(x.status + " : "+ o +" : "+e);
		}
	});
}

function addTable( list ){
    
    var tb =$("#companyList");
    tb.empty();
    var row="";
    $.each(list,function(index,value){
        
         row += '<tr><td ><input type="radio" name="sel"  value="'+value['MAKER_NAME']+'"></td>'
                    +'<td >'+value['MAKER_ID']+'</td>'
                    +'<td >'+value['MAKER_NAME']+'</td></tr>';
                 
               
    });
    tb.append(row);
} 


function goPage(pageNum) {
	searchComapny(pageNum);
}

function printPaging(page) {//total,cpage
	$('.page_num').empty();
	$('.page_num').html(page);

};
</script>
</head>
<body>
<form name='search' onsubmit="formSearch();">
	<input type="hidden" name="currentPage" />
</form>
	<div id="popup">
		<p class="subtitle">제조사 검색</p>
		<div class="table_wrap">
			<table class="board_write">
				<caption>검색 조건</caption>
				<colgroup>
					<col width="15%" />
					<col>
				</colgroup>
				<tr>
					<th>검색 조건</th>
					<td><select name="kind" id="kind">
							<option value="companyName">제조사 이름</option>
							<option value="managerName">관리자 이름</option>
					</select>&nbsp;<input name='searchKeyword' id="searchKeyword" type="text">
						<a class="inner_btn btntext" href="#">검색</a></td>
				</tr>
			</table>
		</div>
		<p class="subtitle">관리자목록</p>
		<div class="pop_table_wrap">
			<table class="basic_list">
				<caption>관리자목록</caption>
				<colgroup>
					<col width="" />
					<col width="" />
					<col width="" />
				</colgroup>
				<tr>
					<th>선택</th>
					<th>제조사 코드</th>
					<th>제조사 이름</th>
				</tr>
				<tbody id="companyList">
					<c:if test="${not empty result.list}">
						<c:forEach var="data" items="${result.list}" varStatus="status">
							<tr>
								<td><input name="sel" type="radio"
									value="${data.MAKER_NAME}"></td>
								<td>${data.MAKER_ID}</td>
								<td id='makerName'>${data.MAKER_NAME}</td>
							</tr>
						</c:forEach>
					</c:if>
					<c:if test="${empty result.list}">
						<tr>
							<td colspan="5">조회된 관리자 정보가 없습니다.</td>
						</tr>
					</c:if>
				</tbody>
			</table>
		</div>
		<div class="page_num"><c:if test= "${not empty result.list}" >${result.paging} </c:if></div>
		<div class="btn_center">
			<a class="css_btn btntext" href="#">선택</a> <a class="css_btn btntext"
				href="#">취소</a>
		</div>
	</div>
</body>
</html>
