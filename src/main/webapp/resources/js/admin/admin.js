/**
 * 
 */

function checkid(){
	
	var form={
			checkValue : $('#userId').val(),			 
			checkName : 'userId'
	}; 
	
		
	$.ajax({
		type : "POST",
		contentType: "application/json; charset=UTF-8",
		url : "/admin/user/idcheck.kdn", //호출 URL
		data : JSON.stringify(form), //데이터
		dataType : "json",
		success : function(resultData){
			if(resultData['result'] == 'nok'){
				alert(resultData['message']);
				$('#idcheck').val('false');
			}else if(resultData['result'] == 'ok'){
				alert(resultData['message']);
				$('#idcheck').val('true');
			}
		},
		error : function(e) {
			alert(x.status + " : "+ o +" : "+e);
		}
	});
}

function userAdd(key,form){
	if(!userFormVal()){
		return;
	};
		
	$.ajax({
		type : "POST",
		contentType: "application/json; charset=UTF-8",
		url : "/admin/user/save/"+key+".kdn", //호출 URL  /production/company/save/add.kdn
		data :  JSON.stringify(form), //데이터JSON.stringify
		dataType : "json",
		success : function(resultData){
			
			var type = (key =='add'? '등록' :'수정');
			
			if(resultData['result'] == 'nok'){
				alert(type+"에 실패 하였습니다.["+resultData['message']+"]");
				
			}else {
				alert(type +"되었습니다.");
				window.location = "/admin/list.kdn";
			}
		},
		error : function(e) {
			alert("error : " + e);
		}
	});
}

function userFormVal(form){
	if(!checkVal('userId','','관리자 ID를 입력해 주세요.'))	return false;
	if(!checkVal('idcheck','false','사용자 중복체크를해 주세요'))	return false;
	if(!checkVal('userName','','관리자 이름을  입력해 주세요.'))	return false;
	if(!checkVal('phone2','','전화번호를  입력해 주세요.'))	return false;
	if(!checkVal('phone3','','전화번호를  입력해 주세요.'))	return false;
	if(!checkVal('password','','임시비밀번호를  입력해 주세요.'))	return false;
	if(!checkVal('companyName',"",'제조사를 입력해 주세요.'))	return false;
	
	return true;
}

/**
 * 
 */
var _order =["companyName","device","firmware","chip","chipversion","police"];	  
var _name =["","MN","FN","CN","CV","POLICY_NAME"];	  

function policeContorl(name,oper){
	
	if(_order.indexOf(name) ==_order.length ||!checkOper(name,oper)){
		return false;
	}
	
	var form={
			companyName : $("#companyName").val(),
			device : $("#device").val(),
			firmware : $("#firmware").val(),
			chip :$("#chip").val(),
			chipversion : $("#chipversion").val(),
			police : $("#police").val(),
			oper : oper,
			name : name
	}; 
	
	send(form);
	 
 
	return false;
	
}

function getParent(name,oper){
	 if(name !='companyName'){
		 var index =_order.indexOf(name);
		 if(index >0){
		 return $('#'+_order[index-1]).val();
		 }else{
			 return "";
		 }
	 }else{
		 return $('#companyName').val();
	 }
};

function checkOper(name,oper){
	 if(oper =='list'){
		if(getParent(name) ==""){
			alert("값 을 선택 하여 주시기 바랍니다.");
			return false;
		};
	 }
	 return true;
}



function isSel(name){
	if($('#'+name).val() ==""){
		alert("선택후 진행 할 수있습니다. ");
		return false;
	}else{
		return true;
	}
	
}

function send(data){
	$.ajax({
		type : "POST",
		contentType: "application/json; charset=UTF-8",
		url : "/admin/device/data.kdn", //호출 URL
		data : JSON.stringify(data), //데이터
		dataType : "json",
		success : function(resultData){
			 
			if(resultData['result'] =='ok'){
				
				runOper(data,resultData);
			}else{
				alert('실패 하였습니다.['+resultData['ERRMSG']+']');
			}
			setfocus(data);
		},
		error : function(e) {
			alert("error : " + e);
		}
	});
}

function setfocus(attid){
	$('#'+attid.name).focus();
};

function runOper(oper,result){
	 if(oper.oper=='list'){
		var inp =  _order.indexOf(oper.name);
		var selecter =$("#"+ _order[inp]);
		selecter.attr("disabled",false);
		selecter.find("option").remove().end().append('<option value="">선택</option>');;
		 
		var list =result['list'];
		for(var i =0 ; i<list.length; i++){
			var value =list[i][_name[inp]];
			if(value != null && value != undefined && value.length>0){
			   selecter.append('<option value="'+value+'">'+value+'</option>');
			}
		}
		
		for(var j = inp+1 ; j<_order.length ; j++){
			$('#'+_order[j]).attr("disabled",true);
			$('#'+_order[j]).find("option").remove().end().append('<option value="">선택</option>');
		}
	}else if(oper.oper =='mod'){
		alert("수정 되었습니다.");
	}
}


function formCheck(){
	if(!checkVal('chipName',"",'칩 이름은 필수 항목입니다.'))	return false;
	if(!checkVal('chipVersion','','칩 버젼은 필수 항목입니다.'))	return false;
	if(!checkVal('deviceName','','기기 이름은 필수 항목입니다.'))	return false;
	
	return true;
}

function checkVal(name,val,message){
	if($('#'+name).val()==val){
		alert(message);
	     $('#'+name).focus();
		return false;
	}
	return true;
}