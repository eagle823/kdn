function checkName(){
	
	var form={
			checkValue : $('#product_name').val(),			 
			checkName : 'product_name'
	}; 
		
	$.ajax({
		type : "POST",
		Accept : "application/json",
		contentType: "application/json; charset=UTF-8",
		url : "/production/list/check.kdn", //호출 URL
		data : JSON.stringify(form), //데이터
		dataType : "json",
		success : function(resultData){
			if(resultData['result'] == "ok"){
				alert(resultData['message']);
				$('#namecheck').val("true");
			}else{
				alert(resultData['message']);
				$('#product_name').focus();
				$('#namecheck').val('false');
			}
		},
		error : function(e) {
			alert("error : " + e);
		}
	});
}

function search(currentPage){
	
	var form={
			searchKeyword : $('#name').val(),
			currentpage : currentPage,
			kind : 'name',
			pageResult : $('#pageResult').val()
	}; 
	
		
	$.ajax({
		type : "POST",
		contentType: "application/json; charset=UTF-8",
		url : "/production/list/data.kdn", //호출 URL
		data : JSON.stringify(form), //데이터
		dataType : "json",
		success : function(resultData){
			 
			if(resultData['result'] =='ok'){
			addTable(resultData['list'] );
		//	setPaging(resultData['page'].pagingPrint);			
			 
			}
		},
		error : function(e) {
			alert("error : " + e);
		}
	});
}

function addTable(list){
	 if(list.length>0){
		 $('#table_wrap').show();
	 }else{
		 $('#table_wrap').hide();
	 }
	var table =$("#manager_list").find(" tbody");
	table.empty();
	var row ;

	$.each(list,function(index,value){
		row =  '<tr class="clickInput">'
		       + ' <td>'+value['BRANCH_NAME']+'</td> '
		       + ' <td>'+value['OPERATOR_ID']+'</td> '
		       + ' <td>'+value['OPERATOR_NAME']+'</td> '
		       + ' <td>'+value['EMAIL']+'</td> '
		       + ' <td>'+value['PHONE']+'</td></tr>';
		
				table.append(row);
	});
	
	
	 	
	$('#manager_list').unbind('click');
	
	$('#manager_list').on('click', '.clickInput', function() {
		$('#manager_list tr').removeClass('listselected');
		var tr=$(this).closest('tr').addClass('listselected');
		var $td = tr.children('td');

		//var part_name = $td.eq(1).text();
		// part_name = $td.eq(2).text();
		$("#product_operator").val( $td.eq(2).text());
		$("#product_oper_email").val( $td.eq(3).text());
		$("#product_oper_phone").val( $td.eq(4).text());
		 

		});
};

function setPaging(pageing){
	$("#page_num").empty();
	$("#page_num").html(pageing).attr('class','page_num');
}

function  goPage(num){
	  search(num);
}	


function productAdd(form,key){
	if(!formCheck()){
		return;
	};

	$.ajax({
		type : "POST",
		contentType: "application/json; charset=UTF-8",
		url : "/production/device/save/"+key+".kdn", //호출 URL  /production/company/save/add.kdn
		data :  JSON.stringify(form), //데이터JSON.stringify
		dataType : "json",
		success : function(resultData){
			alert(resultData['message']);

			if(resultData['result'] == 'ok'){
				window.location = "/production/list.kdn";
			}
		},
		error : function(x, o, e) {
			alert(x.status + " : "+ o +" : "+e);
		}
	});
}

function formCheck(form){
	if(!checkVal('product_name',"",'제조사명을 입력해주세요.'))	return false;
	if(!checkVal('namecheck','false','제조사명 중복체크를 하셔야 합니다.'))	return false;
	if(!checkVal('product_ssn1',"",'사업자등록번호를 입력해주세요.'))	return false;
	if(!checkVal('product_ssn2',"",'사업자등록번호를 입력해주세요.'))	return false;
	if(!checkVal('product_ssn3',"",'사업자등록번호를 입력해주세요.'))	return false;
	if(!checkVal('product_address',"",'주소를 입력해주세요.'))	return false;
	if(!checkVal('ceoname',"",'대표자명을 입력해주세요.'))	return false;
	if(!checkVal('homepage',"",'홈페이지 주소를 입력해주세요.'))	return false;
	if(!checkVal('fax1',"",'Fax를 입력해주세요.'))	return false;
	if(!checkVal('fax2',"",'Fax를 입력해주세요.'))	return false;
	if(!checkVal('fax3',"",'Fax를 입력해주세요.'))	return false;
	if(!checkVal('phone1',"",'대표전화를 입력해주세요.'))	return false;
	if(!checkVal('phone2',"",'대표전화를 입력해주세요.'))	return false;
	if(!checkVal('phone3',"",'대표전화를 입력해주세요.'))	return false;
	return true;
}

function checkVal(name,val,message){
	if($('#'+name).val()==val){
		alert(message);
	     $('#'+name).focus();
		return false;
	}
	return true;
}


