
	$.fn.faq = function(opts){
		return this.each(function(){
			var options = $.extend({ headingRel : true }, opts)
				, $wrap = $(this)
				, $btns = $wrap.find("a.tab")
				, $layers
				, oldActive
				, LayersHash;
			
			// faq 관련 레이어 수집
			LayersHash = $.map($btns, function(n, i){
										return n.hash;
									}).join();
			$layers = $(LayersHash);
	
	
			// initialize
			$layers.hide();
			$btns.bind("click", onChange);
			
			function onChange(){
				if( ! options.headingRel ){
					var visible = $(this.hash).is(":visible");
					( visible ) ? close( this ) : open( this );
	
				}else{
					if( oldActive && oldActive.hash == this.hash ){
						close( this );
						oldActive = null;
						return false;
					}
					if( oldActive ) close( oldActive );
					open( this );
			
					oldActive = this;
				}
				
				return false;
			}
			
			function open( ele ){
				$(ele).parent().addClass("current");
				$(ele.hash).stop(true).slideDown("fast");
				if( options.open ) options.open.call(ele);
			};
	
			function close( ele ){
				$(ele).parent().removeClass("current");
				$(ele.hash).stop(true).slideUp("fast");
				if( options.close ) options.close.call(ele);
			};
			
		});
	};
	
	$(document).ready(function(){
		$("dl").faq({
			headingRel : true, // default : true
			
			open : function (){
				// console.log("open : ", this);
			},
			close : function(){
				// console.log("close : ", this);
			}
		});
	});
