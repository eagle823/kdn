/**
 * 
 */
var _order =["devices","firmwares","mcus"];	  
var _value =["D_MODEL_NO","FW_NO","MCU_NO"];
var _name =["DEVICE_NAME","FW_NAME","MCU_NAME"];
function deviceControl(name,oper){
	
	if(!checkOper(name,oper)){
		return false;
	}
	
	var form={
			oper : oper,
			name : name,
			device :$('#devices').val(),
			firmware :$('#firmwares').val(),
			mcu : $('#mcus').val(),
			value :$('#'+name).val()
			//parent : getParent(name,oper),
	}; 
	
	
	send(form);
 
	return false;
	
}

function getParent(name,oper){
	 if(oper=='add' && name !='device'){
		 var index =_order.indexOf(name+'s');
		 if(index >0){
		 return $('#'+_order[index-1]).val();
		 }else{
			 return "";
		 }
	 }else{
		 return "";
	 }
};

function checkOper(name,oper){
	if(oper =='del'){
		return isSel(name)&&isDel(name);
	}else if(oper =='add'){
		if(!checkVal(name,"",'등록할 값을 입력하여 주시기 바랍니다.'))	return false;
		else return true;
	}else if(oper =='list'){
		return true;
	}
}

function isDel(name){
	
	var po =_order.indexOf(name+"s");
	if(po == _order.length-1){
		return true;
	}else{
		if($('#'+_order[po+1]).val() ==""){
			return true;
		} else{
			alert("하위 정보가 존재 합니다. ");
			return false;
		}
			
	}
	
}

function isSel(name){
	if($('#'+name+"s").val() ==""){
		alert("선택후 진행 할 수있습니다. ");
		return false;
	}else{
		return true;
	}
	
}

function send(data){
	$.ajax({
		type : "POST",
		contentType: "application/json; charset=UTF-8",
		url : "/cert/device/data.kdn", //호출 URL
		data : JSON.stringify(data), //데이터
		dataType : "json",
		success : function(resultData){
			 
			if(resultData['result'] =='ok'){
				
				runOper(data,resultData);
			}else{
				alert("실패 하였습니다.["+resultData['ERRMSG']+"]");
			}
		},
		error : function(e) {
			alert("error : " + e);
		}
	});
}

function runOper(oper,result){
	if(oper.oper =='del'){
		
		 $('#'+oper.name+"s").find("option").each(function() {
		      if(this.value == oper.value) {
		         $(this).remove();
		      }
		   });
	  alert('삭제 하였습니다.');
	}else if(oper.oper=='add'){
		
		$('select#'+oper.name+"s").append('<option value="'+oper.value+'" selected="selected">'+oper.value+'</option>');
		$('#'+oper.name).val("");
		alert('추가 되었습니다.');
	}else if(oper.oper=='list'){
		var inp =  _order.indexOf(oper.name+'s');
		var selecter =$("#"+ oper.name+'s');
		selecter.find("option").remove().end().append('<option value="">선택</option>');;
		 
		var list =result['list'];
		for(var i =0 ; i<list.length; i++){
			selecter.append('<option value="'+list[i][_value[inp]]+'">'+list[i][_name[inp]]+'</option>');
		}
		
		for(var j = inp+2 ; j<_order.length ; j++){
			$('#'+_order[j]).attr("disabled",true);
			$('#'+_order[j]).find("option").remove().end().append('<option value="">선택</option>');
		}
	}
}


function formCheck(){
	if(!checkVal('chipName',"",'칩 이름은 필수 항목입니다.'))	return false;
	if(!checkVal('chipVersion','','칩 버젼은 필수 항목입니다.'))	return false;
	if(!checkVal('deviceName','','기기 이름은 필수 항목입니다.'))	return false;
	
	return true;
}

function checkVal(name,val,message){
	if($('#'+name).val()==val){
		alert(message);
	     $('#'+name).focus();
		return false;
	}
	return true;
}