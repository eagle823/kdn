function certAdd(oidList){
	
	if(!formCheck()){
		return ;
	}
	
	var form={
			oidList : oidList,
			chipName : $('#chipName').val(),
			chipVersion : $('#chipVersion').val(),
			deviceName : $('#deviceName').val()
	}; 
	
		
	$.ajax({
		type : "POST",
		contentType: "application/json; charset=UTF-8",
		url : "/production/list/data.kdn", //호출 URL
		data : JSON.stringify(form), //데이터
		dataType : "json",
		success : function(resultData){
			 
			if(resultData['result'] =='ok'){
			addTable(resultData['list'] );
		//	setPaging(resultData['page'].pagingPrint);			
			 
			}
		},
		error : function(e) {
			alert("error : " + e);
		}
	});
}

function formCheck(){
	if(!checkVal('chipName',"",'칩 이름은 필수 항목입니다.'))	return false;
	if(!checkVal('chipVersion','','칩 버젼은 필수 항목입니다.'))	return false;
	if(!checkVal('deviceName','','기기 이름은 필수 항목입니다.'))	return false;
	//if(!checkVal('corp','','부서를 선택 하여 주시기바랍니다.'))	return false;
	//if(!checkVal('email','','이메일 주소를 입력 하여 주시기 바랍니다..'))	return false;
	//if(!checkVal('password','','임시 비밀 번호를  입력 하여 주시기 바랍니다..'))	return false;
	return true;
}

function checkVal(name,val,message){
	if($('#'+name).val()==val){
		alert(message);
	     $('#'+name).focus();
		return false;
	}
	return true;
}

function getPageing(totalCount,pageSize ,currentPage){
var pageGroupSize = 10;
var link = "<a class=\"paging\" href=\"javascript:goPage('";
var buffer;
  var numPageGroup =  Math.ceil(currentPage/pageGroupSize);

	var pageGroupCount = totalCount/(pageSize*pageGroupSize)+( totalCount % (pageSize*pageGroupSize) == 0 ? 0 : 1);
	var startPage = pageGroupSize*(numPageGroup-1)+1;
	var endPage= startPage + pageGroupSize-1;
	
	var pageCount =0;			 
	pageCount =totalCount / pageSize + ( totalCount % pageSize == 0 ? 0 : 1);
	
	if(endPage > pageCount)endPage = pageCount;
	
	if(totalCount > 0){
		if(numPageGroup > 1){
			buffer+=link+((numPageGroup-2)*pageGroupSize+1 )+"')\">[이전]</a>"+"&nbsp;";  
		}

		for(var i =startPage ; i <= endPage; i ++){
			if(currentPage== i){
				buffer +="<font color='#708090'>"+i+"</font>" ;
			}else{
				buffer+=link+i+"')\">[<font color='#191970'>"+i+"</font>]</a>";
			}
			buffer+="&nbsp;";
		}
		if(numPageGroup < pageGroupCount){
			buffer+=link+(numPageGroup*pageGroupSize+1)+"')\">[다음]</a>";
		}

	}
return buffer;
	}
