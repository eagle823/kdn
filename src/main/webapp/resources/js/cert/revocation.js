
function revocationCert(lists){
	var form={
		dnList : lists		 
	}; 
		
	$.ajax({
		type : "POST",
		contentType: "application/json; charset=UTF-8",
		url : "/cert/revocation/revo.kdn", //호출 URL
		data : JSON.stringify(form), //데이터
		dataType : "json",
		success : function(resultData){
			if(resultData){
			alert("폐지 되었습니다.");
			window.location.reload();
			} else {
				alert("실패 하였습니다.");
			}
		},
		error : function(e) {
			alert("실패 하였습니다.");
		}
	});
}

function certDelete(lists){
	var form = {dnList : lists};
	$.ajax({
		type : "POST",
		contentType: "application/json; charset=UTF-8",
		url : "/cert/revocation/del.kdn", //호출 URL
		data : JSON.stringify(form), //데이터
		dataType : "json",
		success : function(resultData){
			if(resultData){
			alert("삭제 되었습니다.");
			window.location.reload();
			} else {
				alert("실패 하였습니다.");
			}
		},
		error : function(e) {
			alert("실패 하였습니다.");
		}
	});
}